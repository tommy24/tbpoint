#!/usr/bin/perl -w

use Getopt::Long;

GetOptions( "stats_data=s"  => \$stats_data,
            "rename=s"      => \$newnames,
            "autoview"      => \$autoview,
            "rotate=i"      => \$rotateangle,
            "yxratio=f"     => \$YX_Ratio,
            "bw"            => \$bw,
            "xlabel=s"      => \$xlabel,
            "ylabel=s"      => \$ylabel,
            "xlabelcolor=s" => \$xlabelcolor,
            "ylabelcolor=s" => \$ylabelcolor,
            "xmin=f"        => \$xmin,
            "xmax=f"        => \$xmax,
            "ymin=f"        => \$ymin,
            "ymax=f"        => \$ymax,
            "yhash=i"       => \$yhash,
            "mhash=i"       => \$mhash,
            "grid"          => \$grid,
            "xyplot"        => \$xyplot,
            "stack"         => \$stack,
            "pattern"       => \$pattern,
            "ps_output=s"   => \$ps_output_file);

print " ARGV Array : @ARGV \n";

if($stats_data)
{
    $filename = $stats_data;
}
else
{
    die "No Data file provided \nPlease use -stats_data= parameter to prvide a data source \nFailed $!";
}

#file to contain the massaged data from stats.dat
$gnuplot_data="graph.dat";

# the file to contain the final gnuplot script
$gnuplot_script="visualize_data.gnu";


open(FH, $filename) or die "Can't open $filename: $!";

my $line;
my @experiments;
my $BenchmarkSuite = <FH>;

print "Bench marks: $BenchmarkSuite \n";

$line=<FH>;
#print "line is: $line \n";
@fields = split(/:/, $line);
my $temp = $fields[1];

#print "temp is: $temp \n";
# Trim the whitespaces at the beginning and at the end
$temp =~ s/^\s+//;
$temp =~ s/\s+$//;

my @names = split(/\s+/, $temp);

my @Benchmarks;

for $name (@names)
{
    # remove the @ sign and the characters after it
    @parts = split(/@/, $name);
    push @BenchmarksNames, $parts[0];
}

my $numBenchmarks = @BenchmarksNames;

print "Number of Benchmarks: $numBenchmarks \n";

# generate the data file input for Gnuplot
print "@Benchmarks \n";

my @experimentName;
my $experimentData;


my @dataLines;
my @expNames;

my $columns = 0;
my $rows = 0;

my $index = 0;


@expNames = split(/;/, $newnames) if $newnames;

while($line = <FH>)
{
    $line =~ s/^\s+//;
    $line =~ s/\s+$//;
    
    if(length($line))
    {

        $rows++;

        @fields = split(/:/, $line);

        $fields[0] =~ s/^\s+//;
        $fields[0] =~ s/\s+$//;

        $fields[1] =~ s/^\s+//;
        $fields[1] =~ s/\s+$//;


        if( $expNames[$index]) # do nothing
        {
            #push(@expNames,$ARGV[$index]);
        }
        else
        {
            push(@expNames,$fields[0]);
        }

        @rowData = split(/\s+/, $fields[1]);
        $columns = @rowData;

        push(@dataLines, [ @rowData ]);

        $index++;
    }   

}

close FH;


open(GPDATA, ">$gnuplot_data") or die "Can't create file $gnuplot_data: $!";

print GPDATA "Experiments:        @expNames \n";


for($c = 0; $c < $columns; $c++)
{
    print GPDATA "$BenchmarksNames[$c]\t\t" ;
    for($r = 0; $r < $rows; $r++)
    {
        print GPDATA " $dataLines[$r][$c]   ";
    }
    print GPDATA " \n";
}

close GPDATA;

$plotCommand = "plot \'$gnuplot_data\' using 2:xtic(1) ti col ";

$index = 3;

$numExperiments = $rows; #this is actually the number of columns because we transpose the data
                         # to be in the format gnuplot expects

#print "num experiments: $numExperiments\n";


for(;$index <= $numExperiments + 1; $index++)
{
    $plotCommand = $plotCommand . ", '' u $index ti col ";
}

$plotCommand = $plotCommand . "\n";
#print "$plotCommand";

# create the Gnuplot script
open(GPSCRIPT, ">$gnuplot_script") or die "Can't create file $gnuplot_script: $!";

print GPSCRIPT "set boxwidth 0.9 absolute\n";

print GPSCRIPT "set datafile missing '-'\n";

#print GPSCRIPT "set key out vert\n";
print GPSCRIPT "set key tmargin horizontal \n";

if($grid)
{
    print GPSCRIPT "set grid\n";
}

if($yhash)
{
    print GPSCRIPT "set ytics 0, $yhash\n";
}

if($mhash)
{
    print GPSCRIPT "set mytics $mhash\n";
}

if($xlabel)
{
    print GPSCRIPT "set xlabel \"$xlabel\"\n";
}
if($ylabel)
{
    print GPSCRIPT "set ylabel \"$ylabel\"\n";
}
else 
{
   print GPSCRIPT "set ylabel \"IPC\" \n";

}
if($xlabelcolor)
{
}
if($ylabelcolor)
{
}

if($xyplot)
{
    print GPSCRIPT "set style data linespoints\n";
}
else
{
    if($pattern)
    {
        print GPSCRIPT "set style fill pattern border\n";
    }
    else
    {
        print GPSCRIPT "set style fill  solid 1.00 border -1\n";
    }       
    
    print GPSCRIPT "set style histogram clustered gap 1 title  offset character 0, 0, 0\n";
    print GPSCRIPT "set style data histograms\n";
    
    if($stack)
    {
        print GPSCRIPT "set style histogram rowstacked\n";
    }

}


if($rotateangle)
{
    print GPSCRIPT "set xtics border in scale 1,0.5 nomirror rotate by $rotateangle  offset character 0, 0, 0 \n";
}
else
{
}

if($xmin and $xmax)
{
    print GPSCRIPT "set xrange [ $xmin : $xmax ] noreverse nowriteback\n";
}

if($ymin and $ymax)
{
    print GPSCRIPT "set yrange [ $ymin : $ymax ] noreverse nowriteback\n";
}

if($YX_Ratio)
{
     print GPSCRIPT "set size ratio $YX_Ratio\n";
}
else
{
	
     print GPSCRIPT "set size ratio 0.5\n";
     # print GPSCRIPT "set size square\n";
}

print GPSCRIPT $plotCommand;
print GPSCRIPT "set size 1.0, 0.6 \n";


print GPSCRIPT "set terminal push\n";

if($ps_output_file)
{
    $terminal = "";
    if($bw)
    {
        print GPSCRIPT "set style fill pattern border\n";
        $terminal = "set terminal postscript portrait enhanced monochrome dashed lw 1 \"Helvetica\" 14\n";
    }
    else
    {
        $terminal = "set terminal postscript portrait enhanced color dashed lw 1 \"Helvetica\" 14\n";
    }
    
    print GPSCRIPT "$terminal";
    print GPSCRIPT "set output \"$ps_output_file\" \n";
    print GPSCRIPT "replot \n";
}
else
{
    print GPSCRIPT "set terminal pop\n";
    print GPSCRIPT "replot \n";
}

if($autoview)
{
    print GPSCRIPT "pause -1\n";
}

close GPSCRIPT;

if($autoview)
{
    exec("gnuplot $gnuplot_script");
}


