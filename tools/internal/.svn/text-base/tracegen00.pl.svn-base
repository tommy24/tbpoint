#!/usr/bin/perl -w

use Time::HiRes qw(gettimeofday);

@bench_groups_int = (
	'gzip',
	'vpr',
	'gcc',
	'mcf', 
	'crafty',
#	'eon',			<= Couldn't build
	'parser',
	'perlbmk',
#	'gap',			#<= Segmentation Fault
#	'vortex',		<= Couldn't build
	'bzip2',
	'twolf'
 );

@bench_groups_fp = ( 
	'wupwise', 
	'swim', 
	'mgrid', 
	'applu', 
	'mesa', 
#	'galgel',			<= Couldn't build
	'art', 
	'equake', 
	'facerec',
	'ammp', 
	'lucas',
	'fma3d',
	'sixtrack', 
	'apsi' 
);

%bench_dir = (
	"gzip" => "CINT2000/164.gzip",
	"vpr" => "CINT2000/175.vpr",
	"gcc" => "CINT2000/176.gcc",
	"mcf" => "CINT2000/181.mcf",
	"crafty" => "CINT2000/186.crafty",
	"parser" => "CINT2000/197.parser",
	"perlbmk" => "CINT2000/253.perlbmk",
	"gap" => "CINT2000/254.gap",
	"bzip2" => "CINT2000/256.bzip2",
	"twolf" => "CINT2000/300.twolf", 
	"wupwise" => "CFP2000/168.wupwise",
	"swim" => "CFP2000/171.swim",
	"mgrid" => "CFP2000/172.mgrid",
	"applu" => "CFP2000/173.applu",
	"mesa" => "CFP2000/177.mesa",
	"art" => "CFP2000/179.art",
	"equake" => "CFP2000/183.equake",
	"facerec" => "CFP2000/187.facerec",
	"ammp" => "CFP2000/188.ammp",
	"lucas" => "CFP2000/189.lucas",
	"fma3d" => "CFP2000/191.fma3d",
	"sixtrack" => "CFP2000/200.sixtrack",
	"apsi" => "CFP2000/301.apsi" 
);

%bench_exe = (
	"gzip" => "gzip",
	"vpr" => "vpr",
	"gcc" => "cc1",
	"mcf" => "mcf",
	"crafty" => "crafty",
	"parser" => "parser",
	"perlbmk" => "perlbmk",
	"gap" => "gap",
	"bzip2" => "bzip2",
	"twolf" => "twolf", 
	"wupwise" => "wupwise",
	"swim" => "swim",
	"mgrid" => "mgrid",
	"applu" => "applu",
	"mesa" => "mesa",
	"art" => "art",
	"equake" => "equake",
	"facerec" => "facerec",
	"ammp" => "ammp",
	"lucas" => "lucas",
	"fma3d" => "fma3d",
	"sixtrack" => "sixtrack",
	"apsi" => "apsi" 
);

%bench_num_input = (
	"gzip" => "5",
	"vpr" => "2",
	"gcc" => "5",
	"mcf" => "1",
	"crafty" => "1",
	"parser" => "1",
	"perlbmk" => "7",
	"gap" => "1",
	"bzip2" => "3",
	"twolf" => "1", 
	"wupwise" => "1",
	"swim" => "1",
	"mgrid" => "1",
	"applu" => "1",
	"mesa" => "1",
	"art" => "2",
	"equake" => "1",
	"facerec" => "1",
	"ammp" => "1",
	"lucas" => "1",
	"fma3d" => "1",
	"sixtrack" => "1",
	"apsi" => "1" 
);

%bench_arg = (
	"gzip1" => "input.source 60",
	"gzip2" => "input.log 60",
	"gzip3" => "input.graphic 60",
	"gzip4" => "input.random 60",
	"gzip5" => "input.program 60",
	"vpr1" => "net.in arch.in place.out dum.out -nodisp -place_only -init_t 5 -exit_t 0.005 -alpha_t 0.9412 -inner_num 2",
	"vpr2" => "net.in arch.in place.in route.out -nodisp -route_only -route_chan_width 15 -pres_fac_mult 2 -acc_fac 1 -first_iter_pres_fac 4 -initial_pres_fac 8",
	"gcc1" => "166.i -o 166.s",
	"gcc2" => "200.i -o 200.s",
	"gcc3" => "expr.i -o expr.s",
	"gcc4" => "integrate.i -o integrate.s",
	"gcc5" => "scilab.i -o scilab.s",
	"mcf1" => "inp.in",
	"crafty1" => "< crafty.in",
	"parser1" => "2.1.dict -batch < ref.in" ,
	"perlbmk1" => "-I./lib diffmail.pl 2 550 15 24 23 100",
	"perlbmk2" => "-I. -I./lib makerand.pl",
	"perlbmk3" => "-I./lib perfect.pl b 3 m 4",
	"perlbmk4" => "-I./lib splitmail.pl 850 5 19 18 1500",
	"perlbmk5" => "-I./lib splitmail.pl 704 12 26 16 836",
	"perlbmk6" => "-I./lib splitmail.pl 535 13 25 24 1091",
	"perlbmk7" => "-I./lib splitmail.pl 957 12 23 26 1014",
	"gap1" => "-l ./ -q -m 192M < ref.in",
	"bzip21" => "input.source 58",
	"bzip22" => "input.graphic 58",
	"bzip23" => "input.program 58",
	"twolf1" => "ref" ,
	"wupwise1" => "",
	"swim1" => "< swim.in",
	"mgrid1" => "< mgrid.in",
	"applu1" => "< applu.in",
	"mesa1" => "-frames 1000 -meshfile mesa.in -ppmfile mesa.ppm",
	"art1" => "-scanfile c756hel.in -trainfile1 a10.img -trainfile2 hc.img -stride 2 -startx 110 -starty 200 -endx 160 -endy 240 -objects 10",
	"art2" => "-scanfile c756hel.in -trainfile1 a10.img -trainfile2 hc.img -stride 2 -startx 470 -starty 140 -endx 520 -endy 180 -objects 10",
	"equake1" => "< inp.in",
	"facerec1" => "< ref.in",
	"ammp1" => "< ammp.in",
	"lucas1" => "< lucas2.in",
	"fma3d1" => "",
	"sixtrack1" => "",
	"apsi1" => ""
);


$type = "";
$type = $ARGV[0];
if ($type eq "" || $type eq "all") { @bench_groups = (@bench_groups_int, @bench_groups_fp); }
elsif ($type eq "int") { @bench_groups = @bench_groups_int; }
elsif ($type eq "fp") { @bench_groups = @bench_groups_fp; }

$base = "/hparch/benchmarks/spec2000/benchspec";
$exe_tail = "_base.gcc33-high-opt";

# Trace Generator Path Setting
$pin_home       = "/hparch/benchmarks/pin-2.6/source/tools";
$isimpoint_path = "$pin_home/PinPoints/obj-intel64/isimpoint.so";
$simpoint_path  = "$pin_home/PinPoints/bin/simpoint";
$ppgen_path     = "$pin_home/PinPoints/bin/ppgen.3";
$tracegen_path  = "$pin_home/trace_generator_new/obj-intel64/trace_generator.so";
$pin_exe        = "/hparch/benchmarks/pin-2.6/pin";
$current_dir    = "`pwd`";

foreach $bench(@bench_groups)
{
	$bench_dir = "${base}/${bench_dir{$bench}}/run/00000002";
	$pinout_dir = "/storage/traces/spec2000/${bench}";
	$bench_exe = "${bench_dir}/${bench_exe{$bench}}${exe_tail}";
	$num_input = ${bench_num_input{$bench}};
	
	for ($ii = 1; $ii <= $num_input; $ii++)
	{
	
		# File open
		open (OUTFILE, "> ${bench}.${ii}.tracegen.pl"); 
		print (OUTFILE "#!/usr/bin/perl\n\n");
		print (OUTFILE "chdir \"${bench_dir}\"\;\n\n");

		print (OUTFILE "\# ${bench} input ${ii}\n\n");

		$input_index = ${bench}.${ii};
		$bench_arg = "${bench_arg{$input_index}}";

		# bb
		print (OUTFILE "system\(\"mkdir -p ${pinout_dir}/bbdata\"\)\;\n");
		$bb_file = "${pinout_dir}/bbdata/${bench}${ii}";	
		print (OUTFILE "system\(\"rm -rf ${bb_file}.T.0.bb\"\)\;\n");
		print (OUTFILE "system\(\"$pin_exe -t ${isimpoint_path} -o ${bb_file} -slice_size 200000000 -- ${bench_exe} ${bench_arg}\"\)\;\n\n");

		# simpoint
		$bb_data = "${bb_file}.T.0.bb";
		print (OUTFILE "system\(\"$simpoint_path  -loadFVFile ${bb_data} -maxK 1 -coveragePct 0.9999 -saveSimpoints ${bb_data}.simpoints -saveSimpointWeights ${bb_data}.weights -saveLabels ${bb_data}.labels > ${bb_data}.simpointout\"\)\;\n\n");

		# pp
		$pp_file = "${pinout_dir}/ppdata/${bench}${ii}-point";
		print (OUTFILE "system\(\"mkdir -p ${pinout_dir}/ppdata\"\)\;\n");
		print (OUTFILE "system\(\"$ppgen_path ${pp_file} ${bb_data} ${bb_data}.simpoints.lpt0.9999 ${bb_data}.weights.lpt0.9999 ${bb_data}.labels 0 >& ${bb_data}.ppgenout\"\)\;\n\n");

		# trace generation
		print (OUTFILE "system\(\"mkdir -p ${pinout_dir}/pin_traces\"\)\;\n");
		$trace_file = "${pinout_dir}/pin_traces/${bench}.${ii}";
		print (OUTFILE "system\(\"$pin_exe -t $tracegen_path -ppfile ${pp_file}.1.pp -tracename $trace_file -dump 1 -dump_file ${trace_file}.dump -- $bench_exe $bench_arg\"\)\;\n\n");
	
		print (OUTFILE "system\(\"rm -Rf ${pinout_dir}/bbdata/${bench}${ii}*\"\)\;\n\n");
	
		# file close
		close OUTFILE;
		system "chmod +x ${current_dir}/${bench}.${ii}.tracegen.pl"; 
		system "echo \"perl ${current_dir}/${bench}.${ii}.tracegen.pl\" | qsub\n";
	}
}

