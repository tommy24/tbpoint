#!/usr/bin/perl
# **************************************************************************************
# CVS          : $Id: run_batch,v 1.1 2008-08-20 19:05:20 hyesoon Exp $:
# **************************************************************************************

die "You need to set the SIMDIR environment variable to the macsim base directory.\n" unless $ENV{'SIMDIR'};
#die "No pbs binaries.\n" unless -x "/usr/local/bin/qsub";
require ($ENV{'SIMDIR'} . "/tools/bench_common");
require ($ENV{'SIMDIR'} . "/tools/script_utils");

sub AddSlash {
        $string = $_[0];
        $string =~ s/@/\\@/g;
        return $string;
}


# **********************************************************************
# **********************************************************************
# Usage information

$USAGE = "Usage:  '$0 <group name> <benchmark or suite> [run_batch arguments] [simulator arguments]'";

$SIMDIR		= "$ENV{'SIMDIR'}";
$PINDIR		= "$ENV{'PINDIR'}";

$QUEUE		= "hparch";

if ($#ARGV < 1) {
    print(STDERR "$USAGE\n");

    print(STDERR "\t<group name>         : A string to identify this particular batch run (creates a subdirectory in the $SIMDIR/results directory)\n");
    print(STDERR "\t<benchmark or suite> : benchmarks to run (can be regular expression)\n\n");

    print(STDERR "\t++nomail             : don't get any mail from PBS\n");
    print(STDERR "\t++nosub              : don't submit the job to PBS\n");
    print(STDERR "\t++bin <bin>          : specify a binary other than the default\n");
    print(STDERR "\t++print              : just print submit commands, don't actually submit\n");
    print(STDERR "\t++reuse              : reuse without asking for confirmation\n");
    print(STDERR "\t++replace            : replace without asking for confirmation\n");
    print(STDERR "\t++noreplace          : don't replace without asking for confirmation\n");
    print(STDERR "\t++queue <queue>      : specify a queue other than '$QUEUE'\n");
    print(STDERR "\t++params <file>      : copy a parameters file for use by all runs\n");
    print(STDERR "\t++bsubs <file>       : perl file containing benchmark hashes to specify substitutions for particular benchmarks\n");
    print(STDERR "\t++gzip               : gzip all .out files in output directory after run completes\n");
    print(STDERR "\t++gzbin              : gzip all binaries\n");
    print(STDERR "\t++read_ckp           : read checkpointed state or fast-forward (works only for spec00\@ref)\n");
    print(STDERR "\t++rpoint             : representative simulation points for reference input (works only for spec00\@ref)\n");
    print(STDERR "\t++pin                : running pin\n");
    print(STDERR "\t++pintool            : tool name for the pin\n");
    print(STDERR "\t++read_tr            : provide trace list for pin \n");
    print(STDERR "\t++bench_bin          : binary names for traces (needs a argument)\n");
    print(STDERR "\n");

    print_sets(\*STDERR);
    exit(1);
}


# **********************************************************************
# Process arguments

$OS		= substr(`uname`, 0, length(`uname`) - 1);
$SHELL		= $ENV{'SHELL'};

$BINARY		= "$SIMDIR/toname/bin/$OS/opt/toname";
#$BINARY		= "$SIMDIR/bin/$OS/opt/toname";
$RESULTS	= "$SIMDIR/results";
$RESOURCES      = "";
$MAIL_OPT       = "-m ae";

#$SUBMIT_CMD	= "\"/usr/local/bin/qsub -V \$MAIL_OPT -o \$pbs_outdir -e \$pbs_outdir -q \$QUEUE \$RESOURCES -N \$name\"";
$SUBMIT_CMD	= "\"/usr/local/bin/qsub -V \$MAIL_OPT -o \$pbs_outdir -e \$pbs_outdir  -N \$name\"";

$RUN_LOCAL_CMD	= "$SIMDIR/tools/run_macsim";

$RUN_LOCAL_ARGS	= "\"\$group_binary \$outdir \$GZIP ./toname \$bench \$input \$nice \$subst_simopt \$no_file_tag_opt --stdout=stdout --stderr=stderr\"";


$COC_SIMDIR	= "/net/hp95/hparch/sim/$ENV{'USER'}";
$COC_RESULTS    = "$COC_SIMDIR/results";

@OLD_ARGV     = @ARGV;
$GZIP         = 0;
$GZBIN        = 0;
$PRINT	      = 0;
$REUSE        = 0;
$REPLACE      = 0;
$NOREPLACE    = 0;

$RUN_PIN       = 0; 
$PIN_TOOL      = icount;
$BINARY_SET    = 0; 

$INPUT_CONFIRM = 1;

$found_group  = 0;
$found_set    = 0;
$simopt	      = "";
$param_file   = "";
$bsub_file    = "";

$LOCAL_SCRIPT_NAME = "run_macsim";
$LOCAL_SCRIPT_PATH = "$SIMDIR/tools/$LOCAL_SCRIPT_NAME";

die "There is no $LOCAL_SCRIPT_PATH" unless (-f "$LOCAL_SCRIPT_PATH");


while (@ARGV) {
    $option = shift;

    if ($option eq "++nomail") {
	$MAIL_OPT = "-m n";
    } elsif ($option eq "++nosub") {
	$SUBMIT_CMD = "\"\"";
    } elsif ($option eq "++bin") {
	die "Option $option needs argument.\n" unless @ARGV;
	$BINARY = shift;
	$BINARY_SET = 1; 
    } elsif ($option eq "++print") {
        $PRINT = 1;
    } elsif ($option eq "++reuse") {
        $REUSE = 1;
    } elsif ($option eq "++replace") {
        $REPLACE = 1;
    } elsif ($option eq "++noreplace") {
        $NOREPLACE = 1;
    } elsif ($option eq "++queue") {
	die "Option $option needs argument.\n" unless @ARGV;
	$QUEUE = shift;
    } elsif ($option eq "++params") {
	die "Option $option needs argument.\n" unless @ARGV;
	$param_file = shift;
    } elsif ($option eq "++bsubs") {
	die "Option $option needs argument.\n" unless @ARGV;
	$bsub_file = shift;
    } elsif ($option eq "++gzip") {
	$GZIP = 1;
    } elsif ($option eq "++gzbin") {
	$GZBIN = 1;
    } elsif ($option eq "++read_ckp") {
	$simopt .= " +read_ckp";
    } elsif ($option eq "++rpoint") {
	$simopt .= " +rpoint";
    } elsif ($option eq "++pin") {
	$RUN_PIN = 1;
# 	$SUBMIT_CMD	= "\"/usr/local/bin/qsub -V \$MAIL_OPT -o \$pbs_outdir -e \$pbs_outdir -q \$QUEUE \$RESOURCES -l nodes=intel -N \$name\"";
    } elsif ($option eq "++pintool") {
	$RUN_PIN = 1;
	$PIN_TOOL = shift;
# 	$SUBMIT_CMD	= "\"/usr/local/bin/qsub -V \$MAIL_OPT -o \$pbs_outdir -e \$pbs_outdir -q \$QUEUE \$RESOURCES -l nodes=intel -N \$name\"";
	
    } elsif ($option eq "++read_tr") {
	$simopt .= " +read_tr";
    } elsif ($option eq "++bench_bin") {
	$simopt .= " +bench_bin ".shift;
    } elsif ($option eq "++nosubdirs") {
	die "ERROR: ++nosubdirs is no longer supported.\n";
    } elsif ($option eq "++exe_stem") {
	$EXE_STEM = shift;
    } elsif ($option eq "++exe_tail") {
	$EXE_TAIL = shift;
    } elsif (!$found_group) {
	$group = $option;
	$found_group = 1;
    } elsif (!$found_set) {
	$set = $option;
	$found_set = 1;
    } elsif ($option =~ /\+\+/) {
	die "ERROR: Invalid run_batch option '$option'.\n";
    } else {
        $simopt .= " $option";
    }
}
die "ERROR: Must supply run group to run.\n" unless ($found_group); 
die "ERROR: Must supply benchmark regexp or benchmark set to run.\n" unless ($found_set);
die "ERROR: Cannot use ++reuse and ++replace together.\n" if ($REUSE && $REPLACE);
die "ERROR: Cannot use ++noreplace and ++replace together.\n" if ($NOREPLACE && $REPLACE);

if ($RUN_PIN) {
    $RUN_LOCAL_ARGS	= "\"\$group_binary \$outdir \$GZIP pin \$bench \$input \$nice \$subst_simopt \$no_file_tag_opt  \"";
    if (!$BINARY_SET)  {
        # $BINARY ="$PINDIR/tools/$PIN_TOOL/$PIN_TOOL"; 
        $BINARY ="$PIN_TOOL"; 
	
    }
    $RESULTS	= "$PINDIR/results";
    # $SUBMIT_CMD	= "$SUBMIT_CMD -l nodes=intel";
}

# **********************************************************************
# create results directory tree

if (!$PRINT) {
    system("mkdir $RESULTS\n") unless (-d "$RESULTS");
    if (-d "$RESULTS/$group") {
	if (!$REUSE && !$REPLACE && !$NOREPLACE) {
	    do {
		print("WARNING: subdirectory '$group' already exists in '$RESULTS'.  Share directory (y/n)? ");
	    } while (($ans = <STDIN>) !~ /[ynYN]/);
	    exit(0) if ($ans =~ /[Nn]/);
	} elsif ($NOREPLACE) {
	    print("MESSAGE: subdirectory '$group' already exists.  Skipping due to ++noreplace.\n");
	    exit(0);
	}
 	system("ssh mexicanhat.cc.gatech.edu mkdir -p $COC_RESULTS/$group\n") if ($QUEUE eq "coc");
    } else {
	system("mkdir $RESULTS/$group\n");
 	system("ssh mexicanhat.cc.gatech.edu mkdir -p $COC_RESULTS/$group\n") if ($QUEUE eq "coc");
#  	system("ssh mexicanhat.cc.gatech.edu '/bin/sh -c \"if [ -d $COC_RESULTS/$group ]; then echo \"\"; else mkdir -p $COC_RESULTS/$group ; fi\"\n") if ($QUEUE eq "coc");
    }

    system("/bin/echo '$0 " . join(" ", @OLD_ARGV) . "' > $RESULTS/$group/BATCH_CMD\n");
}


# **********************************************************************
# build the runlist

if ($SUITES{(split(/@/, $set))[0]}) { # check for match on benchmark set
    local($suite, $input) = split(/@/, $set);
    local($str) = $SUITES{$suite};
    $str =~ s/std/$input/g if $input;
    #print "str = $str\n";
    @runlist = split(/ /, $str);
#      print("gat:'$str'\n");
#      grep { print "$_: $EXTRA_CODE{$_}\n" } @runlist;

    unless (grep(defined $EXTRA_CODE{$_}, @runlist) == @runlist) {
	if (!$INPUT_CONFIRM)  {
	    do {
		print("WARNING: Input set '$input' is not defined for all benchmarks in suite '$suite'\n");
		$, = ", ";
		print("runlist: "); print(grep(defined $EXTRA_CODE{$_}, @runlist)); print "\n";
		print("Continue? (y/n)\n");
	    } while (($ans = <STDIN>) !~ /[ynYN]/);
	}
	else {
	    $ans = y;
	}
	

	if ($ans =~ /[Yy]/) {
	    @runlist = grep(defined $EXTRA_CODE{$_}, @runlist);
	} else {
	    die "ERROR: Invalid run set specification.\n";
	}
    }

} elsif ($EXTRA_CODE{"$set"}) {	# check for individual benchmark@input 
    @runlist = ( "$set" );
} else {			# look for regexp matches
    @runlist = ();
    print("Doing search for benchmarks matching '$set':\n");
    foreach $bench (sort keys %EXTRA_CODE) {
	if ($bench =~ /$set/) {
	    push(@runlist, $bench);
	} 
    }
    die "\tNo benchmarks match.\n" unless @runlist;
    foreach $bench (@runlist) {
	print("\t$bench\n");
    }
}


# **********************************************************************
# create group binary and params file

die("ERROR: Invalid source binary '$BINARY'.\n") unless -x $BINARY;
die("ERROR: Invalid params file '$param_file'.\n") unless -f $param_file;
die("ERROR: Invalid bench substitutions file '$bsub_file'.\n") unless -f $bsub_file || $bsub_file eq "";
die("ERROR: Must use ++gzbin with a gzipped ++bin.\n") if (!$GZBIN && ($BINARY =~ /\.gz$/));
require($bsub_file) if $bsub_file;

$source_binary    = $BINARY;
$group_binary	  = "$RESULTS/$group/BINARY.$group" . ($GZBIN ? ".gz" : "");
$coc_group_binary = "$COC_RESULTS/$group/BINARY.$group" . ($GZBIN ? ".gz" : "");
$tmp_gzip_binary  = "/tmp/BINARY.$ENV{USER}." . getppid();

if (!$PRINT) {
    if ($GZBIN && $source_binary !~ /\.gz$/) {
	print("Compressing temporary binary to $tmp_gzip_binary.\n");
	die if -f "$tmp_gzip_binary" || -f "$tmp_gzip_binary.bz";
	system("/bin/cp -f $source_binary $tmp_gzip_binary\n");
	system("/bin/gzip --best $tmp_gzip_binary\n");
	$source_binary = "$tmp_gzip_binary.gz";
    }

    if (-x $group_binary) {
	if (!$REUSE && !$REPLACE) {
	    do {
		print("WARNING: group binary already exists.  Use it? (y/n)? ");
	    } while (($ans = <STDIN>) !~ /[ynYN]/);
	}
	if ($REUSE || !$REPLACE && $ans =~ /[Yy]/) {
	    print("Using existing group binary.\n");
	    system("scp $group_binary mexicanhat.cc.gatech.edu:$coc_group_binary\n") if ($QUEUE eq "coc");
	} else {
	    system("cp -f $source_binary $group_binary\n");
	    system("scp $source_binary mexicanhat.cc.gatech.edu:$coc_group_binary\n") if ($QUEUE eq "coc");
	}
    } else {
	if (chk_file_zip($group_binary)) {
	    die("ERROR: Un/Compressed version of binary exists in run directory.\n");
	}

	system("cp -f $source_binary $group_binary\n");
	system("scp $source_binary mexicanhat.cc.gatech.edu:$coc_group_binary\n") if ($QUEUE eq "coc");
    }

    if (-f "$RESULTS/$group/params.in") {
	if (!$REUSE && !$REPLACE) {
	    do {
		print("WARNING: group params.in already exists.  Use it? (y/n)? ");
	    } while (($ans = <STDIN>) !~ /[ynYN]/);
	}
	if ($REUSE || !$REPLACE && $ans =~ /[Yy]/) {
	    print("Using existing params.in.\n");
	    $param_file = "$RESULTS/$group/params.in";
	    system("scp $param_file mexicanhat.cc.gatech.edu:$COC_RESULTS/$group/params.in\n") if ($param_file ne "" && $QUEUE eq "coc");
	} else {
	    system("cp -f $param_file $RESULTS/$group/params.in\n") if ($param_file ne "");
	    system("scp $param_file mexicanhat.cc.gatech.edu:$COC_RESULTS/$group/params.in\n") if ($param_file ne "" && $QUEUE eq "coc");
	}
    } else {
	system("cp -f $param_file $RESULTS/$group/params.in\n") if ($param_file ne "");
	system("scp $param_file mexicanhat.cc.gatech.edu:$COC_RESULTS/$group/params.in\n") if ($param_file ne "" && $QUEUE eq "coc");
    }

    if (-f "$tmp_gzip_binary.gz") {
	print("Removing temporary compressed binary $tmp_gzip_binary.gz.\n");
	system("/bin/rm $tmp_gzip_binary.gz\n");
    }
}


# **********************************************************************
# need to make sure the scripts are all on the coc shadow

if (!$PRINT && $QUEUE eq "coc") {
    system("ssh mexicanhat.cc.gatech.edu mkdir -p $COC_SIMDIR/bin\n");
    foreach $file ("run_macsim", "bench_common", "chk_results") {
	system("scp $SIMDIR/bin/$file mexicanhat.cc.gatech.edu:$COC_SIMDIR/bin/$file\n");
    }    
    $group_binary = $coc_group_binary;
    $ENV{'SIMDIR'} = $COC_SIMDIR;
    $ENV{'SPEC00'} = "/net/hp95/hparch/shared/benchmarks/spec00/benchspec";
    $ENV{'JBS'}    = "/net/hp95/hparch/shared/benchmarks/jbs";
    $ENV{'SPEC95'} = "/net/hp95/hparch/shared/benchmarks/spec95";
}



# **********************************************************************
# change to root directory for sanity

# $RESOURCES = "-l nodes=1:${QUEUE}m";
chdir("/");


# **********************************************************************
# created coc directories---optimized to create the results
# directories with a single ssh

if ($QUEUE eq "coc" && !$PRINT) {
    @COC_DIRS = ();
    foreach $bench (@runlist) {
	if ($bench =~ /(.*)\@(.*)/) {
	    $bench = $1;
	    $input = $2;
	} else {
	    $input = "std";
	}

	$coc_outdir = "$COC_RESULTS/$group/$bench\@$input";
	push(@COC_DIRS, $coc_outdir);
    }
    print ("ssh mexicanhat.cc.gatech.edu mkdir " . join(" ", @COC_DIRS) . " 2> /dev/null\n");
    system("ssh mexicanhat.cc.gatech.edu mkdir " . join(" ", @COC_DIRS) . " 2> /dev/null\n");
}


# **********************************************************************
# main loop to launch all the jobs

foreach $bench4 (@runlist) { 
    if ($bench4 =~ /(.*)\@(.*)/) {
	$bench4 = $1;
	$input4 = $2;
    } else {
	$input4 = "ref";
    }

foreach $bench3 (@runlist) { 
    if ($bench3 =~ /(.*)\@(.*)/) {
	$bench3 = $1;
	$input3 = $2;
    } else {
	$input3 = "ref";
    }

foreach $bench2 (@runlist) { 
    if ($bench2 =~ /(.*)\@(.*)/) {
	$bench2 = $1;
	$input2 = $2;
    } else {
	$input2 = "ref";
    }
foreach $bench (@runlist) {
    if ($bench =~ /(.*)\@(.*)/) {
	$bench = $1;
	$input = $2;
    } else {
	$input = "ref";
    }
	
    $outdir	     = "$RESULTS/$group/${bench}\+${bench2}\+${bench3}\+${bench4}\@$input";
    $coc_outdir	     = "$COC_RESULTS/$group/${bench}\+${bench2}\+${bench3}\+${bench4}\@$input";
    $no_file_tag_opt = "+nofiletag";

    if (!(-d "$outdir")) {
	system("mkdir $outdir\n") unless $PRINT;
#  	system("ssh mexicanhat.cc.gatech.edu mkdir $coc_outdir\n") unless $PRINT || $QUEUE ne "coc";
    }

    if ($QUEUE eq "coc") {
	$outdir = $coc_outdir;
	$pbs_outdir = $coc_outdir;
	$pbs_outdir =~ s/^\/home/mexicanhat.cc.gatech.edu:/;
	$nice = "+nice 5";
    } else {
	$pbs_outdir = $outdir;
	$nice = "+nice 20";
    }
    #print "bench = $bench\n";
    $name = substr($group, 0, 10) . "-" . substr($bench, 0, 4);
    if ($QUEUE ne "coc") {
	$cmd_tail = $param_file eq "" ? "" : " +params $RESULTS/$group/params.in";
    } else {
	$cmd_tail = $param_file eq "" ? "" : " +params $COC_RESULTS/$group/params.in";
    }

    # substitute into the sim options
    $subst_simopt = $simopt;
    $subst_simopt =~ s/%bench/$bench/g;
    $subst_simopt =~ s/%input/$input/g;

    # make substitutions in bsub file, if necessary
    if ($bsub_file) {
	while ($subst_simopt =~ /%(\w+)|({(\w+)})/) {
	    my $substr  = defined($1) ? $1 : $2; 
	    my $subname = defined($1) ? $1 : $3;
	    my $expression = '"$SUB_' . $subname . '{\'' . $bench . '@' . $input . '\'}"';
#  	    print("Searching for '$expression'\n");
 	    my $value = eval($expression);
 	    if ($value) {
 		$subst_simopt =~ s/%$substr/$value/g;
 	    } else {
		die "Substitution value for '%$subname' not found for benchmark '$bench\@$input'.\n";
	    }
	}
    }

    # check substitutions for sanity
    die "Invalid % substitution in run command.\n'$subst_simopt'\n" if ($subst_simopt =~ /%/); 

    #if ($SHELL =~ /\/bin\/(zsh|bash|sh)/) {
    # system() always uses /bin/sh -c, so we should use export
    if (1) {
	$cmd = "export RUN_LOCAL_ARGS=\"" . eval($RUN_LOCAL_ARGS) . "$cmd_tail\"; ";
    } else {
	$cmd = "setenv RUN_LOCAL_ARGS \"" . eval($RUN_LOCAL_ARGS) . "$cmd_tail\"; ";
    }
    #$cmd .= eval($SUBMIT_CMD) . " $RUN_LOCAL_CMD";

    $output_path = "$RESULTS/$group/$bench\+${bench2}\+${bench3}\+${bench4}\@$input";
    $run_script = "$output_path/run_cmd.pl";
 #   print ("run_script path is $run_script\n");
    $local_command = $RUN_LOCAL_CMD." +bin $group_binary +params ../params.in $bench\@$input ${bench2}\@$input ${bench3}\@$input ${bench4}\@$input";
  
    open(OUTPUT, ">", $run_script);
    print OUTPUT "#!/usr/bin/perl\n";
    print OUTPUT "\$ENV{SIMDIR} = \"$ENV{SIMDIR}\";\n";
    print OUTPUT "chdir \"".AddSlash($output_path)."\";\n";
    $local_command = AddSlash($local_command);
    print OUTPUT "system(\"$local_command\");\n";
    close(OUTPUT);
    
    chmod 0555, $run_script;

    $cmd = "echo '/usr/bin/perl $run_script' | ".eval($SUBMIT_CMD);
    if ($PRINT) {
	print("$cmd\n");
    } else {
	system("/bin/echo '$cmd' > $RESULTS/$group/$bench\+${bench2}\+${bench3}\+${bench4}\@$input/RUN_CMD\n");
	system("$cmd | tee $RESULTS/$group/$bench\+${bench2}\+${bench3}\+${bench4}\@$input/JOB_ID\n");
    }
	sleep(1);
}
}
}
}
