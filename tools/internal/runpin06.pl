#!/usr/bin/perl


@bench_groups_int  =   (
		    'perlbench',
		    'bzip2',
		    'gcc',
		    'mcf',
		    'gobmk',
		    'hmmer',
		    'sjeng',
		    'libquantum',
		    'h264ref',
		    'omnetpp',
		    'astar',
		    'xalancbmk' );

@bench_groups_fp = ( 
		     'bwaves',
		     'gamess',
		     'milc',
		     'zeusmp',
		     'gromacs',
		     'cactusADM',
		     'leslie3d',
		     'namd',
		     'dealII',
		     'soplex',
		     'povray',
		     'calculix',
		     'GemsFDTD',
		     'tonto',
		     'lbm',
		     'wrf',
		     'sphinx3'
		     );


@bench_groups_fp_cpp = ( 
			  'namd',
			 'dealII',
			 'soplex',
			 'povray',
			 );


@bench_groups = (@bench_groups_int , @bench_groups_fp_cpp); 

%bench_dir = (    "perlbench" => "400.perlbench"  ,
		  "bzip2" => "401.bzip2",
		  "gcc" => "403.gcc",
		  "mcf" => "429.mcf",
		  "gobmk" => "445.gobmk",
		  "hmmer" => "456.hmmer",
		  "sjeng" => "458.sjeng",
		  "libquantum" => "462.libquantum",
		  "h264ref" => "464.h264ref",
		  "omnetpp" => "471.omnetpp",
		  "astar" => "473.astar",
		  "xalancbmk" => "483.xalancbmk",
		  "bwaves" => "410.bwaves",
		  "gamess" => "416.gamess",
		  'milc' => "433.milc",
		  "zeusmp" => "434.zeusmp",
		  "gromacs" => "435.gromacs",
		  "cactusADM" => "436.cactusADM",
		  "leslie3d" => "437.leslie3d", 
		  "namd" => "444.namd",
		  "dealII" => "447.dealII",
		  "soplex" => "450.soplex",
		  "povray" => "453.povray",
		  "calculix" => "454.calculix",
		  "GemsFDTD" => "459.GemsFDTD",
		  "tonto" => "465.tonto",
		  "lbm" => "470.lbm",
		  "wrf" => "481.wrf",
		  "sphinx3" => "482.sphinx3"
		  );



%bench_bin = (    "perlbench" => "perlbench"  ,
		  "bzip2" => "bzip2",
		  "gcc" => "gcc",
		  "mcf" => "mcf",
		  "gobmk" => "gobmk",
		  "hmmer" => "hmmer",
		  "sjeng" => "sjeng",
		  "libquantum" => "libquantum",
		  "h264ref" => "h264ref",
		  "omnetpp" => "omnetpp",
		  "astar" => "astar",
		  "xalancbmk" => "Xalan",
		  "bwaves" => "bwaves",
		  "gamess" => "gamess",
		  'milc' => "milc",
		  "zeusmp" => "zeusmp",
		  "gromacs" => "gromacs",
		  "cactusADM" => "cactusADM",
		  "leslie3d" => "leslie3d", 
		  "namd" => "namd",
		  "dealII" => "dealII",
		  "soplex" => "soplex",
		  "povray" => "povray",
		  "calculix" => "calculix",
		  "GemsFDTD" => "GemsFDTD",
		  "tonto" => "tonto",
		  "lbm" => "lbm",
		  "wrf" => "wrf",
		  "sphinx3" => "sphinx3"
		  );





%bench_run_ref = (   "perlbench" => "3"   ,
		     "bzip2" => "6",
		     "gcc" => "9",
		     "mcf" => "1",
		     "gobmk" => "5",
		     "hmmer" => "2",
		     "sjeng" => "1",
		     "libquantum" => "1",
		     "h264ref" => "3",
		     "omnetpp" => "1",
		     "astar" => "2",
		     "xalancbmk" => "1",
		     "bwaves" => "1",
		     "gamess" => "1",
		     'milc' => "1",
		     "zeusmp" => "1",
		     "gromacs" => "1",
		     "cactusADM" => "1",
		     "leslie3d" => "1", 
		     "namd" => "1",
		     "dealII" => "1",
		     "soplex" => "1",
		     "povray" => "1",
		     "calculix" => "1",
		     "GemsFDTD" => "1",
		     "tonto" => "1",
		     "lbm" => "1",
		     "wrf" => "1",
		     "sphinx3" => "1");

%bench_run_arg_ref = (
		      "perlbench1" => "-I./lib checkspam.pl 2500 5 25 11 150 1 1 1 1 > checkspam.2500.5.25.11.150.1.1.1.1.out ",
		      "perlbench2" => "-I./lib diffmail.pl 4 800 10 17 19 300 > diffmail.4.800.10.17.19.300.out ",
		      "perlbench3" => "-I./lib splitmail.pl 1600 12 26 16 4500 > splitmail.1600.12.26.16.4500.out ",
		      "bzip21" =>"input.source 280 > input.source.out ",
		      "bzip22" => "chicken.jpg 30 > chicken.jpg.out ",
		      "bzip23" => "liberty.jpg 30 > liberty.jpg.out ",
		      "bzip24" => "input.program 280 > input.program.out ",
		      "bzip25" => "text.html 280 > text.html.out ",
		      "bzip26" => "input.combined 200 > input.combined.out",
		      "gcc1" => "166.i -o 166.s > 166.out ",
		      "gcc2" => "200.i -o 200.s > 200.out ",
		      "gcc3" => "c-typeck.i -o c-typeck.s > c-typeck.out ",
		      "gcc4" => "cp-decl.i -o cp-decl.s > cp-decl.out ",
		      "gcc5" => "expr.i -o expr.s > expr.out ",
		      "gcc6" => "expr2.i -o expr2.s > expr2.out ",
		      "gcc7" => "g23.i -o g23.s > g23.out ", 
		      "gcc8" => "s04.i -o s04.s > s04.out",
		      "gcc9" => "scilab.i -o scilab.s > scilab.out ",
		      "mcf1" => "inp.in > inp.out " ,
		      "gobmk1" => "--quiet --mode gtp < 13x13.tst > 13x13.out ", 
		      "gobmk2" => "--quiet --mode gtp < nngs.tst > nngs.out ",
		      "gobmk3" => "--quiet --mode gtp < score2.tst > score2.out ",
		      "gobmk4" => "--quiet --mode gtp < trevorc.tst > trevorc.out ", 
		      "gobmk5" => "--quiet --mode gtp < trevord.tst > trevord.out ",
		      "hmmer1" => "nph3.hmm swiss41 > nph3.out ", 
		      "hmmer2" => "--fixed 0 --mean 500 --num 500000 --sd 350 --seed 0 retro.hmm > retro.out ",
		      "sjeng1" => "ref.txt > ref.out " ,
		      "libquantum1" => "1397 8 > ref.out ", 
		      "h264ref1" => "-d foreman_ref_encoder_baseline.cfg > foreman_ref_baseline_encodelog.out ",
		      "h264ref2" => "-d foreman_ref_encoder_main.cfg > foreman_ref_main_encodelog.out ",
		      "h264ref3" => "-d sss_encoder_main.cfg > sss_main_encodelog.out ",
		      "omnetpp1" => "omnetpp.ini > omnetpp.log ",
		      "astar1" => "BigLakes2048.cfg > BigLakes2048.out " ,
		      "astar2" => "rivers.cfg > rivers.out ",
		      "xalancbmk1" => "-v t5.xml xalanc.xsl > ref.out ",
		      "namd1" => " --input namd.input --iterations 38 --output namd.out",
		      "deallII1" => " 23" , 
		      "soplex1" => " -m3500 ref.mps",
		      "povray1" => " SPEC-benchmark-ref.ini",
);

die "Set PIN2_2_HOME environment variable for pin-2.2." unless $ENV{'PIN2_2_HOME'};
die "Set PIN2_3_HOME environment variable for pin-2.3." unless $ENV{'PIN2_3_HOME'};
die "Set SIMPOINT_HOME environment variable." unless $ENV{'SIMPOINT_HOME'};
die "Set PIN2UTEX_HOME environment variable." unless $ENV{'PIN2UTEX_HOME'};


$base_2006 = "/hparch/benchmarks/cpu2006";
$base_dir = "${base_2006}/benchspec/CPU2006"; 
$input = "ref";
$exe_base = "_base";
$exe_tail = ".cpu2006.linux64.intel64.qxp";
## FIXME 
#$pin_home = "/net/hp95/hparch/users/hyesoon/pintools/pin-2.3-17236-gcc.3.4.2-ia32e-linux";
$pin2_2_home = $ENV{'PIN2_2_HOME'};
$pin2_3_home = $ENV{'PIN2_3_HOME'};
$simpoint_home = $ENV{'SIMPOINT_HOME'};
$pin2utex_home = $ENV{'PIN2UTEX_HOME'};

$isimpoint_path = "$pin2_3_home/PinPoints/bin/isimpoint.so";
$pin2_2_path = "$pin2_2_home/Bin/pin";
$pin2_3_path = "$pin2_3_home/Bin/pin";
$simpoint_path = "$simpoint_home/simpoint";
$ppgen_path = "$pin2_3_home/PinPoints/bin/ppgen.3";
$pin2utex_path = "$pin2utex_home/pin2utex";

die "No isimpoint.so in $isimpoint_path.\n" unless -x "$isimpoint_path";
die "No pin 2.2 in $pin2_2_path.\n" unless -x "$pin2_2_path";
die "No pin 2.3 in $pin2_3_path.\n" unless -x "$pin2_3_path";
die "No simpoint in $simpoint_path.\n" unless -x "$isimpoint_path";
die "No ppgen.3 in $ppgen_path\n" unless -x "$ppgen_path";
die "No pin2utex in $pin2utex_path.\n" unless -x "$pin2utex_path";

## FIXME tool name 
#$tool_name = "/net/hc281/hyesoon/research/quilin/lprof";
$tool_name = "$pin_home/PinPoints/bin/isimpoint.so";
$tool_arg = "";
$dir_prefix="icc91-base";

$pin_run = 1;
$pin_iter = 1;
$oo = 100; 

foreach $bench (@bench_groups)
{
    $oo = $oo+1; 
    open (OUTFILE, "> runme-$oo"); 
    
    $bench_dir = "${base_dir}/${bench_dir{$bench}}/run/${input}"; 
    $bin_name = ${bench_bin{$bench}}.${exe_base}.${exe_tail};
    # $bin_name = ${bench}.${exe_base}.${exe_tail};
    $bin_dir ="${base_dir}/${bench_dir{$bench}}/exe";
    print (OUTFILE "cd $bench_dir\n"); 
    if (${input} =~ 'ref') { 
	$iter_count = $bench_run_ref{$bench};

	$ii = 0; 
	
	while($ii < $iter_count) {
	    $ii++;
	    $index = ${bench}.${ii};

	    if ($pin_run) { 
		
		for ($jj = 1; $jj <= $pin_iter; $jj++) { 

		    ## generate pin point data file 
		    $pinout_dir = "/storage/traces/cpu2006/CPU2006/${bench_dir{$bench}}/${dir_prefix}/";
		    print (OUTFILE "mkdir -p ${pinout_dir} \n");
		    print (OUTFILE "rm -rf ${pinout_dir}/${bin_name}${ii}.T.0.bb \n");
		    $tool_arg_iter ="-o ${pinout_dir}/${bin_name}${ii} -slice_size 200000000 ";
		    print (OUTFILE "$pin2_3_path -t $isimpoint_path ${tool_arg_iter} -- ${bin_dir}/${bin_name} $bench_run_arg_ref{$index}\n"); 
		    
		    print (OUTFILE "mkdir -p ${pinout_dir}/ppdata \n");
		    print (OUTFILE "cd  ${pinout_dir}/ppdata \n");
		    print (OUTFILE "ln -s ${pinout_dir}/${bin_name}${ii}.T.0.bb \n");
		    $bb_data =  "${pinout_dir}/ppdata/${bin_name}${ii}.T.0.bb";
		    $pp_out = "${pinout_dir}/ppdata/${bin_name}${ii}-point"; 
		    print( OUTFILE "$simpoint_path -loadFVFile ${bb_data} -maxK 1 -coveragePct 0.9999 -saveSimpoints ${bb_data}.simpoints -saveSimpointWeights ${bb_data}.weights -saveLabels ${bb_data}.labels > ${bb_data}.simpointout \n\n");
		    print (OUTFILE "$ppgen_path ${pp_out}  ${bb_data} ${bb_data}.simpoints.lpt0.9999 ${bb_data}.weights.lpt0.9999 ${bb_data}.labels 0 >& ${bb_data}.ppgenout \n\n");
		    
		  print (OUTFILE "cd $bench_dir\n"); 
	
		   # print (OUTFILE "mkdir -p ${pinout_dir}/br_traces \n");
		    
		    $bench_dir = "${base_dir}/${bench_dir{$bench}}/run/${input}"; 
		    
		    $trace_key_ref = "${bench}-icc-base\@ref";
		    $trace_key = ${trace_key_ref}."-${ii}";
		   
 
		    ## trace tool name FIXME!! 
		    #$tr_tool_name = "/net/beaded/shared/pro/pin/trace_support/decoder_pp/decoder_pp";
		    print (OUTFILE "mkdir -p  ${pinout_dir}/pin_traces \n");
		    $pp_out = "${pinout_dir}/ppdata/${bin_name}${ii}-point.1.pp"; 
		    $trace_out = "${pinout_dir}/pin_traces/ref${ii}.pzip.bz2";
		    #$tool_arg_iter =" -raw_trace 1   -ppfile ${pp_out} -cbp_trace_o ${trace_out}";
		    $tool_arg_iter ="  -ppfile ${pp_out} -tracename ${trace_out}";
		    print (OUTFILE "$pin2_2_path -t $pin2utex_path ${tool_arg_iter} -- ${bin_dir}/${bin_name} $bench_run_arg_ref{$index}\n"); 
		    
		    $trace_key_ref = "${bench}-icc-base\@ref";
		    $trace_key = ${trace_key_ref}."-${ii}";
		    
		}
		
	    }
	    
	    else { 
		print (OUTFILE "${bin_dir}/$bin_name $bench_run_arg_ref{$index} \n");   
	    }
	    
	}
    }
    print (OUTFILE "cd ${base_2006} \n");
}

