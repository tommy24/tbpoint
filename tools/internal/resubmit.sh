#! /bin/bash

# simple batch script to re-submit failed jobs - nagesh on july-08-2009 
# TO DO - specify job names when submitting jobs

function usage
{
	echo "usage : ./<script_name> [-r <root_dir>] -e <comma_separated_benchmark_list> <dir_1> <dir_2> ..."
	echo " or "
	echo "usage : ./<script_name> [-r <root_dir>] -o <comma_separated_benchmark_list> <dir_1> <dir_2> ..."
	echo " or "
	echo "usage : ./<script_name> [-r <root_dir>] <dir_1> <dir_2> ..."

	echo -e "\npurpose: resubmit failed jobs\n"

	echo -e "\ncaution: when using root option be careful with wildcards, make sure none of the local directory entries match the wildcard\n"
}

if [ $# -eq 0 ]
then
	usage
	exit
fi

if [ $1 == "-r" ]
then
	root_dir="${2}/"
	shift 2
else
	root_dir=""
fi


flag=0
if [ "$1" == "-e" ]
then
	if [ $# -lt 3 ]
	then
		usage
		exit
	fi
	flag=1
	except_list=(`echo $2 | tr "," " "`)
	shift
	shift
elif [ "$1" == "-o" ]
then
	if [ $# -lt 3 ]
	then
		usage
		exit
	fi
	flag=2
	only_list=(`echo $2 | tr "," " "`)
	shift
	shift
fi

except_count=$((${#except_list[@]}-1))
only_count=$((${#only_list[@]}-1))



cwd=`pwd`

if [ "${root_dir}" != "" ]
then
	cd ${root_dir}
	dirs=(`ls -d $@`)
	cd ${cwd}
else
	dirs=($@)
fi


for dir in "${dirs[@]}"
do
	echo "entering $dir"
	if [ -d "${root_dir}${dir}" ]
	then
		cd ${root_dir}${dir}

		for i in `ls`
		do
			
			if [ -d ${i} -a ! -f ${i}/core.stat.out ]
			then

				#dont know sed
				benchmark=`echo $i | awk -F "@" '{print $1}'`

				if [ $flag -eq 1 -o $flag -eq 2 ]
				then
					if [ $flag -eq 1 ]
					then
						run=1
						for count in `seq 0 ${except_count}`
						do
							if [ ${except_list[$count]} == ${benchmark} ]
							then
								run=0
							fi
						done
					else
						run=0
						for count in `seq 0 ${only_count}`
						do
							if [ ${only_list[$count]} == ${benchmark} ]
							then
								run=1
							fi
						done
					fi
				else
					run=1
				fi

				if [ ${run} -eq 1 ]
				then
					echo -e "\tentering $i"
					cd $i

						echo -e "\t\tqsub run_cmd.pl"
						qsub run_cmd.pl -l nodes=1:ppn=3

					echo -e "\tcd .."
					cd ..
				fi

			fi
		done

		cd ${cwd}
		echo -e "done\n"
	fi

done
