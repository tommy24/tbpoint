#!/usr/bin/python
#########################################################################################
# Author: Jaekyu Lee (kacear@gmail.com)
# Date: 8/21/2011
# Description: check torque-simulation results whether all jobs are completed
# Usage:
#   check_result.py -dir dir1 dir2 dir* -suite SUITE [OPTIONS]
# Options:
#   -force - resubmit all jobs again
#   -summary - show summary
#   -qsub - resubmit incomplete jobs
#   -proc - specify number of processors for the job (torque option)
#   -email - send email when all jobs are completed (TODO)
#########################################################################################


import argparse
import sys
import os
import bench_common
import re
import time


#########################################################################################
# sanity check
#########################################################################################
def sanity_check(args):
  if not args.suite:
    print('error: suite not defined')
    exit(0)

  if not args.suite in bench_common.SUITES:
    print('error: suite %s does not exist in bench_common.py.' % (args.suite))
    exit(0)


#########################################################################################
# create an argument parser
#########################################################################################
def process_options():
  parser = argparse.ArgumentParser(description='xxx')
  parser.add_argument('-d', '-dir', action='append', nargs='*', dest='dir_list',
      help='directory list')
  parser.add_argument('-suite', action='store', dest='suite',
      help='suite name')
  parser.add_argument('-force', action='store_true', dest='force',
      help='enforce re-submit all jobs')
  parser.add_argument('-summary', action='store_true', dest='summary',
      help='show summary')
  parser.add_argument('-qsub', action='store_true', dest='qsub',
      help='re-submit incomplete jobs')
  parser.add_argument('-proc', action='store', default='1', dest='nproc',
      help='torque option: number of processors')
  parser.add_argument('-email', action='store_true', dest='email', \
      help='send email after all jobs are completed')

  return parser


#########################################################################################
# main function
#########################################################################################
def main(argv):
  # parse arguments
  parser = process_options()
  args = parser.parse_args()
  
  sanity_check(args)
  
  # flatten argument list
  args.dir_list = sum(args.dir_list, [])

  current_dir = os.getcwd()
  suite = bench_common.SUITES[args.suite]
#  suite = sum(suite, [])

  done_list = {}

  while 1:
    for dir in args.dir_list:
      if dir in done_list:
        next
      job_total = len(suite)
      job_success = 0
      job_error   = 0
      job_running = 0
      for bench in suite:
        subdir = current_dir + '/' + dir + '/' + bench
        if not os.path.exists(subdir):
          print('Error: dir %s does not exist' % (subdir))
          job_error += 1
          continue

        os.chdir('%s' % (subdir))

        success = False
        error = False
        if os.path.exists(subdir + '/qsub.stderr'):
          if os.path.getsize(subdir + '/qsub.stderr') == 0:
            assert(os.path.exists(subdir + '/qsub.stdout'))

            exit_code = None
            file_handle = open(subdir + '/qsub.stdout', 'r')
            for line in file_handle.readlines():
              if re.match('^Done$', line):
                exit_code = 0
                break
            file_handle.close()

            if exit_code == 0:
              success = True
            else:
              error = True
          else:
            error = True
        else:
          if args.summary:
            print('Running: %s/%s' % (dir, bench))
          job_running += 1
          
        if error:    
          job_error += 1
          if args.summary:
            print('Error: %s/%s' % (dir, bench))
        elif success:
          job_success += 1


        if args.force or (not args.summary and not success and args.qsub):
          cmd = '' 
          cmd += 'qsub '
          cmd += '%s/run.py ' % (subdir)
          cmd += '-l nodes=1:ppn=%s ' % (args.nproc)
          cmd += '-q pool1 '
          cmd += '-N %s_%s ' % (dir, bench)
          cmd += '-o %s/qsub.stdout ' % (subdir)
          cmd += '-e %s/qsub.stderr ' % (subdir)
          print(cmd)
          os.system(cmd)
    
      print(dir)
      print('\ttotal: %d' % job_total)
      print('\tsuccess: %d' % job_success)
      print('\trunning: %d' % job_running)
      print('\terror: %d\n' % job_error)

      if args.email and job_running == 0:
        output_text = '%s done: %d success %d error' % (dir, job_success, job_error)
        os.system('echo "%s" | mutt -s "check_result_%s" %s' % (output_text, dir, os.environ['EMAIL']))
        done_list[dir] = True


    if not args.email:
      break
    else:
      if len(args.dir_list) == len(done_list):
        break
      else:
        time.sleep(60)
        

#########################################################################################
# main routine
#########################################################################################
if __name__ == '__main__':
  main(sys.argv)


#########################################################################################
# End of file
#########################################################################################
