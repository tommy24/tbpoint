#!/usr/bin/python
#########################################################################################
# Author: Jaekyu Lee (kacear@gmail.com)
# Date: 7/172011
# Description: run macsim binary with the specified benchmark locally
#
# Note:
#   1. when you specify -cmd option, wrap entire command with " " and please remove first '--'
#   2. if -bin or -param is not specified, use one in the current directory
#########################################################################################



import sys
import os
import argparse
import bench_common
import trace_common


#########################################################################################
# sanity check
#########################################################################################
def sanity_check():
  global args

  if not args.bench:
    print('error: bench not specified. specify -bench option')
    exit(0)


#########################################################################################
# create an argument parser
#########################################################################################
def process_options():
  parser = argparse.ArgumentParser(description='run_macsim.py')
  parser.add_argument('-bench', action='store', dest='bench', help='benchmark to run. Should be defined in trace_common.py')
  parser.add_argument('-bin', action='store', dest='bin', help='macsim binary to run')
  parser.add_argument('-param', action='store', dest='param', help='params.in file to run')
  parser.add_argument('-cmd', action='store', dest='cmd', help='additional knobs. wrap with "". do not put -- right after -cmd option. see example in README.')

  return parser


#########################################################################################
# main function
#########################################################################################
def main(argv):
  global args
  
  # parse arguments
  parser = process_options()
  args = parser.parse_args()
  
  sanity_check()

  current_dir = os.getcwd()
  
  # create trace_file_list
  bench = args.bench
  bench = bench[:bench.find('@')]
  bench_list = bench.split('_')
  file = open(current_dir + '/trace_file_list', 'w')
  file.write('%d\n' % len(bench_list))
  for ii in range(0, len(bench_list)):
    if not '%s@ref' % (bench_list[ii]) in trace_common.TRACE_FILE:
      print('error: bench %s does not exist in TRACE_FILE' % (bench_list[ii]));
      exit(0)
    file.write('%s\n' % (trace_common.TRACE_FILE['%s@ref' % (bench_list[ii])]))
  file.close()

  # prepare params.in
  if args.param:
    os.system('cp -f %s %s/params.in' % (args.param, current_dir))

  if args.bin:
    os.system('cp -f %s %s/macsim' % (args.bin, current_dir))

  # execute
  if args.cmd:
    os.system('./macsim --%s' % (args.cmd))
  else:
    os.system('./macsim')




#########################################################################################
if __name__ == '__main__':
  main(sys.argv)
