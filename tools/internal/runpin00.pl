#!/usr/bin/perl
@bench_groups  =   (
	 'gzip',
                    'vpr',
                    'gcc',
                    'mcf',
                    'crafty',
                    'parser',
                    'eon',
                    'perlbmk',
                    'gap',
                    'vortex',
                    'bzip2',
                    'twolf');


%bench_dir = ("gzip" => "164.gzip",
		  "vpr" =>  "175.vpr",
		  "gcc" => "176.gcc",
		  "mcf" => "181.mcf",
		  "crafty" => "186.crafty",
		  "parser" => "197.parser",
		  "eon" => "252.eon",
		  "perlbmk" => "253.perlbmk",
		  "gap" => "254.gap",
		  "vortex" => "255.vortex",
		  "bzip2" => "256.bzip2",
		  "twolf" => "300.twolf"); 



%bench_bin = ("gzip" => "gzip",
		  "vpr" =>  "vpr",
		  "gcc" => "cc1",
		  "mcf" => "mcf",
		  "crafty" => "crafty",
		  "parser" => "parser",
		  "eon" => "eon",
		  "perlbmk" => "perlbmk",
		  "gap" => "gap",
		  "vortex" => "vortex",
		  "bzip2" => "bzip2",
		  "twolf" => "twolf"); 
	      

%bench_run_ref = ("gzip" => "5",
		  "vpr" =>  "2",
		  "gcc" => "5",
		  "mcf" => "1",
		  "crafty" => "1",
		  "parser" => "1",
		  "eon" => "3",
		  "perlbmk" => "7",
		  "gap" => "1",
		  "vortex" => "3",
		  "bzip2" => "3",
		  "twolf" => "1"); 


%bench_run_arg_ref = ("gzip1" => "input.source 60 > input.source.out", 
		      "gzip2" => "input.log 60 > input.log.out",
		      "gzip3" => "input.graphic 60 > input.graphic.out", 
		      "gzip4" => "input.random 60 > input.random.out",
		      "gzip5" => "input.program 60 > input.program.out", 
		      "vpr1" => "net.in arch.in place.out dum.out -nodisp -place_only -init_t 5 -exit_t 0.005 -alpha_t 0.9412 -inner_num 2 > place_log.out",
		      "vpr2" => "net.in arch.in place.in route.out -nodisp -route_only -route_chan_width 15 -pres_fac_mult 2 -acc_fac 1 -first_iter_pres_fac 4 -initial_pres_fac 8 > route_log.out",
		      "gcc1" => "166.i -o 166.s > 166.out", 
		      "gcc2" => "200.i -o 200.s > 200.out",
		      "gcc3" => "expr.i -o expr.s > expr.out",
		      "gcc4" => "integrate.i -o integrate.s > integrate.out",
		      "gcc5" => "scilab.i -o scilab.s > scilab.out",
		      "mcf1" => "inp.in > inp.out",
		      "crafty1" => "< crafty.in > crafty.out",
		      "parser1" => "2.1.dict -batch < ref.in > ref.out" ,
		      "eon1" => "chair.control.cook chair.camera chair.surfaces chair.cook.ppm ppm pixels_out.cook > cook_log.out",
		      "eon2" => "chair.control.rushmeier chair.camera chair.surfaces chair.rushmeier.ppm ppm pixels_out.rushmeier > rushmeier_log.out", 
		      "eon3" => "chair.control.kajiya chair.camera chair.surfaces chair.kajiya.ppm ppm pixels_out.kajiya > kajiya_log.out", 
		      "perlbmk1" => "-I./lib diffmail.pl 2 550 15 24 23 100 > 2.550.15.24.23.100.out",
		      "perlbmk2" => "-I. -I./lib makerand.pl > makerand.out",
		      "perlbmk3" => "-I./lib perfect.pl b 3 m 4 > b.3.m.4.out",
		      "perlbmk4" => "-I./lib splitmail.pl 850 5 19 18 1500 > 850.5.19.18.1500.out",
		      "perlbmk5" => "-I./lib splitmail.pl 704 12 26 16 836 > 704.12.26.16.836.out",
		      "perlbmk6" => "-I./lib splitmail.pl 535 13 25 24 1091 > 535.13.25.24.1091.out",
		      "perlbmk7" => "-I./lib splitmail.pl 957 12 23 26 1014 > 957.12.23.26.1014.out",
		      "gap1" => "-l ./ -q -m 192M < ref.in > ref.out",
		      "vortex1" => "lendian1.raw > vortex1.out2",
		      "vortex2" => "lendian2.raw > vortex2.out2",
		      "vortex3" => "lendian3.raw > vortex3.out2",
		      "bzip21" => "input.source 58 > input.source.out",
		      "bzip22" => "input.graphic 58 > input.graphic.out",
		      "bzip23" => "input.program 58 > input.program.out",
		      "twolf1" => "ref > ref.stdout"
		      );


$base_2000 = "/net/hp95/hparch/shared/benchmarks";

$base_dir = "${base_2000}/benchspec/CINT2000"; 
$input = "ref";
$exe_base = "_x86_base";
$exe_base = "_base";
$exe_tail =".cpu2000.itanium.linux.pin44.pronto";
$exe_tail =".cpu2000.itanium.linux.pin14.pronto";
$exe_tail =".linux-ipf-opt";
$exe_tail =".cpu2000.itanium.linux.pin.noif.pronto";
$exe_tail =".ipf_maxif";
$exe_tail =".linux-ipf-opt";
$exe_tail =".linux-ia32-opt";
$exe_tail =".linux-ia32-nopgo_opt";
$exe_tail =".linux.em64t_nopgo.opt";
$exe_tail = ".opt";
$exe_tail = ".icc91.pg-opt";
$exe_tail = ".icc91-opt";
$exe_tail = ".icc91.pg-novaluepg-opt";
$exe_tail = ".icc91.pg-valuepg-opt";
$pin_home = "/net/beaded/shared/pro/pin/pin-2.0-7400HKamd-gcc3.2-ia32-linux";
$tool_name = "/net/beaded/shared/pro/pin/pin-2.0-7400HKamd-gcc3.2-ia32-linux/PinPoints/isimpoint";
$tool_arg = "";
$dir_prefix="icc91-opt-ref";
$dir_prefix="icc91-pg-novaluepg-ref";
$dir_prefix="icc91-novaluepg-ref";
$pin_run = 1;
$pin_iter = 1;

$kk = 0; 
while ($kk < 4) {
    $kk = $kk + 1; 
    
    if ($kk == 1) { 
	$exe_base = "_base";
	$exe_tail = ".icc91-opt";
	$dir_prefix="icc91-opt-ref";
    }
    elsif ($kk == 2) { 
	$exe_base = "_base";
	$exe_tail = ".icc91.pg-novaluepg-opt";
	$dir_prefix="icc91-pg-novaluepg-ref";
    }
    elsif ($kk == 3) { 
	$exe_base = "_base";
	$exe_tail = ".icc91.pg-valuepg-opt";
	$dir_prefix="icc91-novaluepg-ref";
    }
    
foreach $bench (@bench_groups)
{

    $bench_dir = "${base_dir}/${bench_dir{$bench}}/run/${input}"; 
    $bin_name = ${bench_bin{$bench}}.${exe_base}.${exe_tail};
    $bin_dir ="${base_dir}/${bench_dir{$bench}}/exe";
    print ( "cd $bench_dir\n"); 
    if (${input} =~ 'ref') { 
	$iter_count = $bench_run_ref{$bench};

	$ii = 0; 
	
	while($ii < $iter_count) {
	    $ii++;
	    $index = ${bench}.${ii};

	    if ($pin_run) { 
		for ($jj = 1; $jj <= $pin_iter; $jj++) { 
		$pinout_dir = "/misc/hyesoon/traces/cpu2000/CINT2000/${bench_dir{$bench}}/${dir_prefix}";
                print ("mkdir ${pinout_dir}/ppdata \n");
		# print ("rm -rf ${pinout_dir}/${bin_name}${ii}.T.0.bb \n");
		print ("cp ${pinout_dir}/${bin_name}${ii}.T.0.bb ${pinout_dir}/ppdata/. \n");
		$bb_data =  "${pinout_dir}/ppdata/${bin_name}${ii}.T.0.bb";
		$pp_out = "${pinout_dir}/ppdata/${bin_name}${ii}-point"; 
		print("simpoint -loadFVFile ${bb_data} -maxK 1 -coveragePct 0.9999 -saveSimpoints ${bb_data}.simpoints -saveSimpointWeights ${bb_data}.weights -saveLabels ${bb_data}.labels > ${bb_data}.simpointout \n\n");
		print ("ppgen.3 ${pp_out}  ${bb_data} ${bb_data}.simpoints.lpt0.9999 ${bb_data}.weights.lpt0.9999 ${bb_data}.labels 0 >& ${bb_data}.ppgenout \n\n");
		

		#     $tool_arg_iter ="-o ${pinout_dir}/${bin_name}${ii} -slice_size 200000000 ";
		 #    print ("${pin_home}/Bin/pin -t ${tool_name} ${tool_arg_iter} -- ${bin_dir}/${bin_name} $bench_run_arg_ref{$index}\n"); 
		}
	    }
	    else { 
		print ("${bin_dir}/$bin_name $bench_run_arg_ref{$index} \n");   
	    }
	    
	}
    }
    print ("cd ${base_2000} \n");
}
}

