#!/usr/bin/perl

#=====================================================================
# Author: Francis Tseng <tsengf@hps.utexas.edu>
# Expanded by: Jose A. Joao <joao@hps.utexas.edu>
# Expanded by: Jaekyu Lee <jaekyu.lee@cc.gatech.edu>
# Description:
#=====================================================================

#---------------------------------------------------------------------
# Hosts 
#---------------------------------------------------------------------
my %g_groups = ();
$g_groups{'nehalem1'}   = "watermint gardenmint peppermint spearmint";
$g_groups{'intel'}      = "newyorkaster frostaster heathaster";
$g_groups{'harpertown'} = "waterlily zinnia triumphtulip himalayatulip parrottulip safaritulip";
$g_groups{'clovertown'} = "wildrose blanketflower bearflower indianblanket.cc.gatech.edu icelandpoppy.cc.gatech.edu americandaisy";
$g_groups{'desktop'}    = "primrose tearose mysticrose rosemallow";
$g_groups{'old'}        = "redflax";

$g_groups{'all'}        = "$g_groups{'nehalem1'} $g_groups{'intel'} $g_groups{'harpertown'} $g_groups{'clovertown'} $g_groups{'desktop'} $g_groups{'old'}";


#---------------------------------------------------------------------
# Variables
#---------------------------------------------------------------------

my $g_cmd      = "";
my $g_login    = $ENV{'USER'};
my $g_machines = "";
my $g_verbose  = 0;
my $g_log      = 0;
my $g_sleep    = 0;
my $g_copy     = 0;
my $g_uname    = 0;


#---------------------------------------------------------------------
# Program usage
#---------------------------------------------------------------------

sub usage {
  print "Usage: $0 [-l <login>] [-copy] [-v] [-log] [-s <delay>] [-m <machine or group>] [-m ... ] <command>\n";
  print "  WARNING: use with extreme care, especially with \"-l root\"!!!\n\n";
  print "  Options:\n";
  print "   -l <login>: login as user <login> (default=your user)\n";
  print "   -v: verbose (print each command)\n";
  print "   -log: create a local log with the output of each connection (very useful!, doesn't work with -x)\n";
  print "   -s <delay>: sleep for <delay> between opening connections\n";
  print "   -m <machine or group>: run the command for this machine or group (default=all)\n";
  print "   -u: printout hostname\n";
  print "\n";
  foreach (sort keys %g_groups) {
    printf "%-10s -- $g_groups{$_}\n", $_;
  }
  exit;
}


#---------------------------------------------------------------------
# Parse Options
#---------------------------------------------------------------------

&usage if !@ARGV;

while (@ARGV) {
  $option = shift;
  if ($option eq "-l") {
    $g_login = shift;
  } elsif ($option eq "-v") {
    $g_verbose = 1;
  } elsif ($option eq "-log") {
    $g_log = 1;
  } elsif ($option eq "-s") {
    $g_sleep = shift;
  } elsif ($option eq "-u") {
    $g_uname = 1;
  } elsif ($option eq "-m") {
    $option = shift;
    foreach (split / /, $option) {
      if ($g_groups{$_}) {
        $g_machines .= "$g_groups{$_} ";
      } else {
        $g_machines .= "$_ ";
      }
    }
  } else {
    $g_cmd .= "$option ";
  }
}

if ($g_cmd eq "") {
  print "No command specified.\n";
  usage();
}

if ($g_machines eq "") {
  print "No machines specified.  Assuming all...\n";
  $g_machines = $g_groups{'all'};
}


#---------------------------------------------------------------------
# Execution Routine
#---------------------------------------------------------------------

foreach (split(/\s+/, $g_machines)) {
  my $host = "";
  if (/cc.gatech.edu/) {
    $host = $_;
  } else {
    $host = $_ . ".cc.gt.atl.ga.us";
  }

  $ping = `ping -c 1 -w 1 $host \| grep received \| awk {'print \$4'}`;
  if ($ping == "1") {
# You need to generate rsa key if you don't want to type password every time.
    #system "ssh-keygen -t rsa";
    #system "ssh root\@$host mkdir -p .ssh";
    #system "cat ~/.ssh/id_rsa.pub \| ssh root\@$host 'cat >> .ssh/authorized_keys2'";
    #system "ssh $host -l root ls";

    if ($g_uname == 1) {
      print "host: $host\n";
    }

    $command = "ssh $host -l $g_login ";
    $command = $command . "-v " if $g_verbose == 1;
    $command = $command . "2>&1 > $_.log " if $g_log == 1;
    $command = $command . $g_cmd;
    
    system "$command";
    sleep $g_sleep;
  } else {
    print "$host down\n";
  }
}


