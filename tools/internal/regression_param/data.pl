#!/usr/bin/perl





$INST_COUNT{'perlbench@ref'} = "10000";
$INST_COUNT{'bzip2@ref'} = "10000";
$INST_COUNT{'gcc@ref'} = "10000";
$INST_COUNT{'mcf@ref'} = "10000";
$INST_COUNT{'gobmk@ref'} = "10000";
$INST_COUNT{'hmmer@ref'} = "10000";
$INST_COUNT{'sjeng@ref'} = "10000";
$INST_COUNT{'libquantum@ref'} = "10000";
$INST_COUNT{'h264ref@ref'} = "10000";
$INST_COUNT{'omnetpp@ref'} = "10000";
$INST_COUNT{'astar@ref'} = "10000";
$INST_COUNT{'xalancbmk@ref'} = "10000";
$INST_COUNT{'bwaves@ref'} = "10000";
$INST_COUNT{'gamess@ref'} = "10000";
$INST_COUNT{'milc@ref'} = "10000";
$INST_COUNT{'zeusmp@ref'} = "10000";
$INST_COUNT{'gromacs@ref'} = "10000";
$INST_COUNT{'cactusADM@ref'} = "10000";
$INST_COUNT{'leslie3d@ref'} = "10000";
$INST_COUNT{'namd@ref'} = "10000";
$INST_COUNT{'dealII@ref'} = "10000";
$INST_COUNT{'soplex@ref'} = "10000";
$INST_COUNT{'povray@ref'} = "10000";
$INST_COUNT{'calculix@ref'} = "10000";
$INST_COUNT{'GemsFDTD@ref'} = "10000";
$INST_COUNT{'tonto@ref'} = "10000";
$INST_COUNT{'lbm@ref'} = "10000";
$INST_COUNT{'wrf@ref'} = "10000";
$INST_COUNT{'sphinx3@ref'} = "10000";
$INST_COUNT{'binomialoptions@ref'} = "100";
$INST_COUNT{'blackscholes@ref'} = "100";
$INST_COUNT{'convolutionseparable@ref'} = "100";
$INST_COUNT{'dwthaar1d@ref'} = "100";
$INST_COUNT{'dxtc@ref'} = "100";
$INST_COUNT{'matrixmul@ref'} = "100";
$INST_COUNT{'mersennetwister_boxmuller@ref'} = "100";
$INST_COUNT{'montecarlo@ref'} = "100";
$INST_COUNT{'nbody@ref'} = "100";
$INST_COUNT{'postprocessgl@ref'} = "100";
$INST_COUNT{'quasirandomgenerator_kernel@ref'} = "100";
$INST_COUNT{'scalarprod@ref'} = "100";
$INST_COUNT{'simplegl@ref'} = "100";
$INST_COUNT{'eigenvalues_multinterval@ref'} = "100";
$INST_COUNT{'histogram256_kernel@ref'} = "100";
$INST_COUNT{'mandelbrot@ref'} = "100";
$INST_COUNT{'cell_cuda@ref'} = "100";
$INST_COUNT{'needle_gpu@ref'} = "100";
$INST_COUNT{'blackscholes@ref'} = "100";
$INST_COUNT{'mersennetwister_boxmuller@ref'} = "100";
$INST_COUNT{'montecarlo@ref'} = "100";
$INST_COUNT{'scalarprod@ref'} = "100";
$INST_COUNT{'simplegl@ref'} = "100";
$INST_COUNT{'convolutionseparable@ref'} = "100";
$INST_COUNT{'matrixmul@ref'} = "100";
$INST_COUNT{'OceanFFT-2@ref'} = "100";
$INST_COUNT{'mri-fhd-0@ref'} = "100";
$INST_COUNT{'mri-q-0@ref'} = "100";
$INST_COUNT{'pns-0@ref'} = "100";
$INST_COUNT{'backprop-1@ref'} = "100";
$INST_COUNT{'bfs-0@ref'} = "100";
$INST_COUNT{'cfd-0@ref'} = "100";
$INST_COUNT{'streamcluster-0@ref'} = "100";
$INST_COUNT{'gaussian-0@ref'} = "100";









$PARAM{'perlbench@ref'} = "params_x86";
$PARAM{'bzip2@ref'} = "params_x86";
$PARAM{'gcc@ref'} = "params_x86";
$PARAM{'mcf@ref'} = "params_x86";
$PARAM{'gobmk@ref'} = "params_x86";
$PARAM{'hmmer@ref'} = "params_x86";
$PARAM{'sjeng@ref'} = "params_x86";
$PARAM{'libquantum@ref'} = "params_x86";
$PARAM{'h264ref@ref'} = "params_x86";
$PARAM{'omnetpp@ref'} = "params_x86";
$PARAM{'astar@ref'} = "params_x86";
$PARAM{'xalancbmk@ref'} = "params_x86";
$PARAM{'bwaves@ref'} = "params_x86";
$PARAM{'gamess@ref'} = "params_x86";
$PARAM{'milc@ref'} = "params_x86";
$PARAM{'zeusmp@ref'} = "params_x86";
$PARAM{'gromacs@ref'} = "params_x86";
$PARAM{'cactusADM@ref'} = "params_x86";
$PARAM{'leslie3d@ref'} = "params_x86";
$PARAM{'namd@ref'} = "params_x86";
$PARAM{'dealII@ref'} = "params_x86";
$PARAM{'soplex@ref'} = "params_x86";
$PARAM{'povray@ref'} = "params_x86";
$PARAM{'calculix@ref'} = "params_x86";
$PARAM{'GemsFDTD@ref'} = "params_x86";
$PARAM{'tonto@ref'} = "params_x86";
$PARAM{'lbm@ref'} = "params_x86";
$PARAM{'wrf@ref'} = "params_x86";
$PARAM{'sphinx3@ref'} = "params_x86";
$PARAM{'binomialoptions@ref'} = "params_8800gt";
$PARAM{'blackscholes@ref'} = "params_8800gt";
$PARAM{'convolutionseparable@ref'} = "params_8800gt";
$PARAM{'dwthaar1d@ref'} = "params_8800gt";
$PARAM{'dxtc@ref'} = "params_8800gt";
$PARAM{'matrixmul@ref'} = "params_8800gt";
$PARAM{'mersennetwister_boxmuller@ref'} = "params_8800gt";
$PARAM{'montecarlo@ref'} = "params_8800gt";
$PARAM{'nbody@ref'} = "params_8800gt";
$PARAM{'postprocessgl@ref'} = "params_8800gt";
$PARAM{'quasirandomgenerator_kernel@ref'} = "params_8800gt";
$PARAM{'scalarprod@ref'} = "params_8800gt";
$PARAM{'simplegl@ref'} = "params_8800gt";
$PARAM{'eigenvalues_multinterval@ref'} = "params_8800gt";
$PARAM{'histogram256_kernel@ref'} = "params_8800gt";
$PARAM{'mandelbrot@ref'} = "params_8800gt";
$PARAM{'cell_cuda@ref'} = "params_8800gt";
$PARAM{'needle_gpu@ref'} = "params_8800gt";
$PARAM{'blackscholes@ref'} = "params_8800gt";
$PARAM{'mersennetwister_boxmuller@ref'} = "params_8800gt";
$PARAM{'montecarlo@ref'} = "params_8800gt";
$PARAM{'scalarprod@ref'} = "params_8800gt";
$PARAM{'simplegl@ref'} = "params_8800gt";
$PARAM{'convolutionseparable@ref'} = "params_8800gt";
$PARAM{'matrixmul@ref'} = "params_8800gt";
$PARAM{'OceanFFT-2@ref'} = "params_8800gt";
$PARAM{'mri-fhd-0@ref'} = "params_8800gt";
$PARAM{'mri-q-0@ref'} = "params_8800gt";
$PARAM{'pns-0@ref'} = "params_8800gt";
$PARAM{'backprop-1@ref'} = "params_8800gt";
$PARAM{'bfs-0@ref'} = "params_8800gt";
$PARAM{'cfd-0@ref'} = "params_8800gt";
$PARAM{'streamcluster-0@ref'} = "params_8800gt";
$PARAM{'gaussian-0@ref'} = "params_8800gt";
$PARAM{'pns@ref'} = "params_8800gt";
$PARAM{'sad-0@ref'} = "params_8800gt";
$PARAM{'streamcluster@ref'} = "params_8800gt";
$PARAM{'leukocyte-0@ref'} = "params_8800gt";
$PARAM{'leukocyte-1@ref'} = "params_8800gt";
$PARAM{'cell-0@ref'} = "params_8800gt";



$TOO_LONG_LIST{'blackscholes@ref'} = "1";
$TOO_LONG_LIST{'binomialoptions@ref'} = "1";
$TOO_LONG_LIST{'dwthaar1d@ref'} = "1";
$TOO_LONG_LIST{'mersennetwister_boxmuller@ref'} = "1";
$TOO_LONG_LIST{'montecarlo@ref'} = "1";
$TOO_LONG_LIST{'postprocessgl@ref'} = "1";
$TOO_LONG_LIST{'quasirandomgenerator_kernel@ref'} = "1";
$TOO_LONG_LIST{'simplegl@ref'} = "1";
$TOO_LONG_LIST{'eigenvalues_multinterval@ref'} = "1";
$TOO_LONG_LIST{'histogram256_kernel@ref'} = "1";
$TOO_LONG_LIST{'mandelbrot@ref'} = "1";
$TOO_LONG_LIST{'cell_cuda@ref'} = "1";
#$TOO_LONG_LIST{'scalarprod@ref'} = "1";
$TOO_LONG_LIST{'matrixmul@ref'} = "1";
$TOO_LONG_LIST{'OceanFFT-2@ref'} = "1";
$TOO_LONG_LIST{'pns-0@ref'} = "1";
$TOO_LONG_LIST{'backprop-1@ref'} = "1";
$TOO_LONG_LIST{'bfs-0@ref'} = "1";
$TOO_LONG_LIST{'cfd-0@ref'} = "1";
$TOO_LONG_LIST{'streamcluster-0@ref'} = "1";
$TOO_LONG_LIST{'gaussian-0@ref'} = "1";


return true;
