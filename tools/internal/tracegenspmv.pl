#!/usr/bin/perl -w

@matrix_list = (
	'cantilever',
	'accelerator',
	'economics',
	'dense',
	'epidemiology',
	'protein',
	'qcd',
	'tunnel',
	'lp',
	'harbor',
	'circuit',
	'ship',
	'sphere',
	'web',
);

%matrix_input = (
	"cantilever" => "cant.rsa",
	"accelerator" => "cop20k_A.rsa",
	"economics" => "mac_econ_fwd500.rua",
	"dense" => "dense2.pua",
	"epidemiology" => "mc2depi.rua",
	"protein" => "pdb1HYS.rsa",
	"qcd" => "qcd5_4.pua",
	"tunnel" => "pwtk.rsa",
	"lp" => "rail4284.pua",
	"harbor" => "rma10.pua",
	"circuit" => "scircuit.rua",
	"ship" => "shipsec1.rsa",
	"sphere" => "consph.rsa",
	"web" => "webbase-1M.rua",
);

%vdist = (
	"cantilever" => "832",
	"accelerator" => "960",
	"economics" => "896",
	"dense" => "960",
	"epidemiology" => "832",
	"protein" => "832",
	"qcd" => "832",
	"tunnel" => "832",
	"lp" => "832",
	"harbor" => "640",
	"circuit" => "832",
	"ship" => "832",
	"sphere" => "832",
	"web" => "704",
);

%cdist = (
	"cantilever" => "256",
	"accelerator" => "64",
	"economics" => "192",
	"dense" => "0",
	"epidemiology" => "192",
	"protein" => "256",
	"qcd" => "256",
	"tunnel" => "256",
	"lp" => "0",
	"harbor" => "192",
	"circuit" => "128",
	"ship" => "256",
	"sphere" => "256",
	"web" => "128",
);

$pintool = "/hparch/benchmarks/pin-2.6/source/tools/trace_generator_new/obj-intel64/trace_generator.so";
$res_dir = "/storage/traces/spmv_new";
$num_th = "1";
$bin = "/home/kacear/PREF_Kernels/prefetchXblikle/pthreads/run.hparch";
$dir = "/home/kacear/PREF_Kernels/prefetchXblikle/pthreads";

foreach $mat (@matrix_list)
{
	$in = "${dir}/${matrix_input{$mat}}";
	$v  = "${vdist{$mat}}";
	$c  = "${cdist{$mat}}";

	# Original
	$name = "${res_dir}/${mat}";
	$pin_arg = "-interval 1 -vdist 0 -cdist 0 -xdist 0 -dump 1 -dump_file ${name}.dump -tracename ${name}";
	open  (OUTFILE, "> ./scripts/${mat}.tgen.pl");
	print (OUTFILE "#!/usr/bin/perl\n\n");
	print (OUTFILE "system \"pin -t ${pintool} ${pin_arg} -- ${bin} ${in} ${num_th} 1\"\;\n\n");
	close OUTFILE;

	system "chmod +x ./scripts/${mat}.tgen.pl";
	system "echo \"perl ${dir}/scripts/${mat}.tgen.pl\" | qsub\n";

	
	
	# Prefetch C V
	$name = "${res_dir}/${mat}_pref";
	$pin_arg = "-interval 1 -vdist ${v} -cdist ${c} -xdist 0 -dump 1 -dump_file ${name}.dump -tracename ${name}";
	open  (OUTFILE1, "> ./scripts/${mat}_pref.tgen.pl");
	print (OUTFILE1 "#!/usr/bin/perl\n\n");
	print (OUTFILE1 "system \"pin -t ${pintool} ${pin_arg} -- ${bin} ${in} ${num_th} 1\"\;\n\n");
	close OUTFILE1;
	
	system "chmod +x ./scripts/${mat}_pref.tgen.pl";
	system "echo \"perl ${dir}/scripts/${mat}_pref.tgen.pl\" | qsub\n";



	# Prefetch C V X
	$name = "${res_dir}/${mat}_pref_x";
	$pin_arg = "-interval 1 -vdist ${v} -cdist ${c} -xdist 32 -dump 1 -dump_file ${name}.dump -tracename ${name}";
	open  (OUTFILE2, "> ./scripts/${mat}_pref_x.tgen.pl");
	print (OUTFILE2 "#!/usr/bin/perl\n\n");
	print (OUTFILE2 "system \"pin -t ${pintool} ${pin_arg} -- ${bin} ${in} ${num_th} 1\"\;\n\n");
	close OUTFILE2;
	
	system "chmod +x ./scripts/${mat}_pref_x.tgen.pl";
	system "echo \"perl ${dir}/scripts/${mat}_pref_x.tgen.pl\" | qsub\n";
	

}
