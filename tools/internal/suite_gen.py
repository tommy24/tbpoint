#!/usr/bin/python


import bench_common
import sys


sys.stdout.write('SUITES[\'gpu_gpu\'] = [')
count = 0


## If you want to use two different suite
## comment out next 4 lines
## remove 2 commented line
for ii in range(0, len(bench_common.SUITES['cuda_all'])):
  for jj in range(ii+1, len(bench_common.SUITES['cuda_all'])):
    bench1 = bench_common.SUITES['cuda_all'][ii]
    bench2 = bench_common.SUITES['cuda_all'][jj]

#for bench1 in bench_common.SUITES['cuda_mem']:
#  for bench2 in bench_common.SUITES['cuda_mem']:
    sys.stdout.write('\'%s_%s@ref\', ' % (bench1[:bench1.find('@')], bench2[:bench2.find('@')]))
    count += 1
sys.stdout.write(']\n')

print('total %d' % count)


