#!/usr/bin/perl
@bench_groups  =   (
	 'gzip',
                    'vpr',
                    'gcc',
                    'mcf',
                    'crafty',
                    'parser',
                    'eon',
                    'perlbmk',
                    'gap',
                    'vortex',
                    'bzip2',
                    'twolf');


%bench_dir = ("gzip" => "164.gzip",
		  "vpr" =>  "175.vpr",
		  "gcc" => "176.gcc",
		  "mcf" => "181.mcf",
		  "crafty" => "186.crafty",
		  "parser" => "197.parser",
		  "eon" => "252.eon",
		  "perlbmk" => "253.perlbmk",
		  "gap" => "254.gap",
		  "vortex" => "255.vortex",
		  "bzip2" => "256.bzip2",
		  "twolf" => "300.twolf"); 



%bench_bin = ("gzip" => "gzip",
		  "vpr" =>  "vpr",
		  "gcc" => "cc1",
		  "mcf" => "mcf",
		  "crafty" => "crafty",
		  "parser" => "parser",
		  "eon" => "eon",
		  "perlbmk" => "perlbmk",
		  "gap" => "gap",
		  "vortex" => "vortex",
		  "bzip2" => "bzip2",
		  "twolf" => "twolf"); 
	      

%bench_run_ref = ("gzip" => "5",
		  "vpr" =>  "2",
		  "gcc" => "5",
		  "mcf" => "1",
		  "crafty" => "1",
		  "parser" => "1",
		  "eon" => "3",
		  "perlbmk" => "7",
		  "gap" => "1",
		  "vortex" => "3",
		  "bzip2" => "3",
		  "twolf" => "1"); 


%bench_run_arg_ref = ("gzip1" => "input.source 60 > input.source.out", 
		      "gzip2" => "input.log 60 > input.log.out",
		      "gzip3" => "input.graphic 60 > input.graphic.out", 
		      "gzip4" => "input.random 60 > input.random.out",
		      "gzip5" => "input.program 60 > input.program.out", 
		      "vpr1" => "net.in arch.in place.out dum.out -nodisp -place_only -init_t 5 -exit_t 0.005 -alpha_t 0.9412 -inner_num 2 > place_log.out",
		      "vpr2" => "net.in arch.in place.in route.out -nodisp -route_only -route_chan_width 15 -pres_fac_mult 2 -acc_fac 1 -first_iter_pres_fac 4 -initial_pres_fac 8 > route_log.out",
		      "gcc1" => "166.i -o 166.s > 166.out", 
		      "gcc2" => "200.i -o 200.s > 200.out",
		      "gcc3" => "expr.i -o expr.s > expr.out",
		      "gcc4" => "integrate.i -o integrate.s > integrate.out",
		      "gcc5" => "scilab.i -o scilab.s > scilab.out",
		      "mcf1" => "inp.in > inp.out",
		      "crafty1" => "< crafty.in > crafty.out",
		      "parser1" => "2.1.dict -batch < ref.in > ref.out" ,
		      "eon1" => "chair.control.cook chair.camera chair.surfaces chair.cook.ppm ppm pixels_out.cook > cook_log.out",
		      "eon2" => "chair.control.rushmeier chair.camera chair.surfaces chair.rushmeier.ppm ppm pixels_out.rushmeier > rushmeier_log.out", 
		      "eon3" => "chair.control.kajiya chair.camera chair.surfaces chair.kajiya.ppm ppm pixels_out.kajiya > kajiya_log.out", 
		      "perlbmk1" => "-I./lib diffmail.pl 2 550 15 24 23 100 > 2.550.15.24.23.100.out",
		      "perlbmk2" => "-I. -I./lib makerand.pl > makerand.out",
		      "perlbmk3" => "-I./lib perfect.pl b 3 m 4 > b.3.m.4.out",
		      "perlbmk4" => "-I./lib splitmail.pl 850 5 19 18 1500 > 850.5.19.18.1500.out",
		      "perlbmk5" => "-I./lib splitmail.pl 704 12 26 16 836 > 704.12.26.16.836.out",
		      "perlbmk6" => "-I./lib splitmail.pl 535 13 25 24 1091 > 535.13.25.24.1091.out",
		      "perlbmk7" => "-I./lib splitmail.pl 957 12 23 26 1014 > 957.12.23.26.1014.out",
		      "gap1" => "-l ./ -q -m 192M < ref.in > ref.out",
		      "vortex1" => "lendian1.raw > vortex1.out2",
		      "vortex2" => "lendian2.raw > vortex2.out2",
		      "vortex3" => "lendian3.raw > vortex3.out2",
		      "bzip21" => "input.source 58 > input.source.out",
		      "bzip22" => "input.graphic 58 > input.graphic.out",
		      "bzip23" => "input.program 58 > input.program.out",
		      "twolf1" => "ref > ref.stdout"
		      );


$base_2000 = "/site/spt/usr5/hkim11/cpu2000.1.3.ipf"; 
$base_2000 = "/proj/vssad2/users/hkim11/cpu2000.1.3/cpu2000.1.3.x86"; 
$base_2000 = "/net/bebe/shared/spec1.2";
$base_2000 = "/net/pool57/shared/hyesoon/spec2000";
$base_dir = "${base_2000}/benchspec/CINT2000"; 
$input = "ref";
$exe_base = "_x86_base";
$exe_base = "_base";
$exe_tail =".cpu2000.itanium.linux.pin44.pronto";
$exe_tail =".cpu2000.itanium.linux.pin14.pronto";
$exe_tail =".linux-ipf-opt";
$exe_tail =".cpu2000.itanium.linux.pin.noif.pronto";
$exe_tail =".ipf_maxif";
$exe_tail =".linux-ipf-opt";
$exe_tail =".linux-ia32-opt";
$exe_tail =".linux-ia32-nopgo_opt";
$exe_tail =".linux.em64t_nopgo.opt";
$exe_tail = ".opt";
$exe_tail = ".icc91.pg-opt";
$exe_tail = ".icc91.pg-valuepg-opt";
$exe_tail = ".icc91-opt";
$vtl_name = "/opt/intel/vtune/bin/vtl";
$vtl_run = 1;
$vtl_iter = 2;
foreach $bench (@bench_groups)
{

    $bench_dir = "${base_dir}/${bench_dir{$bench}}/run/${input}"; 
    $bin_name = ${bench_bin{$bench}}.${exe_base}.${exe_tail};
    $bin_dir ="${base_dir}/${bench_dir{$bench}}/exe";
    print ("cd $bench_dir\n"); 
    if (${input} =~ 'ref') { 
	$iter_count = $bench_run_ref{$bench};

	$ii = 0; 
	
	while($ii < $iter_count) {
	    $ii++;
	    $index = ${bench}.${ii};

	    if ($vtl_run) { 
		$vtl_data_name = "${base_2000}/vtl_data/${index}.data";
		print ("rm ${vtl_data_name}\n");
		for ($jj = 1; $jj <= $vtl_iter; $jj++) { 
		    # print ("${bin_dir}/$bin_name $bench_run_arg_ref{$index} \n");   
                  $vtl_result_name = "${base_2000}/vtl_data/${bin_name}.${index}.${jj}.res";
		   print ("${vtl_name} activity -c sampling -o \"-cal no\" -of ${base_2000}/vtlsetting.${jj} -app ${bin_dir}/${bin_name},\"$bench_run_arg_ref{$index}\" -d 0 run \n");
		   # print ("${vtl_name} activity -c sampling -o \"-cal no\" -of ${base_2000}/vtlsetting${jj} -app ${bin_dir}/${bin_name},\"$bench_run_arg_ref{$index}\" -d 0 run \n");
		   #print ("${vtl_name} activity -c sampling -o \"-cal no\" -of ~/cpu2000.1.3.ipf/vtlsetting${jj} -app ${bin_dir}/${bin_name},\"$bench_run_arg_ref{$index}\" run \n");
		   print ("/opt/intel/vtune/bin/vtl view > $vtl_result_name \n"); 
if ($jj == 5) { 
	           print ("cat   $vtl_result_name |  awk '/${bin_name}/{if(\$1 == \"${bin_name}\") {print \$3; print \$4;  }}' >> $vtl_data_name \n");
 }
else { 
	           print ("cat   $vtl_result_name |  awk '/${bin_name}/{if(\$1 == \"${bin_name}\") {print \$3; print \$4;  print \$5; print \$6  }}' >> $vtl_data_name \n");

} 
#	           print ("cat   $vtl_result_name |  awk '/${bin_name}/{if(\$1 == \"${bin_name}\") {print \$3; print \$4; print \$5; print \$6  }}' >> $vtl_data_name \n");
	           #print ("cat   $vtl_result_name |  awk '/${bin_name}/{if(\$1 == \$2){print \$3; print \$4; print \$5; print \$6  }}' >> $vtl_data_name \n");
		   # print ("/opt/intel/vtune/bin/vtl view |  awk '/${bin_name}/{if(\$1 == \$2){print \$3; print \$4; print \$5; print \$6  }}' >> $vtl_data_name \n");
		    # /opt/intel/vtune/bin/vtl activity -c sampling -o "-cal no" -of ~/cpu2000.1.3.ipf/vtlsetting1 -app ./gzip,"input.combined 32" run
		}
		
	    }
	    else { 
		print ("${bin_dir}/$bin_name $bench_run_arg_ref{$index} \n");   
	    }
	    
     }
    }
    print ("cd ${base_2000} \n");
}
print ("./get_vtldata.pl > ${bin_name}.dat\n");


%bench_run_arg_ref_err = ("gzip1" => "input.source 60 > input.source.out 2> input.source.err", 
		      "gzip2" => "input.log 60 > input.log.out 2> input.log.err",
		      "gzip3" => "input.graphic 60 > input.graphic.out 2> input.graphic.err", 
		      "gzip4" => "input.random 60 > input.random.out 2> input.random.err",
		      "gzip5" => "input.program 60 > input.program.out 2> input.program.err", 
		      "vpr1" => "net.in arch.in place.out dum.out -nodisp -place_only -init_t 5 -exit_t 0.005 -alpha_t 0.9412 -inner_num 2 > place_log.out 2> place_log.err",
		      "vpr2" => "net.in arch.in place.in route.out -nodisp -route_only -route_chan_width 15 -pres_fac_mult 2 -acc_fac 1 -first_iter_pres_fac 4 -initial_pres_fac 8 > route_log.out 2> route_log.err",
		      "gcc1" => "166.i -o 166.s > 166.out 2> 166.err", 
		      "gcc2" => "200.i -o 200.s > 200.out 2> 200.err",
		      "gcc3" => "expr.i -o expr.s > expr.out 2> expr.err",
		      "gcc4" => "integrate.i -o integrate.s > integrate.out 2> integrate.err",
		      "gcc5" => "scilab.i -o scilab.s > scilab.out 2> scilab.err",
		      "mcf1" => "inp.in > inp.out 2> inp.err",
		      "crafty1" => "< crafty.in > crafty.out 2> crafty.err",
		      "parser1" => "2.1.dict -batch < ref.in > ref.out 2> ref.err",
		      "eon1" => "chair.control.cook chair.camera chair.surfaces chair.cook.ppm ppm pixels_out.cook > cook_log.out 2> cook_log.err",
		      "eon2" => "chair.control.rushmeier chair.camera chair.surfaces chair.rushmeier.ppm ppm pixels_out.rushmeier > rushmeier_log.out 2> rushmeier_log.err", 
		      "eon3" => "chair.control.kajiya chair.camera chair.surfaces chair.kajiya.ppm ppm pixels_out.kajiya > kajiya_log.out 2> kajiya_log.err", 
		      "perlbmk1" => "-I./lib diffmail.pl 2 550 15 24 23 100 > 2.550.15.24.23.100.out 2> 2.550.15.24.23.100.err
",
		      "perlbmk2" => "-I. -I./lib makerand.pl > makerand.out 2> makerand.err",
		      "perlbmk3" => "-I./lib perfect.pl b 3 m 4 > b.3.m.4.out 2> b.3.m.4.err",
		      "perlbmk4" => "-I./lib splitmail.pl 850 5 19 18 1500 > 850.5.19.18.1500.out 2> 850.5.19.18.1500.err",
		      "perlbmk5" => "-I./lib splitmail.pl 704 12 26 16 836 > 704.12.26.16.836.out 2> 704.12.26.16.836.err",
		      "perlbmk6" => "-I./lib splitmail.pl 535 13 25 24 1091 > 535.13.25.24.1091.out 2> 535.13.25.24.1091.err",
		      "perlbmk7" => "-I./lib splitmail.pl 957 12 23 26 1014 > 957.12.23.26.1014.out 2> 957.12.23.26.1014.err
",
		      "gap1" => "-l ./ -q -m 192M < ref.in > ref.out 2> ref.err",
		      "vortex1" => "lendian1.raw > vortex1.out2 2> vortex1.err",
		      "vortex2" => "lendian2.raw > vortex2.out2 2> vortex2.err",
		      "vortex3" => "lendian3.raw > vortex3.out2 2> vortex3.err",
		      "bzip21" => "input.source 58 > input.source.out 2> input.source.err",
		      "bzip22" => "input.graphic 58 > input.graphic.out 2> input.graphic.err",
		      "bzip23" => "input.program 58 > input.program.out 2> input.program.err",
		      "twolf1" => "ref > ref.stdout 2> ref.err"
		      );
