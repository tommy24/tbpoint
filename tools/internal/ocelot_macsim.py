#!/usr/bin/python


###############################################################################################
# \file   ocelot_macsim.py
# \brief  Execute MacSim with the specified benchmark.
# \author Jaekyu Lee (kacear@gmail.com)
# \date   March 23, 2011
# \todo   Support spec traces
#
# This script will run the MacSim with the specified benchmark. First, check the existence of
# specified benchmark. If not exist, generate a new trace. Then, execute this.
###############################################################################################


### Import Modules
import os
import sys
import getopt
from shutil import copyfile
### Import Modules


### Benchmark list
SUITE_CUDA2_2 = [
  'BlackScholes',           
  'Mandelbrot',             
  'MersenneTwister',
  'MonteCarlo',
  'MonteCarloMultiGPU',
  'SobelFilter',
  'SobolQRNG',
  'AlignedTypes',
  'AsyncAPI',
  'BandwidthTest',
  'BicubicTexture',
  'BinomialOptions',
  'Bitonic',
  'BoxFilter',
  'Clock',
  'ConvolutionFFT2D',
  'ConvolutionSeparable',
  'ConvolutionTexture',
  'CppIntegration',
  'Dct8x8',
  'DeviceQuery',
  'DwtHaar1D',
  'Dxtc',
  'Eigenvalues',
  'FastWalshTransform',
  'FluidsGL',
  'Histogram256',
  'Histogram64',
  'ImageDenoising',
  'MatrixMul',
  'Nbody',
  'OceanFFT',
  'Particles',
  'PostProcessGL',
  'QuasirandomGenerator',
  'RecursiveGaussian',
  'Reduction',
  'ScalarProd',
  'Scan',
  'ScanLargeArray',
  'SimpleAtomicIntrinsics',
  'SimpleCUBLAS',
  'SimpleCUFFT',
  'SimpleGL',
  'SimpleMultiGPU',
  'SimpleStreams',
  'SimpleTemplates',
  'SimpleTexture',
  'SimpleTexture3D',
  'SimpleVoteIntrinsics',
  'SimpleZeroCopy',
#  'SimplerCUFFT',
  'Template',
  'ThreadFenceReduction',
  'Transpose',
  'TransposeNew',
  'VolumeRender',
]
### Benchmark list


### Help message
def help():
  print "[-b, --bench]  benchmark to execute"
  print "[-e, --exe]    macsim binary path"
  print "[-p, --param]  macsim params.in path"
  print "[-m, --macsim] macsim commandlines (start with \" and end with \")"
  print "[-l, --list]   list of benchmarks"
  print "[-h, --help]   help message"
  sys.exit(0)
### Help message


### Global variables
bench_list  = []
binary_path = ''
param_path  = ''
macsim_arg  = ''
print_list  = False
### Global variables


### Argument Parsing
argv = sys.argv
opts, args = getopt.getopt(sys.argv[1:], "b:e:p:m:hl", ["bench=", "exe=", "param=", "macsim=", "help", "list"])

for opt, arg in opts:
  if opt in ('-b', '--bench'):
    bench_list.append(arg)
  elif opt in ('-e', '--exe'):
    binary_path = arg
  elif opt in ('-p', '--param'):
    param_path = arg
  elif opt in ('-m', '--macsim'):
    macsim_arg = arg
  elif opt in ('-l', '--list'):
    print_list = True
  elif opt in ('-h', '--help'):
    help()
### Argument Parsing


### Path information
TRACE_PATH     = os.environ["TRACE_PATH"]
GPUOCELOT_PATH = '/home/jaekyu/sim/gpuocelot/tests'
SIMDIR_PATH    = os.environ["SIMDIR"]
TRACEGEN_PATH  = '%s/tools/gpuocelot_tracegen.py' % (SIMDIR_PATH)
CURRENT_PATH   = os.getcwd()
### Path information


### Process all benchmarks (parse sub directory)
SUITE = {}
for bench in SUITE_CUDA2_2:
  SUITE[bench] = 'ptx/cuda2.2'
### Process all benchmarks (parse sub directory)


if print_list == True:
  for key, value in SUITE.iteritems():
    print "%-30s %s" % (key, value)
  sys.exit(0)


### Main loop
for bench in bench_list:
  ### Check the existence of traces
  bench_trace_path = '%s/%s/%s/kernel_config.txt' % (TRACE_PATH, SUITE[bench], bench)
  if os.path.exists(bench_trace_path) == False:
    ### Call gpuocelot to generate trace
    os.system("%s -b %s" % (TRACEGEN_PATH, bench)) 
    ### Call gpuocelot to generate trace


  ### Copy Binary
  if binary_path != '':
    if os.path.exists(binary_path) == False:
      print "binary %s does not exist" % (binary_path)
    else:
      copyfile(binary_path, '%s/macsim' % (CURRENT_PATH));
#    os.chmod('%s/macsim' % (CURRENT_PATH), '+x');
    os.system("chmod +x %s/macsim" % (CURRENT_PATH));


  ### Make trace_file_list
  if os.path.exists('%s/trace_file_list' % (CURRENT_PATH)) == True:
    os.remove('%s/trace_file_list' % (CURRENT_PATH));
  file = open('%s/trace_file_list' % (CURRENT_PATH), 'w');
  file.write('1\n');
  file.write(bench_trace_path);
  file.write('\n');
  file.close();
  

  ### Copy params.in file
  if param_path != '':
    if os.path.exists(param_path) == False:
      print "parameter file %s does not exist" % (param_path)
    else:
      copyfile(param_path, '%s/params.in' % (CURRENT_PATH));

  ### Run MacSim
  print "macsim %s" % (macsim_arg)
  os.system("./macsim %s" % (macsim_arg))
  ### Run MacSim
### Main loop
