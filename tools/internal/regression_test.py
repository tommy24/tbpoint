#!/usr/bin/python
#########################################################################################
# Author : Jaekyu Lee (kacear@gmail.com)
# Date : 7/14/2011
# Description
#   regression test
#########################################################################################


import optparse
import sys
import os.path
import os
import bench_common
import trace_common
import difflib
import glob
import re
from collections import defaultdict
import datetime


params_list = ['x86_c1_t1', 'x86_c1_t2', 'x86_c2_t1', 'ptx_g80_6core', 'ptx_fermi_6core', 'hetero_cpu1_gpu2']
  
regression_suite = defaultdict(list)
regression_suite['x86_c1_t1']        = bench_common.SUITES['regression_spec06']
regression_suite['x86_c1_t2']        = bench_common.SUITES['regression_spec06_2app']
regression_suite['x86_c2_t1']        = bench_common.SUITES['regression_spec06_2app']
regression_suite['ptx_g80_6core']    = bench_common.SUITES['regression_ptx']
regression_suite['ptx_fermi_6core']  = bench_common.SUITES['regression_ptx']
regression_suite['hetero_cpu1_gpu2'] = bench_common.SUITES['regression_hetero']


def process_options():
  parser = optparse.OptionParser(usage='usage: %prog [options]', add_help_option=True)
  parser.add_option('--bin', action='store', type='string', dest='bin', default='', help='input file name')
  parser.add_option('--disable-email', action='store_false', dest='disable_email', help='send an email after the regression test')
  parser.add_option('--show-diff', action='store_true', dest='show_diff', help='show the detail of differences')

  return parser


def main(argv):
  parser = process_options()
  options, args = parser.parse_args(argv)

  # prepare macsim binary
  bin = ''
  if not options.bin:
    print('warning: binary not specified. Use ./macsim')
    bin = os.getcwd() + '/macsim'
  else:
    bin = options.bin

  if not os.path.exists(bin):
    print('error: binary does not exist')
    exit(0)

  # regression test configuration
  params_path = os.environ['SIMDIR'] + '/tools/macsim_regression/'

  # create test directory
  test_dir = '/tmp/regression_' + str(os.getppid())
  if os.path.exists(test_dir):
    os.system('rm -rf %s' % (test_dir))
  os.system('mkdir -p %s' % (test_dir))
      
  # copy macsim binary
  os.system('cp %s %s/macsim' % (bin, test_dir))


  total_test = 0
  for key, val in regression_suite.items():
    total_test += len(val)

  # regression test
  total_tested = 1
  for key, val in regression_suite.items():
    params_file = params_path + key + '.params.in'
    for suite in val:
      test_subdir = test_dir + '/' + key + '/' + suite 
      os.system('mkdir -p %s' % (test_subdir))
      os.system('cp %s %s/params.in' % (params_file, test_subdir))

      trace_file_name = test_subdir + '/trace_file_list'
      file = open(trace_file_name, 'w')
      suite = suite[:suite.find('@')]
      bench_list = suite.split('_')
      file.write('%d\n' % len(bench_list))
      for bench in bench_list:
        file.write('%s\n' % trace_common.TRACE_FILE['%s@ref' % (bench)])
      file.close()

      os.chdir(test_subdir)
      print('running %s %s - %d of %d' % (key, suite, total_tested, total_test))
      total_tested += 1
      os.system('../../macsim 2>&1 1>/dev/null')

  # check results
  regression_result = defaultdict(list) 
  regression_data_dir = os.environ['SIMDIR'] + '/tools/macsim_regression/data'
  for key, val in regression_suite.items():
    for suite in val:
      regression_result[key].append(suite)
      test_subdir = test_dir + '/' + key + '/' + suite 
      compare_dir = regression_data_dir + '/' + key + '/' + suite
      file_list = glob.glob('*.stat.out')

      has_diff = False
      has_fail = False
      for filename in file_list:
        file_new  = test_subdir + '/' + filename
        file_orig = compare_dir + '/' + filename

        if not os.path.exists(file_new):
          has_fail = True
          continue
      
        diff = difflib.unified_diff(open(trace_file_name).readlines(), open(trace_file_name).readlines())
        diff_lines = ''
        for line in diff:
          diff_lines += line

        if len(diff_lines) > 0:
          has_diff = True
      
      if len(file_list) == 0:
        regression_result[key].append(-1) # fail
      else:
        if has_fail == True:
          regression_result[key].append(-1) # fail
        elif has_diff == True:
          regression_result[key].append(1) # diff
        else:
          regression_result[key].append(0) # pass


  # prepare output
  output_text = ''
  now = datetime.datetime.now()
  output_text += 'date: ' + str(now)
  output_text += '\n'

  svn_version = os.popen('svn info svn+ssh://purpleconeflower.cc.gt.atl.ga.us/hparch/svn/sim/macsim')
  revision = ''
  for line in svn_version.readlines():
#    if re.match(r"^Revision:\s", line):
#      revision = line.split()[1]
    output_text += line
#  output_text += 'Revision: %s' % (revision)
  output_text += '\n'


  num_total_pass = 0
  num_total_fail = 0
  num_total_diff = 0
  for key, val in regression_result.items():
    output_text += key + '\n'
    num_pass = 0
    num_fail = 0
    num_diff = 0
    for ii in range(0, len(val), 2):
      output_text += '\t' + val[ii] + ' : '
      if val[ii+1] == 0:
        output_text += 'pass\n'
        num_pass += 1
        num_total_pass += 1
      elif val[ii+1] == 1:
        output_text += 'diff\n'
        num_diff += 1
        num_total_diff += 1
      elif val[ii+1] == -1:
        output_text += 'fail\n'
        num_fail += 1
        num_total_fail += 1
    output_text += '\ttotal %d pass %d diff %d fail %d\n\n' % (num_pass+num_diff+num_fail, num_pass, num_diff, num_fail)

  output_text += 'Summary: total %d pass %d diff %d fail %d\n' % (num_total_pass+num_total_diff+num_total_fail, num_total_pass, num_total_diff, num_total_fail)


  # prepare and send the result email
  if options.disable_email != True:
    if 'EMAIL' in os.environ:
      os.system('echo "%s" | mutt -s "macsim_regression_%d-%d-%d" %s' % (output_text, now.year, now.month, now.day, os.environ['EMAIL']))
    else:
      print('warning: please set env. variable EMAIL to send a result')


  # show summary in stdout 
  print('\nRegression test summary')
  print(output_text)

  # clear data
#  os.system('rm -rf %s' % (test_dir)) 


if __name__ == '__main__':
  main(sys.argv)

