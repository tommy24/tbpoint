#!/usr/bin/python


import os
import spec06_common


"""
Main routine
"""
def main():
  exe_tail = '_base.x86_64_gcc'

  # Trace Generator Path Setting
  pin_dir        = '/home/jaekyu/software/pin-2.12-56759-gcc.4.4.7-linux' ### FIXME
  sim_dir        = '/home/jaekyu/sim/macsim_tracegen' ### FIXME

  pin_home       = '%s/source/tools' % pin_dir
  pin_exe        = '%s/pin' % pin_dir
  isimpoint_path = '%s/PinPoints/obj-intel64/isimpoint.so' % pin_home
  simpoint_path  = '%s/PinPoints/bin/simpoint' % pin_home
  ppgen_path     = '%s/PinPoints/bin/ppgen.3' % pin_home
  tracegen_path  = '%s/tools/x86_trace_generator/obj-intel64/trace_generator.so' % sim_dir
  current_dir    = os.getcwd() 
  base           = '%s/benchspec/CPU2006' % current_dir

# Misc
#  if os.path.exists('%s/scripts' % current_dir):
#    os.system('rm -rf %s/scripts' % current_dir)
#  os.system('mkdir -p %s/scripts' % current_dir);
  if not os.path.exists('%s/scripts' % current_dir):
    os.system('mkdir -p %s/scripts' % current_dir)

  bench_groups = spec06_common.spec06_groups_int + spec06_common.spec06_groups_fp

  for bench in bench_groups:
    bench_dir  = '%s/%s/run/run_base_ref_x86_64_gcc.0000' % (base, spec06_common.spec06_dirs[bench])
    pinout_dir = '/trace1/x86/spec2006/trace_simpoint/%s' % bench ### FIXME
    bench_exe  = '%s/%s%s' % (bench_dir, spec06_common.spec06_exes[bench], exe_tail)
    num_input  = int(spec06_common.spec06_num_input[bench])
    
    for ii in range(1, num_input+1):
      # File open
      script_file = open('%s/scripts/%s.%d.tracegen.py' % (current_dir, bench, ii), 'w')
      script_file.write('#!/usr/bin/python\n\n')
      script_file.write('# %s input %d\n\n' % (bench, ii))
      script_file.write('import os\n')
      script_file.write('import time\n\n')
      script_file.write('os.chdir(\'%s\')\n\n' % bench_dir)

    
      input_index = '%s%d' % (bench, ii)
      bench_arg = spec06_common.spec06_args[input_index]


      # bb
      script_file.write('print(\'>>>>>>>>>>>>>>>>>>>> BB\')\n')
      script_file.write('os.system(\'mkdir -p %s/bbdata\')\n' % (pinout_dir))
      bb_file = '%s/bbdata/%s%d'% (pinout_dir, bench, ii)
      script_file.write('os.system(\'rm -rf %s.T.0.bb\')\n' % bb_file)
      script_file.write('os.system(\'%s -t %s -o %s -slice_size 500000000 -- %s %s\')\n' 
          % (pin_exe, isimpoint_path, bb_file, bench_exe, bench_arg))
      script_file.write('time.sleep(10)\n\n')


      # simpoint
      bb_data = '%s.T.0.bb' % bb_file
      script_file.write('print(\'>>>>>>>>>>>>>>>>>>>> SimPoint\')\n')
      script_file.write('os.system(\'%s -loadFVFile %s -maxK 1 -coveragePct 0.9999 -saveSimpoints %s.simpoints -saveSimpointWeights %s.weights -saveLabels %s.labels > %s.simpointout\')\n' 
        % (simpoint_path, bb_data, bb_data, bb_data, bb_data, bb_data))
      script_file.write('time.sleep(10)\n\n')
  

      # pp
      pp_file = '%s/ppdata/%s%d-point' % (pinout_dir, bench, ii)
      script_file.write('print(\'>>>>>>>>>>>>>>>>>>>> PP\')\n')
      script_file.write('os.system(\'mkdir -p %s/ppdata\')\n' % pinout_dir) 
      script_file.write('os.system(\'%s %s %s %s.simpoints.lpt0.9999 %s.weights.lpt0.9999 %s.labels 0 &> %s.ppgenout\')\n'
        %(ppgen_path, pp_file, bb_data, bb_data, bb_data, bb_data, bb_data))
      script_file.write('time.sleep(10)\n\n')


      # Read PP file to get the starting point
      script_file.write('ppfile = open(\'%s.1.pp\', \'r\')\n' % pp_file)
      script_file.write('starting = \'0\'\n')
      script_file.write('length = \'200000000\'\n')
      script_file.write('for line in ppfile:\n')
      script_file.write('  line = line.rstrip(\'\\n\')\n')
      script_file.write('  tokens = filter(None, line.split(\' \'))\n')
      script_file.write('  if len(tokens) > 0 and tokens[0] == \'region\':\n')
      script_file.write('    starting = tokens[6]\n')
      script_file.write('    length = tokens[5]\n')
      script_file.write('ppfile.close()\n\n')


      # trace generation
      script_file.write('print(\'>>>>>>>>>>>>>>>>>>>> Trace Generation\')\n')
      script_file.write('print(\'>>>>>>>>>>>>>>>>>>>> PP : %s\' %s)\n' % ('%s', '% (starting)'))
      script_file.write('os.system(\'mkdir -p %s/pin_traces\')\n' % pinout_dir) 
      trace_file = '%s/pin_traces/%s.%d' % (pinout_dir, bench, ii)
      script_file.write('os.system(\'%s -t %s -skip %s -max %s -tracename %s -dump 1 -dump_file %s.dump -- %s %s\' %s)\n\n'
          % (pin_exe, tracegen_path, '%s', '%s', trace_file, trace_file, bench_exe, bench_arg, '% (starting, length)'))
#      script_file.write('os.system(\'%s -t %s -ppfile %s.1.pp -tracename %s -dump 1 -dump_file %s.dump -- %s %s\')\n\n'
#          % (pin_exe, tracegen_path, pp_file, trace_file, trace_file, bench_exe, bench_arg))

#      script_file.write('rm -rf %s/bbdata/%s%d*\n\n' % (pinout_dir, bench, ii))

      # file close
      script_file.close()
      os.system('chmod +x %s/scripts/%s.%d.tracegen.py' % (current_dir, bench, ii))
      
      os.system('qsub %s/scripts/%s.%d.tracegen.py' % (current_dir, bench, ii))


if __name__ == '__main__':
  main()



