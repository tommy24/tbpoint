#!/usr/bin/python


import sys
import os
import time
import getopt
import subprocess
import shlex
import re


cuda22_suite = [
  'AlignedTypes',
  'AsyncAPI',
  'BandwidthTest',
  'BicubicTexture',
  'BinomialOptions',
  'Bitonic',
  'BlackScholes',
  'BoxFilter',
  'ConvolutionFFT2D',
  'ConvolutionSeparable',
  'ConvolutionTexture',
  'CppIntegration',
  'Dct8x8',
  'DwtHaar1D',
  'Dxtc',
  'Eigenvalues',
  'FastWalshTransform',
  'FluidsGL',
  'Histogram256',
  'Histogram64',
#  'ImageDenoising', cuda32
  'Mandelbrot',
  'MatrixMul',
  'MersenneTwister',
  'MonteCarlo',
  'MonteCarloMultiGPU',
  'Nbody',
  'OceanFFT',
  'Particles',
  'PostProcessGL',
  'QuasirandomGenerator',
  'RecursiveGaussian',
  'Reduction',
  'ScalarProd',
  'Scan',
  'ScanLargeArray',
  'SimpleAtomicIntrinsics',
  'SimpleCUBLAS',
  'SimpleCUFFT',
  'SimpleGL',
  'SimpleMultiGPU',
  'SimpleStreams',
  'SimpleTemplates',
  'SimpleTexture',
  'SimpleTexture3D',
  'SimpleVoteIntrinsics',
  'SimpleZeroCopy',
  'SobelFilter',
  'SobolQRNG',
#  'SimplerCUFFT',
  'Template',
  'ThreadFenceReduction',
  'Transpose',
  'TransposeNew',
  'VolumeRender',
]


cuda22_suite_dir = {
  'AlignedTypes' : 'alignedTypes',
  'AsyncAPI' : 'asyncAPI',
  'BandwidthTest' : 'bandwidthTest',
  'BicubicTexture' : 'bicubicTexture',
  'BinomialOptions' : 'binomialOptions',
  'Bitonic' : 'bitonic',
  'BlackScholes' : 'BlackScholes',
  'BoxFilter' : 'boxFilter',
  'ConvolutionFFT2D' : 'convolutionFFT2D',
  'ConvolutionSeparable' : 'convolutionSeparable',
  'ConvolutionTexture' : 'convolutionTexture',
  'CppIntegration' : 'cppIntegration',
  'Dct8x8' : 'dct8x8',
  'DwtHaar1D' : 'dwtHaar1D',
  'Dxtc' : 'dxtc',
  'Eigenvalues' : 'eigenvalues',
  'FastWalshTransform' : 'fastWalshTransform',
  'FluidsGL' : 'fluidsGL',
  'Histogram256' : 'histogram256',
  'Histogram64' : 'histogram64',
#  'ImageDenoising' : 'imageDenoising', cuda32
  'Mandelbrot' : 'Mandelbrot',
  'MatrixMul' : 'matrixMul',
  'MersenneTwister' : 'MersenneTwister',
  'MonteCarlo' : 'MonteCarlo',
  'MonteCarloMultiGPU' : 'MonteCarloMultiGPU',
  'Nbody' : 'nbody',
  'OceanFFT' : 'oceanFFT',
  'Particles' : 'particles',
  'PostProcessGL' : 'postProcessGL',
  'QuasirandomGenerator' : 'quasirandomGenerator',
  'RecursiveGaussian' : 'recursiveGaussian',
  'Reduction' : 'reduction',
  'ScalarProd' : 'scalarProd',
  'Scan' : 'Scan',
  'ScanLargeArray' : 'scanLargeArray',
  'SimpleAtomicIntrinsics' : 'simpleAtomicIntrinsics',
  'SimpleCUBLAS' : 'simpleCUBLAS',
  'SimpleCUFFT' : 'simpleCUFFT',
  'SimpleGL' : 'simpleGL',
  'SimpleMultiGPU' : 'simpleMultiGPU',
  'SimpleStreams' : 'simpleStreams',
  'SimpleTemplates' : 'simpleTemplates',
  'SimpleTexture' : 'simpleTexture',
  'SimpleTexture3D' : 'simpleTexture3D',
  'SimpleVoteIntrinsics' : 'simpleVoteIntrinsics',
  'SimpleZeroCopy' : 'simpleZeroCopy',
  'SobelFilter' : 'SobelFilter',
  'SobolQRNG' : 'SobolQRNG',
#  'SimplerCUFFT' : 'SimplerCUFFT',
  'Template' : 'template',
  'ThreadFenceReduction' : 'threadFenceReduction',
  'Transpose' : 'transpose',
  'TransposeNew' : 'transposeNew',
  'VolumeRender' : 'volumeRender',
}


cuda22_suite_bin = {}
for bench in cuda22_suite:
  cuda22_suite_bin[bench] = '/cuda2.2/%s' % (bench)

cuda22_make_cmd = {}
for bench in cuda22_suite:
  cuda22_make_cmd[bench] = 'make %s' % (bench)

cuda22_clean_cmd = {}
for bench in cuda22_suite:
  cuda22_clean_cmd[bench] = 'rm -f %s' % (bench)


###############################################################################################


cuda32_suite = [
  'BlackScholes', # Done
#  'FDTD3d',
#  'FunctionPointers', # No Rule
  'Interval', # No Rule
#  'Mandelbrot', # Display, Too big (use 2.2)
  'MersenneTwister', # Done
  'MonteCarlo', # Done
#  'MonteCarloMultiGPU',
  'SobelFilter', # Done
  'SobolQRNG', # Done
#  'AlignedTypes',
#  'AsyncAPI',
#  'BandwidthTest',
  'BicubicTexture', # Display
  'BilateralFilter', # Display
  'BinomialOptions', # Done
  'BoxFilter', # Display
#  'ConcurrentKernels',
#  'ConjugateGradient',
#  'ConvolutionFFT2D', # Driver Error
#  'ConvolutionSeparable', # Done
#  'ConvolutionTexture', # Done
#  'CppIntegration',
  'Eigenvalues', # Done
  'FastWalshTransform1', # Done
#  'FluidsGL', # Display
  'Histogram', # Done
  'LineOfSight', # No Rule
  'MarchingCubes', # No Rule
  'MatrixMul',
#  'MatrixMulDrv',
  'MergeSort', # Done
  'Nbody', # Done
  'OceanFFT', # NVIDIA Driver
  'Particles', # No Rule
#  'PostProcessGL',
  'QuasirandomGenerator', # Done
  'RadixSort', # No Rule
  'RecursiveGaussian', # Done
  'Reduction', # Done
  'ScalarProd', # Done
#  'Scan',
#  'SimpleAtomicIntrinsics',
#  'SimpleMPI',
#  'SimpleMultiCopy',
#  'SimpleMultiGPU',
#  'SimplePitchLinearTexture',
#  'SimpleStreams',
#  'SimpleTemplates',
#  'SimpleTexture',
#  'SimpleTexture3D',
#  'SimpleTextureDrv',
#  'SimpleVoteIntrinsics',
#  'SimpleZeroCopy',
  'SmokeParticles', # No Rule
  'SortingNetworks', # Too big
#  'Template', 
#  'ThreadFenceReduction', # Membar
#  'ThreadMigration', # No Rule
  'Transpose', # File open error
  'VectorAdd', # Done
  'VectorAddDrv', # No Rule
  'VolumeRender', # Done
  'ImageDenoising', # Done
  'Dct8x8', # Done
  'DwtHaar1D', # Done
  'Dxtc', # Done
];


cuda32_suite_dir = {
  'BlackScholes' : 'BlackScholes',
#  'FDTD3d' : 'FDTD3d',
  'FunctionPointers' : 'FunctionPointers',
  'Interval' : 'Interval',
  'Mandelbrot' : 'Mandelbrot',
  'MersenneTwister' : 'MersenneTwister',
  'MonteCarlo' : 'MonteCarlo',
  'MonteCarloMultiGPU' : 'MonteCarloMultiGPU',
  'SobelFilter' : 'SobelFilter',
  'SobolQRNG' : 'SobolQRNG',
#  'AlignedTypes' : 'alignedTypes',
#  'AsyncAPI' : 'asyncAPI',
#  'BandwidthTest' : 'bandwidthTest',
  'BicubicTexture' : 'bicubicTexture',
  'BilateralFilter' : 'bilateralFilter',
  'BinomialOptions' : 'binomialOptions',
  'BoxFilter' : 'boxFilter',
  'ConcurrentKernels' : 'concurrentKernels',
#  'ConjugateGradient' : 'conjugateGradient',
  'ConvolutionFFT2D' : 'convolutionFFT2D',
#  'ConvolutionSeparable' : 'convolutionSeparable', # use 2.2
#  'ConvolutionTexture' : 'convolutionTexture', # use 2.2
  'CppIntegration' : 'cppIntegration',
  'Dct8x8' : 'dct8x8',
  'DwtHaar1D' : 'dwtHaar1D',
  'Dxtc' : 'dxtc',
  'Eigenvalues' : 'eigenvalues',
  'FastWalshTransform1' : 'fastWalshTransform',
  'FluidsGL' : 'fluidsGL',
  'Histogram' : 'histogram',
  'ImageDenoising' : 'imageDenoising',
  'LineOfSight' : 'lineOfSight',
  'MarchingCubes' : 'marchingCubes',
  'MatrixMul' : 'matrixMul',
#  'MatrixMulDrv' : 'matrixMulDrv',
  'MergeSort' : 'mergeSort',
  'Nbody' : 'nbody',
  'OceanFFT' : 'oceanFFT',
  'Particles' : 'particles',
  'PostProcessGL' : 'postProcessGL',
  'QuasirandomGenerator' : 'quasirandomGenerator',
  'RadixSort' : 'radixSort',
  'RecursiveGaussian' : 'recursiveGaussian',
  'Reduction' : 'reduction',
  'ScalarProd' : 'scalarProd',
  'Scan' : 'scan',
  'SimpleAtomicIntrinsics' : 'simpleAtomicIntrinsics',
  'SimpleMPI' : 'simpleMPI',
  'SimpleMultiCopy' : 'simpleMultiCopy',
  'SimpleMultiGPU' : 'simpleMultiGPU',
  'SimplePitchLinearTexture' : 'simplePitchLinearTexture',
  'SimpleStreams' : 'simpleStreams',
  'SimpleTemplates' : 'simpleTemplates',
  'SimpleTexture' : 'simpleTexture',
  'SimpleTexture3D' : 'simpleTexture3D',
  'SimpleTextureDrv' : 'simpleTextureDrv',
  'SimpleVoteIntrinsics' : 'simpleVoteIntrinsics',
  'SimpleZeroCopy' : 'simpleZeroCopy',
  'SmokeParticles' : 'smokeParticles',
  'SortingNetworks' : 'sortingNetworks',
  'Template' : 'template',
  'ThreadFenceReduction' : 'threadFenceReduction',
  'ThreadMigration' : 'threadMigration',
  'Transpose' : 'transpose',
  'VectorAdd' : 'vectorAdd',
  'VectorAddDrv' : 'vectorAddDrv',
  'VolumeRender' : 'volumeRender',
};


cuda32_suite_bin = {}
for bench in cuda32_suite:
  cuda32_suite_bin[bench] = '/cuda3.2/%s' % (bench)

cuda32_make_cmd = {}
for bench in cuda32_suite:
  cuda32_make_cmd[bench] = 'make %s' % (bench)

cuda32_clean_cmd = {}
for bench in cuda32_suite:
  cuda32_clean_cmd[bench] = 'rm -f %s' % (bench)


###############################################################################################


rodinia_suite = [
  'backPropagation' ,
  'bfs'             ,
  'cell'            ,
  'cfd'             ,
  'dynproc'         ,
  'gaussian'        ,
  'hotspot'         , 
  'heartwall'       ,
  'kmeans'          ,
  'leukocyte'       ,
  'lud'             ,
  'mummergpu'       , 
  'needlemanWunsch' ,
  'nearest_neighbor',
  'srad'            , 
  'streamcluster'   ,
];


rodinia_suite_dir = {
  'backPropagation' : 'backprop',
  'bfs'             : 'bfs',
  'cell'            : 'cell_cuda',
  'cfd'             : 'cfd',
  'dynproc'         : 'dynproc_cuda',
  'gaussian'        : 'gaussian',
  'hotspot'         : 'hotspot',
  'heartwall'       : 'heartwall',
  'kmeans'          : 'kmeans',
  'leukocyte'       : 'leukocyte',
  'lud'             : 'lud',
  'mummergpu'       : 'mummergpu',
  'needlemanWunsch' : 'nw',
  'nearest_neighbor': 'nn_cuda',
  'srad'            : 'srad',
  'streamcluster'   : 'streamcluster',
}

rodinia_suite_bin = {}
for bench in rodinia_suite:
  rodinia_suite_bin[bench] = '/rodinia_1.0/cuda/%s/run' % (rodinia_suite_dir[bench])

rodinia_make_cmd = {}
for bench in rodinia_suite:
  rodinia_make_cmd[bench] = 'make'

rodinia_clean_cmd = {}
for bench in rodinia_suite:
  rodinia_clean_cmd[bench] = 'make clean'


###############################################################################################


erc_suite = [
  'AES',
#  'BitonicSort',
  'SAD',
  'SHA1',
  'RayTracing',
  'RadixSort3',
];

erc_suite_dir = { 
  'AES' : 'AES',
  'BitonicSort' : 'Bitonic_Sort',
  'SAD' : 'SAD',
  'SHA1' : 'SHA1',
  'RayTracing' : 'Ray_Tracing',
  'RadixSort3' : 'Radix_Sort',
};

erc_suite_bin = {}
for bench in erc_suite:
  erc_suite_bin[bench] = '/ercbench/%s/run' % (erc_suite_dir[bench])

erc_make_cmd = {}
for bench in erc_suite:
  erc_make_cmd[bench] = 'make'

erc_clean_cmd = {}
for bench in erc_suite:
  erc_clean_cmd[bench] = 'make clean'


###############################################################################################


parboil_suite = [
  'fft',
  'bfs',
  'stencil',
#  'mri-gridding',
  'mm',
  'lbm',
  'tpacf',
  'sad',
#  'spmv',
  'histo',
  'mri-q',
  'cutcp',
];

parboil_suite_size = {
  'fft' : 'default',
  'bfs' : 'NY', # 1M, NY, SF
  'stencil' : 'default',
  'mri-gridding' : 'default',
  'mm'      : 'large', # small
  'lbm'     : 'short', # long
  'tpacf'   : 'default',
  'sad'     : 'default',
#  'spmv'    : 'large', # small median large
  'histo'   : 'default',
  'mri-q'   : 'small', # large
  'cutcp'   : 'small',
};

parboil_suite_dir = {}
for bench in parboil_suite:
  parboil_suite_dir[bench] = bench

parboil_suite_bin = {}
for bench in parboil_suite:
  parboil_suite_bin[bench] = '/parboil2.0/parboil run %s cuda' % (parboil_suite_dir[bench])

parboil_make_cmd = {}
for bench in parboil_suite:
  parboil_make_cmd[bench] = 'parboil compile %s cuda' % (bench)

parboil_clean_cmd = {}
for bench in parboil_suite:
  parboil_clean_cmd[bench] = 'parboil clean %s cuda' % (bench)


###############################################################################################


## Path setup
BENCH_LIST = []
SUITE = {}
SUITE_SRC = {}
SUITE_BIN = {}
SUITE_BASE_DIR = {}
SUITE_MAKE_CMD = {}
SUITE_CLEAN_CMD = {}
SUITE_SIZE = {}
for bench in cuda22_suite:
  SUITE[bench]          = 'cuda2.2'
  SUITE_SRC[bench]      = '/cuda2.2/tests/%s' % (cuda22_suite_dir[bench])
  SUITE_BIN[bench]      = cuda22_suite_bin[bench]
  SUITE_BASE_DIR[bench] = '/cuda2.2'
  SUITE_MAKE_CMD[bench] = cuda22_make_cmd[bench]
  SUITE_CLEAN_CMD[bench] = cuda22_clean_cmd[bench]
  SUITE_SIZE[bench] = ''
for bench in cuda32_suite:
  SUITE[bench]           = 'cuda3.2'
  SUITE_SRC[bench]       = '/cuda3.2/tests/%s' % (cuda32_suite_dir[bench])
  SUITE_BIN[bench]       = cuda32_suite_bin[bench]
  SUITE_BASE_DIR[bench]  = '/cuda3.2'
  SUITE_MAKE_CMD[bench]  = cuda32_make_cmd[bench]
  SUITE_CLEAN_CMD[bench] = cuda32_clean_cmd[bench] 
  SUITE_SIZE[bench]      = ''
for bench in rodinia_suite:
  SUITE[bench]           = 'rodinia'
  SUITE_SRC[bench]       = '/rodinia_1.0/cuda/%s' % (rodinia_suite_dir[bench])
  SUITE_BIN[bench]       = rodinia_suite_bin[bench]
  SUITE_BASE_DIR[bench]  = '/rodinia_1.0/cuda/%s' % (rodinia_suite_dir[bench])
  SUITE_MAKE_CMD[bench]  = rodinia_make_cmd[bench]
  SUITE_CLEAN_CMD[bench] = rodinia_clean_cmd[bench] 
  SUITE_SIZE[bench]      = ''
for bench in erc_suite:
  SUITE[bench]           = 'erc'
  SUITE_SRC[bench]       = '/ercbench/%s' % (erc_suite_dir[bench])
  SUITE_BIN[bench]       = erc_suite_bin[bench]
  SUITE_BASE_DIR[bench]  = '/ercbench/%s' % (erc_suite_dir[bench])
  SUITE_MAKE_CMD[bench]  = erc_make_cmd[bench]
  SUITE_CLEAN_CMD[bench] = erc_clean_cmd[bench] 
  SUITE_SIZE[bench]      = ''
for bench in parboil_suite:
  SUITE[bench]           = 'parboil'
  SUITE_SRC[bench]       = '/parboil2.0/benchmarks/%s/src/cuda' % (parboil_suite_dir[bench])
  SUITE_BIN[bench]       = parboil_suite_bin[bench]
  SUITE_BASE_DIR[bench]  = '/parboil2.0'
  SUITE_MAKE_CMD[bench]  = parboil_make_cmd[bench]
  SUITE_CLEAN_CMD[bench] = parboil_clean_cmd[bench] 
  SUITE_SIZE[bench]      = parboil_suite_size[bench]



## Print help messages
def help():
  print "[-r, --rebuild] rebuild binaries"
  print "[-h, --help]    help"
  print "[-b, --bench]   benchmark"
  print "[-c, --compute] compute version"
  sys.exit(0)
  

## global variable / initialization
g_help        = False
g_bench       = ""
g_rebuild     = False
g_compute     = '2.0'
g_compute_arg = 'sm_20'


### argument parsing
argv = sys.argv
opts, args = getopt.getopt(sys.argv[1:], "rb:c:h", ["rebuild", "bench=", "compute=", "help"])

for opt, arg in opts:
  # rebuild
  if opt in ('-r', '--rebuild'):
    g_rebuild = True
  # help
  elif opt in ('-h', '--help'):
    g_help = True
  # benchmark
  elif opt in ('-b', '--bench'):
    g_bench = arg
  elif opt in ('-c', '--compute'):
    g_compute = arg


if (g_help == True):
  help()


if g_compute == '1.3':
  g_compute_arg = 'sm_13'


if g_bench != "":
  if g_bench == "cuda32":
    BENCH_LIST = cuda32_suite
  elif g_bench == "rodinia":
    BENCH_LIST = rodinia_suite
  elif g_bench == "parboil":
    BENCH_LIST = parboil_suite
  elif g_bench == "erc":
    BENCH_LIST = erc_suite
  else:
    BENCH_LIST = g_bench.split("+")
else:
  BENCH_LIST = cuda32_suite + rodinia_suite


g_current_directory = os.getcwd();
OCELOT_PATH = '/home/jaekyu/sim/gpuocelot_1137/tests'


for bench in BENCH_LIST:
  if bench in SUITE_SRC == False:
    continue

  print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
  print ">>>>>  Running %s" % (bench)


  ## Getting register usage information
  print ">>>>>  Getting kernel register usage information %s" % (bench)
  file_name = g_current_directory + '/occupancy_' + bench
  file = open(file_name, 'w')

  directory = OCELOT_PATH + SUITE_SRC[bench]
  if os.path.exists(directory) == False:
    print directory
    continue
    
  print "chdir %s" % (directory)
  os.chdir(directory)
  cmd = "nvcc --cubin --ptxas-options=-v -arch %s *.cu -I ../../sdk -I../../shared" % (g_compute_arg)
  print cmd;
  p = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=True)     
  (fi, fo, fe) = (p.stdin, p.stdout, p.stderr)
    

  kernel_name = ''
  register    = ''
  shared_mem  = '0'
  for line in fe.readlines():
    token = line.split()
    if len(token) == 0:
      pass
    elif kernel_name != '':
      assert token[0] == 'ptxas'
      assert (token[5] == 'registers,' or token[5] == 'registers')
      register = token[4]
      if len(token) >= 8 and token[8] == 'smem,':
        shared_mem_list = token[6].split('+')
        if len(shared_mem_list) > 1:
          shared_mem = str(int(shared_mem_list[0]) + int(shared_mem_list[1]));
        else:
          shared_mem = token[6]
      file.write("%s %s %s\n" % (kernel_name, register, shared_mem))
      kernel_name = ''
      register = ''
      shared_mem = '0'
    elif token[0] == 'ptxas':
      if kernel_name == '':
        assert token[5] == 'function', 'token[5]:%s' % (token[5])
        kernel_name = token[6].replace('\'', '')

  file.close()
  ## Getting register usage information DONE

#  os.system("nvcc --cuda *.cu -arch sm_20 -I../../sdk -I../../shared")


  ## (re)build binary if not exist or rebuild option is set ###################################
  base_directory = OCELOT_PATH + SUITE_BASE_DIR[bench]
  if os.path.exists(base_directory) == False:
    continue


  print "chdir %s" % (base_directory)
  os.chdir(base_directory)
  BIN = OCELOT_PATH + SUITE_BIN[bench]
  if os.path.exists(BIN) != True or g_rebuild == True:
    print ">>>>>  Building %s" % (BIN)

    ## Delete existing binary
    clean_cmd = SUITE_CLEAN_CMD[bench]
    print clean_cmd
    os.system('%s' % (clean_cmd))
#    if os.path.exists(bench):
#      print "rm -f %s" % (bench)
#      os.system("rm -f %s" % (bench))

    print("%s" % (SUITE_MAKE_CMD[bench]))
    os.system("%s" % (SUITE_MAKE_CMD[bench]))
  else:
    print ">>>>>  Binary exists"
  ## (re)build binary if not exist or rebuild option is set DONE

  
  ## set trace path ########################################################################### 
  trace_path = "/trace/ptx/%s/%s" % (SUITE[bench], bench)
  os.environ["TRACE_PATH"]       = trace_path
  os.environ["USE_KERNEL_NAME"]  = "1"
  os.environ["KERNEL_INFO_PATH"] = file_name
  os.environ["COMPUTE_VERSION"]  = g_compute
  ## set trace path done
 

  ## delete directory if same trace exists ####################################################
  if os.path.exists(trace_path):
    print ">>>>>  Delete existing directories"
    os.system("rm -rf %s" % (trace_path))
  ## delete directory if same trace exists DONE


  # execute binary ############################################################################
  print "%s %s" % (BIN, SUITE_SIZE[bench])
  os.system("%s %s" % (BIN, SUITE_SIZE[bench]))
  # execute binary DONE


  ## delete occupancy file ####################################################################
  os.remove(file_name);
  ## delete occupancy file

  print ">>>>>  %s Done" % (bench)
  print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"



