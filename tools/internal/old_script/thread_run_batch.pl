#!/usr/bin/perl

# Settings
@splash2 = ( 
	'barnes', 
	'fft', 
	'fmm', 
	'lu_contiguous', 
	'lu_non_contiguous', 
	'ocean_contiguous', 
	'ocean_non_contiguous', 
	'radiosity', 
	'radix', 
	'raytrace', 
	'water_nsquared', 
	'water_spatial' 
);
@parsec = ( 
	'blackscholes', 
	'bodytrack', 
	'canneal', 
	'dedup', 
	'facesim', 
	'ferret', 
	'fluidanimate',
	'freqmine', 
	'streamcluster', 
	'swaptions', 
	'vips',
	'x264' 
);

@asymmetry = (
	'c_fft',
	'c_fft6',
	'c_qsort',
	'cg20.cua',
	'g4.rua',
	'g5.rua',
	'barnes',
	'cholesky',
	'fmm',
	'lu_contiguous',
	'lu_non_contiguous',
	'radiosity',
	'raytrace'
);

@parsec_temp = ( 'fluidanimate' );

# Paths
$home_dir = "${ENV{'HOME'}}";
$result_dir = "${ENV{'SIMDIR'}}/threadsim_results";
$trace_dir = "/hparch/proj/thread/traces";

# CommandLine Parsing
$proj = $ARGV[0];
$suite = "";
$arg = "";

$ARGC = $#ARGV + 1;

$num_th = 8;

if( $ARGC > 7 ) { die "Wrong commandlines\n"; }

for( $ii=1 ; $ii<$ARGC ; $ii+=2 ) {
	$command = $ARGV[$ii];
	if( $command eq "-suite" ) { $suite = $ARGV[$ii+1]; }
	elsif( $command eq "-arg" ) { $arg = $ARGV[$ii+1]; }
	elsif( $command eq "-th" ) { $num_th = $ARGV[$ii+1]; }
}


# Suite
@suite_list = split(/@/, $suite);

if( $suite_list[0] eq "splash2" ) {
	@BENCH = @splash2;
	$bench_name = "SPLASH2";
	$bench_count = 12;
	$suffix = "";
}
elsif( $suite_list[0] eq "parsec" ) {
	@BENCH = @parsec;
	$bench_name = "PARSEC_${suite_list[1]}";
	$bench_count = 12;
	$suffix = "${suite_list[1]}";
}
elsif( $suite_list[0] eq "asymmetry" ) {
	@BENCH = @asymmetry;
	$bench_name = "ASYMMETRY";
	$bench_count = 6;
	$suffix = "";
}


# Create Directries
$proj_dir = "${result_dir}/${proj}";
if( !(-e $proj_dir) ) {
	system "mkdir -p ${proj_dir}";
}
else {
	print "${proj} already exists. It will be overwritten.\n";
}

foreach $bench (@BENCH) {
	if( $suffix eq "" ) { $bench_dir = "${result_dir}/${proj}/${bench}"; }
	else { $bench_dir = "${result_dir}/${proj}/${bench}\@${suffix}"; }
	if( !(-e $bench_dir) ) {
		system "mkdir -p ${bench_dir}";
	}
}


# Main Execution

$bin = "thsim";

foreach $bench (@BENCH) {
	chdir $dir;
	if( $suffix eq "" ) { $temp_file = "${result_dir}/${proj}/${bench}/temp.pl"; }
	else { $temp_file = "${result_dir}/${proj}/${bench}\@${suffix}/temp.pl"; }

	if( -e $temp_file ) { print "Script already exists! It will be overwritten.\n"; }
	
	open OUT, ">$temp_file" or die "Cannot open $temp_file for write :$!";

	print OUT "#!/usr/bin/perl\n\n";
	if( $suffix eq "" ) { 
		print OUT "chdir \"${result_dir}/${proj}/${bench}\"\;\n";
		print OUT "system \"${bin} -in ${trace_dir}/${suite_list[0]}/th_${num_th}/${bench}.trace.out -out ${bench} ${arg}\"\;\n"; 
	}
	else { 
		print OUT "chdir \"${result_dir}/${proj}/${bench}\\\@${suffix}\"\;\n";
		print OUT "system \"${bin} -in ${trace_dir}/${suite_list[0]}/th_${num_th}/${bench}\\\@${suffix}.trace.out -out ${bench} ${arg}\"\;\n"; 
	}

	close OUT;

	if( $suffix eq "" ) { chdir "${result_dir}/${proj}/${bench}"; }
	else { chdir "${result_dir}/${proj}/${bench}\@${suffix}"; }
	system "echo \"perl ${temp_file}\" | qsub\n";
}
