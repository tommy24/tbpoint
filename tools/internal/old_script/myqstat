#!/usr/bin/perl -w
# **************************************************************************************
# CVS          : $Id: myqstat,v 1.1 2008-08-20 19:05:49 hyesoon Exp $:
# **************************************************************************************

# **********************************************************************
# usage information

$USAGE = "Usage:  '$0 [-h] [options...]'";

if (@ARGV == 1 && $ARGV[0] eq "-h") {
  print("$USAGE\n");

  print("\t-m        : Print memory usage\n");
  print("\t-c        : Print CPU usage\n");
  print("\t-l        : Print long job names\n");
  print("\t-u [user] : Print jobs belonging to [user] or self if omitted\n");

  exit(0);
}


# **********************************************************************
# process arguments (future)

$MEMORY	   = 0;
$CPU   	   = 0;
$LONGNAMES = 0;
$USER      = "";

while (@ARGV) {
  $option = shift;

  if ($option eq "-m") {
    $MEMORY = 1;
  } elsif ($option eq "-c") {
    $CPU = 1;
  } elsif ($option eq "-l") {
    $LONGNAMES = 1;
  } elsif ($option eq "-ll") {
    $LONGNAMES = 2;
  } elsif ($option eq "-u") {
    my $tmp = shift;
    if (!defined($tmp)) {
      $tmp = $ENV{'USER'};
    } elsif ($tmp =~ /^-/) {
      unshift(@ARGV, $tmp);
      $tmp = $ENV{'USER'};
    } 
    $USER = $tmp;
  } else {
    die "Invalid myqstat option '$option'.\n";
  }
}


# **********************************************************************
# pbsnodes wrapper

open(IN, "qstat -f |") || die;

@outarray = ();

@lines = <IN>;
push(@lines, "\n");


$g_init = 0;

foreach (@lines) {
  chomp;

  if (!defined($line)) {
    $line = $_;
    next;
  } else {
    if (/^\t(.+)$/) {
      $line .= $1;
      next;
    } 
    $temp = $_;
    $_ = $line;
    $line = $temp;
  }


  if ($_ =~ /Job Id: (.*)$/) {
    if ($g_init == 1) {
      if (!$USER || $user eq $USER || $USER eq "all") {
        $new = [];

        push(@{$new}, [$job_id,     -9]);
        push(@{$new}, [$user,      -10]);
        push(@{$new}, [$job_name,  -30]);
        push(@{$new}, [$job_bench, -10]) if $LONGNAMES;
        push(@{$new}, [$queue,      -8]);
        push(@{$new}, [$state,      -1]);
        push(@{$new}, [$wtime,       8]);
        push(@{$new}, [$ctime,       8]) if $CPU;
        push(@{$new}, [$vmem,        9]) if $MEMORY;
        push(@{$new}, [$machine,   -20]);

        push(@outarray, $new);
      }
    }
    $g_init    = 1;
    $job_id	   = $1;
    @temp      = split(/\./, $job_id);
    $job_id    = $temp[0];
    $user	     = "";
    $queue	   = "";
    $job_name  = "";
    $job_bench = "";
    $state	   = "";
    $ctime	   = "";
    $wtime	   = "";
    $vmem	     = "";
    $machine   = "";
    $max_name  = 1;
    $max_bench = 1;

  } elsif ($_ =~ /^\s*Job_Owner = (\w+)@.*$/) {
    $user = $1;

  } elsif (!$LONGNAMES && /^\s*Job_Name = (.+)$/) {
    $job_name = $1;

  } elsif ($LONGNAMES && /^\s*Output_Path = (.+)$/) {
    my @path_split = split(/\//, $1);
    $job_name  = splice(@path_split, -2, 1);
    $max_name  = length($job_name) > $max_name ? length($job_name) : $max_name;
    $job_bench = splice(@path_split, -1, 1);
    $job_bench =~ s/@.*$//;
    $max_bench = length($job_bench) > $max_bench ? length($job_bench) : $max_bench;

  } elsif ($_ =~ /^\s*queue = (.+)$/) {
    $queue = $1;

  } elsif ($_ =~ /^\s*job_state = (.)$/) {
    $state = $1;

  } elsif ($_ =~ /^\s*resources_used.cput = (..:..:..)$/) {
    $ctime = $1;

  } elsif ($_ =~ /^\s*resources_used.walltime = (..:..:..)$/) {
    $wtime = $1;

  } elsif ($_ =~ /^\s*resources_used.vmem = (.+)$/) {
    $vmem = $1;

  } elsif ($_ =~ /^\s*exec_host = (.+)$/) {
    $machine = $1;
    $machine =~ s/.cc.gt.atl.ga.us\/.//;
    $machine =~ s/.cc.gatech.edu\/.//;

  } elsif ($_ =~ /^\s*$/) {
  }
}

if ($g_init == 1 && (!$USER || $user eq $USER || $USER eq "all")) {
  $new = [];

  push(@{$new}, [$job_id,     -9]);
  push(@{$new}, [$user,      -10]);
  push(@{$new}, [$job_name,  -30]);
  push(@{$new}, [$job_bench, -10]) if $LONGNAMES;
  push(@{$new}, [$queue,      -8]);
  push(@{$new}, [$state,      -1]);
  push(@{$new}, [$wtime,       8]);
  push(@{$new}, [$ctime,       8]) if $CPU;
  push(@{$new}, [$vmem,        9]) if $MEMORY;
  push(@{$new}, [$machine,   -20]);

  push(@outarray, $new);
}

@max_dims = ();
@flushlr  = ();

foreach (@outarray) {
  @out = @{$_};

  foreach $ii (0..$#out) {
    $flushlr[$ii] = $out[$ii][1] < 0 ? "-" : "";

    $max_dims[$ii] = 1 unless defined $max_dims[$ii];
    $max_dims[$ii] = (length($out[$ii][0]) > $max_dims[$ii]
      ? length($out[$ii][0]) : $max_dims[$ii]);
    unless ($LONGNAMES == 2) {
      $max_dims[$ii] = abs($out[$ii][1]) if ($max_dims[$ii] > abs($out[$ii][1]));
    }
  }
}

foreach (@outarray) {
  @out = @{$_};

  foreach $ii (0..$#out) {
    print(sprintf("%$flushlr[$ii]${max_dims[$ii]}.${max_dims[$ii]}s  ", $out[$ii][0]));
  }
  print("\n");
}
