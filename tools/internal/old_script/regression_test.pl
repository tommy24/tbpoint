#!/usr/bin/perl


use Getopt::Long;


die "You need to set up \$SIMDIR\n" unless $ENV{'SIMDIR'};
die "You need to specify 'macsim' binary\n" if (!-e "./macsim");
require ($ENV{'SIMDIR'} . "/tools/bench_common");
require ($ENV{'SIMDIR'} . "/tools/regression_param/data.pl");


$G_USAGE  = "** How to generate regression test data\n";
$G_USAGE .= "1. You should run this in your 'macsim' directory unless it failed.\n";
$G_USAGE .= "2. USAGE => regression_gen.pl\n";
$G_USAGE .= "*. if you need to add more benches in the regression test,\n";
$G_USAGE .= "   you need to define \$PARAM and \$INST_COUNT yourself.\n";


GetOptions(
  "help"    => \$G_HELP,
  "suite=s" => \$G_BENCH,
);


if ($G_HELP) {
  print "$G_USAGE";
  exit 0;
}



if (!$G_BENCH) {
  $G_BENCH = "$SUITES{'spec06'} $SUITES{'cuda'}";
} else {
	die "You need to define $G_BENCH suite\n" if (!defined($SUITES{$G_BENCH}));
  $G_BENCH = "$SUITES{$G_BENCH}"; 
}


@SUITE_TEMP = split(/ /, $SUITES{'cuda'});
foreach (@SUITE_TEMP) {
  $PARAM{$_} = "params_8800gt";
}

@SUITE_TEMP = split(/ /, $SUITES{'spec06'});
foreach (@SUITE_TEMP) {
  $PARAM{$_} = "params_x86";
}

@G_SUITE     = split(/ /, $G_BENCH);
$G_DATA_FILE = "$ENV{'SIMDIR'}/tools/regression_param/data.pl"; 
$G_PARAM_DIR = "$ENV{'SIMDIR'}/tools/regression_param";
$G_DIRECTORY = "$ENV{'SIMDIR'}/tools/regression_data"; # regression directory
$G_TEST_DIR  = "/tmp/regression_" . getppid();


if (-d $G_TEST_DIR) {
  system "rm -rf $G_TEST_DIR";
}
system "mkdir -p $G_TEST_DIR";


$G_FAIL_COUNT = 0;
$G_SKIP_COUNT = 0;
foreach (@G_SUITE) {
  ## test-run
  die "Please define \$PARAM{$_}\n" if (!defined(${PARAM{$_}}));
  $param_file = "${G_PARAM_DIR}/$PARAM{$_}";
  $inst_count = "$INST_COUNT{$_}";

  $dir = "$G_TEST_DIR/$_";

  if (-d $dir) {
    system "rm -rf $dir";
  }

  system "mkdir -p $dir";
  system "cp $param_file $dir/params.in";
  system "cp ./macsim $dir";

  $cmd = "run_macsim +bin ./macsim $_ --sim_cycle_count=50000 --stdout=stdout";
  #$cmd = "run_macsim +bin ./macsim $_ --max_insts=$inst_count --stdout=stdout";
  if (!$G_PRINT) {
    $cmd .= " > /dev/null";
  }

  chdir "$dir";

  print "$cmd\n";
  system "$cmd";


  ## compare result
  @files = glob "*.stat.out";
  foreach $file (@files) {
    $compare_file = "$G_DIRECTORY/$_/$file";
    $compare_file =~ s/\@ref//;
    $diff = `diff $file $compare_file`;

#		$cyc_count_old = `cat $G_DIRECTORY/$_/general.stat.out \| grep CYC_COUNT_TOT \| awk {'print \$2'}`;
#		$cyc_count_new = `cat general.stat.out \| grep CYC_COUNT_TOT \| awk {'print \$2'}`;
#		print "$cyc_count_old $cyc_count_new\n";

    if ($diff) {
			@diff_line = split(/\n/, $diff);

#      if ($file eq "pref.stat.out") {
#        next;
#      }
			if ($file ne "general.stat.out" or $#diff_line != 3 and substr($diff_line[1], 0, 10) ne "< EXE_TIME") {
				print "test failed: $_ ($file)\n";
        print "$diff\n";
				$G_FAIL_COUNT++;
				last;
			}
    }
  }
}


## cleanup directory
system "rm -rf /tmp/regression";


if ($G_FAIL_COUNT == 0) {
  print "Test successful ($G_SKIP_COUNT skipped)\n";
} else {
  printf "Total $G_FAIL_COUNT (out of %d, $G_SKIP_COUNT skipped) failed\n", ($#G_SUITE + 1);
}
