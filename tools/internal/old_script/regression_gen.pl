#!/usr/bin/perl


use Time::HiRes qw(gettimeofday);
use Getopt::Long;


die "You need to set up \$SIMDIR\n" unless $ENV{'SIMDIR'};
die "You need to specify 'macsim' binary\n" if (!-e "./macsim");
require ($ENV{'SIMDIR'} . "/tools/bench_common");
require ($ENV{'SIMDIR'} . "/tools/regression_param/data.pl");


$G_USAGE  = "** How to generate regression test data\n";
$G_USAGE .= "1. You should run this in your 'macsim' directory unless it failed.\n";
$G_USAGE .= "2. USAGE => regression_gen.pl\n";
$G_USAGE .= "*. if you need to add more benches in the regression test,\n";
$G_USAGE .= "   you need to define \$PARAM and \$INST_COUNT yourself.\n";


GetOptions(
  "help"    => \$G_HELP,
  "suite=s" => \$G_BENCH,
);


if ($G_HELP) {
  print "$G_USAGE";
  exit 0;
}


#die "You need to define $G_BENCH suite\n" if (!defined($SUITES{$G_BENCH}));


if (!$G_BENCH) {
  $G_BENCH = "$SUITES{'cuda'} $SUITES{'spec06'}";
} else {
  $G_BENCH = "$SUITES{$G_BENCH}"; 
}

@SUITE_TEMP = split(/ /, $SUITES{'cuda'});
foreach (@SUITE_TEMP) {
  $PARAM{$_} = "params_8800gt";
}

@SUITE_TEMP = split(/ /, $SUITES{'spec06'});
foreach (@SUITE_TEMP) {
  $PARAM{$_} = "params_x86";
}


@G_SUITE     = split(/ /, $G_BENCH);
$G_DATA_FILE = "$ENV{'SIMDIR'}/tools/regression_param/data.pl"; 
$G_PARAM_DIR = "$ENV{'SIMDIR'}/tools/regression_param";
$G_DIRECTORY = "$ENV{'SIMDIR'}/tools/regression_data"; # regression directory


foreach (@G_SUITE) {
  if (!defined(${PARAM{$_}})) {
    print "Please define \$PARAM{$_} for bench $_\n";
    next;
  }
  $param_file = "${G_PARAM_DIR}/$PARAM{$_}";
  #$inst_count = "$INST_COUNT{$_}";

  $dir = "$G_DIRECTORY/$_";
  $dir =~ s/\@ref//;

  if (-d $dir) {
    system "rm -rf $dir";
  }

  system "mkdir -p $dir";
  system "cp $param_file $dir/params.in";
  system "cp ./macsim $dir";

  $cmd = "run_macsim +bin ./macsim $_ --sim_cycle_count=50000 --stdout=stdout";
  #$cmd = "run_macsim +bin ./macsim $_ --max_insts=$inst_count --stdout=stdout";
  if (!$G_PRINT) {
    $cmd .= " > /dev/null";
  }

  chdir "$dir";

  print "$cmd\n";
  $before = gettimeofday();
  system "$cmd";
  $elapsed = gettimeofday() - $before;

  print "$elapsed seconds\n";
}
