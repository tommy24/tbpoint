#!/usr/bin/perl -w

# **************************************************************************************
# CVS          : $Id: bench_common,v 1.1 2008-08-14 03:44:52 hyesoon Exp $:
# **************************************************************************************

# This file is common benchmark names/input information


# ******************************************************************************
# Global Variables (taken from environment variables)
# ******************************************************************************

$BENCHMARKS = "/hparch/benchmarks";

if ($ENV{'BENCHMARKS'}) {
    $BENCHMARKS = $ENV{'BENCHMARKS'};
}

$SPEC00	= "$BENCHMARKS/cpu2000/benchspec";
$SPEC95 = "$BENCHMARKS/spec95";
$JBS	= "$BENCHMARKS/jbench";
$OLDEN  = "$BENCHMARKS/olden";
$OBS	= "$BENCHMARKS/obench";
$CPPBS	= "$BENCHMARKS/c++";
$SPEC06 = "$BENCHMARKS/cpu2006/benchspec";
$NPB    = "$BENCHMARKS/npb"; 


# ******************************************************************************
# Benchmark Subroutines
#
# Some useful subroutines to access the information in this file.
# ******************************************************************************

# {{{ Benchmark Subroutines

sub print_benchmarks {
    die if ($#_ > 0);
    local($kk, @b_line, $last_b_name, $stream);
    $stream = $_[0];
    print($stream "Benchmarks:");
    foreach $kk (sort(keys(%EXTRA_CODE))) {
	@b_line = split(/@/, $kk);
	if ($b_line[0] eq $last_b_name) {
	    printf($stream "$b_line[1] ");
	} else {
	    printf($stream "\n%15s --- ", $b_line[0]);
	}
	$last_b_name = $b_line[0];
    }
    print($stream "\n");
}

sub print_sets {
    die if ($#_ > 0);
    local($kk, $g_line, $stream);
    $stream = $_[0];
    print($stream "Benchmarks sets (input is std unless specified):\n");
    foreach $kk (sort(keys(%SUITES))) {
	$g_line = "$SUITES{$kk}";
	$g_line =~ s/\@std//g;
	printf($stream "%15s --- $g_line\n", $kk);
    }
    print($stream "\n");
}

# }}}


# ******************************************************************************
# Benchmark Sets
#
# Run_batch uses these to launchdifferent groups of benchmarks.  The
# 'std' input set can be overridden by specifying <suite>@<set> on the run_batch
# command line.
# ******************************************************************************

# {{{ Benchmark Sets
## CAUTION: DO NOT PUT MORE THAN ONE WHITESPACE!!! 

%SUITES = ();


$SUITES{'cuda'} = 'BicubicTexture@ref BlackScholes1@ref ConvolutionSeparable@ref ConvolutionTexture@ref Dct8x8@ref Dxtc1@ref FastWalshTransform@ref Histogram2561@ref ImageDenoising1@ref MergeSort@ref MonteCarlo1@ref QuasirandomGenerator1@ref Reduction1@ref SobelFilter1@ref SobolQRNG1@ref ScalarProd1@ref VolumeRender@ref';

$SUITES{'rodinia'} =  'backPropagation@ref cell@ref hotspot@ref cfd@ref nearest_neighbor@ref bfs@ref needlemanWunsch@ref';

$SUITES{'erc'} =  'RayTracing@ref SAD@ref SHA1@ref AES@ref';

$SUITES{'parboil'} = 'lbm-gpu@ref fft@ref mm@ref stencil@ref';

#$SUITES{'remain'} = "$SUITES{'parboil'} RecursiveGaussian@ref FastWalshTransform1@ref ConvolutionSeparable1@ref ConvolutionTexture1@ref MergeSort@ref cfd@ref heartwall@ref bfs@ref RadixSort3@ref"; 
$SUITES{'remain'} = 'histo@ref';

$SUITES{'micro11'} = "$SUITES{'cuda'} $SUITES{'rodinia'} $SUITES{'erc'} $SUITES{'parboil'}";

$SUITES{'spec06int'} = 'perlbench@ref bzip2@ref gcc@ref mcf@ref gobmk@ref hmmer@ref sjeng@ref libquantum@ref h264ref@ref omnetpp@ref astar@ref xalancbmk@ref';
$SUITES{'spec06fp'} = 'bwaves@ref gamess@ref milc@ref zeusmp@ref gromacs@ref cactusADM@ref leslie3d@ref namd@ref dealII@ref soplex@ref povray@ref calculix@ref GemsFDTD@ref tonto@ref lbm@ref wrf@ref sphinx3@ref';
$SUITES{'spec06'} = "$SUITES{'spec06int'} $SUITES{'spec06fp'}";

#$SUITES{'hetero'} = 'omnetpp@ref+Dct8x8@ref omnetpp@ref+ConvolutionSeparable@ref wrf@ref+Dct8x8@ref wrf@ref+ConvolutionSeparable@ref';
#$SUITES{'hetero'} = 'omnetpp_sad@ref omnetpp_stencil@ref omnetpp_needlemanWunsch@ref soplex_sad@ref soplex_stencil@ref soplex_needlemanWunsch@ref wrf_sad@ref wrf_stencil@ref wrf_needlemanWunsch@ref mcf_sad@ref mcf_stencil@ref mcf_needlemanWunsch@ref leslie3d_sad@ref leslie3d_stencil@ref leslie3d_needlemanWunsch@ref sphinx3_sad@ref sphinx3_stencil@ref sphinx3_needlemanWunsch@ref';
$SUITES{'hetero'} = 'perlbench_needlemanWunsch@ref bzip2_needlemanWunsch@ref gcc_needlemanWunsch@ref mcf_needlemanWunsch@ref gobmk_needlemanWunsch@ref hmmer_needlemanWunsch@ref sjeng_needlemanWunsch@ref libquantum_needlemanWunsch@ref h264ref_needlemanWunsch@ref omnetpp_needlemanWunsch@ref astar_needlemanWunsch@ref xalancbmk_needlemanWunsch@ref bwaves_needlemanWunsch@ref gamess_needlemanWunsch@ref milc_needlemanWunsch@ref zeusmp_needlemanWunsch@ref gromacs_needlemanWunsch@ref cactusADM_needlemanWunsch@ref leslie3d_needlemanWunsch@ref namd_needlemanWunsch@ref dealII_needlemanWunsch@ref soplex_needlemanWunsch@ref povray_needlemanWunsch@ref calculix_needlemanWunsch@ref GemsFDTD_needlemanWunsch@ref tonto_needlemanWunsch@ref lbm_needlemanWunsch@ref wrf_needlemanWunsch@ref sphinx3_needlemanWunsch@ref';
$SUITES{'hetero1'} = 'perlbench_stencil@ref bzip2_stencil@ref gcc_stencil@ref mcf_stencil@ref gobmk_stencil@ref hmmer_stencil@ref sjeng_stencil@ref libquantum_stencil@ref h264ref_stencil@ref omnetpp_stencil@ref astar_stencil@ref xalancbmk_stencil@ref bwaves_stencil@ref gamess_stencil@ref milc_stencil@ref zeusmp_stencil@ref gromacs_stencil@ref cactusADM_stencil@ref leslie3d_stencil@ref namd_stencil@ref dealII_stencil@ref soplex_stencil@ref povray_stencil@ref calculix_stencil@ref GemsFDTD_stencil@ref tonto_stencil@ref lbm_stencil@ref wrf_stencil@ref sphinx3_stencil@ref';
$SUITES{'hetero2'} = 'perlbench_cfd@ref bzip2_cfd@ref gcc_cfd@ref mcf_cfd@ref gobmk_cfd@ref hmmer_cfd@ref sjeng_cfd@ref libquantum_cfd@ref h264ref_cfd@ref omnetpp_cfd@ref astar_cfd@ref xalancbmk_cfd@ref bwaves_cfd@ref gamess_cfd@ref milc_cfd@ref zeusmp_cfd@ref gromacs_cfd@ref cactusADM_cfd@ref leslie3d_cfd@ref namd_cfd@ref dealII_cfd@ref soplex_cfd@ref povray_cfd@ref calculix_cfd@ref GemsFDTD_cfd@ref tonto_cfd@ref lbm_cfd@ref wrf_cfd@ref sphinx3_cfd@ref';
$SUITES{'het'} = 'mcf_cfd@ref mcf_stencil@ref omnetpp_cfd@ref omnetpp_stencil@ref astar_cfd@ref astar_stencil@ref leslie3d_cfd@ref leslie3d_stencil@ref sphinx3_cfd@ref sphinx3_stencil@ref wrf_cfd@ref wrf_stencil@ref soplex_cfd@ref soplex_stencil@ref';
$SUITES{'ttt'} = 'ConvolutionSeparable@ref ConvolutionTexture@ref FastWalshTransform@ref SobelFilter1@ref ScalarProd1@ref heartwall@ref nearest_neighbor@ref bfs@ref hotspot@ref';
$SUITES{'tlp'} = 'SAD@ref';









##### 2 CPUs + 1 GPU
## 1
$SUITES{'cpu1gpua'} = 'soplex_gcc_Dxtc1@ref gcc_omnetpp_Dxtc1@ref sphinx3_soplex_cell@ref omnetpp_soplex_FastWalshTransform@ref soplex_gcc_Dxtc1@ref wrf_sphinx3_FastWalshTransform@ref leslie3d_bzip2_Dxtc1@ref wrf_lbm_cell@ref sphinx3_lbm_cell@ref gcc_lbm_FastWalshTransform@ref soplex_omnetpp_VolumeRender@ref soplex_omnetpp_cell@ref gcc_soplex_cell@ref soplex_sphinx3_Dxtc1@ref gcc_omnetpp_cell@ref';
$SUITES{'cpu1gpub'} = 'omnetpp_sphinx3_MergeSort@ref sphinx3_gcc_mm@ref omnetpp_sphinx3_SAD@ref soplex_omnetpp_mm@ref omnetpp_sphinx3_SAD@ref mcf_soplex_MergeSort@ref omnetpp_bzip2_MergeSort@ref leslie3d_wrf_hotspot@ref omnetpp_sphinx3_hotspot@ref leslie3d_soplex_MergeSort@ref soplex_omnetpp_stencil@ref gcc_soplex_SAD@ref omnetpp_sphinx3_MergeSort@ref soplex_sphinx3_MergeSort@ref gcc_sphinx3_hotspot@ref';
$SUITES{'cpu1gpuc'} = 'omnetpp_sphinx3_SobolQRNG1@ref gcc_sphinx3_SobolQRNG1@ref gcc_sphinx3_RayTracing@ref sphinx3_soplex_SobolQRNG1@ref gcc_soplex_QuasirandomGenerator1@ref leslie3d_soplex_SobolQRNG1@ref wrf_bzip2_SobolQRNG1@ref wrf_leslie3d_QuasirandomGenerator1@ref wrf_soplex_QuasirandomGenerator1@ref astar_wrf_SobolQRNG1@ref sphinx3_soplex_RayTracing@ref soplex_omnetpp_SobolQRNG1@ref gcc_soplex_SobolQRNG1@ref omnetpp_sphinx3_QuasirandomGenerator1@ref gcc_soplex_RayTracing@ref';
$SUITES{'cpu1gpud'} = 'gcc_sphinx3_Reduction1@ref gcc_omnetpp_AES@ref omnetpp_sphinx3_Reduction1@ref soplex_sphinx3_AES@ref omnetpp_soplex_Reduction1@ref astar_bzip2_Histogram2561@ref gcc_wrf_Reduction1@ref wrf_gcc_Histogram2561@ref sphinx3_bzip2_Histogram2561@ref bzip2_mcf_AES@ref gcc_omnetpp_Reduction1@ref omnetpp_gcc_BlackScholes1@ref soplex_sphinx3_Histogram2561@ref sphinx3_gcc_Reduction1@ref omnetpp_soplex_Reduction1@ref';
$SUITES{'cpu1gpue'} = 'gcc_soplex_bfs@ref omnetpp_gcc_lbm-gpu@ref omnetpp_gcc_bfs@ref soplex_omnetpp_lbm-gpu@ref sphinx3_soplex_cfd@ref soplex_lbm_cfd@ref astar_sphinx3_cfd@ref leslie3d_soplex_lbm-gpu@ref omnetpp_astar_bfs@ref lbm_soplex_cfd@ref soplex_sphinx3_lbm-gpu@ref omnetpp_gcc_cfd@ref gcc_sphinx3_bfs@ref soplex_gcc_bfs@ref soplex_sphinx3_cfd@ref';
$SUITES{'cpu2gpua'} = 'omnetpp_namd_VolumeRender@ref sphinx3_hmmer_FastWalshTransform@ref omnetpp_hmmer_Dxtc1@ref omnetpp_xalancbmk_Dxtc1@ref omnetpp_povray_VolumeRender@ref sphinx3_h264ref_cell@ref soplex_povray_cell@ref bzip2_gamess_VolumeRender@ref mcf_gamess_Dxtc1@ref astar_calculix_Dxtc1@ref omnetpp_hmmer_VolumeRender@ref soplex_gromacs_Dxtc1@ref gcc_gobmk_Dxtc1@ref omnetpp_namd_FastWalshTransform@ref gcc_gobmk_Dxtc1@ref';
$SUITES{'cpu2gpub'} = 'sphinx3_sjeng_SAD@ref omnetpp_povray_MergeSort@ref omnetpp_povray_stencil@ref soplex_gobmk_mm@ref soplex_gamess_MergeSort@ref gcc_calculix_hotspot@ref bzip2_tonto_stencil@ref wrf_xalancbmk_stencil@ref mcf_hmmer_stencil@ref gcc_hmmer_mm@ref omnetpp_calculix_SAD@ref omnetpp_povray_stencil@ref soplex_gamess_stencil@ref gcc_hmmer_SAD@ref gcc_xalancbmk_MergeSort@ref';
$SUITES{'cpu2gpuc'} = 'soplex_tonto_SobolQRNG1@ref gcc_dealII_RayTracing@ref sphinx3_namd_RayTracing@ref omnetpp_gromacs_QuasirandomGenerator1@ref sphinx3_namd_SobolQRNG1@ref gcc_povray_RayTracing@ref wrf_xalancbmk_RayTracing@ref astar_gobmk_QuasirandomGenerator1@ref soplex_perlbench_QuasirandomGenerator1@ref wrf_calculix_QuasirandomGenerator1@ref gcc_perlbench_RayTracing@ref soplex_calculix_RayTracing@ref omnetpp_povray_QuasirandomGenerator1@ref sphinx3_gobmk_SobolQRNG1@ref gcc_sjeng_RayTracing@ref';
$SUITES{'cpu2gpud'} = 'soplex_gamess_AES@ref gcc_h264ref_Reduction1@ref omnetpp_h264ref_BlackScholes1@ref soplex_sjeng_Reduction1@ref sphinx3_povray_Histogram2561@ref sphinx3_perlbench_BlackScholes1@ref leslie3d_h264ref_AES@ref astar_h264ref_BlackScholes1@ref gcc_xalancbmk_AES@ref sphinx3_sjeng_AES@ref sphinx3_gamess_AES@ref sphinx3_tonto_AES@ref soplex_dealII_Reduction1@ref soplex_calculix_Histogram2561@ref soplex_hmmer_Histogram2561@ref';
$SUITES{'cpu2gpue'} = 'omnetpp_perlbench_cfd@ref omnetpp_hmmer_bfs@ref soplex_namd_lbm-gpu@ref sphinx3_dealII_lbm-gpu@ref omnetpp_calculix_cfd@ref gcc_tonto_lbm-gpu@ref wrf_namd_cfd@ref wrf_namd_cfd@ref lbm_tonto_lbm-gpu@ref astar_perlbench_bfs@ref omnetpp_hmmer_bfs@ref soplex_povray_bfs@ref gcc_dealII_lbm-gpu@ref omnetpp_dealII_lbm-gpu@ref sphinx3_namd_cfd@ref';
#$SUITES{'cpu3gpua'} = 'gromacs_tonto_cell@ref gobmk_calculix_cell@ref gromacs_gobmk_cell@ref hmmer_tonto_VolumeRender@ref dealII_perlbench_FastWalshTransform@ref';
#$SUITES{'cpu3gpub'} = 'hmmer_povray_hotspot@ref tonto_povray_stencil@ref gamess_gromacs_mm@ref namd_sjeng_mm@ref gobmk_xalancbmk_SAD@ref';
#$SUITES{'cpu3gpuc'} = 'calculix_dealII_RayTracing@ref gamess_hmmer_RayTracing@ref dealII_gromacs_SobolQRNG1@ref gromacs_xalancbmk_SobolQRNG1@ref gromacs_dealII_QuasirandomGenerator1@ref';
#$SUITES{'cpu3gpud'} = 'h264ref_tonto_Histogram2561@ref povray_perlbench_BlackScholes1@ref h264ref_dealII_Histogram2561@ref xalancbmk_namd_AES@ref gromacs_calculix_AES@ref';
#$SUITES{'cpu3gpue'} = 'dealII_tonto_lbm-gpu@ref h264ref_namd_bfs@ref gobmk_perlbench_cfd@ref gromacs_gamess_cfd@ref namd_calculix_bfs@ref';
## 2
#$SUITES{'cpu1gpua'} = 'wrf_sphinx3_FastWalshTransform@ref leslie3d_bzip2_Dxtc1@ref wrf_lbm_cell@ref sphinx3_lbm_cell@ref gcc_lbm_FastWalshTransform@ref';
#$SUITES{'cpu1gpub'} = 'mcf_soplex_MergeSort@ref omnetpp_bzip2_MergeSort@ref leslie3d_wrf_hotspot@ref omnetpp_sphinx3_hotspot@ref leslie3d_soplex_MergeSort@ref';
#$SUITES{'cpu1gpuc'} = 'leslie3d_soplex_SobolQRNG1@ref wrf_bzip2_SobolQRNG1@ref wrf_leslie3d_QuasirandomGenerator1@ref wrf_soplex_QuasirandomGenerator1@ref astar_wrf_SobolQRNG1@ref';
#$SUITES{'cpu1gpud'} = 'astar_bzip2_Histogram2561@ref gcc_wrf_Reduction1@ref wrf_gcc_Histogram2561@ref sphinx3_bzip2_Histogram2561@ref bzip2_mcf_AES@ref';
#$SUITES{'cpu1gpue'} = 'soplex_lbm_cfd@ref astar_sphinx3_cfd@ref leslie3d_soplex_lbm-gpu@ref omnetpp_astar_bfs@ref lbm_soplex_cfd@ref';
#$SUITES{'cpu2gpua'} = 'sphinx3_h264ref_cell@ref soplex_povray_cell@ref bzip2_gamess_VolumeRender@ref mcf_gamess_Dxtc1@ref astar_calculix_Dxtc1@ref';
#$SUITES{'cpu2gpub'} = 'gcc_calculix_hotspot@ref bzip2_tonto_stencil@ref wrf_xalancbmk_stencil@ref mcf_hmmer_stencil@ref gcc_hmmer_mm@ref';
#$SUITES{'cpu2gpuc'} = 'gcc_povray_RayTracing@ref wrf_xalancbmk_RayTracing@ref astar_gobmk_QuasirandomGenerator1@ref soplex_perlbench_QuasirandomGenerator1@ref wrf_calculix_QuasirandomGenerator1@ref';
#$SUITES{'cpu2gpud'} = 'sphinx3_perlbench_BlackScholes1@ref leslie3d_h264ref_AES@ref astar_h264ref_BlackScholes1@ref gcc_xalancbmk_AES@ref sphinx3_sjeng_AES@ref';
#$SUITES{'cpu2gpue'} = 'gcc_tonto_lbm-gpu@ref wrf_namd_cfd@ref wrf_namd_cfd@ref lbm_tonto_lbm-gpu@ref astar_perlbench_bfs@ref';
#$SUITES{'cpu3gpua'} = 'gobmk_sjeng_Dxtc1@ref namd_hmmer_Dxtc1@ref tonto_gromacs_cell@ref h264ref_sjeng_cell@ref hmmer_sjeng_FastWalshTransform@ref';
#$SUITES{'cpu3gpub'} = 'xalancbmk_dealII_mm@ref sjeng_povray_stencil@ref gamess_perlbench_SAD@ref xalancbmk_gobmk_stencil@ref perlbench_dealII_mm@ref';
#$SUITES{'cpu3gpuc'} = 'h264ref_calculix_RayTracing@ref perlbench_sjeng_SobolQRNG1@ref povray_gamess_SobolQRNG1@ref perlbench_gamess_SobolQRNG1@ref calculix_xalancbmk_QuasirandomGenerator1@ref';
#$SUITES{'cpu3gpud'} = 'tonto_namd_BlackScholes1@ref h264ref_hmmer_BlackScholes1@ref gromacs_dealII_AES@ref dealII_perlbench_Reduction1@ref dealII_tonto_BlackScholes1@ref';
#$SUITES{'cpu3gpue'} = 'tonto_sjeng_bfs@ref povray_calculix_bfs@ref tonto_xalancbmk_lbm-gpu@ref perlbench_xalancbmk_bfs@ref tonto_h264ref_bfs@ref';
## 3
#$SUITES{'cpu1gpua'} = 'soplex_omnetpp_VolumeRender@ref soplex_omnetpp_cell@ref gcc_soplex_cell@ref soplex_sphinx3_Dxtc1@ref gcc_omnetpp_cell@ref';
#$SUITES{'cpu1gpub'} = 'soplex_omnetpp_stencil@ref gcc_soplex_SAD@ref omnetpp_sphinx3_MergeSort@ref soplex_sphinx3_MergeSort@ref gcc_sphinx3_hotspot@ref';
#$SUITES{'cpu1gpuc'} = 'sphinx3_soplex_RayTracing@ref soplex_omnetpp_SobolQRNG1@ref gcc_soplex_SobolQRNG1@ref omnetpp_sphinx3_QuasirandomGenerator1@ref gcc_soplex_RayTracing@ref';
#$SUITES{'cpu1gpud'} = 'gcc_omnetpp_Reduction1@ref omnetpp_gcc_BlackScholes1@ref soplex_sphinx3_Histogram2561@ref sphinx3_gcc_Reduction1@ref omnetpp_soplex_Reduction1@ref';
#$SUITES{'cpu1gpue'} = 'soplex_sphinx3_lbm-gpu@ref omnetpp_gcc_cfd@ref gcc_sphinx3_bfs@ref soplex_gcc_bfs@ref soplex_sphinx3_cfd@ref';
#$SUITES{'cpu2gpua'} = 'omnetpp_hmmer_VolumeRender@ref soplex_gromacs_Dxtc1@ref gcc_gobmk_Dxtc1@ref omnetpp_namd_FastWalshTransform@ref gcc_gobmk_Dxtc1@ref';
#$SUITES{'cpu2gpub'} = 'omnetpp_calculix_SAD@ref omnetpp_povray_stencil@ref soplex_gamess_stencil@ref gcc_hmmer_SAD@ref gcc_xalancbmk_MergeSort@ref';
#$SUITES{'cpu2gpuc'} = 'gcc_perlbench_RayTracing@ref soplex_calculix_RayTracing@ref omnetpp_povray_QuasirandomGenerator1@ref sphinx3_gobmk_SobolQRNG1@ref gcc_sjeng_RayTracing@ref';
#$SUITES{'cpu2gpud'} = 'sphinx3_gamess_AES@ref sphinx3_tonto_AES@ref soplex_dealII_Reduction1@ref soplex_calculix_Histogram2561@ref soplex_hmmer_Histogram2561@ref';
#$SUITES{'cpu2gpue'} = 'omnetpp_hmmer_bfs@ref soplex_povray_bfs@ref gcc_dealII_lbm-gpu@ref omnetpp_dealII_lbm-gpu@ref sphinx3_namd_cfd@ref';
#$SUITES{'cpu3gpua'} = 'h264ref_dealII_Dxtc1@ref dealII_povray_VolumeRender@ref namd_gobmk_VolumeRender@ref gromacs_perlbench_cell@ref dealII_gobmk_FastWalshTransform@ref';
#$SUITES{'cpu3gpub'} = 'tonto_gamess_SAD@ref gromacs_gamess_hotspot@ref gromacs_calculix_hotspot@ref h264ref_povray_MergeSort@ref sjeng_hmmer_mm@ref';
#$SUITES{'cpu3gpuc'} = 'gromacs_h264ref_RayTracing@ref hmmer_sjeng_RayTracing@ref tonto_perlbench_RayTracing@ref hmmer_gromacs_QuasirandomGenerator1@ref gromacs_gobmk_QuasirandomGenerator1@ref';
#$SUITES{'cpu3gpud'} = 'h264ref_hmmer_Reduction1@ref dealII_perlbench_Histogram2561@ref h264ref_gromacs_Reduction1@ref sjeng_perlbench_Histogram2561@ref gobmk_sjeng_Reduction1@ref';
#$SUITES{'cpu3gpue'} = 'h264ref_povray_lbm-gpu@ref perlbench_gobmk_lbm-gpu@ref namd_xalancbmk_cfd@ref tonto_hmmer_cfd@ref gromacs_perlbench_lbm-gpu@ref';

$SUITES{'mix2cpu1'} = "$SUITES{'cpu1gpua'} $SUITES{'cpu1gpub'} $SUITES{'cpu1gpuc'} $SUITES{'cpu1gpud'} $SUITES{'cpu1gpue'}";
$SUITES{'mix2cpu2'} = "$SUITES{'cpu2gpua'} $SUITES{'cpu2gpub'} $SUITES{'cpu2gpuc'} $SUITES{'cpu2gpud'} $SUITES{'cpu2gpue'}";
#$SUITES{'cpu3'} = "$SUITES{'cpu3gpua'} $SUITES{'cpu3gpub'} $SUITES{'cpu3gpuc'} $SUITES{'cpu3gpud'} $SUITES{'cpu3gpue'}";
$SUITES{'mix2'} = "$SUITES{'mix2cpu1'} $SUITES{'mix2cpu2'}";
#$SUITES{'cpu2gpu'} = "$SUITES{'cpu1'} $SUITES{'cpu2'} $SUITES{'cpu3'}";
##### 2 CPUs + 1 GPU



##### 1 CPU + 1 GPU
$SUITES{'memgpub'} = 'soplex_BicubicTexture@ref soplex_ConvolutionTexture@ref soplex_FastWalshTransform@ref soplex_SobelFilter1@ref soplex_VolumeRender@ref soplex_hotspot@ref soplex_mm@ref soplex_SAD@ref sphinx3_BicubicTexture@ref sphinx3_ConvolutionTexture@ref sphinx3_FastWalshTransform@ref sphinx3_SobelFilter1@ref sphinx3_VolumeRender@ref sphinx3_hotspot@ref sphinx3_mm@ref sphinx3_SAD@ref omnetpp_BicubicTexture@ref omnetpp_ConvolutionTexture@ref omnetpp_FastWalshTransform@ref omnetpp_SobelFilter1@ref omnetpp_VolumeRender@ref omnetpp_hotspot@ref omnetpp_mm@ref omnetpp_SAD@ref';
#$SUITES{'typee'} = '';
$SUITES{'memgpub'} = 'gcc_SobelFilter1@ref gcc_hotspot@ref gcc_mm@ref omnetpp_SobelFilter1@ref omnetpp_hotspot@ref omnetpp_mm@ref soplex_SobelFilter1@ref soplex_hotspot@ref soplex_mm@ref sphinx3_SobelFilter1@ref sphinx3_hotspot@ref sphinx3_mm@ref';
$SUITES{'memgpub'} = 'gcc_stencil@ref gcc_mm@ref gcc_SAD@ref gcc_hotspot@ref gcc_MergeSort@ref omnetpp_stencil@ref omnetpp_mm@ref omnetpp_SAD@ref omnetpp_hotspot@ref omnetpp_MergeSort@ref soplex_stencil@ref soplex_mm@ref soplex_SAD@ref soplex_hotspot@ref soplex_MergeSort@ref sphinx3_stencil@ref sphinx3_mm@ref sphinx3_SAD@ref sphinx3_hotspot@ref sphinx3_MergeSort@ref';
$SUITES{'memgpue'} = 'gcc_cfd@ref gcc_bfs@ref gcc_lbm-gpu@ref omnetpp_cfd@ref omnetpp_bfs@ref omnetpp_lbm-gpu@ref soplex_cfd@ref soplex_bfs@ref soplex_lbm-gpu@ref sphinx3_cfd@ref sphinx3_bfs@ref sphinx3_lbm-gpu@ref';
$SUITES{'memgpuc'} = 'gcc_QuasirandomGenerator1@ref gcc_SobolQRNG1@ref gcc_RayTracing@ref omnetpp_QuasirandomGenerator1@ref omnetpp_SobolQRNG1@ref omnetpp_RayTracing@ref soplex_QuasirandomGenerator1@ref soplex_SobolQRNG1@ref soplex_RayTracing@ref sphinx3_QuasirandomGenerator1@ref sphinx3_SobolQRNG1@ref sphinx3_RayTracing@ref';
$SUITES{'memgpua'} = 'gcc_cell@ref gcc_Dxtc1@ref gcc_FastWalshTransform@ref gcc_VolumeRender@ref omnetpp_cell@ref omnetpp_Dxtc1@ref omnetpp_FastWalshTransform@ref omnetpp_VolumeRender@ref soplex_cell@ref soplex_Dxtc1@ref soplex_FastWalshTransform@ref soplex_VolumeRender@ref sphinx3_cell@ref sphinx3_Dxtc1@ref sphinx3_FastWalshTransform@ref sphinx3_VolumeRender@ref';
$SUITES{'memgpud'} = 'gcc_BlackScholes1@ref gcc_Histogram2561@ref gcc_Reduction1@ref gcc_AES@ref omnetpp_BlackScholes1@ref omnetpp_Histogram2561@ref omnetpp_Reduction1@ref omnetpp_AES@ref soplex_BlackScholes1@ref soplex_Histogram2561@ref soplex_Reduction1@ref soplex_AES@ref sphinx3_BlackScholes1@ref sphinx3_Histogram2561@ref sphinx3_Reduction1@ref sphinx3_AES@ref';
$SUITES{'memgpu'} = "$SUITES{'memgpua'} $SUITES{'memgpub'} $SUITES{'memgpuc'} $SUITES{'memgpud'} $SUITES{'memgpue'}";


$SUITES{'compgpua'} = 'perlbench_FastWalshTransform@ref hmmer_FastWalshTransform@ref xalancbmk_cell@ref perlbench_Dxtc1@ref namd_FastWalshTransform@ref povray_Dxtc1@ref gromacs_cell@ref gobmk_Dxtc1@ref dealII_FastWalshTransform@ref tonto_cell@ref perlbench_cell@ref tonto_cell@ref hmmer_Dxtc1@ref gobmk_cell@ref povray_VolumeRender@ref gobmk_FastWalshTransform@ref xalancbmk_VolumeRender@ref tonto_cell@ref h264ref_FastWalshTransform@ref namd_cell@ref gromacs_cell@ref gobmk_Dxtc1@ref namd_FastWalshTransform@ref hmmer_cell@ref povray_FastWalshTransform@ref';
$SUITES{'compgpub'} = 'povray_stencil@ref sjeng_hotspot@ref dealII_stencil@ref sjeng_MergeSort@ref h264ref_stencil@ref povray_stencil@ref dealII_hotspot@ref gamess_stencil@ref xalancbmk_hotspot@ref xalancbmk_stencil@ref namd_MergeSort@ref calculix_hotspot@ref tonto_SAD@ref sjeng_MergeSort@ref perlbench_MergeSort@ref calculix_MergeSort@ref tonto_stencil@ref h264ref_stencil@ref xalancbmk_stencil@ref gromacs_stencil@ref namd_stencil@ref tonto_SAD@ref perlbench_MergeSort@ref gamess_MergeSort@ref perlbench_mm@ref';
$SUITES{'compgpuc'} = 'xalancbmk_SobolQRNG1@ref gromacs_SobolQRNG1@ref gobmk_QuasirandomGenerator1@ref povray_QuasirandomGenerator1@ref calculix_SobolQRNG1@ref gobmk_QuasirandomGenerator1@ref h264ref_QuasirandomGenerator1@ref gobmk_SobolQRNG1@ref tonto_QuasirandomGenerator1@ref sjeng_RayTracing@ref dealII_QuasirandomGenerator1@ref sjeng_SobolQRNG1@ref tonto_RayTracing@ref xalancbmk_SobolQRNG1@ref gromacs_QuasirandomGenerator1@ref perlbench_QuasirandomGenerator1@ref tonto_SobolQRNG1@ref tonto_RayTracing@ref namd_SobolQRNG1@ref hmmer_SobolQRNG1@ref povray_RayTracing@ref namd_SobolQRNG1@ref dealII_QuasirandomGenerator1@ref xalancbmk_SobolQRNG1@ref sjeng_RayTracing@ref';
$SUITES{'compgpud'} = 'xalancbmk_Histogram2561@ref namd_AES@ref tonto_Histogram2561@ref xalancbmk_Reduction1@ref sjeng_AES@ref dealII_Histogram2561@ref dealII_Histogram2561@ref gromacs_Histogram2561@ref sjeng_Histogram2561@ref xalancbmk_BlackScholes1@ref hmmer_Reduction1@ref tonto_AES@ref dealII_Histogram2561@ref calculix_Histogram2561@ref h264ref_BlackScholes1@ref sjeng_Reduction1@ref gromacs_Histogram2561@ref dealII_Reduction1@ref h264ref_Reduction1@ref xalancbmk_Histogram2561@ref perlbench_BlackScholes1@ref namd_Histogram2561@ref xalancbmk_BlackScholes1@ref perlbench_Histogram2561@ref namd_Reduction1@ref';
$SUITES{'compgpue'} = 'povray_cfd@ref gobmk_lbm-gpu@ref xalancbmk_lbm-gpu@ref hmmer_cfd@ref povray_lbm-gpu@ref sjeng_lbm-gpu@ref gamess_bfs@ref xalancbmk_bfs@ref calculix_bfs@ref calculix_lbm-gpu@ref hmmer_cfd@ref calculix_bfs@ref h264ref_bfs@ref dealII_cfd@ref tonto_bfs@ref h264ref_bfs@ref sjeng_cfd@ref sjeng_lbm-gpu@ref sjeng_bfs@ref sjeng_cfd@ref h264ref_cfd@ref perlbench_cfd@ref namd_lbm-gpu@ref xalancbmk_cfd@ref namd_bfs@ref';

#$SUITES{'compgpu'} = 'dealII_Reduction1@ref gromacs_SobolQRNG1@ref dealII_cfd@ref ';

$SUITES{'compgpu'} = "$SUITES{'compgpub'} $SUITES{'compgpuc'} $SUITES{'compgpud'} $SUITES{'compgpue'}";
#$SUITES{'compgpu'} = "$SUITES{'compgpua'} $SUITES{'compgpub'} $SUITES{'compgpuc'} $SUITES{'compgpud'} $SUITES{'compgpue'}";
$SUITES{'mix1'} = "$SUITES{'compgpu'} $SUITES{'memgpu'}";
##### 1 CPU + 1 GPU


##### 2 GPUs
$SUITES{'gpu2aa'} = 'FastWalshTransform_VolumeRender@ref FastWalshTransform_Dxtc1@ref VolumeRender_Dxtc1@ref VolumeRender_FastWalshTransform@ref VolumeRender_Dxtc1@ref';
$SUITES{'gpu2ab'} = 'FastWalshTransform_mm@ref VolumeRender_MergeSort@ref VolumeRender_hotspot@ref FastWalshTransform_hotspot@ref VolumeRender_MergeSort@ref';
$SUITES{'gpu2ac'} = 'Dxtc1_RayTracing@ref FastWalshTransform_QuasirandomGenerator1@ref VolumeRender_QuasirandomGenerator1@ref Dxtc1_SobolQRNG1@ref cell_RayTracing@ref';
$SUITES{'gpu2ad'} = 'Dxtc1_AES@ref Dxtc1_BlackScholes1@ref Dxtc1_Histogram2561@ref cell_AES@ref cell_BlackScholes1@ref';
$SUITES{'gpu2ae'} = 'VolumeRender_cfd@ref cell_cfd@ref cell_cfd@ref FastWalshTransform_bfs@ref VolumeRender_bfs@ref';
$SUITES{'gpu2bb'} = 'MergeSort_MergeSort@ref MergeSort_SAD@ref mm_mm@ref stencil_hotspot@ref hotspot_stencil@ref';
$SUITES{'gpu2bc'} = 'MergeSort_RayTracing@ref stencil_RayTracing@ref stencil_SobolQRNG1@ref hotspot_QuasirandomGenerator1@ref stencil_RayTracing@ref';
$SUITES{'gpu2bd'} = 'MergeSort_Reduction1@ref mm_Reduction1@ref MergeSort_Histogram2561@ref SAD_BlackScholes1@ref stencil_AES@ref';
$SUITES{'gpu2be'} = 'stencil_lbm-gpu@ref MergeSort_lbm-gpu@ref stencil_lbm-gpu@ref stencil_bfs@ref MergeSort_bfs@ref';
$SUITES{'gpu2cc'} = 'SobolQRNG1_QuasirandomGenerator1@ref RayTracing_SobolQRNG1@ref QuasirandomGenerator1_RayTracing@ref SobolQRNG1_SobolQRNG1@ref QuasirandomGenerator1_QuasirandomGenerator1@ref';
$SUITES{'gpu2cd'} = 'SobolQRNG1_Reduction1@ref RayTracing_Reduction1@ref RayTracing_Reduction1@ref SobolQRNG1_Reduction1@ref SobolQRNG1_Reduction1@ref';
$SUITES{'gpu2ce'} = 'QuasirandomGenerator1_cfd@ref RayTracing_lbm-gpu@ref SobolQRNG1_bfs@ref QuasirandomGenerator1_cfd@ref SobolQRNG1_bfs@ref';
$SUITES{'gpu2dd'} = 'Histogram2561_Histogram2561@ref Reduction1_BlackScholes1@ref AES_BlackScholes1@ref Histogram2561_AES@ref Reduction1_BlackScholes1@ref';
$SUITES{'gpu2de'} = 'BlackScholes1_bfs@ref AES_lbm-gpu@ref Reduction1_cfd@ref AES_cfd@ref Reduction1_bfs@ref';
$SUITES{'gpu2ee'} = 'cfd_bfs@ref cfd_lbm-gpu@ref bfs_bfs@ref cfd_cfd@ref lbm-gpu_cfd@ref';

$SUITES{'gpu2'} = "$SUITES{'gpu2aa'} $SUITES{'gpu2ab'} $SUITES{'gpu2ac'} $SUITES{'gpu2ad'} $SUITES{'gpu2ae'} $SUITES{'gpu2bb'} $SUITES{'gpu2bc'} $SUITES{'gpu2bd'} $SUITES{'gpu2be'} $SUITES{'gpu2cc'} $SUITES{'gpu2cd'} $SUITES{'gpu2cd'} $SUITES{'gpu2dd'} $SUITES{'gpu2de'} $SUITES{'gpu2ee'}";
##### 2 GPUs


##### 4 CPUs + 1 GPU
$SUITES{'mix4a'} = 'gcc_gcc_gamess_h264ref_VolumeRender@ref gcc_soplex_hmmer_povray_cell@ref gcc_soplex_hmmer_gromacs_cell@ref omnetpp_gcc_gobmk_gamess_FastWalshTransform@ref sphinx3_sphinx3_h264ref_povray_FastWalshTransform@ref soplex_omnetpp_omnetpp_calculix_FastWalshTransform@ref gcc_omnetpp_sphinx3_hmmer_VolumeRender@ref omnetpp_omnetpp_omnetpp_hmmer_VolumeRender@ref omnetpp_sphinx3_sphinx3_perlbench_FastWalshTransform@ref omnetpp_gcc_gcc_xalancbmk_FastWalshTransform@ref gcc_soplex_omnetpp_gcc_FastWalshTransform@ref sphinx3_gcc_gcc_sphinx3_FastWalshTransform@ref soplex_omnetpp_sphinx3_sphinx3_VolumeRender@ref sphinx3_omnetpp_gcc_gcc_FastWalshTransform@ref sphinx3_omnetpp_omnetpp_sphinx3_cell@ref';
$SUITES{'mix4b'} = 'sphinx3_sphinx3_dealII_gromacs_MergeSort@ref soplex_soplex_calculix_gamess_hotspot@ref gcc_soplex_hmmer_gamess_mm@ref omnetpp_omnetpp_povray_xalancbmk_MergeSort@ref gcc_soplex_gromacs_povray_stencil@ref sphinx3_omnetpp_omnetpp_perlbench_MergeSort@ref omnetpp_sphinx3_sphinx3_perlbench_stencil@ref soplex_omnetpp_sphinx3_hmmer_MergeSort@ref soplex_omnetpp_soplex_calculix_SAD@ref gcc_soplex_soplex_gromacs_stencil@ref gcc_sphinx3_sphinx3_sphinx3_MergeSort@ref sphinx3_gcc_omnetpp_soplex_mm@ref omnetpp_gcc_gcc_soplex_MergeSort@ref sphinx3_soplex_omnetpp_gcc_hotspot@ref omnetpp_omnetpp_gcc_omnetpp_SAD@ref';
$SUITES{'mix4c'} = 'sphinx3_omnetpp_perlbench_gobmk_QuasirandomGenerator1@ref omnetpp_soplex_hmmer_calculix_RayTracing@ref gcc_gcc_dealII_xalancbmk_SobolQRNG1@ref omnetpp_omnetpp_tonto_gromacs_QuasirandomGenerator1@ref sphinx3_gcc_gamess_gobmk_SobolQRNG1@ref soplex_soplex_sphinx3_xalancbmk_SobolQRNG1@ref gcc_gcc_gcc_xalancbmk_QuasirandomGenerator1@ref gcc_sphinx3_sphinx3_sjeng_SobolQRNG1@ref soplex_gcc_gcc_h264ref_RayTracing@ref sphinx3_omnetpp_soplex_tonto_RayTracing@ref omnetpp_omnetpp_gcc_gcc_SobolQRNG1@ref soplex_gcc_soplex_sphinx3_SobolQRNG1@ref omnetpp_gcc_omnetpp_gcc_SobolQRNG1@ref sphinx3_omnetpp_gcc_sphinx3_SobolQRNG1@ref omnetpp_soplex_sphinx3_sphinx3_RayTracing@ref';
$SUITES{'mix4d'} = 'sphinx3_omnetpp_hmmer_namd_BlackScholes1@ref soplex_sphinx3_perlbench_dealII_Reduction1@ref gcc_gcc_gamess_gobmk_Histogram2561@ref omnetpp_sphinx3_h264ref_h264ref_Histogram2561@ref omnetpp_gcc_namd_tonto_Reduction1@ref gcc_sphinx3_soplex_hmmer_BlackScholes1@ref soplex_omnetpp_gcc_gromacs_Reduction1@ref omnetpp_omnetpp_omnetpp_tonto_BlackScholes1@ref soplex_omnetpp_sphinx3_perlbench_AES@ref soplex_soplex_soplex_h264ref_BlackScholes1@ref omnetpp_sphinx3_sphinx3_soplex_AES@ref omnetpp_gcc_sphinx3_omnetpp_AES@ref sphinx3_soplex_omnetpp_gcc_Histogram2561@ref gcc_sphinx3_soplex_soplex_AES@ref omnetpp_soplex_omnetpp_gcc_Reduction1@ref';
$SUITES{'mix4e'} = 'sphinx3_sphinx3_calculix_tonto_cfd@ref sphinx3_gcc_dealII_povray_bfs@ref omnetpp_soplex_calculix_tonto_bfs@ref soplex_sphinx3_gobmk_namd_lbm-gpu@ref gcc_soplex_perlbench_gromacs_lbm-gpu@ref gcc_omnetpp_sphinx3_sjeng_cfd@ref omnetpp_gcc_soplex_namd_bfs@ref omnetpp_omnetpp_omnetpp_h264ref_cfd@ref soplex_omnetpp_soplex_xalancbmk_cfd@ref gcc_gcc_gcc_h264ref_bfs@ref sphinx3_omnetpp_sphinx3_soplex_lbm-gpu@ref soplex_gcc_omnetpp_sphinx3_bfs@ref soplex_omnetpp_gcc_omnetpp_lbm-gpu@ref soplex_omnetpp_gcc_gcc_lbm-gpu@ref gcc_gcc_soplex_soplex_lbm-gpu@ref';
$SUITES{'mix4'} = "$SUITES{'mix4a'} $SUITES{'mix4b'} $SUITES{'mix4c'} $SUITES{'mix4d'} $SUITES{'mix4e'}";
##### 4 CPUs + 1 GPU



#$SUITES{'streammix1a'} = 'cactusADM_FastWalshTransform@ref cactusADM_FastWalshTransform@ref bwaves_VolumeRender@ref bwaves_Dxtc1@ref cactusADM_Dxtc1@ref';
#$SUITES{'streammix1b'} = 'zeusmp_hotspot@ref cactusADM_hotspot@ref bwaves_stencil@ref bwaves_hotspot@ref cactusADM_stencil@ref';
#$SUITES{'streammix1c'} = 'cactusADM_SobolQRNG1@ref bwaves_SobolQRNG1@ref bwaves_SobolQRNG1@ref cactusADM_QuasirandomGenerator1@ref cactusADM_SobolQRNG1@ref';
#$SUITES{'streammix1d'} = 'zeusmp_AES@ref bwaves_AES@ref cactusADM_AES@ref cactusADM_Histogram2561@ref cactusADM_Histogram2561@ref';
#$SUITES{'streammix1e'} = 'cactusADM_bfs@ref bwaves_bfs@ref cactusADM_lbm-gpu@ref bwaves_bfs@ref bwaves_bfs@ref';
$SUITES{'streammix1a'} = 'merge-sepia_FastWalshTransform@ref merge-convolve_FastWalshTransform@ref merge-sepia_VolumeRender@ref merge-convolve_cell@ref merge-sepia_Dxtc1@ref';
$SUITES{'streammix1b'} = 'libquantum_SAD@ref merge-convolve_MergeSort@ref libquantum_mm@ref merge-convolve_SAD@ref merge-convolve_SAD@ref';
$SUITES{'streammix1c'} = 'libquantum_RayTracing@ref merge-convolve_RayTracing@ref merge-sepia_RayTracing@ref merge-convolve_QuasirandomGenerator1@ref merge-sepia_SobolQRNG1@ref';
$SUITES{'streammix1d'} = 'merge-sepia_AES@ref merge-convolve_Histogram2561@ref merge-sepia_AES@ref libquantum_BlackScholes1@ref merge-convolve_Reduction1@ref';
$SUITES{'streammix1e'} = 'milc_cfd@ref merge-convolve_bfs@ref milc_bfs@ref milc_cfd@ref merge-convolve_cfd@ref';
$SUITES{'streammix1'} = "$SUITES{'streammix1a'} $SUITES{'streammix1b'} $SUITES{'streammix1c'} $SUITES{'streammix1d'} $SUITES{'streammix1e'}";



$SUITES{'streamtest'} = 'cactusADM@ref bwaves@ref zeusmp@ref libquantum@ref milc@ref GemsFDTD@ref merge-svm@ref merge-binomial@ref merge-convolve@ref merge-linear@ref merge-sepia@ref merge-fmm@ref';


for (keys %SUITES) {
  print "SUITES[\'$_\'] = [";
  $line = $SUITES{$_};
  @list = split(/ /, $line);  
  $count = 0;
  foreach $bench (@list) {
    if ($count++ > 0) {
      print ", ";
    }
    print "\'$bench\'"
  }
  print "]\n";
}
