#!/usr/bin/perl
# **************************************************************************************
# CVS          : $Id: run_cmp_batch,v 1.1 2008-08-21 02:12:39 hyesoon Exp $:
# **************************************************************************************

die "You need to set the SIMDIR environment variable to the scarab base directory.\n" unless $ENV{'SIMDIR'};
#die "No pbs binaries.\n" unless -x "/usr/local/bin/qsub";
require ($ENV{'SIMDIR'} . "/bin/bench_common");
require ($ENV{'SIMDIR'} . "/bin/script_utils");


# **********************************************************************
# **********************************************************************
# Usage information

$USAGE = "Usage:  '$0 <group name> <benchmark or set> [run_batch arguments] [simulator arguments]'";

$SIMDIR		= "$ENV{'SIMDIR'}";
$PINDIR		= "$ENV{'PINDIR'}";

$QUEUE		= "hps";

if ($#ARGV < 1) {
    print(STDERR "$USAGE\n");

    print(STDERR "\t<group name>         : A string to identify this particular batch run (creates a subdirectory in the $SIMDIR/results directory)\n");
    print(STDERR "\t<benchmark or suite> : benchmarks to run (can be regular expression)\n\n");

    print(STDERR "\t++nomail             : don't get any mail from PBS\n");
    print(STDERR "\t++nosub              : don't submit the job to PBS\n");
    print(STDERR "\t++bin <bin>          : specify a binary other than the default\n");
    print(STDERR "\t++print              : just print submit commands, don't actually submit\n");
    print(STDERR "\t++reuse              : reuse without asking for confirmation\n");
    print(STDERR "\t++replace            : replace without asking for confirmation\n");
    print(STDERR "\t++noreplace          : don't replace without asking for confirmation\n");
    print(STDERR "\t++queue <queue>      : specify a queue other than '$QUEUE'\n");
    print(STDERR "\t++params <file>      : copy a parameters file for use by all runs\n");
    print(STDERR "\t++bsubs <file>       : perl file containing benchmark hashes to specify substitutions for particular benchmarks\n");
    print(STDERR "\t++gzip               : gzip all .out files in output directory after run completes\n");
    print(STDERR "\t++gzbin              : gzip all binaries\n");
    print(STDERR "\t++read_ckp           : read checkpointed state or fast-forward (works only for spec00\@ref)\n");
    print(STDERR "\t++rpoint             : representative simulation points for reference input (works only for spec00\@ref)\n");
    print(STDERR "\t++pin                : running pin\n");
    print(STDERR "\t++pintool            : tool name for the pin\n");
    print(STDERR "\t++read_tr            : provide trace list for pin \n");
    print(STDERR "\t++bench_bin          : binary names for traces (needs a argument)\n");
    print(STDERR "\t++big_mem            : big memory memory jobs");
    print(STDERR "\n");

    print_sets(\*STDERR);
    exit(1);
}


# **********************************************************************
# Process arguments

$OS		= substr(`uname`, 0, length(`uname`) - 1);
$SHELL		= $ENV{'SHELL'};

$BINARY		= "$SIMDIR/bin/$OS/scarab";
$RESULTS	= "$SIMDIR/results";
$RESOURCES      = "";
$MAIL_OPT       = "-m ae";

$SUBMIT_CMD	= "\"/usr/bin/qsub -V \$MAIL_OPT -o \$pbs_outdir -e \$pbs_outdir -q \$QUEUE \$RESOURCES -N \$name\"";

$RUN_LOCAL_CMD	= "$SIMDIR/bin/run_local";

$RUN_LOCAL_ARGS	= "\"\$group_binary \$outdir \$GZIP cmp_scarab \$bench \$input \$nice \$subst_simopt \$no_file_tag_opt --stdout=stdout --stderr=stderr\"";


$LRC_SIMDIR	= "/home/projects/patt/work/$ENV{'USER'}";
$LRC_RESULTS    = "$LRC_SIMDIR/results";

@OLD_ARGV     = @ARGV;
$GZIP         = 0;
$GZBIN        = 0;
$PRINT	      = 0;
$REUSE        = 0;
$REPLACE      = 0;
$NOREPLACE    = 0;

$RUN_PIN       = 0; 
$PIN_TOOL      = icount;
$BINARY_SET    = 0; 

$INPUT_CONFIRM = 1;

$found_group  = 0;
$found_set    = 0;
$simopt	      = "";
$param_file   = "";
$bsub_file    = "";

while (@ARGV) {
    $option = shift;

    if ($option eq "++nomail") {
	$MAIL_OPT = "-m n";
    } elsif ($option eq "++nosub") {
	$SUBMIT_CMD = "\"\"";
    } elsif ($option eq "++bin") {
	die "Option $option needs argument.\n" unless @ARGV;
	$BINARY = shift;
	$BINARY_SET = 1; 
    } elsif ($option eq "++print") {
        $PRINT = 1;
    } elsif ($option eq "++reuse") {
        $REUSE = 1;
    } elsif ($option eq "++replace") {
        $REPLACE = 1;
    } elsif ($option eq "++noreplace") {
        $NOREPLACE = 1;
    } elsif ($option eq "++queue") {
	die "Option $option needs argument.\n" unless @ARGV;
	$QUEUE = shift;
    } elsif ($option eq "++params") {
	die "Option $option needs argument.\n" unless @ARGV;
	$param_file = shift;
    } elsif ($option eq "++bsubs") {
	die "Option $option needs argument.\n" unless @ARGV;
	$bsub_file = shift;
    } elsif ($option eq "++gzip") {
	$GZIP = 1;
    } elsif ($option eq "++gzbin") {
	$GZBIN = 1;
    } elsif ($option eq "++read_ckp") {
	$simopt .= " +read_ckp";
    } elsif ($option eq "++rpoint") {
	$simopt .= " +rpoint";
    } elsif ($option eq "++pin") {
	$RUN_PIN = 1;
# 	$SUBMIT_CMD	= "\"/usr/bin/qsub -V \$MAIL_OPT -o \$pbs_outdir -e \$pbs_outdir -q \$QUEUE \$RESOURCES -l nodes=intel -N \$name\"";
    } elsif ($option eq "++pintool") {
	$RUN_PIN = 1;
	$PIN_TOOL = shift;
# 	$SUBMIT_CMD	= "\"/usr/bin/qsub -V \$MAIL_OPT -o \$pbs_outdir -e \$pbs_outdir -q \$QUEUE \$RESOURCES -l nodes=intel -N \$name\"";
	
    } elsif ($option eq "++read_tr") {
	$simopt .= " +read_tr";
    } elsif ($option eq "++bench_bin") {
	$simopt .= " +bench_bin ".shift;
    } elsif ($option eq "++big_mem") {
	$SUBMIT_CMD = "\"/usr/bin/qsub  -l pmem=3072m -V \$MAIL_OPT -o \$pbs_outdir -e \$pbs_outdir -q \$QUEUE \$RESOURCES -N \$name\"";
	#print("$SUBMIT_CMD\n");
    } elsif ($option eq "++nosubdirs") {
	die "ERROR: ++nosubdirs is no longer supported.\n";
    } elsif ($option eq "++exe_stem") {
	$EXE_STEM = shift;
    } elsif ($option eq "++exe_tail") {
	$EXE_TAIL = shift;
    } elsif (!$found_group) {
	$group = $option;
	$found_group = 1;
    } elsif (!$found_set) {
	$set = $option;
	$found_set = 1;
    } elsif ($option =~ /\+\+/) {
	die "ERROR: Invalid run_batch option '$option'.\n";
    } else {
        $simopt .= " $option";
    }
}
die "ERROR: Must supply run group to run.\n" unless ($found_group); 
die "ERROR: Must supply benchmark regexp or benchmark set to run.\n" unless ($found_set);
die "ERROR: Cannot use ++reuse and ++replace together.\n" if ($REUSE && $REPLACE);
die "ERROR: Cannot use ++noreplace and ++replace together.\n" if ($NOREPLACE && $REPLACE);

if ($RUN_PIN) {
    $RUN_LOCAL_ARGS	= "\"\$group_binary \$outdir \$GZIP pin \$bench \$input \$nice \$subst_simopt \$no_file_tag_opt  \"";
    if (!$BINARY_SET)  {
        # $BINARY ="$PINDIR/tools/$PIN_TOOL/$PIN_TOOL"; 
        $BINARY ="$PIN_TOOL"; 
	
    }
    $RESULTS	= "$PINDIR/results";
    # $SUBMIT_CMD	= "$SUBMIT_CMD -l nodes=intel";
}

# **********************************************************************
# create results directory tree

if (!$PRINT) {
    system("mkdir $RESULTS\n") unless (-d "$RESULTS");
    if (-d "$RESULTS/$group") {
	if (!$REUSE && !$REPLACE && !$NOREPLACE) {
	    do {
		print("WARNING: subdirectory '$group' already exists in '$RESULTS'.  Share directory (y/n)? ");
	    } while (($ans = <STDIN>) !~ /[ynYN]/);
	    exit(0) if ($ans =~ /[Nn]/);
	} elsif ($NOREPLACE) {
	    print("MESSAGE: subdirectory '$group' already exists.  Skipping due to ++noreplace.\n");
	    exit(0);
	}
 	system("ssh linux01.ece.utexas.edu mkdir -p $LRC_RESULTS/$group\n") if ($QUEUE eq "lrc");
    } else {
	system("mkdir $RESULTS/$group\n");
 	system("ssh linux01.ece.utexas.edu mkdir -p $LRC_RESULTS/$group\n") if ($QUEUE eq "lrc");
#  	system("ssh linux01.ece.utexas.edu '/bin/sh -c \"if [ -d $LRC_RESULTS/$group ]; then echo \"\"; else mkdir -p $LRC_RESULTS/$group ; fi\"\n") if ($QUEUE eq "lrc");
    }

    system("/bin/echo '$0 " . join(" ", @OLD_ARGV) . "' > $RESULTS/$group/BATCH_CMD\n");
}


# **********************************************************************
# build the runlist

if ($SUITES{(split(/@/, $set))[0]}) { # check for match on benchmark set
    local($suite, $input) = split(/@/, $set);
    local($str) = $SUITES{$suite};
    $str =~ s/std/$input/g if $input;
    @runlist = split(/ /, $str);
#      print("gat:'$str'\n");
#      grep { print "$_: $EXTRA_CODE{$_}\n" } @runlist;

    unless (grep(defined $EXTRA_CODE{$_}, @runlist) == @runlist) {
	if (!$INPUT_CONFIRM)  {
	    do {
		print("WARNING: Input set '$input' is not defined for all benchmarks in suite '$suite'\n");
		$, = ", ";
		print("runlist: "); print(grep(defined $EXTRA_CODE{$_}, @runlist)); print "\n";
		print("Continue? (y/n)\n");
	    } while (($ans = <STDIN>) !~ /[ynYN]/);
	}
	else {
	    $ans = y;
	}
	

	if ($ans =~ /[Yy]/) {
	    @runlist = grep(defined $EXTRA_CODE{$_}, @runlist);
	} else {
	    die "ERROR: Invalid run set specification.\n";
	}
    }

} elsif ($EXTRA_CODE{"$set"}) {	# check for individual benchmark@input 
    @runlist = ( "$set" );
} elsif ($set =~ /\+/) { #multi core simulation
    local ($benches, $input) =  split(/@/, $set);
    local @multi_list = split(/\+/, $benches);
    foreach $bench (@multi_list) {
	local $name = $bench."@".$input;
	#print "Searching $name\n";
	die "\tNo benchmarks match.\n" unless $EXTRA_CODE{"$name"};
    }
    @runlist = ( "$set" );
} else {			# look for regexp matches
    @runlist = ();
    print("Doing search for benchmarks matching '$set':\n");
    foreach $bench (sort keys %EXTRA_CODE) {
	if ($bench =~ /$set/) {
	    push(@runlist, $bench);
	} 
    }
    die "\tNo benchmarks match.\n" unless @runlist;
    foreach $bench (@runlist) {
	print("\t$bench\n");
    }
}


# **********************************************************************
# create group binary and params file

die("ERROR: Invalid source binary '$BINARY'.\n") unless -x $BINARY;
die("ERROR: Invalid params file '$param_file'.\n") unless -f $param_file || $param_file eq "";
die("ERROR: Invalid bench substitutions file '$bsub_file'.\n") unless -f $bsub_file || $bsub_file eq "";
die("ERROR: Must use ++gzbin with a gzipped ++bin.\n") if (!$GZBIN && ($BINARY =~ /\.gz$/));

require($bsub_file) if $bsub_file;

$source_binary    = $BINARY;
$group_binary	  = "$RESULTS/$group/BINARY.$group" . ($GZBIN ? ".gz" : "");
$lrc_group_binary = "$LRC_RESULTS/$group/BINARY.$group" . ($GZBIN ? ".gz" : "");
$tmp_gzip_binary  = "/tmp/BINARY.$ENV{USER}." . getppid();

if (!$PRINT) {
    if ($GZBIN && $source_binary !~ /\.gz$/) {
	print("Compressing temporary binary to $tmp_gzip_binary.\n");
	die if -f "$tmp_gzip_binary" || -f "$tmp_gzip_binary.bz";
	system("/bin/cp -f $source_binary $tmp_gzip_binary\n");
	system("/bin/gzip --best $tmp_gzip_binary\n");
	$source_binary = "$tmp_gzip_binary.gz";
    }

    if (-x $group_binary) {
	if (!$REUSE && !$REPLACE) {
	    do {
		print("WARNING: group binary already exists.  Use it? (y/n)? ");
	    } while (($ans = <STDIN>) !~ /[ynYN]/);
	}
	if ($REUSE || !$REPLACE && $ans =~ /[Yy]/) {
	    print("Using existing group binary.\n");
	    system("scp $group_binary linux01.ece.utexas.edu:$lrc_group_binary\n") if ($QUEUE eq "lrc");
	} else {
	    system("cp -f $source_binary $group_binary\n");
	    system("scp $source_binary linux01.ece.utexas.edu:$lrc_group_binary\n") if ($QUEUE eq "lrc");
	}
    } else {
	if (chk_file_zip($group_binary)) {
	    die("ERROR: Un/Compressed version of binary exists in run directory.\n");
	}

	system("cp -f $source_binary $group_binary\n");
	system("scp $source_binary linux01.ece.utexas.edu:$lrc_group_binary\n") if ($QUEUE eq "lrc");
    }

    if (-f "$RESULTS/$group/PARAMS.in") {
	if (!$REUSE && !$REPLACE) {
	    do {
		print("WARNING: group PARAMS.in already exists.  Use it? (y/n)? ");
	    } while (($ans = <STDIN>) !~ /[ynYN]/);
	}
	if ($REUSE || !$REPLACE && $ans =~ /[Yy]/) {
	    print("Using existing PARAMS.in.\n");
	    $param_file = "$RESULTS/$group/PARAMS.in";
	    system("scp $param_file linux01.ece.utexas.edu:$LRC_RESULTS/$group/PARAMS.in\n") if ($param_file ne "" && $QUEUE eq "lrc");
	} else {
	    system("cp -f $param_file $RESULTS/$group/PARAMS.in\n") if ($param_file ne "");
	    system("scp $param_file linux01.ece.utexas.edu:$LRC_RESULTS/$group/PARAMS.in\n") if ($param_file ne "" && $QUEUE eq "lrc");
	}
    } else {
	system("cp -f $param_file $RESULTS/$group/PARAMS.in\n") if ($param_file ne "");
	system("scp $param_file linux01.ece.utexas.edu:$LRC_RESULTS/$group/PARAMS.in\n") if ($param_file ne "" && $QUEUE eq "lrc");
    }

    if (-f "$tmp_gzip_binary.gz") {
	print("Removing temporary compressed binary $tmp_gzip_binary.gz.\n");
	system("/bin/rm $tmp_gzip_binary.gz\n");
    }
}


# **********************************************************************
# need to make sure the scripts are all on the lrc shadow

if (!$PRINT && $QUEUE eq "lrc") {
    system("ssh linux01.ece.utexas.edu mkdir -p $LRC_SIMDIR/bin\n");
    foreach $file ("run_local", "run_scarab", "bench_common", "chk_results") {
	system("scp $SIMDIR/bin/$file linux01.ece.utexas.edu:$LRC_SIMDIR/bin/$file\n");
    }    
    $group_binary = $lrc_group_binary;
    $ENV{'SIMDIR'} = $LRC_SIMDIR;
    $ENV{'SPEC00'} = "/home/projects/patt/spec00/benchspec";
    $ENV{'JBS'}    = "/home/projects/patt/jbs";
    $ENV{'SPEC95'} = "/home/projects/patt/spec95";
}



# **********************************************************************
# change to root directory for sanity

# $RESOURCES = "-l nodes=1:${QUEUE}m";
chdir("/");


# **********************************************************************
# created lrc directories---optimized to create the results
# directories with a single ssh

if ($QUEUE eq "lrc" && !$PRINT) {
    @LRC_DIRS = ();
    foreach $bench (@runlist) {
	if ($bench =~ /(.*)\@(.*)/) {
	    $bench = $1;
	    $input = $2;
	} else {
	    $input = "std";
	}

	$lrc_outdir = "$LRC_RESULTS/$group/$bench\@$input";
	push(@LRC_DIRS, $lrc_outdir);
    }
    print ("ssh linux01.ece.utexas.edu mkdir " . join(" ", @LRC_DIRS) . " 2> /dev/null\n");
    system("ssh linux01.ece.utexas.edu mkdir " . join(" ", @LRC_DIRS) . " 2> /dev/null\n");
}


# **********************************************************************
# main loop to launch all the jobs

foreach $bench (@runlist) {
    if ($bench =~ /(.*)\@(.*)/) {
	$bench = $1;
	$input = $2;
    } else {
	$input = "std";
    }

    $outdir	     = "$RESULTS/$group/$bench\@$input";
    $lrc_outdir	     = "$LRC_RESULTS/$group/$bench\@$input";
    $no_file_tag_opt = "+nofiletag";

    if (!(-d "$outdir")) {
	system("mkdir $outdir\n") unless $PRINT;
#  	system("ssh linux01.ece.utexas.edu mkdir $lrc_outdir\n") unless $PRINT || $QUEUE ne "lrc";
    }

    if ($QUEUE eq "lrc") {
	$outdir = $lrc_outdir;
	$pbs_outdir = $lrc_outdir;
	$pbs_outdir =~ s/^\/home/snapper.ece.utexas.edu:/;
	$nice = "+nice 5";
    } else {
	$pbs_outdir = $outdir;
	$nice = "+nice 20";
    }

    $name = substr($group, 0, 10) . "-" . substr($bench, 0, 4);
    if ($QUEUE ne "lrc") {
	$cmd_tail = $param_file eq "" ? "" : " +params $RESULTS/$group/PARAMS.in";
    } else {
	$cmd_tail = $param_file eq "" ? "" : " +params $LRC_RESULTS/$group/PARAMS.in";
    }

    # substitute into the sim options
    $subst_simopt = $simopt;
    $subst_simopt =~ s/%bench/$bench/g;
    $subst_simopt =~ s/%input/$input/g;

    # make substitutions in bsub file, if necessary
    if ($bsub_file) {
	while ($subst_simopt =~ /%(\w+)|({(\w+)})/) {
	    my $substr  = defined($1) ? $1 : $2; 
	    my $subname = defined($1) ? $1 : $3;
	    my $expression = '"$SUB_' . $subname . '{\'' . $bench . '@' . $input . '\'}"';
#  	    print("Searching for '$expression'\n");
 	    my $value = eval($expression);
 	    if ($value) {
 		$subst_simopt =~ s/%$substr/$value/g;
 	    } else {
		die "Substitution value for '%$subname' not found for benchmark '$bench\@$input'.\n";
	    }
	}
    }

    # check substitutions for sanity
    die "Invalid % substitution in run command.\n'$subst_simopt'\n" if ($subst_simopt =~ /%/); 

    #if ($SHELL =~ /\/bin\/(zsh|bash|sh)/) {
    # system() always uses /bin/sh -c, so we should use export
    if (1) {
	$cmd = "export RUN_LOCAL_ARGS=\"" . eval($RUN_LOCAL_ARGS) . "$cmd_tail\"; ";
    } else {
	$cmd = "setenv RUN_LOCAL_ARGS \"" . eval($RUN_LOCAL_ARGS) . "$cmd_tail\"; ";
    }
    $cmd .= eval($SUBMIT_CMD) . " $RUN_LOCAL_CMD";

    if ($PRINT) {
	print("$cmd\n");
    } else {
	system("/bin/echo '$cmd' > $RESULTS/$group/$bench\@$input/RUN_CMD\n");
	system("$cmd | tee $RESULTS/$group/$bench\@$input/JOB_ID\n");
    }
}
