#!/usr/bin/perl
use Term::ANSIColor;

# Settings
@splash2 = ( 
	'barnes', 
	'fft', 
	'fmm', 
	'lu_contiguous', 
	'lu_non_contiguous', 
	'ocean_contiguous', 
	'ocean_non_contiguous', 
	'radiosity', 
	'radix', 
	'raytrace', 
	'water_nsquared', 
	'water_spatial' );
@parsec = ( 
	'blackscholes', 
	'bodytrack', 
	'canneal', 
	'dedup', 
	'facesim', 
	'ferret', 
	'fluidanimate', 
	'freqmine', 
	'streamcluster', 
	'swaptions', 
	'vips', 
	'x264' 
);
@asymmetry = (
	'c_fft',
	'c_fft6',
	'c_qsort',
	'cg20.cua',
	'g4.rua',
	'g5.rua',
	'barnes',
	'cholesky',
	'fmm',
	'lu_contiguous',
	'lu_non_contiguous',
	'radiosity',
	'raytrace'
);

%commandline = (
	"-d" => "Data",
	"-base" => "Baseline",
	"-suite" => "Benchmark Suite",
	"-nocolor" => "No color",
	"-widenames" => "Fullname",
	"-printonlydelta" => "Print only Delta",
	"-amean" => "Arithmetic mean",
	"-gmean" => "Geometric mean",
	"-hmean" => "Harmonic mean",
	"-thresh" => "Threshold",
	"-stat" => "Stat Variable"
);

# Color Settings
$POS_THRESHOLD = 5;
$NEG_THRESHOLD = -5;
$POS_THRESHOLD_COLOR = "cyan";
$NEG_THRESHOLD_COLOR = "magenta";
$BASELINE_COLOR = "yellow";
$KNOB_NO_COLOR = 1;
$SPACE = 0;
$TAB = 11;

$KNOW_WIDE_NAME = 0;

# Paths
$result_dir = "${ENV{'SIMDIR'}}/threadsim_results";

# CommandLine Parsing
$base = "";
$suite = "";
$file_num = 0;
$KNOB_PRINT_VALUE = 1;

$KNOB_PRINT_AMEAN = 0;
$KNOB_PRINT_GMEAN = 0;
$KNOB_PRINT_HMEAN = 0;
$KNOB_STAT = "";

while( @ARGV ) {
	$command = shift;
	if( $command eq "-d" ) {
		$filelist[$file_num++] = shift;
		
		$loop_exit = 0;
		do {
			$filelist_temp = shift;
			if( $filelist_temp eq "" || ${commandline{$filelist_temp}} ne "" ) {
				unshift(@ARGV, $filelist_temp);
				$loop_exit = 1;
			}
			else {
				$filelist[$file_num++] = $filelist_temp;
			}
		} while ( $loop_exit == 0);
	}
	elsif( $command eq "-base" ) { $base = shift; $KNOB_NO_COLOR = 0; }
	elsif( $command eq "-suite" ) { $suite = shift; }
	elsif( $command eq "-nocolor" ) { $KNOB_NO_COLOR = 1; }
	elsif( $command eq "-widenames" ) { $KNOW_WIDE_NAME = 1; }
	elsif( $command eq "-printonlydelta" ) { $KNOB_PRINT_VALUE = 0; }
	elsif( $command eq "-amean" ) { $KNOB_PRINT_AMEAN = 1; }
	elsif( $command eq "-gmean" ) { $KNOB_PRINT_GMEAN = 1; }
	elsif( $command eq "-hmean" ) { $KNOB_PRINT_HMEAN = 1; }
	elsif( $command eq "-thresh" ) { $POS_THRESHOLD = shift; $NEG_THRESHOLD = -$POS_THRESHOLD; }
	elsif( $command eq "-stat" ) { $KNOB_STAT = shift; }
	else { die "Unknown command!\n"; }
}


# Commandline Error Check
if( $file_num == 0 || $suite eq "" ) { die "Wrong commandlines!\n"; }


# Suite
@suite_list = split(/@/, $suite);

if( $suite_list[0] eq "splash2" ) {
	@BENCH = @splash2;
	$KNOB_SUITE_NAME = "SPLASH2";
	$KNOB_BENCH_COUNT = 12;
	$suffix = "";
	$SPACE = 7;
}
elsif( $suite_list[0] eq "parsec" ) {
	@BENCH = @parsec;
	$KNOB_SUITE_NAME = "PARSEC_${suite_list[1]}";
	$KNOB_BENCH_COUNT = 12;
	$suffix = "\@${suite_list[1]}";
	if( $KNOB_WIDE_NAME == 1 ) { $SPACE = 23; }
	else { $SPACE = 5; }
}
elsif( $suite_list[0] eq "asymmetry" ) {
	@BENCH = @asymmetry;
	$KNOB_SUITE_NAME = "ASYMMETRY BENCHMARKS";
	$KNOB_BENCH_COUNT = 13;
	$suffix = "";
	if( $KNOB_WIDE_NAME == 1 ) { $SPACE = 20; }
	else { $SPACE = 7; }
}


# Base
$index = 0;
$proj_index = 0;

if( $base ne "" ) {
	$base_dir = "${result_dir}/${base}";
	if( !(-e $base_dir) ) { die "${base} does not exist!\n"; }
	
	chdir $base_dir;
		
	$proj_name[$proj_index++] = $base;
	foreach $bench (@BENCH) {
		$bench_dir = "${base_dir}/${bench}${suffix}";
		if( !(-e $bench_dir) ) { $array[$index++] = ""; }
		else {
			chdir $bench_dir;

			if( $KNOB_STAT eq "" ) {
			$file = "${bench}.tot";
			if( !(-e $file) ) { $array[$index++] = ""; }
			else {
				open(DATAFILE, "<$file");
				$line = <DATAFILE>;
				chomp($line);
				$array[$index++] = $line;
			}
			}
			else {
			$file = "data.stat.out";
			if( !(-e $file) ) { $array[$index++] = ""; }
			else {
				open(DATAFILE, "<$file");
				while( $line = <DATAFILE> ) {
				chomp($line);
				@line_parsed = split(' ', $line);
				if( $line_parsed[0] eq $KNOB_STAT ) {
					$array[$index++] = $line_parsed[1];
					last;
				}
				}
			}
			}
		}
	}
}


# Processing Data
foreach $file (@filelist) {
	$proj_name[$proj_index++] = $file;
	$proj_dir = "${result_dir}/${file}";
	if( !(-e $proj_dir) ) { die "${proj_dir} does not exist!\n"; }

	chdir $proj_dir;

	foreach $bench (@BENCH) {
		$bench_dir = "${proj_dir}/${bench}${suffix}";
		if( !(-e $bench_dir) ) { $array[$index++] = ""; }
		else {
			chdir $bench_dir;
			
			if( $KNOB_STAT eq "" ) {
			$file = "${bench}.tot";
			if( !(-e $file) ) { $array[$index++] = ""; }
			else {
				open(DATAFILE, "<$file");
				$line = <DATAFILE>;
				chomp($line);
				$array[$index++] = $line;
			}
			}
			else {
			$file = "data.stat.out";
			if( !(-e $file) ) { $array[$index++] = ""; }
			else {
				open(DATAFILE, "<$file");
				while( $line = <DATAFILE> ) {
				chomp($line);
				@line_parsed = split(' ', $line);
				if( $line_parsed[0] eq $KNOB_STAT ) {
					$array[$index++] = $line_parsed[1];
					last;
				}
				}
			}
			}
		}
	}

}


# Calculate Delta
if( $base ne "" ) {
	#if( $NO_COLOR == 0 ) {
	#	$NO_COLOR = 0;
	#}
	for( $ii=0 ; $ii<$KNOB_BENCH_COUNT ; $ii++ ) { 
		$delta[$ii] = 0.00; 
	}
	for( $ii=$KNOB_BENCH_COUNT ; $ii<$index ; $ii++ ) { 
		if( $array[$ii%$KNOB_BENCH_COUNT] == 0 ) { 
			$delta[$ii] = 0.00; 
		}
		else { 
			$delta[$ii] = ($array[$ii] - $array[$ii%$KNOB_BENCH_COUNT]) / $array[$ii%$KNOB_BENCH_COUNT] * 100.0; 
		}
	}
}

# Print Data
if( $KNOB_PRINT_VALUE == 1 ) {
	print "${KNOB_SUITE_NAME}\n";
	printf "Benchmark             : ";
	# Print benchmark names
	foreach $bench (@BENCH) {
		if( $KNOW_WIDE_NAME == 0 ) { 
			$bench = "${bench}______"; 
		}
		$bench = substr($bench, 0, $SPACE);
		$bench = "${bench}${suffix}";
		if( $KNOW_WIDE_NAME == 1 ) { 
			$TAB = $SPACE + 1; 
		}
		printf "%-${TAB}s", substr($bench, 0, $TAB-1);
	}

	$tab_plus_three = $TAB + 3;
	if( $KNOB_PRINT_AMEAN == 1 ) {
		printf "%-${tab_plus_three}s", "amean";
	}
	if( $KNOB_PRINT_GMEAN == 1 ) {
		printf "%-${tab_plus_three}s", "gmean";
	}
	if( $KNOB_PRINT_HMEAN == 1 ) {
		printf "%-${tab_plus_three}s", "hmean";
	}

	# Print values
	$proj_index = 0;
	for( $ii=0 ; $ii<=$index ; $ii++ ) {
		# Print a project name
		if( $ii % $KNOB_BENCH_COUNT == 0 ) {

			# Average ================================================================
			if( $ii != 0 ) {
				if( $count_for_average != 0 ) {
					$arithmetic_mean = $arithmetic_sum / $count_for_average;
					$geometric_mean = $geometric_mul ** (1.0 / $count_for_average);
				}
				else {
					$arithmetic_mean = 0.0;
					$geometric_mean = 0.0;
				}
				if( $harmonic_sum == 0 ) {
					$harmonic_mean = 0.0;
				}
				else {
					$harmonic_mean = $count_for_average / $harmonic_sum;
				}

				$tab_plus_three = $TAB + 3;
				if( $KNOB_PRINT_AMEAN == 1 ) {
					printf "%-${tab_plus_three}.2f", $arithmetic_mean;
				}
				if( $KNOB_PRINT_GMEAN == 1 ) {
					printf "%-${tab_plus_three}.2f", $geometric_mean;
				}
				if( $KNOB_PRINT_HMEAN == 1 ) {
					printf "%-${tab_plus_three}.2f", $harmonic_mean;
				}
			}

			if( $ii == $index ) {
				last;
			}

			$count_for_average = 0;
			$arithmetic_sum = 0;
			$geometric_mul = 1;
			$harmonic_sum = 0;
			# Average ================================================================
			
			print "\n";

			if( $KNOB_NO_COLOR == 0 ) {
				print color("reset");
			}
			printf "%-21s : ", $proj_name[$proj_index++];
		}

		# Color setting
		if( $KNOB_NO_COLOR == 0 ) {
			if( $ii < $KNOB_BENCH_COUNT )          { $text_color = $BASELINE_COLOR; }
			elsif( $delta[$ii] >= $POS_THRESHOLD ) { $text_color = $POS_THRESHOLD_COLOR; }
			elsif( $delta[$ii] <= $NEG_THRESHOLD ) { $text_color = $NEG_THRESHOLD_COLOR; }
			else                                   { $text_color = "reset"; }
			print color($text_color);
		}

		# Print a value
		if( $KNOW_WIDE_NAME == 1 ) { 
			$TAB = $SPACE + 1; 
		}
		printf "%-${TAB}s", $array[$ii];

		# Average ================================================================
		if( $array[$ii] ne "" ) {
			if( $array[$ii] != 0 ) {
				$geometric_mul *= $array[$ii];
				$harmonic_sum += (1.0 / $array[$ii]);
			}
			$arithmetic_sum += $array[$ii];
			$count_for_average++;
		}
		# Average ================================================================
	}

	if( $KNOB_NO_COLOR == 0 ) {
		print color("reset");
	}
	print "\n";
}

# Print Delta
if( $base ne "" ) {
	print "\nDELTA\n";
	printf "Benchmark             : ";
	# Print benchmark names 
	foreach $bench (@BENCH) {
		if( $KNOW_WIDE_NAME == 0 ) { 
			$bench = "${bench}______"; 
		}
		$bench = substr($bench, 0, $SPACE);
		$bench = "${bench}${suffix}";
		if( $KNOW_WIDE_NAME == 1 ) { 
			$TAB = $SPACE + 1; 
		}
		printf "%-${TAB}s", substr($bench, 0, $TAB-1);
	}
	
	$tab_plus_three = $TAB + 3;
	if( $KNOB_PRINT_AMEAN == 1 ) {
		printf "%-${tab_plus_three}s", "amean";
	}
	if( $KNOB_PRINT_GMEAN == 1 ) {
		printf "%-${tab_plus_three}s", "gmean";
	}
	if( $KNOB_PRINT_HMEAN == 1 ) {
		printf "%-${tab_plus_three}s", "hmean";
	}
	
	# Print values
	$proj_index = 0;
	for( $ii=0 ; $ii<=$index ; $ii++ ) {
		# Print a project name
		if( $ii % $KNOB_BENCH_COUNT == 0 ) {

			# Average ================================================================
			if( $ii != 0 ) {
				if( $count_for_average != 0 ) {
					$arithmetic_mean = $arithmetic_sum / $count_for_average;
					$geometric_mean = $geometric_mul ** (1.0 / $count_for_average);
				}
				else {
					$arithmetic_mean = 0.0;
					$geometric_mean = 0.0;
				}
				if( $harmonic_sum == 0 ) {
					$harmonic_mean = 0.0;
				}
				else {
					$harmonic_mean = $count_for_average / $harmonic_sum;
				}

				$tab_plus_three = $TAB + 3;
				if( $KNOB_PRINT_AMEAN == 1 ) {
					printf "%-${tab_plus_three}.2f", $arithmetic_mean;
				}
				if( $KNOB_PRINT_GMEAN == 1 ) {
					printf "%-${tab_plus_three}.2f", $geometric_mean;
				}
				if( $KNOB_PRINT_HMEAN == 1 ) {
					printf "%-${tab_plus_three}.2f", $harmonic_mean;
				}
			}

			if( $ii == $index ) {
				last;
			}

			$count_for_average = 0;
			$arithmetic_sum = 0;
			$geometric_mul = 1;
			$harmonic_sum = 0;
			# Average ================================================================

			print "\n";
			if( $KNOB_NO_COLOR == 0 ) {
				print color("reset");
			}
			printf "%-21s : ", $proj_name[$proj_index++];
		}
		
		# Color setting
		if( $KNOB_NO_COLOR == 0 ) {
			if( $ii < $KNOB_BENCH_COUNT )          { $text_color = $BASELINE_COLOR; }
			elsif( $delta[$ii] >= $POS_THRESHOLD ) { $text_color = $POS_THRESHOLD_COLOR; }
			elsif( $delta[$ii] <= $NEG_THRESHOLD ) { $text_color = $NEG_THRESHOLD_COLOR; }
			else                                   { $text_color = "reset"; }
			print color($text_color);
		}

		# Print a value
		if( $KNOW_WIDE_NAME == 1 ) { 
			$TAB = $SPACE + 1; 
		}
		printf "%-${TAB}.2f", $delta[$ii];
		
		# Average ================================================================
		if( $delta[$ii] ne "" ) {
			if( $delta[$ii] != 0 ) {
				$geometric_mul *= $delta[$ii];
				$harmonic_sum += (1.0 / $delta[$ii]);
			}
			$arithmetic_sum += $delta[$ii];
			$count_for_average++;
		}
		# Average ================================================================
	}

	print "\n";
}

if( $KNOB_NO_COLOR == 0 ) {
	print color("reset");
}

