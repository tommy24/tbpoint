#!/usr/bin/perl

use Time::HiRes qw(gettimeofday);
use Cwd;

@bench_groups_int  =   (
		    'perlbench',
		    'bzip2',
		    'gcc',
		    'mcf',
		    'gobmk',
		    'hmmer',
		    'sjeng',
		    'libquantum',
		    'h264ref',
		    'omnetpp',
		    'astar',
		    'xalancbmk' );

@bench_groups_fp = ( 
		     'bwaves',
		     'gamess',
		     'milc',
		     'zeusmp',
		     'gromacs',
		     'cactusADM',
		     'leslie3d',
		     'namd',
		     'dealII',
		     'soplex',
		     'povray',
		     'calculix',
		     'GemsFDTD',
		     'tonto',
		     'lbm',
#		     'wrf',				<= Could build, but couldn't run it.
		     'sphinx3'
		     );

@bench_groups_fp_cpp = ( 
			  'namd',
			 'dealII',
			 'soplex',
			 'povray',
			 );


#@bench_groups = (@bench_groups_int , @bench_groups_fp_cpp); 

%bench_dir = (    
		  "perlbench" => "400.perlbench",
		  "bzip2" => "401.bzip2",
		  "gcc" => "403.gcc",
		  "mcf" => "429.mcf",
		  "gobmk" => "445.gobmk",
		  "hmmer" => "456.hmmer",
		  "sjeng" => "458.sjeng",
		  "libquantum" => "462.libquantum",
		  "h264ref" => "464.h264ref",
		  "omnetpp" => "471.omnetpp",
		  "astar" => "473.astar",
		  "xalancbmk" => "483.xalancbmk",
		  "bwaves" => "410.bwaves",
		  "gamess" => "416.gamess",
		  'milc' => "433.milc",
		  "zeusmp" => "434.zeusmp",
		  "gromacs" => "435.gromacs",
		  "cactusADM" => "436.cactusADM",
		  "leslie3d" => "437.leslie3d", 
		  "namd" => "444.namd",
		  "dealII" => "447.dealII",
		  "soplex" => "450.soplex",
		  "povray" => "453.povray",
		  "calculix" => "454.calculix",
		  "GemsFDTD" => "459.GemsFDTD",
		  "tonto" => "465.tonto",
		  "lbm" => "470.lbm",
		  "wrf" => "481.wrf",
		  "sphinx3" => "482.sphinx3"
		  );



%bench_exe = (    
		  "perlbench" => "perlbench"  ,
		  "bzip2" => "bzip2",
		  "gcc" => "gcc",
		  "mcf" => "mcf",
		  "gobmk" => "gobmk",
		  "hmmer" => "hmmer",
		  "sjeng" => "sjeng",
		  "libquantum" => "libquantum",
		  "h264ref" => "h264ref",
		  "omnetpp" => "omnetpp",
		  "astar" => "astar",
		  "xalancbmk" => "Xalan",
		  "bwaves" => "bwaves",
		  "gamess" => "gamess",
		  'milc' => "milc",
		  "zeusmp" => "zeusmp",
		  "gromacs" => "gromacs",
		  "cactusADM" => "cactusADM",
		  "leslie3d" => "leslie3d", 
		  "namd" => "namd",
		  "dealII" => "dealII",
		  "soplex" => "soplex",
		  "povray" => "povray",
		  "calculix" => "calculix",
		  "GemsFDTD" => "GemsFDTD",
		  "tonto" => "tonto",
		  "lbm" => "lbm",
		  "wrf" => "wrf",
		  "sphinx3" => "sphinx_livepretend"
		  );

%bench_num_input = (   
			 "perlbench" => "3"   ,
		     "bzip2" => "6",
		     "gcc" => "9",
		     "mcf" => "1",
		     "gobmk" => "5",
		     "hmmer" => "2",
		     "sjeng" => "1",
		     "libquantum" => "1",
		     "h264ref" => "3",
		     "omnetpp" => "1",
		     "astar" => "2",
		     "xalancbmk" => "1",
		     "bwaves" => "1",
		     "gamess" => "3",
		     'milc' => "1",
		     "zeusmp" => "1",
		     "gromacs" => "1",
		     "cactusADM" => "1",
		     "leslie3d" => "1", 
		     "namd" => "1",
		     "dealII" => "1",
		     "soplex" => "2",
		     "povray" => "1",
		     "calculix" => "1",
		     "GemsFDTD" => "1",
		     "tonto" => "1",
		     "lbm" => "1",
		     "wrf" => "1",
		     "sphinx3" => "1");

%bench_arg = (
		      "perlbench1" => "-I./lib checkspam.pl 2500 5 25 11 150 1 1 1 1",
		      "perlbench2" => "-I./lib diffmail.pl 4 800 10 17 19 300",
		      "perlbench3" => "-I./lib splitmail.pl 1600 12 26 16 4500",
		      "bzip21" =>"input.source 280",
		      "bzip22" => "chicken.jpg 30",
		      "bzip23" => "liberty.jpg 30",
		      "bzip24" => "input.program 280",
		      "bzip25" => "text.html 280",
		      "bzip26" => "input.combined 200",
		      "gcc1" => "166.i > 166.s",
		      "gcc2" => "200.i > 200.s",
		      "gcc3" => "c-typeck.i > c-typeck.s",
		      "gcc4" => "cp-decl.i > cp-decl.s",
		      "gcc5" => "expr.i > expr.s",
		      "gcc6" => "expr2.i > expr2.s",
		      "gcc7" => "g23.i > g23.s", 
		      "gcc8" => "s04.i > s04.s",
		      "gcc9" => "scilab.i > scilab.s",
		      "mcf1" => "inp.in" ,
		      "gobmk1" => "--quiet --mode gtp < 13x13.tst", 
		      "gobmk2" => "--quiet --mode gtp < nngs.tst",
		      "gobmk3" => "--quiet --mode gtp < score2.tst",
		      "gobmk4" => "--quiet --mode gtp < trevorc.tst", 
		      "gobmk5" => "--quiet --mode gtp < trevord.tst",
		      "hmmer1" => "nph3.hmm swiss41", 
		      "hmmer2" => "--fixed 0 --mean 500 --num 500000 --sd 350 --seed 0 retro.hmm",
		      "sjeng1" => "ref.txt" ,
		      "libquantum1" => "1397 8", 
		      "h264ref1" => "-d foreman_ref_encoder_baseline.cfg",
		      "h264ref2" => "-d foreman_ref_encoder_main.cfg",
		      "h264ref3" => "-d sss_encoder_main.cfg",
		      "omnetpp1" => "omnetpp.ini",
		      "astar1" => "BigLakes2048.cfg" ,
		      "astar2" => "rivers.cfg",
		      "xalancbmk1" => "-v t5.xml xalanc.xsl",
			  "bwaves1" => "",
			  "gamess1" => "< cytosine.2.config",
			  "gamess2" => "< h2ocu2+.gradient.config",
			  "gamess3" => "< triazolium.config",
			  "milc1" => "< su3imp.in",
			  "zeusmp1" => "",
			  "gromacs1" => "-silent -deffnm gromacs -nice 0",
			  "cactusADM1" => "benchADM.par",
			  "leslie3d1" => "< leslie3d.in",
			  "namd1" => "--input namd.input --iterations 38 --output namd.out",
			  "dealII1" => "23",
			  "soplex1" => " -s1 -e -m45000 pds-50.mps",
			  "soplex2" => " -m3500 ref.mps ",
			  "povray1" => "SPEC-benchmark-ref.ini",
			  "calculix1" => "hyperviscoplastic ",
			  "GemsFDTD1" => "",
			  "tonto1" => "",
			  "lbm1" => "3000 reference.dat 0 0 100_100_130_ldc.of",
			  "sphinx31" => "ctlfile . args.an4 ",
);

# Bench Setting
$type = $ARGV[0];
if ($type eq "" || $type eq "all") { @bench_groups = (@bench_groups_int, @bench_groups_fp); }
elsif ($type eq "int") { @bench_groups = @bench_groups_int; }
elsif ($type eq "fp") { @bench_groups = @bench_groups_fp; }

$exe_tail = "_base.x86_64_icc";

# Trace Generator Path Setting
$pin_home       = "/hparch/benchmarks/pin-2.6/source/tools";
$isimpoint_path = "$pin_home/PinPoints/obj-intel64/isimpoint.so";
$simpoint_path  = "$pin_home/PinPoints/bin/simpoint";
$ppgen_path     = "$pin_home/PinPoints/bin/ppgen.3";
$tracegen_path  = "$pin_home/trace_generator_new/obj-intel64/trace_generator.so";
$pin_exe        = "/hparch/benchmarks/pin-2.6/pin";
$current_dir    = getcwd();
$base           = "$current_dir";

# Misc

if (!(-e "${current_dir}/scripts"))
{
	system "mkdir -p ${current_dir}/scripts";
}

foreach $bench (@bench_groups)
{
	$bench_dir  = "${base}/${bench_dir{$bench}}/run/run_base_ref_x86_64_icc.0000";
	$pinout_dir = "/trace/1/spec2006/trace_simpoint/${bench}";
	$bench_exe  = "${bench_dir}/${bench_exe{$bench}}${exe_tail}";
	$num_input  = ${bench_num_input{$bench}};


	for ($ii = 1; $ii <= $num_input; $ii++)
	{
	
		# File open
		open (OUTFILE, "> ${current_dir}/scripts/${bench}.${ii}.tracegen.pl"); 
		print (OUTFILE "#!/usr/bin/perl\n\n");
		print (OUTFILE "chdir \"${bench_dir}\"\;\n\n");

		print (OUTFILE "\# ${bench} input ${ii}\n\n");

		$input_index = ${bench}.${ii};
		$bench_arg = "${bench_arg{$input_index}}";

		# bb
		print (OUTFILE "system\(\"mkdir -p ${pinout_dir}/bbdata\"\)\;\n");
		$bb_file = "${pinout_dir}/bbdata/${bench}${ii}";	
		print (OUTFILE "system\(\"rm -rf ${bb_file}.T.0.bb\"\)\;\n");
		print (OUTFILE "system\(\"$pin_exe -t ${isimpoint_path} -o ${bb_file} -slice_size 200000000 -- ${bench_exe} ${bench_arg}\"\)\;\n\n");

		# simpoint
		$bb_data = "${bb_file}.T.0.bb";
		print (OUTFILE "system\(\"$simpoint_path  -loadFVFile ${bb_data} -maxK 1 -coveragePct 0.9999 -saveSimpoints ${bb_data}.simpoints -saveSimpointWeights ${bb_data}.weights -saveLabels ${bb_data}.labels > ${bb_data}.simpointout\"\)\;\n\n");

		# pp
		$pp_file = "${pinout_dir}/ppdata/${bench}${ii}-point";
		print (OUTFILE "system\(\"mkdir -p ${pinout_dir}/ppdata\"\)\;\n");
		print (OUTFILE "system\(\"$ppgen_path ${pp_file} ${bb_data} ${bb_data}.simpoints.lpt0.9999 ${bb_data}.weights.lpt0.9999 ${bb_data}.labels 0 >& ${bb_data}.ppgenout\"\)\;\n\n");

		# trace generation
		print (OUTFILE "system\(\"mkdir -p ${pinout_dir}/pin_traces\"\)\;\n");
		$trace_file = "${pinout_dir}/pin_traces/${bench}.${ii}";
		print (OUTFILE "system\(\"$pin_exe -t $tracegen_path -ppfile ${pp_file}.1.pp -tracename $trace_file -dump 1 -dump_file ${trace_file}.dump -- $bench_exe $bench_arg\"\)\;\n\n");
	
		print (OUTFILE "system\(\"rm -Rf ${pinout_dir}/bbdata/${bench}${ii}*\"\)\;\n\n");
	
		# file close
		close OUTFILE;
		system "chmod +x ${current_dir}/scripts/${bench}.${ii}.tracegen.pl"; 
		#system "qsub ${current_dir}/scripts/${bench}.${ii}.tracegen.pl\" | qsub\n";
	}
}

