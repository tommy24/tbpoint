#!/usr/bin/python
#########################################################################################
# Author: Jaekyu Lee (kacear@gmail.com)
# Date: 7/17/2011
# Description: get torque queue status
# Usage: 
#   myqstat.py [OPTIONS]
# Options:
#   -m - show memory usage
#   -u - show specified user only
#   -run - qrun jobs with Q state
#   -kill - kill jobs from specified user
#   -killall - kill jobs from all users
#   -rerun - rerun completed jobs
#   -v - verbose
#########################################################################################


import os
import sys
import re
import subprocess
import argparse
from collections import defaultdict


#########################################################################################
# create an argument parser
#########################################################################################
def process_options():
  parser = argparse.ArgumentParser(description='check torque queue status')
  parser.add_argument('-m', action='store_true', dest='mem', help='print memory usage')
  parser.add_argument('-u', action='store', dest='user', help='user id filter')
  parser.add_argument('-run', action='store_true', dest='run', help='qrun waiting jobs')
  parser.add_argument('-kill', action='store_true', dest='kill', help='kill own jobs')
  parser.add_argument('-killall', action='store_true', dest='killall', help='kill all jobs')
  parser.add_argument('-rerun', action='store_true', dest='rerun', help='rerun completed jobs')
  parser.add_argument('-v', '-verbose', action='store_true', dest='verbose', help='verbose')

  return parser


#########################################################################################
# job information data structure
#########################################################################################
class job_struct():
  global args

  def __init__(self):
    self.reset()

  def reset(self):
    self.id = ''
    self.name = ''
    self.owner = ''
    self.mem = ''
    self.time = ''
    self.state = ''
    self.queue = ''
    self.host = ''


def sanity_check(args):
  if args.kill and args.killall:
    print('error: kill and killall options can not be enabled togeter')
    exit(2)


#########################################################################################
# main function
#########################################################################################
def main(argv):
  global args
  global header
  global max_len






  # parse arguments
  parser = process_options()
  args = parser.parse_args()
  
  # initialization
  if not args.user:
    user_id = os.getlogin()
  else:
    user_id = args.user
  
  
  sanity_check(args)


  ## get qstat output
  output = os.popen('qstat -f')

  result_list = []
  find_next = False
  for line in output.readlines():
    if line[0] == '\t':
      continue
    line = line.lstrip(' ').lstrip('\t')
    #line = line.lstrip(' ').lstrip('\t').rstrip('\n')
    if re.match('Job Id', line):
      new_job = job_struct()
      result_list.append(new_job)

      token = re.split('[ \t\n]', line)
      new_job.id = token[2].replace('.purpleconeflower.cc.gt.atl.ga.us', '')
      find_next = False
      continue
    elif find_next == True:
      continue

    if re.match('^Job_Name', line):
      token = re.split('[ \t\n]', line)
      new_job.name = token[2]
    elif re.match('^Job_Owner', line):
      token = re.split('[ \t\n]', line)
      job_owner = token[2]
      job_owner = job_owner[:job_owner.find('@')]
      new_job.owner = job_owner
    elif re.match('^resources_used.vmem', line) and args.mem:
      token = re.split('[ \t\n]', line)
      new_job.mem = token[2]
    elif re.match('^resources_used.walltime', line):
      token = re.split('[ \t\n]', line)
      new_job.time = token[2]
    elif re.match('^job_state', line):
      token = re.split('[ \t\n]', line)
      new_job.state = token[2]
    elif re.match('^queue', line):
      token = re.split('[ \t\n]', line)
      new_job.queue = token[2]
    elif re.match('^exec_host', line):
      token = re.split('[ \t\n]', line)
      new_job.host = token[2][:token[2].find('.')]
      find_next = True


  maxlen = {}
  maxlen['id'] = 2
  maxlen['name'] = 4
  maxlen['owner'] = 5
  maxlen['mem'] = 3
  maxlen['time'] = 4
  maxlen['state'] = 5
  maxlen['queue'] = 5
  maxlen['host'] = 4
  for item in result_list:
    if len(item.id) > maxlen['id']:
      maxlen['id'] = len(item.id)
    if len(item.name) > maxlen['name']:
      maxlen['name'] = len(item.name)
    if len(item.owner) > maxlen['owner']:
      maxlen['owner'] = len(item.owner)
    if len(item.mem) > maxlen['mem']:
      maxlen['mem'] = len(item.mem)
    if len(item.time) > maxlen['time']:
      maxlen['time'] = len(item.time)
    if len(item.state) > maxlen['state']:
      maxlen['state'] = len(item.state)
    if len(item.queue) > maxlen['queue']:
      maxlen['queue'] = len(item.queue)
    if len(item.host) > maxlen['host']:
      maxlen['host'] = len(item.host)

  if len(result_list) > 0:
    # print header
    if not args.user or args.user == item.owner:
      sys.stdout.write('%s' % ('id'.ljust(maxlen['id'] + 2)))
      sys.stdout.write('%s' % ('owner'.ljust(maxlen['owner'] + 2)))
      sys.stdout.write('%s' % ('name'.ljust(maxlen['name'] + 2)))
      sys.stdout.write('%s' % ('time'.ljust(maxlen['time'] + 2)))
      sys.stdout.write('%s' % ('state'.ljust(maxlen['state'] + 2)))
      sys.stdout.write('%s' % ('host'.ljust(maxlen['host'] + 2)))
      sys.stdout.write('%s' % ('queue'.ljust(maxlen['queue'] + 2)))
      if args.mem:
        sys.stdout.write('%s' % ('mem'.ljust(maxlen['mem'] + 2)))
      sys.stdout.write('\n')


  waiting_jobs = []
  jobs_to_kill = []
  jobs_to_rerun = []
  for item in result_list:
    # job kill
    if args.killall:
      if item.state == 'Q' or item.state == 'R':
        jobs_to_kill.append(item.id)
    elif args.kill and item.owner == user_id:
      if item.state == 'Q' or item.state == 'R':
        jobs_to_kill.append(item.id)
    elif args.rerun and item.owner == user_id and (item.state == 'C' or item.state == 'E'):
      jobs_to_rerun.append(item.id)



    if not args.user or args.user == item.owner:
      if item.state == 'Q':
        waiting_jobs.append(item.id)
      sys.stdout.write('%s' % (item.id.ljust(maxlen['id'] + 2)))
      sys.stdout.write('%s' % (item.owner.ljust(maxlen['owner'] + 2)))
      sys.stdout.write('%s' % (item.name.ljust(maxlen['name'] + 2)))
      sys.stdout.write('%s' % (item.time.ljust(maxlen['time'] + 2)))
      sys.stdout.write('%s' % (item.state.ljust(maxlen['state'] + 2)))
      sys.stdout.write('%s' % (item.host.ljust(maxlen['host'] + 2)))
      sys.stdout.write('%s' % (item.queue.ljust(maxlen['queue'] + 2)))
      if args.mem:
        sys.stdout.write('%s' % (item.mem.ljust(maxlen['mem'] + 2)))
      sys.stdout.write('\n')


  if args.run and len(waiting_jobs) > 0:
    if args.verbose:
      print('qrun %s' % (' '.join(waiting_jobs)))
    os.system('qrun %s' % (' '.join(waiting_jobs)))

  if args.killall or args.kill:
    if len(jobs_to_kill) == 0:
      print('warning: nothing to kill')
    else:
      if args.killall:
        answer = raw_input('warning: do you really want to kill all (own+others) jobs? (y/n) ')
      elif args.kill:
        answer = raw_input('warning: do you really want to kill all (own) jobs? (y/n) ')

      if answer == 'y' or answer == 'yes':
        if args.verbose:
          print('qdel %s' % (' '.join(jobs_to_kill)))
        os.system('qdel %s' % (' '.join(jobs_to_kill)))

  if args.rerun:
    if len(jobs_to_rerun) == 0:
      print('warning: nothing to rerun')
    else:
      if args.verbose:
        print('qrerun %s' % (' '.join(jobs_to_rerun)))
      os.system('qrerun %s' % (' '.join(jobs_to_rerun)))


#########################################################################################    
if __name__ == '__main__':
  main(sys.argv)

