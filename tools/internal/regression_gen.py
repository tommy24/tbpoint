#!/usr/bin/python
#########################################################################################
# Author : Jaekyu Lee (kacear@gmail.com)
# Date : 7/14/2011
# Description
#   generate regression data
#########################################################################################


import sys
import optparse
import os
import regression_test
import trace_common


"""
Process arguments
"""
def process_options():
  parser = optparse.OptionParser(usage='usage: %prog [options]', add_help_option=True)
  parser.add_option('--bin', action='store', type='string', dest='bin', default='', help='input file name')

  return parser


"""
Main routine
"""
def main(argv):
  parser = process_options()
  options, args = parser.parse_args(argv)

  # prepare macsim binary
  bin = ''
  if not options.bin:
    print('warning: binary not specified. Use ./macsim')
    bin = os.getcwd() + '/macsim'
  else:
    bin = options.bin

  if not os.path.exists(bin):
    print('error: binary does not exist')
    exit(0)

  # regression test configuration
  params_path = os.environ['SIMDIR'] + '/tools/macsim_regression/'

  # create test directory
  test_dir = params_path + 'data'
  if os.path.exists(test_dir):
    os.system('rm -rf %s' % (test_dir))
  os.system('mkdir -p %s' % (test_dir))
      
  # copy macsim binary
  os.system('cp %s %s/macsim' % (bin, test_dir))
  
  # regression gen
  for key, val in regression_test.regression_suite.items():
    params_file = params_path + key + '.params.in'
    for suite in val:
      test_subdir = test_dir + '/' + key + '/' + suite 
      os.system('mkdir -p %s' % (test_subdir))
      os.system('cp %s %s/params.in' % (params_file, test_subdir))

      trace_file_name = test_subdir + '/trace_file_list'
      file = open(trace_file_name, 'w')
      suite = suite[:suite.find('@')]
      bench_list = suite.split('_')
      file.write('%d\n' % len(bench_list))
      for bench in bench_list:
        file.write('%s\n' % trace_common.TRACE_FILE['%s@ref' % (bench)])
      file.close()

      print('generating... %s %s' % (key, suite))
      os.chdir(test_subdir)
      #os.system('../../macsim')
      os.system('../../macsim 1> /dev/null')
      #os.system('../../macsim 2>&1 1>/dev/null')


if __name__ == '__main__':
  main(sys.argv)
