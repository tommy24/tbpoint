#!/usr/bin/python

import os
import re
import sys
import glob
import subprocess
import argparse
import thread
from threading import Thread
import threading


bench_list = [
## rodinia
  'b+tree',
  'backprop',        ##
  'bfs',             ##
  'cell_cuda',
  'cfd',             ##
  'gaussian',        ## too short
  'heartwall',       ##
  'hotspot',         ##
  'kmeans',          ## 
  'lavaMD',          ## assertion failure
  'leukocyte',       ##
  'lud',             ##
#  'mummergpu',      ## compile error
  'nn',              ##
  'nw',              ## 
#  'particlefilter',  ## segfault
  'pathfinder',      ##
  'srad_v1',    ##
  'srad_v2',    ##
  'streamcluster',   ##
## parboil
#  'bfs',
  'stencil',
  'mri-gridding',
  'lbm',
  'tpacf',
  'sad',
  'spmv',
  'histo',
  'sgemm',
  'mri-q',
  'cutcp',
## CUDA
  'BlackScholes',
  'FDTD3d',
#  'FunctionPointers',
  'HSOpticalFlow',
  'Interval',
  'Mandelbrot',
  'MersenneTwister',
  'MonteCarlo',
  'MonteCarloMultiGPU',
  'SobelFilter',
  'SobolQRNG',
#  'alignedTypes',
#  'asyncAPI',
#  'bandwidthTest',
  'bicubicTexture',
  'bilateralFilter',
  'binomialOptions',
  'boxFilter',
#  'clock',
#  'concurrentKernels',
  'convolutionFFT2D',
  'convolutionSeparable',
  'convolutionTexture',
#  'cppIntegration',
  'data',
  'dct8x8',
#  'deviceQuery',
#  'deviceQueryDrv',
  'dwtHaar1D',
  'dxtc',
  'eigenvalues',
  'fastWalshTransform',
  'fluidsGL',
  'histogram',
  'imageDenoising',
  'inlinePTX',
  'lineOfSight',
  'marchingCubes',
  'matrixMul',
  'matrixMulDrv',
  'matrixMulDynlinkJIT',
  'mergeSort',
  'nbody',
#  'newdelete',
  'oceanFFT',
  'particles',
  'postProcessGL',
#  'ptxjit',
  'quasirandomGenerator',
  'radixSortThrust',
  'recursiveGaussian',
  'reduction',
  'scalarProd',
#  'scan',
#  'simpleAssert',
#  'simpleAtomicIntrinsics',
#  'simpleCubemapTexture',
#  'simpleGL',
#  'simpleLayeredTexture',
#  'simpleMPI',
#  'simpleMultiCopy',
#  'simpleMultiGPU',
#  'simpleP2P',
#  'simplePitchLinearTexture',
#  'simplePrintf',
#  'simpleStreams',
#  'simpleSurfaceWrite',
#  'simpleTemplates',
#  'simpleTexture',
#  'simpleTexture3D',
#  'simpleTextureDrv',
#  'simpleVoteIntrinsics',
#  'simpleZeroCopy',
  'smokeParticles',
]


bench_suite = {
## rodinia
  'b+tree'          : 'rodinia',
  'backprop'        : 'rodinia', 
  'cell_cuda'       : 'rodinia',
  'cfd'             : 'rodinia',     
  'gaussian'        : 'rodinia', 
  'heartwall'       : 'rodinia',
  'hotspot'         : 'rodinia',         
  'kmeans'          : 'rodinia',          
  'lavaMD'          : 'rodinia',          
  'leukocyte'       : 'rodinia',       
  'lud'             : 'rodinia',             
  'mummergpu'       : 'rodinia',      
  'nn'              : 'rodinia',              
  'nw'              : 'rodinia',              
  'particlefilter'  : 'rodinia', 
  'pathfinder'      : 'rodinia',      
  'srad_v1'         : 'rodinia',    
  'srad_v2'         : 'rodinia',    
  'streamcluster'   : 'rodinia',   
  'bfs'             : 'rodinia',             
## parboil
#  'bfs',
  'stencil'         : 'parboil',
  'mri-gridding'    : 'parboil',
  'lbm'             : 'parboil',
  'tpacf'           : 'parboil',
  'sad'             : 'parboil',
  'spmv'            : 'parboil',
  'histo'           : 'parboil',
  'sgemm'           : 'parboil',
  'mri-q'           : 'parboil',
  'cutcp'           : 'parboil',
## CUDA
  'BlackScholes'          : 'cuda',
  'FDTD3d'                : 'cuda',
  'HSOpticalFlow'         : 'cuda',
  'Interval'              : 'cuda',
  'Mandelbrot'            : 'cuda',
  'MersenneTwister'       : 'cuda',
  'MonteCarlo'            : 'cuda',
  'MonteCarloMultiGPU'    : 'cuda',
  'SobelFilter'           : 'cuda',
  'SobolQRNG'             : 'cuda',
  'bicubicTexture'        : 'cuda',
  'bilateralFilter'       : 'cuda',
  'binomialOptions'       : 'cuda',
  'boxFilter'             : 'cuda',
  'convolutionFFT2D'      : 'cuda',
  'convolutionSeparable'  : 'cuda',
  'convolutionTexture'    : 'cuda',
  'data'                  : 'cuda',
  'dct8x8'                : 'cuda',
  'dwtHaar1D'             : 'cuda',
  'dxtc'                  : 'cuda',
  'eigenvalues'           : 'cuda',
  'fastWalshTransform'    : 'cuda',
  'fluidsGL'              : 'cuda',
  'histogram'             : 'cuda',
  'imageDenoising'        : 'cuda',
  'inlinePTX'             : 'cuda',
  'lineOfSight'           : 'cuda',
  'marchingCubes'         : 'cuda',
  'matrixMul'             : 'cuda',
  'matrixMulDrv'          : 'cuda',
  'matrixMulDynlinkJIT'   : 'cuda',
  'mergeSort'             : 'cuda',
  'nbody'                 : 'cuda',
  'oceanFFT'              : 'cuda',
  'particles'             : 'cuda',
  'postProcessGL'         : 'cuda',
  'quasirandomGenerator'  : 'cuda',
  'radixSortThrust'       : 'cuda',
  'recursiveGaussian'     : 'cuda',
  'reduction'             : 'cuda',
  'scalarProd'            : 'cuda',
  'smokeParticles'        : 'cuda',
#  'FunctionPointers'     : 'cuda',
#  'alignedTypes'         : 'cuda',
#  'asyncAPI'             : 'cuda',
#  'bandwidthTest'        : 'cuda',
#  'clock'                : 'cuda',
#  'concurrentKernels'    : 'cuda',
#  'cppIntegration'       : 'cuda',
#  'deviceQuery'          : 'cuda',
#  'deviceQueryDrv'       : 'cuda',
#  'newdelete'            : 'cuda',
#  'ptxjit'               : 'cuda',
#  'scan' : 'cuda',
#  'simpleAssert' : 'cuda',
#  'simpleAtomicIntrinsics' : 'cuda',
#  'simpleCubemapTexture' : 'cuda',
#  'simpleGL' : 'cuda',
#  'simpleLayeredTexture' : 'cuda',
#  'simpleMPI' : 'cuda',
#  'simpleMultiCopy' : 'cuda',
#  'simpleMultiGPU' : 'cuda',
#  'simpleP2P' : 'cuda',
#  'simplePitchLinearTexture' : 'cuda',
#  'simplePrintf' : 'cuda',
#  'simpleStreams' : 'cuda',
#  'simpleSurfaceWrite' : 'cuda',
#  'simpleTemplates' : 'cuda',
#  'simpleTexture' : 'cuda',
#  'simpleTexture3D' : 'cuda',
#  'simpleTextureDrv' : 'cuda',
#  'simpleVoteIntrinsics' : 'cuda',
#  'simpleZeroCopy' : 'cuda',
}


"""
Global variables
"""
ocelot_dir = '/usr/local/jaekyu/gpuocelot_may13'
config_file = '/usr/local/jaekyu/configure.ocelot'


"""
Argument parser
"""
def process_options():
  parser = argparse.ArgumentParser(description='gputracegen.py')
  parser.add_argument('-suite', action='append', nargs='*', dest='suite', help='suite list')
  parser.add_argument('-bench_list', action='append', nargs='*', dest='bench_list', help='suite list')
  parser.add_argument('-bench', action='store', dest='bench', help='benchmark')
  parser.add_argument('-j', action='store', type=int, dest='thread', default=1, help='# thread')

  return parser


"""
Get occupancy (register, shared memory) of a kernel
Store this information into the file
"""
def get_occupancy(src_dir, extra_inc):
  file_name = src_dir + '/occupancy.txt'
  file = open(file_name, 'w')

  ## you have to fix this with your configuration!!!!!
  cmd = 'nvcc --cubin --ptxas-options=-v -arch sm_20 *.cu %s -I. -DCUDA_OCCUPANCY' % extra_inc
  p = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE, 
      stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=True)     
  (fi, fo, fe) = (p.stdin, p.stdout, p.stderr)


  kernel_name = ''
  register    = ''
  shared_mem  = '0'
  for line in fe.readlines():
    token = line.split()
    if len(token) == 0:
      pass
    elif re.match('ptxas info    : Compiling entry function', line):
      kernel_name = token[6].replace('\'', '')
    elif re.match('ptxas info    : Used ', line):
      register = token[4]
      if len(token) >= 8 and token[8] == 'smem,':
        shared_mem_list = token[6].split('+')
        if len(shared_mem_list) > 1:
          shared_mem = str(int(shared_mem_list[0]) + int(shared_mem_list[1]));
        else:
          shared_mem = token[6]
      file.write("%s %s %s\n" % (kernel_name, register, shared_mem))
      kernel_name = ''
      register = ''
      shared_mem = '0'
  file.close()


"""
Create a trace
"""
def create_trace(src_dir, suite, bench, cmd):
  # set trace path
  trace_directory = '/trace1/ptx_may2013'
  trace_path = '%s/%s/%s' % (trace_directory, suite, bench)

  # set env. variables
  os.environ['TRACE_PATH']       = trace_path
  os.environ['USE_KERNEL_NAME']  = '1'
  os.environ['KERNEL_INFO_PATH'] = '%s/occupancy.txt' % src_dir
  os.environ['COMPUTE_VERSION']  = '2.0'

  # delete existing traces
  if os.path.exists(trace_path):
    print('>>>>> deleting existing traces')
    os.system('rm -rf %s' % trace_path)

  # run a binary to create traces
  os.system('%s < /usr/local/jaekyu/dummy.txt' % cmd)


"""
Create traces for rodinia suite
"""
def rodinia(only_bench):
  directory = '/usr/local/jaekyu/gpuocelot_may13/rodinia_2.3'
  suite = [
    'b+tree',          ##
    'backprop',        ##
    'bfs',             ##
    'cell_cuda',       ##
    'cfd',             ##
    'gaussian',        ## 
    'heartwall',       ## register error
    'hotspot',         ##
    'kmeans',          ## 
    'lavaMD',          ## 
    'leukocyte',       ##
    'lud',             ##
#    'mummergpu',       ## error
    'nn',              ##
    'nw',              ## 
#    'particlefilter',  ## seg fault 
    'pathfinder',      ##
    'srad_v1',         ##
    'srad_v2',         ##
    'streamcluster',   ##
  ]

  ## benchmark directories
  bench_dirs = {}
  for bench in suite:
    bench_dirs[bench] = bench
  bench_dirs['srad_v1'] = 'srad/srad_v1'
  bench_dirs['srad_v2'] = 'srad/srad_v2'


  ## source directories to get occupancy
  bench_src_dirs = {}
  for bench in suite:
    bench_src_dirs[bench] = ''
  bench_src_dirs['b+tree'] = '/kernel'
  bench_src_dirs['lavaMD'] = '/kernel'
  bench_src_dirs['leukocyte'] = '/CUDA'
  bench_src_dirs['lud'] = '/cuda'
  bench_src_dirs['mummergpu'] = '/src'


  ## extra inc directory
  bench_inc = {}
  for bench in suite:
    bench_inc[bench] = ''
  bench_inc['leukocyte'] = '-I../meschach_lib -I/usr/local/jaekyu/gpuocelot_may13/tests/cuda4.1sdk/sdk'
  bench_inc['lud'] = '-I../common'
  bench_inc['cfd'] = '-I/usr/local/cuda-5.0/samples/common/inc -I/usr/local/jaekyu/gpuocelot_may13/tests/cuda4.1sdk/sdk'


  bench_cmd = {
    'b+tree'         : './b+tree.out file ../../data/b+tree/mil.txt command ../../data/b+tree/command.txt',
    'backprop'       : './backprop 1048576', ## 262144
    'bfs'            : './bfs ../../data/bfs/graph1MW_6.txt', 
    'cell_cuda'      : './cell 96 96 96 10 2',
#    'cell_cuda'      : './cell 24 24 24 10 2',
    'cfd'            : './euler3d ../../data/cfd/missile.domn.0.2M',
    'gaussian'       : './gaussian ../../data/gaussian/matrix1024.txt', 
    'heartwall'      : './heartwall ../../data/heartwall/test.avi 5',
    'hotspot'        : './hotspot 1024 2 2 ../../data/hotspot/temp_1024 ../../data/hotspot/power_1024 output.out',
    'kmeans'         : './kmeans -o -i ../../data/kmeans/kdd_cup', 
    'lavaMD'         : './lavaMD -boxes1d 10',     
    'leukocyte'      : './CUDA/leukocyte  ../../data/leukocyte/testfile.avi 5',
    'lud'            : 'cuda/lud_cuda -i ../../data/lud/2048vi.dat',
    'mummergpu'      : 'bin/mummergpu ../../data/mummergpu/NC_003997.fna ../../data/mummergpu/NC_003997_q100bp.fna > NC_00399.out',
    'nn'             : './nn filelist_4 -r 5 -lat 30 -lng 90',
    'nw'             : './needle 2048 10',
    'particlefilter' : './particlefilter_float -x 128 -y 128 -z 10 -np 1000',
    'pathfinder'     : './pathfinder 1000000 1000 20',
    'srad_v1'        : './srad 100 0.5 502 458',
    'srad_v2'        : './srad 2048 2048 0 127 0 127 0.5 2',
    'streamcluster'  : './sc_gpu 10 20 256 65536 65536 1000 none output.txt 1', 
  }



  for bench in suite:
    if only_bench != None and bench != only_bench:
      continue
    print('>>>>> running %s' % bench)
    bench_dir = '%s/cuda/%s' % (directory, bench_dirs[bench])
    os.chdir(bench_dir)
    os.system('make clean')
    os.system('make')
    
    os.system('cp %s .' % config_file)

    # get occupancy
    print('%s%s' % (bench_dir, bench_src_dirs[bench]))
    os.chdir('%s%s' % (bench_dir, bench_src_dirs[bench]))
    get_occupancy(bench_dir, bench_inc[bench])
    os.chdir('%s' % bench_dir)

    # create a trace
    create_trace(bench_dir, 'rodinia_2.1', bench, bench_cmd[bench])


"""
Create traces for parboil suite
"""
def parboil(only_bench):
  directory = '/usr/local/jaekyu/gpuocelot_may13/parboil'
  suite = [
#    'bfs',
    'stencil',
    'mri-gridding',
    'lbm',
    'tpacf',
    'sad',
    'spmv',
    'histo',
    'sgemm',
    'mri-q',
    'cutcp',
  ]

  dataset = {
    'bfs' : '1M', # 'NY', 'SF', 'UT'
    'cutcp' : 'large', # 'small'
    'histo' : 'default', # 'large'
    'lbm' : 'long', # 'short'
    'mri-gridding' : 'small',
    'mri-q' : 'large', # 'small'
    'sad' : 'default', # 'large'
    'sgemm' : 'medium', # 'small'
    'spmv' : 'large', # 'medium', 'small'
    'stencil' : 'default', # 'small'
    'tpacf' : 'large', # 'medium', 'small'
  }

  os.chdir(directory)

  for bench in suite:
    if only_bench != None and bench != only_bench:
      continue
    print('>>>>> running %s' % bench)
    bench_dir = '%s/benchmarks/%s/src/cuda' % (directory, bench)

    # clean
    os.system('./parboil clean %s cuda' % bench)

    # build
    os.system('./parboil compile %s cuda' % bench)

    # copy configure.ocelot file
    os.system('cp %s %s/../../' % (config_file, bench_dir))

    os.chdir(bench_dir)
    get_occupancy(bench_dir, '-I../../../../common/include')

    os.chdir(directory)
    create_trace(bench_dir, 'parboil_2.5', bench, './parboil run %s cuda %s' % (bench, dataset[bench]))


"""
Cuda 4.1 suite
"""
def cuda4_1(only_bench):
  suite_dir = '%s/tests/cuda4.1sdk' % ocelot_dir
  bin_dir = '%s/.release_build' % suite_dir

  suite = [
    'BlackScholes',
    'FDTD3d',
#    'FunctionPointers',
    'HSOpticalFlow',
    'Interval',
    'Mandelbrot',
    'MersenneTwister',
    'MonteCarlo',
    'MonteCarloMultiGPU',
    'SobelFilter',
    'SobolQRNG',
#    'alignedTypes',
#    'asyncAPI',
#    'bandwidthTest',
    'bicubicTexture',
    'bilateralFilter',
    'binomialOptions',
    'boxFilter',
#    'clock',
#    'concurrentKernels',
    'convolutionFFT2D',
    'convolutionSeparable',
    'convolutionTexture',
#    'cppIntegration',
    'data',
    'dct8x8',
#    'deviceQuery',
#    'deviceQueryDrv',
    'dwtHaar1D',
    'dxtc',
    'eigenvalues',
    'fastWalshTransform',
    'fluidsGL',
    'histogram',
    'imageDenoising',
    'inlinePTX',
    'lineOfSight',
    'marchingCubes',
    'matrixMul',
    'matrixMulDrv',
    'matrixMulDynlinkJIT',
    'mergeSort',
    'nbody',
#    'newdelete',
    'oceanFFT',
    'particles',
    'postProcessGL',
#    'ptxjit',
    'quasirandomGenerator',
    'radixSortThrust',
    'recursiveGaussian',
    'reduction',
    'scalarProd',
#    'scan',
#    'simpleAssert',
#    'simpleAtomicIntrinsics',
#    'simpleCubemapTexture',
#    'simpleGL',
#    'simpleLayeredTexture',
#    'simpleMPI',
#    'simpleMultiCopy',
#    'simpleMultiGPU',
#    'simpleP2P',
#    'simplePitchLinearTexture',
#    'simplePrintf',
#    'simpleStreams',
#    'simpleSurfaceWrite',
#    'simpleTemplates',
#    'simpleTexture',
#    'simpleTexture3D',
#    'simpleTextureDrv',
#    'simpleVoteIntrinsics',
#    'simpleZeroCopy',
    'smokeParticles',
  ]

  os.chdir(suite_dir)

  # cleanup
#  os.system('scons -c')

  # buildup
#  os.system('scons -j 3')

  # copy configure.ocelot file
  os.chdir('%s/.release_build' % suite_dir)
  os.system('cp %s .' % config_file)

  # generate traces
  for bench in suite:
    if only_bench != None and bench != only_bench:
      continue
    print('>>>>> running %s' % bench)

    src_dir = '%s/tests/%s' % (suite_dir, bench)
    os.chdir(src_dir)
    get_occupancy(src_dir, '')

    os.chdir(bin_dir)
    binary = bench[0].title()
    binary = './' + binary + bench[1:]
    create_trace(src_dir, 'cuda4_2', bench, binary)


"""
Cuda 3.2 SDK suite
"""
def cuda3_2(only_bench):
  suite_dir = '%s/tests/cuda3.2' % ocelot_dir
  bin_dir = '%s/.release_build' % suite_dir

  # get benchmark list
  os.chdir('%s/tests' % suite_dir)
  suite = glob.glob('*')
  for bench in suite:
    if not os.path.isdir(bench):
      suite.remove(bench)
  suite.sort()

  os.chdir(suite_dir)

  # cleanup
  os.system('scons -c')

  # buildup
  os.system('scons -j 3')

  # copy configure.ocelot file
  os.chdir('%s/.release_build' % suite_dir)
  os.system('cp %s .' % config_file)

  # generate traces
  for bench in suite:
    if only_bench != None and bench != only_bench:
      continue
    print('>>>>> running %s' % bench)

    src_dir = '%s/tests/%s' % (suite_dir, bench)
    os.chdir(src_dir)
    get_occupancy(src_dir)

    os.chdir(bin_dir)
    binary = bench[0].title()
    binary = './' + binary + bench[1:]
    create_trace(src_dir, 'cuda3_2', bench, binary)


"""
Cuda 2.2 SDK suite
"""
def cuda2_2(only_bench):
  suite_dir = '%s/tests/cuda2.2' % ocelot_dir
  bin_dir = '%s/.release_build' % suite_dir

  # get benchmark list
  os.chdir('%s/tests' % suite_dir)
  suite = glob.glob('*')
  for bench in suite:
    if not os.path.isdir(bench):
      suite.remove(bench)
  suite.sort()

  os.chdir(suite_dir)
  
  # cleanup
  os.system('scons -c')

  # buildup
  os.system('scons -j 3')

  # copy configure.ocelot file
  os.chdir('%s/.release_build' % suite_dir)
  os.system('cp %s .' % config_file)

  # generate traces
  for bench in suite:
    if only_bench != None and bench != only_bench:
      continue
    print('>>>>> running %s' % bench)

    src_dir = '%s/tests/%s' % (suite_dir, bench)
    os.chdir(src_dir)
    get_occupancy(src_dir)

    os.chdir(bin_dir)
    binary = bench[0].title()
    binary = './' + binary + bench[1:]
    create_trace(src_dir, 'cuda2_2', bench, binary)


"""
Main function
"""
def main():
  parser = process_options()
  args = parser.parse_args()


  if args.suite == None:
    if args.bench_list == None:
      sys.exit()
    
    args.bench_list = sum(args.bench_list, [])
    for bench in args.bench_list:
      if bench_suite[bench] == 'rodinia':
        rodinia(bench)
      elif bench_suite[bench] == 'parboil':
        parboil(bench)
      elif bench_suite[bench] == 'cuda':
        cuda4_1(bench)
  else:
    args.suite = sum(args.suite, [])
    for suite in args.suite:
      if suite == 'rodinia':
        rodinia(args.bench)
      elif suite == 'parboil':
        parboil(args.bench)
      elif suite == 'cuda4.1':
        cuda4_1(args.bench)
      elif suite == 'cuda3.2':
        cuda3_2(args.bench)
      elif suite == 'cuda2.2':
        cuda2_2(args.bench)
      elif suite == 'all':
        rodinia(args.bench)
        parboil(args.bench)
        cuda4_1(args.bench)
        cuda3_2(args.bench)
        cuda2_2(args.bench)


if __name__ == '__main__':
  main()

