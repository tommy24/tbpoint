#!/usr/bin/perl



@cpu_mem = ('gcc', 'omnetpp', 'soplex', 'sphinx3');
@cpu_comp = ('perlbench', 'gobmk', 'hmmer', 'sjeng', 'h264ref', 'xalancbmk', 'gamess', 'gromacs', 'namd', 'dealII', 'povray', 'calculix', 'tonto');


@gpu_a = ('cell', 'Dxtc1', 'FastWalshTransform', 'VolumeRender');
@gpu_b = ('stencil', 'mm', 'SAD', 'hotspot', 'MergeSort');
@gpu_c = ('QuasirandomGenerator1', 'SobolQRNG1', 'RayTracing');
@gpu_d = ('BlackScholes1', 'Histogram2561', 'Reduction1', 'AES');
@gpu_e = ('cfd', 'bfs', 'lbm-gpu');


###############################################################################################
$num_bench = 5;
# non + non + A
print "\$SUITES{'mix4a'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $cpu1_index = int(rand(@cpu_mem));
  $cpu2_index = int(rand(@cpu_mem));
  $cpu3_index = int(rand(@cpu_comp));
  $cpu4_index = int(rand(@cpu_comp));
  $gpu_index = int(rand(@gpu_a));

  print "$cpu_mem[$cpu1_index]_$cpu_mem[$cpu2_index]_$cpu_comp[$cpu3_index]_$cpu_comp[$cpu4_index]_$gpu_a[$gpu_index]\@ref";
}
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $cpu1_index = int(rand(@cpu_mem));
  $cpu2_index = int(rand(@cpu_mem));
  $cpu3_index = int(rand(@cpu_mem));
  $cpu4_index = int(rand(@cpu_comp));
  $gpu_index = int(rand(@gpu_a));

  print "$cpu_mem[$cpu1_index]_$cpu_mem[$cpu2_index]_$cpu_mem[$cpu3_index]_$cpu_comp[$cpu4_index]_$gpu_a[$gpu_index]\@ref";
}
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $cpu1_index = int(rand(@cpu_mem));
  $cpu2_index = int(rand(@cpu_mem));
  $cpu3_index = int(rand(@cpu_mem));
  $cpu4_index = int(rand(@cpu_mem));
  $gpu_index = int(rand(@gpu_a));

  print "$cpu_mem[$cpu1_index]_$cpu_mem[$cpu2_index]_$cpu_mem[$cpu3_index]_$cpu_mem[$cpu4_index]_$gpu_a[$gpu_index]\@ref";
}
print "';\n";


# non + non + B
print "\$SUITES{'mix4b'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $cpu1_index = int(rand(@cpu_mem));
  $cpu2_index = int(rand(@cpu_mem));
  $cpu3_index = int(rand(@cpu_comp));
  $cpu4_index = int(rand(@cpu_comp));
  $gpu_index = int(rand(@gpu_b));

  print "$cpu_mem[$cpu1_index]_$cpu_mem[$cpu2_index]_$cpu_comp[$cpu3_index]_$cpu_comp[$cpu4_index]_$gpu_b[$gpu_index]\@ref";
}
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $cpu1_index = int(rand(@cpu_mem));
  $cpu2_index = int(rand(@cpu_mem));
  $cpu3_index = int(rand(@cpu_mem));
  $cpu4_index = int(rand(@cpu_comp));
  $gpu_index = int(rand(@gpu_b));

  print "$cpu_mem[$cpu1_index]_$cpu_mem[$cpu2_index]_$cpu_mem[$cpu3_index]_$cpu_comp[$cpu4_index]_$gpu_b[$gpu_index]\@ref";
}
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $cpu1_index = int(rand(@cpu_mem));
  $cpu2_index = int(rand(@cpu_mem));
  $cpu3_index = int(rand(@cpu_mem));
  $cpu4_index = int(rand(@cpu_mem));
  $gpu_index = int(rand(@gpu_b));

  print "$cpu_mem[$cpu1_index]_$cpu_mem[$cpu2_index]_$cpu_mem[$cpu3_index]_$cpu_mem[$cpu4_index]_$gpu_b[$gpu_index]\@ref";
}
print "';\n";


# non + non + C
print "\$SUITES{'mix4c'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $cpu1_index = int(rand(@cpu_mem));
  $cpu2_index = int(rand(@cpu_mem));
  $cpu3_index = int(rand(@cpu_comp));
  $cpu4_index = int(rand(@cpu_comp));
  $gpu_index = int(rand(@gpu_c));

  print "$cpu_mem[$cpu1_index]_$cpu_mem[$cpu2_index]_$cpu_comp[$cpu3_index]_$cpu_comp[$cpu4_index]_$gpu_c[$gpu_index]\@ref";
}
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $cpu1_index = int(rand(@cpu_mem));
  $cpu2_index = int(rand(@cpu_mem));
  $cpu3_index = int(rand(@cpu_mem));
  $cpu4_index = int(rand(@cpu_comp));
  $gpu_index = int(rand(@gpu_c));

  print "$cpu_mem[$cpu1_index]_$cpu_mem[$cpu2_index]_$cpu_mem[$cpu3_index]_$cpu_comp[$cpu4_index]_$gpu_c[$gpu_index]\@ref";
}
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $cpu1_index = int(rand(@cpu_mem));
  $cpu2_index = int(rand(@cpu_mem));
  $cpu3_index = int(rand(@cpu_mem));
  $cpu4_index = int(rand(@cpu_mem));
  $gpu_index = int(rand(@gpu_c));

  print "$cpu_mem[$cpu1_index]_$cpu_mem[$cpu2_index]_$cpu_mem[$cpu3_index]_$cpu_mem[$cpu4_index]_$gpu_c[$gpu_index]\@ref";
}
print "';\n";


# non + non + D
print "\$SUITES{'mix4d'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $cpu1_index = int(rand(@cpu_mem));
  $cpu2_index = int(rand(@cpu_mem));
  $cpu3_index = int(rand(@cpu_comp));
  $cpu4_index = int(rand(@cpu_comp));
  $gpu_index = int(rand(@gpu_d));

  print "$cpu_mem[$cpu1_index]_$cpu_mem[$cpu2_index]_$cpu_comp[$cpu3_index]_$cpu_comp[$cpu4_index]_$gpu_d[$gpu_index]\@ref";
}
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $cpu1_index = int(rand(@cpu_mem));
  $cpu2_index = int(rand(@cpu_mem));
  $cpu3_index = int(rand(@cpu_mem));
  $cpu4_index = int(rand(@cpu_comp));
  $gpu_index = int(rand(@gpu_d));

  print "$cpu_mem[$cpu1_index]_$cpu_mem[$cpu2_index]_$cpu_mem[$cpu3_index]_$cpu_comp[$cpu4_index]_$gpu_d[$gpu_index]\@ref";
}
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $cpu1_index = int(rand(@cpu_mem));
  $cpu2_index = int(rand(@cpu_mem));
  $cpu3_index = int(rand(@cpu_mem));
  $cpu4_index = int(rand(@cpu_mem));
  $gpu_index = int(rand(@gpu_d));

  print "$cpu_mem[$cpu1_index]_$cpu_mem[$cpu2_index]_$cpu_mem[$cpu3_index]_$cpu_mem[$cpu4_index]_$gpu_d[$gpu_index]\@ref";
}
print "';\n";

# non + non + A
print "\$SUITES{'mix4e'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $cpu1_index = int(rand(@cpu_mem));
  $cpu2_index = int(rand(@cpu_mem));
  $cpu3_index = int(rand(@cpu_comp));
  $cpu4_index = int(rand(@cpu_comp));
  $gpu_index = int(rand(@gpu_e));

  print "$cpu_mem[$cpu1_index]_$cpu_mem[$cpu2_index]_$cpu_comp[$cpu3_index]_$cpu_comp[$cpu4_index]_$gpu_e[$gpu_index]\@ref";
}
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $cpu1_index = int(rand(@cpu_mem));
  $cpu2_index = int(rand(@cpu_mem));
  $cpu3_index = int(rand(@cpu_mem));
  $cpu4_index = int(rand(@cpu_comp));
  $gpu_index = int(rand(@gpu_e));

  print "$cpu_mem[$cpu1_index]_$cpu_mem[$cpu2_index]_$cpu_mem[$cpu3_index]_$cpu_comp[$cpu4_index]_$gpu_e[$gpu_index]\@ref";
}
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $cpu1_index = int(rand(@cpu_mem));
  $cpu2_index = int(rand(@cpu_mem));
  $cpu3_index = int(rand(@cpu_mem));
  $cpu4_index = int(rand(@cpu_mem));
  $gpu_index = int(rand(@gpu_e));

  print "$cpu_mem[$cpu1_index]_$cpu_mem[$cpu2_index]_$cpu_mem[$cpu3_index]_$cpu_mem[$cpu4_index]_$gpu_e[$gpu_index]\@ref";
}
print "';\n";


###############################################################################################
##### ===== ----- Multiple GPUs ----- ===== #####
$num_bench = 5;

print "\$SUITES{'gpu2aa'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $gpu_index1 = int(rand(@gpu_a));
  $gpu_index2 = int(rand(@gpu_a));

  print "$gpu_a[$gpu_index1]_$gpu_a[$gpu_index2]\@ref";
}
print "';\n";

print "\$SUITES{'gpu2ab'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $gpu_index1 = int(rand(@gpu_a));
  $gpu_index2 = int(rand(@gpu_b));

  print "$gpu_a[$gpu_index1]_$gpu_b[$gpu_index2]\@ref";
}
print "';\n";


print "\$SUITES{'gpu2ac'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $gpu_index1 = int(rand(@gpu_a));
  $gpu_index2 = int(rand(@gpu_c));

  print "$gpu_a[$gpu_index1]_$gpu_c[$gpu_index2]\@ref";
}
print "';\n";


print "\$SUITES{'gpu2ad'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $gpu_index1 = int(rand(@gpu_a));
  $gpu_index2 = int(rand(@gpu_d));

  print "$gpu_a[$gpu_index1]_$gpu_d[$gpu_index2]\@ref";
}
print "';\n";


print "\$SUITES{'gpu2ae'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $gpu_index1 = int(rand(@gpu_a));
  $gpu_index2 = int(rand(@gpu_e));

  print "$gpu_a[$gpu_index1]_$gpu_e[$gpu_index2]\@ref";
}
print "';\n";


print "\$SUITES{'gpu2bb'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $gpu_index1 = int(rand(@gpu_b));
  $gpu_index2 = int(rand(@gpu_b));

  print "$gpu_b[$gpu_index1]_$gpu_b[$gpu_index2]\@ref";
}
print "';\n";


print "\$SUITES{'gpu2bc'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $gpu_index1 = int(rand(@gpu_b));
  $gpu_index2 = int(rand(@gpu_c));

  print "$gpu_b[$gpu_index1]_$gpu_c[$gpu_index2]\@ref";
}
print "';\n";


print "\$SUITES{'gpu2bd'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $gpu_index1 = int(rand(@gpu_b));
  $gpu_index2 = int(rand(@gpu_d));

  print "$gpu_b[$gpu_index1]_$gpu_d[$gpu_index2]\@ref";
}
print "';\n";


print "\$SUITES{'gpu2be'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $gpu_index1 = int(rand(@gpu_b));
  $gpu_index2 = int(rand(@gpu_e));

  print "$gpu_b[$gpu_index1]_$gpu_e[$gpu_index2]\@ref";
}
print "';\n";


print "\$SUITES{'gpu2cc'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $gpu_index1 = int(rand(@gpu_c));
  $gpu_index2 = int(rand(@gpu_c));

  print "$gpu_c[$gpu_index1]_$gpu_c[$gpu_index2]\@ref";
}
print "';\n";


print "\$SUITES{'gpu2cd'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $gpu_index1 = int(rand(@gpu_c));
  $gpu_index2 = int(rand(@gpu_d));

  print "$gpu_c[$gpu_index1]_$gpu_d[$gpu_index2]\@ref";
}
print "';\n";


print "\$SUITES{'gpu2ce'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $gpu_index1 = int(rand(@gpu_c));
  $gpu_index2 = int(rand(@gpu_e));

  print "$gpu_c[$gpu_index1]_$gpu_e[$gpu_index2]\@ref";
}
print "';\n";


print "\$SUITES{'gpu2dd'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $gpu_index1 = int(rand(@gpu_d));
  $gpu_index2 = int(rand(@gpu_d));

  print "$gpu_d[$gpu_index1]_$gpu_d[$gpu_index2]\@ref";
}
print "';\n";


print "\$SUITES{'gpu2de'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $gpu_index1 = int(rand(@gpu_d));
  $gpu_index2 = int(rand(@gpu_e));

  print "$gpu_d[$gpu_index1]_$gpu_e[$gpu_index2]\@ref";
}
print "';\n";


print "\$SUITES{'gpu2ee'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $gpu_index1 = int(rand(@gpu_e));
  $gpu_index2 = int(rand(@gpu_e));

  print "$gpu_e[$gpu_index1]_$gpu_e[$gpu_index2]\@ref";
}
print "';\n";



###############################################################################################




$num_bench = 25;

print "\$SUITES{'compgpua'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $cpu_index = int(rand(@cpu_comp));
  $gpu_index = int(rand(@gpu_a));

  print "$cpu_comp[$cpu_index]_$gpu_a[$gpu_index]";
}
print "';\n";

print "\$SUITES{'compgpub'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $cpu_index = int(rand(@cpu_comp));
  $gpu_index = int(rand(@gpu_b));

  print "$cpu_comp[$cpu_index]_$gpu_b[$gpu_index]";
}
print "';\n";

print "\$SUITES{'compgpuc'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $cpu_index = int(rand(@cpu_comp));
  $gpu_index = int(rand(@gpu_c));

  print "$cpu_comp[$cpu_index]_$gpu_c[$gpu_index]";
}
print "';\n";

print "\$SUITES{'compgpud'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $cpu_index = int(rand(@cpu_comp));
  $gpu_index = int(rand(@gpu_d));

  print "$cpu_comp[$cpu_index]_$gpu_d[$gpu_index]";
}
print "';\n";

print "\$SUITES{'compgpue'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $cpu_index = int(rand(@cpu_comp));
  $gpu_index = int(rand(@gpu_e));

  print "$cpu_comp[$cpu_index]_$gpu_e[$gpu_index]";
}
print "';\n";

###############################################################################################



$num_bench = 5;
# mem + mem + A
print "\$SUITES{'cpu1gpua'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $cpu1_index = int(rand(@cpu_mem));
  $cpu2_index = $cpu1_index;
  while ($cpu2_index == $cpu1_index) {
    $cpu2_index = int(rand(@cpu_mem));
  }
  $gpu_index = int(rand(@gpu_a));

  print "$cpu_mem[$cpu1_index]_$cpu_mem[$cpu2_index]_$gpu_a[$gpu_index]";
}
print "';\n";

# mem + mem + B
print "\$SUITES{'cpu1gpub'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $cpu1_index = int(rand(@cpu_mem));
  $cpu2_index = $cpu1_index;
  while ($cpu2_index == $cpu1_index) {
    $cpu2_index = int(rand(@cpu_mem));
  }
  $gpu_index = int(rand(@gpu_b));

  print "$cpu_mem[$cpu1_index]_$cpu_mem[$cpu2_index]_$gpu_b[$gpu_index]";
}
print "';\n";

# mem + mem + C
print "\$SUITES{'cpu1gpuc'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $cpu1_index = int(rand(@cpu_mem));
  $cpu2_index = $cpu1_index;
  while ($cpu2_index == $cpu1_index) {
    $cpu2_index = int(rand(@cpu_mem));
  }
  $gpu_index = int(rand(@gpu_c));

  print "$cpu_mem[$cpu1_index]_$cpu_mem[$cpu2_index]_$gpu_c[$gpu_index]";
}
print "';\n";

# mem + mem + D
print "\$SUITES{'cpu1gpud'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $cpu1_index = int(rand(@cpu_mem));
  $cpu2_index = $cpu1_index;
  while ($cpu2_index == $cpu1_index) {
    $cpu2_index = int(rand(@cpu_mem));
  }
  $gpu_index = int(rand(@gpu_d));

  print "$cpu_mem[$cpu1_index]_$cpu_mem[$cpu2_index]_$gpu_d[$gpu_index]";
}
print "';\n";

# mem + mem + E
print "\$SUITES{'cpu1gpue'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $cpu1_index = int(rand(@cpu_mem));
  $cpu2_index = $cpu1_index;
  while ($cpu2_index == $cpu1_index) {
    $cpu2_index = int(rand(@cpu_mem));
  }
  $gpu_index = int(rand(@gpu_e));

  print "$cpu_mem[$cpu1_index]_$cpu_mem[$cpu2_index]_$gpu_e[$gpu_index]";
}
print "';\n";


###############################################################################################


# mem + non + A
print "\$SUITES{'cpu2gpua'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $cpu1_index = int(rand(@cpu_mem));
  $cpu2_index = int(rand(@cpu_comp));
  $gpu_index = int(rand(@gpu_a));

  print "$cpu_mem[$cpu1_index]_$cpu_comp[$cpu2_index]_$gpu_a[$gpu_index]";
}
print "';\n";

# mem + non + B
print "\$SUITES{'cpu2gpub'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $cpu1_index = int(rand(@cpu_mem));
  $cpu2_index = int(rand(@cpu_comp));
  $gpu_index = int(rand(@gpu_b));

  print "$cpu_mem[$cpu1_index]_$cpu_comp[$cpu2_index]_$gpu_b[$gpu_index]";
}
print "';\n";

# mem + non + C
print "\$SUITES{'cpu2gpuc'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $cpu1_index = int(rand(@cpu_mem));
  $cpu2_index = int(rand(@cpu_comp));
  $gpu_index = int(rand(@gpu_c));

  print "$cpu_mem[$cpu1_index]_$cpu_comp[$cpu2_index]_$gpu_c[$gpu_index]";
}
print "';\n";

# mem + non + D
print "\$SUITES{'cpu2gpud'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $cpu1_index = int(rand(@cpu_mem));
  $cpu2_index = int(rand(@cpu_comp));
  $gpu_index = int(rand(@gpu_d));

  print "$cpu_mem[$cpu1_index]_$cpu_comp[$cpu2_index]_$gpu_d[$gpu_index]";
}
print "';\n";

# mem + non + E
print "\$SUITES{'cpu2gpue'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $cpu1_index = int(rand(@cpu_mem));
  $cpu2_index = int(rand(@cpu_comp));
  $gpu_index = int(rand(@gpu_e));

  print "$cpu_mem[$cpu1_index]_$cpu_comp[$cpu2_index]_$gpu_e[$gpu_index]";
}
print "';\n";


###############################################################################################


# non + non + A
print "\$SUITES{'cpu3gpua'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $cpu1_index = int(rand(@cpu_comp));
  $cpu2_index = $cpu1_index;
  while ($cpu2_index == $cpu1_index) {
    $cpu2_index = int(rand(@cpu_comp));
  }
  $gpu_index = int(rand(@gpu_a));

  print "$cpu_comp[$cpu1_index]_$cpu_comp[$cpu2_index]_$gpu_a[$gpu_index]";
}
print "';\n";

# non + non + B
print "\$SUITES{'cpu3gpub'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $cpu1_index = int(rand(@cpu_comp));
  $cpu2_index = $cpu1_index;
  while ($cpu2_index == $cpu1_index) {
    $cpu2_index = int(rand(@cpu_comp));
  }
  $gpu_index = int(rand(@gpu_b));

  print "$cpu_comp[$cpu1_index]_$cpu_comp[$cpu2_index]_$gpu_b[$gpu_index]";
}
print "';\n";

# non + non + C
print "\$SUITES{'cpu3gpuc'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $cpu1_index = int(rand(@cpu_comp));
  $cpu2_index = $cpu1_index;
  while ($cpu2_index == $cpu1_index) {
    $cpu2_index = int(rand(@cpu_comp));
  }
  $gpu_index = int(rand(@gpu_c));

  print "$cpu_comp[$cpu1_index]_$cpu_comp[$cpu2_index]_$gpu_c[$gpu_index]";
}
print "';\n";

# non + non + D
print "\$SUITES{'cpu3gpud'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $cpu1_index = int(rand(@cpu_comp));
  $cpu2_index = $cpu1_index;
  while ($cpu2_index == $cpu1_index) {
    $cpu2_index = int(rand(@cpu_comp));
  }
  $gpu_index = int(rand(@gpu_d));

  print "$cpu_comp[$cpu1_index]_$cpu_comp[$cpu2_index]_$gpu_d[$gpu_index]";
}
print "';\n";

# non + non + E
print "\$SUITES{'cpu3gpue'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $cpu1_index = int(rand(@cpu_comp));
  $cpu2_index = $cpu1_index;
  while ($cpu2_index == $cpu1_index) {
    $cpu2_index = int(rand(@cpu_comp));
  }
  $gpu_index = int(rand(@gpu_e));

  print "$cpu_comp[$cpu1_index]_$cpu_comp[$cpu2_index]_$gpu_e[$gpu_index]";
}
print "';\n";


###############################################################################################
# streaming CPU (merge) + GPU
# STREAMMIX1
@merge = ('milc', 'libquantum', 'merge-convolve', 'merge-sepia');
$num_bench = 5;

print "\$SUITES{'streammix1a'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $cpu_index = int(rand(@merge));
  $gpu_index = int(rand(@gpu_a));

  print "$merge[$cpu_index]_$gpu_a[$gpu_index]\@ref";
}
print "';\n";

print "\$SUITES{'streammix1b'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $cpu_index = int(rand(@merge));
  $gpu_index = int(rand(@gpu_b));

  print "$merge[$cpu_index]_$gpu_b[$gpu_index]\@ref";
}
print "';\n";

print "\$SUITES{'streammix1c'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $cpu_index = int(rand(@merge));
  $gpu_index = int(rand(@gpu_c));

  print "$merge[$cpu_index]_$gpu_c[$gpu_index]\@ref";
}
print "';\n";

print "\$SUITES{'streammix1d'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $cpu_index = int(rand(@merge));
  $gpu_index = int(rand(@gpu_d));

  print "$merge[$cpu_index]_$gpu_d[$gpu_index]\@ref";
}
print "';\n";

print "\$SUITES{'streammix1e'} = '";
for ($ii = 0; $ii < $num_bench; ++$ii) {
  if ($ii != 0) {
    print " ";
  }
  $cpu_index = int(rand(@merge));
  $gpu_index = int(rand(@gpu_e));

  print "$merge[$cpu_index]_$gpu_e[$gpu_index]\@ref";
}
print "';\n";




###############################################################################################








