#!/usr/bin/python
#########################################################################################
# Author: Jaekyu Lee (kacear@gmail.com)
# Date: 7/5/2011
# Description: plot utility using matplotlib (python)
#########################################################################################


import optparse
import sys
import os.path
import re


#########################################################################################
# parsing options
#########################################################################################
def process_options():
  parser = optparse.OptionParser(usage='usage: %prog [options]', add_help_option=True)
  parser.add_option('--input', action='store', type='string', dest='input', default='in', help='input file name')
  parser.add_option('--output', action='store', type='string', dest='output', default='out', help='output file name')
  parser.add_option('--show-error', action='store_true', dest='show_error', help='show error lines in the figure')
  parser.add_option('--type', action='store', type='int', dest='type', default=0, \
      help='figure type (0: bar, 1: stacked-bar, 2: line, 3: line with markers, 4: horizontal bars')
  parser.add_option('--rotate_xlabel', action='store', type='string', dest='rotate_xlabel', default = '', help='rotate x-axis labels by -- degree')
  parser.add_option('--xsize', action='store', type='string', dest='xsize', default = '6.8', help='x size by inches')
  parser.add_option('--ysize', action='store', type='string', dest='ysize', default = '2.5', help='y size by inches')
  parser.add_option('--xlabel', action='store', type='string', dest='xlabel', default = '', help='y size by inches')
  parser.add_option('--ylabel', action='store', type='string', dest='ylabel', default = 'ylabel', help='y label')
  parser.add_option('--ylabel2', action='store', type='string', dest='ylabel2', default = 'ylabel2', help='second y label')
  parser.add_option('--ymin', action='store', type='string', dest='ymin', default = '', help='y min')
  parser.add_option('--ymax', action='store', type='string', dest='ymax', default = '', help='y max')
  parser.add_option('--adjust_top', action='store', type='string', dest='adj_top', default = '0.9', help='subplot top location')
  parser.add_option('--adjust_bottom', action='store', type='string', dest='adj_bottom', default = '0.1', help='subplot bottom location')
  parser.add_option('--adjust_right', action='store', type='string', dest='adj_right', default = '0.9', help='subplot right location')
  parser.add_option('--adjust_left', action='store', type='string', dest='adj_left', default = '0.125', help='subplot right location')
  parser.add_option('--font_size', action='store', type='string', dest='fsize', default = '9', help='subplot right location')
  parser.add_option('--legend_size', action='store', type='string', dest='lsize', default = '9', help='legend font size')
  parser.add_option('--legend_pos', action='store', type='string', dest='lpos', default = '9', help='legend location (9: upper center, 5: right')
  parser.add_option('--legend_ncol', action='store', type='string', dest='lncol', default = '', help='legend location (9: upper center, 5: right')
  parser.add_option('--second_yaxis', action='store_true', dest='second_yaxis', help='second axis')
  parser.add_option('--marker_only', action='store_true', dest='marker_only', help='print markers only without lines')
  parser.add_option('--marker_size', action='store', dest='marker_size', default='2', help='marker size')

  return parser


#########################################################################################
# write a matplotlib script
#########################################################################################
def write_script(options):
  ## initialization
  second_yaxis = 0
  if options.second_yaxis == True:
    second_yaxis = 1


  ## read input files
  ifile_name = options.input + '.txt'
  if os.path.exists(ifile_name) == False:
    print('error: input file %s does not exist' % ifile_name)
    exit(2)

  ifile = open(options.input + '.txt', 'r')

  line = ifile.readline()
  legend = []
  data = []
  xticklabel = line.split()
  for line in ifile:
    if re.match(r"^\s*$", line) or re.match(r"^\n$", line):
      continue

    line = line.lstrip()
    legend.append(line.split()[0])
    data.append(line.split()[1:])

  ifile.close()

  ## write MatPlotLib script
  ofile_name = options.input + '.py'
  if os.path.exists(ofile_name) == True:
    print('warning: output file exists')

  ofile = open(ofile_name, 'w')

  
  ## python library list
  ofile.write('#!/usr/bin/python\n')
  ofile.write('import matplotlib\n')
  ofile.write('matplotlib.use(\'Agg\')\n')
  ofile.write('import matplotlib.pyplot as plt\n')
  ofile.write('import numpy as np\n')
  ofile.write('import datetime\n')
  ofile.write('import matplotlib.font_manager\n')
  ofile.write('from matplotlib.backends.backend_pdf import PdfPages\n')
  ofile.write('from pylab import *\n')
  ofile.write('\n\n')


  ## default setting 
  ofile.write('## default settings\n')
  ofile.write('color_map = [\'0.5\', \'0.9\', \'0\', \'1\',       \'1\',      \'1\',  \'1\', \'1\']\n')
  ofile.write('hatch_map = [\'\',    \'\',    \'\',  \'//////\',  \'xxxxxx\', \'\\\\\\\\\\\\\\\\\', \'---------\', \'---\']\n')
  ofile.write('\n\n')


  ## prepare output file 
  ofile.write('## open the output file\n')
  ofile.write('pdf = PdfPages(\'%s.pdf\')\n' % (options.input))
  ofile.write('\n\n')


  ## prepare data
  ofile.write('## prepare data\n')
  ofile.write('xlabel = [')
  for ii in range(0, len(xticklabel)):
    ofile.write('\'%s\'' % (xticklabel[ii]))
    if ii != len(xticklabel) - 1:
      ofile.write(', ')
  ofile.write(']\n')

  for ii in range(0, len(data)):
    ofile.write('data%d = (' % (ii))
    for jj in range(0, len(data[ii])):
      ofile.write(data[ii][jj])
      if jj != len(data[ii]) - 1:
        ofile.write(', ')
    ofile.write(')\n')


  ## set number of bars
  ofile.write('num_bar_in_one_index = %d\n\n\n' % (len(data) - second_yaxis))


  ofile.write('N = %d\n' % (len(data[0])))
  ofile.write('index = np.arange(N)\n\n\n')


  ## setup figures
  ofile.write('## setup figures\n')
  ofile.write('matplotlib.rc(\'ytick\', labelsize=%s)\n' % (options.fsize))
  ofile.write('matplotlib.rc(\'xtick\', labelsize=%s)\n' % (options.fsize))
  ofile.write('matplotlib.rcParams[\'legend.fontsize\']      = \'%s\'\n' % (options.lsize))
  ofile.write('matplotlib.rcParams[\'font.family\']          = \'serif\'\n')
  ofile.write('matplotlib.rcParams[\'axes.linewidth\']       = \'0.5\'\n')
  ofile.write('matplotlib.rcParams[\'grid.color\']           = \'0.5\'\n')
  ofile.write('matplotlib.rcParams[\'grid.linestyle\']       = \'solid\'\n')
  ofile.write('matplotlib.rcParams[\'grid.linewidth\']       = \'0.2\'\n')
  ofile.write('matplotlib.rcParams[\'legend.columnspacing\'] = \'1\'\n')
  ofile.write('matplotlib.rcParams[\'legend.handletextpad\'] = \'0.5\'\n')
  ofile.write('matplotlib.rcParams[\'legend.numpoints\']     = \'1\'\n')
  ofile.write('matplotlib.rcParams[\'xtick.direction\']      = \'out\'\n')
  if options.type != 3:
    ofile.write('matplotlib.rcParams[\'xtick.major.size\']     = \'0\'\n')
    ofile.write('matplotlib.rcParams[\'xtick.minor.size\']     = \'4\'\n')
  else:
    ofile.write('matplotlib.rcParams[\'ytick.major.size\']     = \'0\'\n')
    ofile.write('matplotlib.rcParams[\'ytick.minor.size\']     = \'4\'\n')
  ofile.write('matplotlib.rcParams[\'ytick.direction\']      = \'out\'\n')
  ofile.write('fig = plt.figure(figsize=(%s,%s))\n' % (options.xsize, options.ysize))
  ofile.write('ax1 = fig.add_subplot(111)\n')
  ofile.write('\n')
    

  ## marker list
  markers = ['o', '*', 'D', 'x', '^', 's', 'd', 'h', '+', '*', ',', 'o', '.', '1', 'p', '3', '2', '4', 'H', 'v', 'x', '<', '>', '|', '_']


  ## type 0: Bar graphs
  if options.type == 0:
    ofile.write('width = 1.0 / (num_bar_in_one_index + 2)\n')
    for ii in range(0, len(data) - second_yaxis):
      ofile.write('bar%d = ax1.bar(index + width * %d, data%d, width, color=color_map[%d], hatch=hatch_map[%d], linewidth=0.2)\n' % (ii, ii+1, ii, ii, ii));
  ## type 1: stacked bar graph
  elif options.type == 1: 
    ofile.write('width = 1 - (0.18 + 0.18)\n')
    for ii in range(0, len(data) - second_yaxis):
      if ii != 0:
        ofile.write('bar%d = ax1.bar(index + 0.18, data%d, width, color=color_map[%d], hatch=hatch_map[%d], linewidth=0.2, bottom=data)\n' % (ii, ii, ii, ii));
        ofile.write('data = np.add(data, data%d)\n' % (ii))
      else:
        ofile.write('bar%d = ax1.bar(index + 0.18, data%d, width, color=color_map[%d], hatch=hatch_map[%d], linewidth=0.2)\n' % (ii, ii, ii, ii));
        ofile.write('data = data0\n');
  ## type 2: line graph
  elif options.type == 2: 
    for ii in range(0, len(data) - second_yaxis):
      if options.marker_only == True:
        ofile.write('bar%d, = ax1.plot(index+0.5, data%d, marker=\'%s\', markersize=%s, linewidth=0)\n' % (ii, ii, markers[ii], options.marker_size))
      else:
        ofile.write('bar%d, = ax1.plot(index+0.5, data%d, marker=\'%s\', markersize=%s)\n' % (ii, ii, markers[ii], options.marker_size))
  ## type 3: y-Bar graphs
  elif options.type == 3: 
    ofile.write('width = (1 - (0.18 + 0.18)) / num_bar_in_one_index\n')
    for ii in range(0, len(data) - second_yaxis):
      ofile.write('bar%d = ax1.barh(index + 0.18 + width * %d, data%d, width, color=color_map[%d], hatch=hatch_map[%d], linewidth=0.2)\n' % (ii, ii, ii, ii, ii));
  ofile.write('\n')


  ##
  ofile.write('ax1.set_axisbelow(True)\n\n')

  ## set title - FIXME
  ofile.write('ax1.set_title(\'\')\n')

  ## y-axis label
  if options.type != 3:
    ofile.write('ax1.set_ylabel(\'%s\', fontsize=%s, va=\'center\')\n' % (options.ylabel, options.fsize))


  ## x-axis label
  if options.xlabel != '':
    ofile.write('ax1.set_xlabel(\'%s\', fontsize=%s, ha=\'center\')\n' % (options.xlabel, options.fsize))
  ofile.write('\n')


  ## adjust figure region
  ofile.write('subplots_adjust(top=%s, right=%s, left=%s, bottom=%s)\n' % (options.adj_top, options.adj_right, options.adj_left, options.adj_bottom))


  ## show y-axis grid
  if options.type != 3:
    ofile.write('gca().yaxis.grid(True)\n')


  ## rotate x label by xxx degree
  if options.rotate_xlabel:
    ofile.write('fig.autofmt_xdate(bottom=%s, rotation=%s, ha=\'right\')\n' % (options.adj_bottom, options.rotate_xlabel))
  ofile.write('\n')


  ## in case of dual-y-axis
  if second_yaxis == True:
    ofile.write('ax2 = ax1.twinx()\n')
    ofile.write('bar%d, = ax2.plot(index+0.5, data%d, marker=\'%s\', markersize=5, linewidth=0, markeredgewidth=1.0, markeredgecolor=\'0\')\n' % (len(data)-1, len(data)-1, '_'))
    ofile.write('ax2.set_ylabel(\'%s\', fontsize=%s)\n' % (options.ylabel2, options.fsize))
    ofile.write('ylim(ymin=0)\n')


  ## setup x-tick and y-tick
  if options.type != 3:
    ofile.write('ax1.set_xticks(index + 0.5)\n')
    ofile.write('ax1.set_xticks(index + 1, minor=True)\n')
  else:
    ofile.write('ax1.set_yticks(index + 0.5)\n')
    ofile.write('ax1.set_yticks(index + 1, minor=True)\n')

  ofile.write('ax1.xaxis.set_ticks_position(\'bottom\')\n')
  ofile.write('ax1.yaxis.set_ticks_position(\'left\')\n')
  if options.type != 3:
    ofile.write('xlim(xmax=%d)\n' % (len(data[0])))


  ## setup y-min value
  if options.ymin:
    ofile.write('ylim(ymin=%s)\n' % options.ymin)


  ## setup y-max value
  if options.ymax:
    ofile.write('ylim(ymax=%s)\n' % options.ymax)


  ## set axis label
  axis = 'x'
  if options.type == 3:
    axis = 'y'
  if options.rotate_xlabel:
    ofile.write('ax1.set_%sticklabels(xlabel, size=%s, ha=\'right\')\n' % (axis, options.fsize))
  else:
    ofile.write('ax1.set_%sticklabels(xlabel, size=%s, ha=\'center\')\n' % (axis, options.fsize))
  

  ## setup legend
  if len(data) > 1:
    ofile.write('leg = fig.legend((')
    for ii in range(0, len(data) - second_yaxis):
      index = ii
      if options.type == 1:
        index = len(data) - second_yaxis - ii - 1

      if options.type != 2:
        ofile.write('bar%d[0]' % (index))
      else:
        ofile.write('bar%d' % (index))
      if ii != len(data) - 1:
        ofile.write(', ')

    if second_yaxis == 1:
      ofile.write('bar%d' % (len(data) - 1))

    ofile.write('), (')

    for ii in range(0, len(data)):
      index = ii
      if options.type == 1:
        index = len(data) - second_yaxis - ii - 1
      ofile.write('\'%s\'' % (legend[index]))
      if ii != len(data) - 1:
        ofile.write(', ')
  
    ncol = 1
    if options.lncol:
      ncol = int(options.lncol)
    elif options.lpos == '9' or options.lpos == '1':
      ncol = len(data)
    else:
      ncol = 1
    ofile.write('), ncol=%d, loc=%s)\n' % (ncol, options.lpos))
    ofile.write('leg.get_frame().set_linewidth(0.1)\n')
  ofile.write('\n\n')


  ## save figures
  ofile.write('## save the figure\n')
  ofile.write('pdf.savefig()\n')
  ofile.write('close()\n')
  ofile.write('pdf.close()\n')

  ofile.close()


  ## make it executable and run it
  os.system('chmod +x %s' % (ofile_name))
  os.system('python %s' % (ofile_name))


#########################################################################################
# main routine
#########################################################################################
def main(argv):
  parser = process_options()
  options, args = parser.parse_args(argv)

  write_script(options)


#########################################################################################
if __name__ == '__main__':
  main(sys.argv)
  

#########################################################################################
# End of file
#########################################################################################
