#!/usr/bin/perl


use constant MIN_RECOVERY_TIME => 5;
use constant MAX_RECOVERY_ITER => 3;

  

%G_RECOVERED_JOBS = ();



for ($iteration = 0; $iteration < 10000000; ++$iteration) {
  ## clear data periodically
  if ($iteration % 60 == 0) {
    %G_RECOVERED_JOBS = ();
  }
  $G_DATA = `myqstat -ll`;
  @G_DATA_LIST = split(/\n/, $G_DATA);


## Recovery
  foreach (@G_DATA_LIST) {
    @line = split(/[ ]+/, $_);
    @time = split(/:/, $line[6]);
    my $elapsed = $time[0] * 3600 + $time[1] * 60 + $time[2];
    if ($line[5] eq "C" and int($elapsed) <= MIN_RECOVERY_TIME) {
      if (!exists($G_RECOVERED_JOBS{$line[0]})) {
        $G_RECOVERED_JOBS{$line[0]} = "1";
        system "qrerun $line[0]";
      } else {
        my $count = $G_RECOVERED_JOBS{$line[0]};
        if ($count < MAX_RECOVERY_ITER) {
          system "qrerun $line[0]";
          $G_RECOVERED_JOBS{$line[0]} = $count + 1;
        }
      }
    }
  }



  %G_USER_HASH = ();
  %G_JOB_Q_HASH = ();
  %G_JOB_R_HASH = ();
  %G_JOB_COUNT_HASH = ();

  $G_DATA = `myqstat -ll`;
  @G_DATA_LIST = split(/\n/, $G_DATA);


  foreach (@G_DATA_LIST) {
    @line = split(/[ ]+/, $_);
    if (!exists($G_USER_HASH{$line[1]})) {
      $G_USER_HASH{$line[1]} = "true";
    }
  }

  @G_USER_LIST = ();
  foreach $key (keys %G_USER_HASH) {
    push(@G_USER_LIST, $key);
  }


## get running/pending jobs per each user
  foreach $user (@G_USER_LIST) {
    @temp_Q_list = ();
    @temp_R_list = ();
    foreach (@G_DATA_LIST) {
      @line = split(/[ ]+/, $_);
      if ($line[1] eq $user) {
        if ($line[5] eq "Q") {
          push(@temp_Q_list, $line[0]);
        } elsif ($line[5] eq "R") {
          push(@temp_R_list, $line[0]);
        }
      }
    }
    $G_JOB_Q_HASH{$user} = [@temp_Q_list];
    $G_JOB_R_HASH{$user} = [@temp_R_list];
    $G_JOB_COUNT_HASH{$user} = $#temp_R_list + 1;

#    print "$user $#temp_R_list\n";
  }


## delete a user who doesn't have any pending jobs
  foreach $user (@G_USER_LIST) {
    @list = @{$G_JOB_Q_HASH{$user}};
    if ($#list == -1) {
      $G_USER_HASH{$user} = ();
      $G_JOB_Q_HASH{$user} = ();
      $G_JOB_R_HASH{$user} = ();
      $G_JOB_COUNT_HASH{$user} = ();
      delete($G_USER_HASH{$user});
      delete($G_JOB_Q_HASH{$user});
      delete($G_JOB_R_HASH{$user});
      delete($G_JOB_COUNT_HASH{$user});
    }
  }


## find min R user
  $min = 100000000;
  foreach $user (keys %G_USER_HASH) {
    @list = @{$G_JOB_R_HASH{$user}};
    if (($#list + 1) < $min) {
      $min = $#list + 1;
    }
  }


  $termination = 0;
  $executed = 1;
  while (1) {
    if ($termination == 1 or $executed == 0) {
      last;
    }

    $executed = 0;
    foreach $user (keys %G_USER_HASH) {
      @list_R = @{$G_JOB_R_HASH{$user}};
      @list_Q = @{$G_JOB_Q_HASH{$user}};

      if ($#list_R + 1 <= $min) {
        $executed = 1;
        $job_id = $list_Q[0];
        system "qrun $job_id";
        $state = `myqstat -ll \| grep $job_id \| grep $user \| awk {'print \$6'}`;
        chomp($state);
        if ($state eq "R") {
#          print "$user $job_id launched\n";
          shift @list_Q;
          push(@list_R, $job_id);
          $G_JOB_Q_HASH{$user} = [@list_Q];
          $G_JOB_R_HASH{$user} = [@list_R];
          $min = $#list_R + 1;
        } else {
          $termination = 1;
          last;
        }
      }
    }
  }
  system "sleep 300";
}


