#!/usr/bin/python
#########################################################################################
# Author: Jaekyu Lee (kacear@gmail.com)
#########################################################################################

import argparse
import sys
import re
import os
import glob
import gzip
import bench_common
import math
from decimal import *
from collections import defaultdict
import decimal
import copy


#########################################################################################
# sanity check
#########################################################################################
def sanity_check(args):
  if not args.suite:
    print('error: please specify suite.')
    exit(0)

  if not args.suite in bench_common.SUITES:
    print('error: suite %s does not exist in bench_common.py.' % (args.suite))
    exit(0)

  if not args.stat_list:
    print('error: please specify stat.')
    exit(0)

  if args.printonlydelta and not args.base:
    print('error: base directory not specified')
    exit(0)

  if (args.hspeedup or args.wspeedup or args.speedup) and (args.numappl == 0 or not args.base):
    print('error: please specify numappl and base directory')
    exit(0)


#########################################################################################
# create an argument parser
#########################################################################################
def process_options():
  parser = argparse.ArgumentParser(description='get_stats.py')
  parser.add_argument('-d', '-dir', action='append', nargs='*', dest='dir_list', help='directory lists to check stats')
  parser.add_argument('-b', '-base', action='store', dest='base', help='base directory name to compare')
  parser.add_argument('-stat', action='append', nargs='*', dest='stat_list', help='stat')
  parser.add_argument('-suite', action='store', dest='suite', help='suite name')
  parser.add_argument('-prec', action='store', type=int, default=0, dest='precision', help='precision')
  parser.add_argument('-amean', action='store_true', dest='amean', help='show average of stats (per directory)')
  parser.add_argument('-hmean', action='store_true', dest='hmean', help='show harmonic mean of stats per directory')
  parser.add_argument('-gmean', action='store_true', dest='gmean', help='show geo mean of stats per directory')
  parser.add_argument('-c_amean', action='store_true', dest='c_amean', help='show average of core stats. AVG(XX_CORE_0, XX_CORE_1, .., XX_CORE_N)')
  parser.add_argument('-c_hmean', action='store_true', dest='c_hmean', help='show harmonic mean of core stats. AVG(XX_CORE_0, XX_CORE_1, .., XX_CORE_N)')
  parser.add_argument('-c_gmean', action='store_true', dest='c_gmean', help='show geo mean of core stats. AVG(XX_CORE_0, XX_CORE_1, .., XX_CORE_N)')
  parser.add_argument('-widenames', action='store_true', dest='widenames', help='show full benchmark names')
  parser.add_argument('-core', action='append', nargs='*', dest='core_list', help='specify core ids for stats. ex -core 0 or -core 0-6')
  parser.add_argument('-appl', action='store', dest='appl', help='specify an application id for stats. ex -appl 0 or -appl 1')
  parser.add_argument('-printonlydelta', action='store_true', dest='printonlydelta', help='print only delta (compared to base directory) information')
  parser.add_argument('-disable-warning', action='store_true', dest='disable_warning', help='disable all warning messages')
  parser.add_argument('-raw', action='store_true', dest='raw_data', help='print raw stats. Note that there is a ratio stat type. By default, show this ratio, but -raw option shows original counter')

  # testing
  parser.add_argument('-only', action='store', dest='only', help='')

  # multi-program stats
  parser.add_argument('-hspeedup', action='store_true', dest='hspeedup', help='')
  parser.add_argument('-wspeedup', action='store_true', dest='wspeedup', help='')
  parser.add_argument('-speedup',  action='store_true', dest='speedup',  help='')
  parser.add_argument('-numappl',  action='store', type=int, default=0, dest='numappl',  help='')

  return parser


#########################################################################################
# get an average
#########################################################################################
def get_amean(data):
  result = sum(float(v) for v in filter(None, data))
  try:
    result /= len(filter(None, data))
  except ZeroDivisionError:
    result = 0

  return result


#########################################################################################
# get a geometric mean (use log-based geometric mean to avoid overflow)
#########################################################################################
def get_gmean(data):
  result = 0
  try:
    for v in filter(None, data):
      result += math.log(float(v))

    result /= len(filter(None, data))
    result = math.exp(result)
  except (ZeroDivisionError, ValueError) as e:
    result = 0

  return result


#########################################################################################
# get a harmonic mean
#########################################################################################
def get_hmean(data):
  try:
    result = len(filter(None, data)) / sum(1/float(v) for v in filter(None, data))
  except ZeroDivisionError:
    result = 0

  return result


#########################################################################################
# additional argument handling routine
#########################################################################################
def process_arguments(args):
  args.stat_list = sum(args.stat_list, [])
  args.dir_list = sum(args.dir_list, [])
  for ii in range(0, len(args.dir_list)):
    args.dir_list[ii] = args.dir_list[ii].rstrip('/')
  
  if args.core_list:
    args.core_list = sum(args.core_list, [])


#########################################################################################
# Get stat list
#########################################################################################
def get_stat_list(arg_stat, core_list):
  stat_list = []
  for stat in arg_stat:
    ## complex stat
    if stat.find('=') != -1:
      stat = stat[stat.find('=')+1:]
      temp_list = filter(None, re.split('[\(\)\+\*\/\-]', stat))
      ## remove constant factor
      for temp_stat in temp_list:
        if not re.match('[0-9]+\.*[0-9]*', temp_stat):
          stat_list.append(temp_stat)
    ## regular stat
    else:
      stat_list.append(stat)

  ## apply core stat if core ids are set
  if not core_list:
    return stat_list

  core_stat_list = []
  for ii in range(0, len(stat_list)):
    for cid in core_list:
      core_stat_list.append('%s_CORE_%d' % (stat_list[ii], cid))

  return core_stat_list



#########################################################################################
# Parse core list (sorted)
#########################################################################################
def get_core_list(arg_core):
  if not arg_core:
    return []

  core_set = set()
  for cid in arg_core:
    if cid.find('-') != -1:
      for ii in range(int(re.split('\-', cid)[0]), int(re.split('\-', cid)[1]) + 1):
        core_set.add(ii)
    else:
      core_set.add(int(cid))

  core_list = sorted(list(core_set))
  return core_list


#########################################################################################
# Open a statistics file
#########################################################################################
def open_file(filename):
  basename,ext = os.path.splitext(filename)
  ## open a stat file
  if ext == '.gz':
    file = gzip.open(filename, 'rm')
    gzip_file = True
  else:
    file = open(filename, 'r')
    gzip_file = False
  return gzip_file, file 


#########################################################################################
# return a value (with corresponding stat) from a file
#########################################################################################
def find_stat_from_file(dir, stat, appl):
  stat_file_list = []

  if appl == '-1':
    stat_file_list.extend(glob.glob('*.stat.out'))
    stat_file_list.extend(glob.glob('*.stat.out.gz'))
  else:
    stat_file_list.extend(glob.glob('*.stat.out.%s' % appl))
    stat_file_list.extend(glob.glob('*.stat.out.%s.gz' % appl))

  for stat_file in stat_file_list:
    gzip_file, file = open_file(stat_file)
    ## parse each line to find a stat
    for line in file.readlines():
      ## gzip file - decode it first
      if gzip_file == True:
        line = line.decode()

      if re.match(r'%s\s+' % (stat), line):
        return stat_file

  return ''


#########################################################################################
# Find which file has a stat
#########################################################################################
def get_a_stat(stat, stat_file, args):
  if not os.path.exists(stat_file):
    return None

  gzip_file, file = open_file(stat_file)
    
  for line in file.readlines():
    if gzip_file == True:
      line = line.decode()
 
    if re.match(r'%s\s+' % (stat), line):
      if args.raw_data:
        result = re.split('\s+', line)[1]
      else:
        result = re.split('\s+', line)[2]
      result = result.rstrip('%')
      return result

  return None


#########################################################################################
# Convert a number based on the precision setting
#########################################################################################
def get_precision(num, prec):
  result = str(round(float(num), prec)).rstrip('0')
  if prec == 0 and result.find('.') != -1:
    result = result[0:result.find('.')]
  elif prec > 0 and result.find('.') == -1:
    result += '.'
  elif prec > 0 and len(result) - result.find('.') - 1 != prec:
    result += '0' * (prec - (len(result) - result.find('.') - 1))

  return result



#########################################################################################
# Print data in nice format
#########################################################################################
def print_stat(result, dir_list, bench_list, core_list, stat_list, args, max_dir_len, max_stat_len):
  ## print header
  sys.stdout.write('%s' % ' '.ljust(max_dir_len+2))
  for bench in bench_list:
    sys.stdout.write('%s  ' % bench[0:max_stat_len].rjust(max_stat_len))
  sys.stdout.write('\n')

  for dir in dir_list:
    sys.stdout.write('%s  ' % dir.ljust(max_dir_len))
    for item in result[dir]:
      if item != None:
        sys.stdout.write('%s  ' % (item.rjust(max_stat_len)))
      else:
        sys.stdout.write('%s  ' % ('0'.rjust(max_stat_len)))

    sys.stdout.write('\n')
    

#########################################################################################
# Evaluate complex-stat
#########################################################################################
def merge_stat(args, bench_list, result, dir, cid, stat):
  if cid == -1:
    SUFFIX = ''
  else:
    SUFFIX = '_CORE_%s' % cid

  new_result = defaultdict(list)
  dir_list = []

  ## complex-stat
  if stat.find('=') != -1:
    sub_result = []
    new_stat = stat[0:stat.find('=')]
    return_stat_name = new_stat
    stat = stat[stat.find('=')+1:]

    ## parse complex-stat and evaluate it by replacing each simple-stat with a value
    temp_list = filter(None, re.split('[\(\)\+\*\/\-]', stat))
    for ii in range(0, len(bench_list)):
      eval_string = stat
      for item in temp_list:
        if re.match('[0-9]+\.*[0-9]*', item):
          continue
        value = result['%s*%s%s' % (dir, item, SUFFIX)][ii]
        eval_string = re.sub(item, 'float(' + str(value) + ')', eval_string)

      ## evaluation
      try:
        value = str(eval(eval_string))
      except ZeroDivisionError:
        value = None
      except TypeError:
        value = None
      sub_result.append(value)
    new_result['%s(%s%s)' % (dir, new_stat, SUFFIX)] = sub_result
    dir_list.append('%s(%s%s)' % (dir, new_stat, SUFFIX))
  ## simple-stat
  else:
    return_stat_name = stat
    new_result['%s(%s%s)' % (dir, stat, SUFFIX)] = result['%s*%s%s' % (dir, stat, SUFFIX)]
    dir_list.append('%s(%s%s)' % (dir, stat, SUFFIX))

  return return_stat_name, dir_list, new_result 


#########################################################################################
# collect stats 
#########################################################################################
def collect_stat(dir_list, current_dir, stat_list, bench_list, args, appl):
  result = defaultdict(list)
  stat_file = {} ## the filename with a stat
  for dir_item in dir_list:
    dir = current_dir + '/' + dir_item
    ## directory existence check
    if os.path.exists(dir) != True:
      print('warning: %s does not exist' % dir)
      continue

    for stat in stat_list:
      sub_result = []
      for bench in bench_list:
        sub_dir = dir + '/' + bench
        if not os.path.exists(sub_dir):
          sub_result.append(None)
          continue

        os.chdir(sub_dir)

        ## find the stat file that has 'stat'
        if not stat in stat_file:
          filename = find_stat_from_file(sub_dir, stat, appl)
          if filename == '':
#            print('warning: stat file not found %s/%s for %s' % (dir, bench, stat))
            sub_result.append(None)
            continue
          stat_file[stat] = filename

        ## add a stat
        sub_result.append(get_a_stat(stat, stat_file[stat], args))
        
      ## add a sub result
      key = '%s*%s' % (dir_item, stat)
      result[key] = sub_result

  return result
  

#########################################################################################
# Combine stats (complex stat or per-core stat)
#########################################################################################
def combine_stat(dir_list, args, core_list, bench_list, result):
  new_result = defaultdict(list)
  orig_order_dir_list = []
  for dir in dir_list:
    for stat in args.stat_list:
      #################################
      # regular option
      #################################
      if not core_list:
        new_stat, dir_list_sub, new_result_sub = merge_stat(args, bench_list, result, dir, -1, stat)
        for key, val in new_result_sub.items():
          new_result[key] = val
        orig_order_dir_list.append(dir_list_sub)
      #################################
      # per core option
      #################################
      else:
        for cid in core_list:
          new_stat, dir_list_sub, new_result_sub = merge_stat(args, bench_list, result, dir, cid, stat)
          for key, val in new_result_sub.items():
            new_result[key] = val
          if not args.c_amean and not args.c_gmean and not args.c_hmean:
            orig_order_dir_list.append(dir_list_sub)


        #################################
        # core average (amean, gmean, hmean) data 
        #################################
        if args.c_amean or args.c_gmean or args.c_hmean:
          amean_data = []
          gmean_data = []
          hmean_data = []
          for ii in range(0, len(bench_list)):
            ## collecting per-bench core data
            core_data = []
            for cid in core_list:
              core_data.append(new_result['%s(%s_CORE_%d)' % (dir, new_stat, cid)][ii])

            if args.c_amean:
              amean_data.append(str(get_precision(get_amean(core_data), args.precision)))

            if args.c_gmean:
              gmean_data.append(str(get_precision(get_gmean(core_data), args.precision)))

            if args.c_hmean:
              hmean_data.append(str(get_precision(get_hmean(core_data), args.precision)))

          ## remove raw per-core data
          for cid in core_list:
            del new_result['%s(%s_CORE_%d)' % (dir, new_stat, cid)]

          ## add core average data
          if args.c_amean:
            new_result['%s(%s_AMEAN)' % (dir, new_stat)] = amean_data
            orig_order_dir_list.append(['%s(%s_AMEAN)' % (dir, new_stat)])

          if args.c_gmean:
            new_result['%s(%s_GMEAN)' % (dir, new_stat)] = gmean_data
            orig_order_dir_list.append(['%s(%s_GMEAN)' % (dir, new_stat)])

          if args.c_hmean:
            new_result['%s(%s_HMEAN)' % (dir, new_stat)] = hmean_data
            orig_order_dir_list.append(['%s(%s_HMEAN)' % (dir, new_stat)])
  return new_result, orig_order_dir_list


#########################################################################################
#########################################################################################
def get_max_len(data, current_max_len):
  max_len = current_max_len
  for key, val in data.items():
    val = filter(None, val)
    if val and len(max(val, key=len)) > max_len:
      max_len = len(max(val, key=len))

  return max_len
    

#########################################################################################
# Calculate delta between two data
#########################################################################################
def calculate_delta(base, data, args):
  result = defaultdict(list)

  for key, val in data.items():
    sub_result = []
    stat = key[key.find('(')+1:key.rfind(')')]
    for ii in range(0, len(val)):
      base_val = float(base['%s(%s)' % (args.base, stat)][ii])
      sub_result.append(str((float(val[ii]) - base_val)/base_val * 100.0))

    result[key] = sub_result

  return result


#########################################################################################
# Add average values to the data
#########################################################################################
def add_average(data, args):
  if not args.amean and not args.gmean and not args.hmean:
    return data

  for key, val in data.items():
    if args.amean:
      val.append(str(get_precision(get_amean(val), args.precision)))
    if args.gmean:
      val.append(str(get_precision(get_gmean(val), args.precision)))
    if args.hmean:
      val.append(str(get_precision(get_hmean(val), args.precision)))

  return data


#########################################################################################
# Apply specified precision to a number
#########################################################################################
def apply_precision(data, precision):
  for key, val in data.items():
    for ii in range(0, len(val)):
      if val[ii] == None:
        continue
      val[ii] = get_precision(val[ii], precision)

  return data


#########################################################################################
# Get statistics
# 1. collect stats
# 2. calculate complex-stat
# 3. calculate average
# 4. print stats
#########################################################################################
def get_stat(dir_list, current_dir, bench_list, core_list, stat_list, args, appl):
  result = collect_stat(dir_list, current_dir, stat_list, bench_list, args, appl)
  new_result, orig_order_dir_list = combine_stat(dir_list, args, core_list, bench_list, result)

  if args.base:
    base_result = collect_stat([args.base], current_dir, stat_list, bench_list, args, appl)
    base_new_result, base_order_dir_list = combine_stat([args.base], args, core_list, bench_list, base_result)
    #################################
    # delta calculation
    #################################
    delta = calculate_delta(base_new_result, new_result, args)

  
  #################################
  # average calculation 
  #################################
  new_result = add_average(new_result, args)
  if args.base:
    base_new_result = add_average(base_new_result, args)
    delta = add_average(delta, args)

  if args.amean:
    bench_list.append('AMEAN')
  if args.gmean:
    bench_list.append('GMEAN')
  if args.hmean:
    bench_list.append('HMEAN')

  #################################
  # precision control
  #################################
  new_result = apply_precision(new_result, args.precision)
  if args.base:
    base_new_result = apply_precision(base_new_result, args.precision)
    delta1 = copy.deepcopy(delta)
    delta = apply_precision(delta, args.precision)

  #################################
  # calculate max length for printing
  #################################
  max_dir_len = len(max(new_result.keys(), key=len))
  if args.base and not args.printonlydelta and len(max(base_new_result.keys(), key=len)) > max_dir_len:
    max_dir_len = len(max(base_new_result.keys(), key=len))
  
  max_stat_len = 0
  if not args.printonlydelta:
    max_stat_len = get_max_len(new_result, max_stat_len)
  
  if args.base and not args.printonlydelta:
    max_stat_len = get_max_len(base_new_result, max_stat_len)

  if args.printonlydelta:
    max_stat_len = get_max_len(delta, max_stat_len)

  ## if widenames option is set, print full benchname
  if args.widenames:
    if len(max(bench_list, key=len)) > max_stat_len:
      max_stat_len = len(max(bench_list, key=len))


  #################################
  # print result 
  #################################
  if args.base and not args.printonlydelta:
    base_order_dir_list = sum(base_order_dir_list, [])
    print_stat(base_new_result, base_order_dir_list, bench_list, core_list, stat_list, args, max_dir_len, max_stat_len)

  orig_order_dir_list = sum(orig_order_dir_list, [])
  if not args.printonlydelta:
    print_stat(new_result, orig_order_dir_list, bench_list, core_list, stat_list, args, max_dir_len, max_stat_len)

  if args.base:
    print_stat(delta, orig_order_dir_list, bench_list, core_list, stat_list, args, max_dir_len, max_stat_len)

  sys.stdout.write('\n')

  if args.base:
    return delta1, orig_order_dir_list


#########################################################################################
# main function
#########################################################################################
def main(argv):
  # parse arguments
  parser = process_options()
  args = parser.parse_args()

  sanity_check(args)
  process_arguments(args)
  
  current_dir = os.getcwd();

  dir_list   = args.dir_list
  if args.only:
    bench_list = []
    for orig_bench in bench_common.SUITES[args.suite]:
      if re.match(r'%s' % (args.only), orig_bench):
        bench_list.append(orig_bench)
  else:
    bench_list = bench_common.SUITES[args.suite]  
  orig_bench_size = len(bench_list)
  core_list  = get_core_list(args.core_list)
  stat_list  = get_stat_list(args.stat_list, core_list)

  #################################
  ## get speedup result for multi-program workloads
  #################################
  if args.hspeedup or args.wspeedup or args.speedup:
    result_list = []
    for ii in range(0, args.numappl):
      result, ordered_dir_list = get_stat(dir_list, current_dir, bench_list, core_list, stat_list, args, str(ii))
      result_list.append(result)
      while len(bench_list) > orig_bench_size:
        bench_list.pop(-1)
  
    ## get speedup results (geometric mean of each speedup)
    speedup_result = defaultdict(list)
    for key in result_list[0].keys():
      sub_result = []
      for ii in range(0, orig_bench_size):
        subsub_result = []
        for result in result_list:
          # convert delta to speedup
          subsub_result.append(str(1.0/((100.0 + float(result[key][ii]))/100.0)))
        sub_result.append(str(get_gmean(subsub_result)))
        
      ## add geometric mean of speedup
      sub_result.append(str(get_precision(get_gmean(sub_result), 5)))
      speedup_result[key] = sub_result

    ## add a column entry for geometric mean
    bench_list.append('GMEAN')

    ## precision control
    for key, val in speedup_result.items():
      for ii in range(0, len(val)):
        val[ii] = get_precision(val[ii], 5)
 

    ## formating
    stat_len = 3 + 5 
    if args.widenames:
      stat_len = len(max(bench_list, key=len))

    dir_len = len(max(ordered_dir_list, key=len))

    ## print speedup result
    sys.stdout.write('%s  ' % ' '.rjust(dir_len))
    for bench in bench_list:
      sys.stdout.write('%s  ' % bench[0:stat_len].rjust(stat_len))
    sys.stdout.write('\n')
    for dir in ordered_dir_list:
      sys.stdout.write('%s  ' % dir.ljust(dir_len))
      for item in speedup_result[dir]:
        sys.stdout.write('%s  ' % item.rjust(stat_len))
      sys.stdout.write('\n')

  #################################
  # regular stat
  #################################
  else:
    if args.appl:
      get_stat(dir_list, current_dir, bench_list, core_list, stat_list, args, args.appl)
    else:
      get_stat(dir_list, current_dir, bench_list, core_list, stat_list, args, '-1')


#########################################################################################
# main routine
#########################################################################################
if __name__ == '__main__':
  main(sys.argv)
