#!/usr/bin/perl


# Copy bin
# Copy params
# Create tracefile

###############################################################################################
use Getopt::Long;
###############################################################################################


###############################################################################################
## Configuration
require ($ENV{'SIMDIR'} . "/tools/bench_common");
require ($ENV{'SIMDIR'} . "/tools/trace_common");
###############################################################################################


GetOptions("dir=s" => \$dir,
           "cmd=s" => \$cmd,
           "param=s" => \$param,
           "suite=s" => \$suite,
);

die unless $dir;
die unless $suite;
die unless defined{$SUITES{$suite}};

if (!$param) {
  $param = "params.in";
}

$result_dir = "/user/jaekyu/results/" . $dir;

#$TRACE_FILE{}
#print $result_dir;
#print "$SUITES{'spec06'}\n";


# Make result directory
if (-e $result_dir) {
  print "directory exist\n";
}
else {
  system("mkdir -p $result_dir");
}


# Copy binary
system("cp -f ./macsim $result_dir");

# Copy parameter
system("cp -f $param $result_dir/params.in");


@benchs = split(/ /, $SUITES{$suite});
foreach $bench1 (@benchs) {
  $bench = $bench1;
  $bench =~ s/\@ref//;
  $bench_result_dir = "${result_dir}/$bench\@ref";
  if (!-e $bench_result_dir) {
    system("mkdir -p $bench_result_dir");
  }


  $bench_temp = $bench;
  $bench_temp =~ s/\@ref//;
  @sub_bench = split(/_/, $bench_temp);
  $trace_num = @sub_bench;


  $trace_file_name = "$bench_result_dir/trace_file_list";
  open(TRACE, ">", $trace_file_name);
  print TRACE "$trace_num\n";
  foreach (@sub_bench) {
    $bench_temp = $_ . "\@ref";
    print TRACE "$TRACE_FILE{$bench_temp}\n";
  }
  close(TRACE);
    
  $script_file_name = "$bench_result_dir/run_cmd.pl";
  $temp_work_dir = "/tmp/result_$bench" . getppid();
  open(SCRIPT, ">", $script_file_name);
  print SCRIPT "#!/usr/bin/perl\n";
  print SCRIPT "system(\"mkdir -p ${temp_work_dir}\")\;\n";
  print SCRIPT "system(\"cp -f ${result_dir}/macsim ${temp_work_dir}\")\;\n";
  print SCRIPT "system(\"cp -f ${result_dir}/params.in ${temp_work_dir}\")\;\n";
  print SCRIPT "system(\"cp -f ${result_dir}/$bench\\\@ref/trace_file_list ${temp_work_dir}\")\;\n";
  print SCRIPT "chdir \"${temp_work_dir}\";\n";
  print SCRIPT "system(\"./macsim ${cmd}\")\;\n";
  print SCRIPT "\@outfile = glob \"*.out *stat*\";\n";
  print SCRIPT "foreach \$out (\@outfile) {\n";
  print SCRIPT "  system(\"gzip --best \$out\");\n";
  print SCRIPT "}\n";
  print SCRIPT "system(\"mv -f *.gz ${result_dir}/$bench\\\@ref\");\n";
  print SCRIPT "system(\"rm -rf ${temp_work_dir}\");\n";
  close(SCRIPT);

  system("chmod +x $script_file_name");
#    print"qsub -V -m n -o ${result_dir}/${cpu_bench_temp}_${gpu_bench_temp}\\\@ref -e ${result_dir}/${cpu_bench_temp}_${gpu_bench_temp}\\\@ref -q pool1 -l nodes\=1:ppn\=1 -N ${result_dir}/${cpu_bench_temp}_${gpu_bench_temp}\\\@ref/run_cmd.pl";
  system("qsub ${result_dir}/$bench\\\@ref/run_cmd.pl -V -m n -o ${result_dir}/$bench\\\@ref -e ${result_dir}/$bench\\\@ref -q pool1 -l nodes\=1:ppn\=1 -N ${dir}-${bench}");
}

