#!/usr/bin/python
#########################################################################################
# Description:
#   trace path that will be used by other script files
# Note:
#   - How to add a new trace?
#     TRACE_FILE['bench@ref'] = path
# TODO:
#   - remove @ref
#########################################################################################


import sys


TRACE_FILE = {}

# Spec 2006
trace_path = '/trace/x86/spec2006/trace_simpoint'

TRACE_FILE['perlbench@ref-2']  = trace_path + '/perlbench/pin_traces/perlbench.2.txt'
TRACE_FILE['perlbench@ref-1']  = trace_path + '/perlbench/pin_traces/perlbench.1.txt'
TRACE_FILE['perlbench@ref']    = TRACE_FILE['perlbench@ref-2'];
TRACE_FILE['bzip2@ref-1']      = trace_path + '/bzip2/pin_traces/bzip2.1.txt'
TRACE_FILE['bzip2@ref']        = TRACE_FILE['bzip2@ref-1'];
TRACE_FILE['gcc@ref-3']        = trace_path + '/gcc/pin_traces/gcc.3.txt'
TRACE_FILE['gcc@ref-2']        = trace_path + '/gcc/pin_traces/gcc.2.txt'
TRACE_FILE['gcc@ref-1']        = trace_path + '/gcc/pin_traces/gcc.1.txt'
TRACE_FILE['gcc@ref']          = TRACE_FILE['gcc@ref-2'];
TRACE_FILE['mcf@ref-1']        = trace_path + '/mcf/pin_traces/mcf.1.txt'
TRACE_FILE['mcf@ref']          = TRACE_FILE['mcf@ref-1'];
TRACE_FILE['gobmk@ref-1']      = trace_path + '/gobmk/pin_traces/gobmk.1.txt'
TRACE_FILE['gobmk@ref']        = TRACE_FILE['gobmk@ref-1'];
TRACE_FILE['hmmer@ref-2']      = trace_path + '/hmmer/pin_traces/hmmer.2.txt'
TRACE_FILE['hmmer@ref-1']      = trace_path + '/hmmer/pin_traces/hmmer.1.txt'
TRACE_FILE['hmmer@ref']        = TRACE_FILE['hmmer@ref-1'];
TRACE_FILE['sjeng@ref-1']      = trace_path + '/sjeng/pin_traces/sjeng.1.txt'
TRACE_FILE['sjeng@ref']        = TRACE_FILE['sjeng@ref-1'];
TRACE_FILE['libquantum@ref-1'] = trace_path + '/libquantum/pin_traces/libquantum.1.txt'
TRACE_FILE['libquantum@ref']   = TRACE_FILE['libquantum@ref-1'];
TRACE_FILE['h264ref@ref-1']    = trace_path + '/h264ref/pin_traces/h264ref.1.txt'
TRACE_FILE['h264ref@ref']      = TRACE_FILE['h264ref@ref-1'];
TRACE_FILE['omnetpp@ref-1']    = trace_path + '/omnetpp/pin_traces/omnetpp.1.txt'
TRACE_FILE['omnetpp@ref']      = TRACE_FILE['omnetpp@ref-1'];
TRACE_FILE['astar@ref-1']      = trace_path + '/astar/pin_traces/astar.1.txt'
TRACE_FILE['astar@ref']        = TRACE_FILE['astar@ref-1'];
TRACE_FILE['xalancbmk@ref-1']  = trace_path + '/xalancbmk/pin_traces/xalancbmk.1.txt'
TRACE_FILE['xalancbmk@ref']    = TRACE_FILE['xalancbmk@ref-1'];

TRACE_FILE['bwaves@ref-1']     = trace_path + '/bwaves/pin_traces/bwaves.1.txt'
TRACE_FILE['bwaves@ref']       = TRACE_FILE['bwaves@ref-1'];
TRACE_FILE['gamess@ref-1']     = trace_path + '/gamess/pin_traces/gamess.1.txt'
TRACE_FILE['gamess@ref']       = TRACE_FILE['gamess@ref-1'];
TRACE_FILE['milc@ref-1']       = trace_path + '/milc/pin_traces/milc.1.txt'
TRACE_FILE['milc@ref']         = TRACE_FILE['milc@ref-1'];
TRACE_FILE['zeusmp@ref-1']     = trace_path + '/zeusmp/pin_traces/zeusmp.1.txt'
TRACE_FILE['zeusmp@ref']       = TRACE_FILE['zeusmp@ref-1'];
TRACE_FILE['gromacs@ref-1']    = trace_path + '/gromacs/pin_traces/gromacs.1.txt'
TRACE_FILE['gromacs@ref']      = TRACE_FILE['gromacs@ref-1'];
TRACE_FILE['cactusADM@ref-1']  = trace_path + '/cactusADM/pin_traces/cactusADM.1.txt'
TRACE_FILE['cactusADM@ref']    = TRACE_FILE['cactusADM@ref-1'];
TRACE_FILE['leslie3d@ref-1']   = trace_path + '/leslie3d/pin_traces/leslie3d.1.txt'
TRACE_FILE['leslie3d@ref']     = TRACE_FILE['leslie3d@ref-1'];
TRACE_FILE['namd@ref-1']       = trace_path + '/namd/pin_traces/namd.1.txt'
TRACE_FILE['namd@ref']         = TRACE_FILE['namd@ref-1'];
TRACE_FILE['dealII@ref-1']     = trace_path + '/dealII/pin_traces/dealII.1.txt'
TRACE_FILE['dealII@ref']       = TRACE_FILE['dealII@ref-1'];
TRACE_FILE['soplex@ref-1']     = trace_path + '/soplex/pin_traces/soplex.1.txt'
TRACE_FILE['soplex@ref']       = TRACE_FILE['soplex@ref-1'];
TRACE_FILE['povray@ref-1']     = trace_path + '/povray/pin_traces/povray.1.txt'
TRACE_FILE['povray@ref']       = TRACE_FILE['povray@ref-1'];
TRACE_FILE['calculix@ref-1']   = trace_path + '/calculix/pin_traces/calculix.1.txt'
TRACE_FILE['calculix@ref']     = TRACE_FILE['calculix@ref-1'];
TRACE_FILE['GemsFDTD@ref-1']   = trace_path + '/GemsFDTD/pin_traces/GemsFDTD.1.txt'
TRACE_FILE['GemsFDTD@ref']     = TRACE_FILE['GemsFDTD@ref-1'];
TRACE_FILE['tonto@ref-1']      = trace_path + '/tonto/pin_traces/tonto.1.txt'
TRACE_FILE['tonto@ref']        = TRACE_FILE['tonto@ref-1'];
TRACE_FILE['lbm@ref-1']        = trace_path + '/lbm/pin_traces/lbm.1.txt'
TRACE_FILE['lbm@ref']          = TRACE_FILE['lbm@ref-1'];
TRACE_FILE['wrf@ref-1']        = trace_path + '/wrf/pin_traces/wrf.1.txt'
TRACE_FILE['wrf@ref']          = TRACE_FILE['wrf@ref-1'];
TRACE_FILE['sphinx3@ref-1']     = trace_path + '/sphinx3/pin_traces/sphinx3.1.txt'
TRACE_FILE['sphinx3@ref']       = TRACE_FILE['sphinx3@ref-1'];


# Spec 2006 Baseline (all)
trace_path = '/trace/1/spec2006/trace_blueprint/TraceBaseAll'

TRACE_FILE['perlbench_base@ref-1']  = trace_path + '/perlbench/pin_traces/perlbench.1.raw'
TRACE_FILE['perlbench_base@ref']    = TRACE_FILE['perlbench_base@ref-1'];
TRACE_FILE['bzip2_base@ref-1']      = trace_path + '/bzip2/pin_traces/bzip2.1.raw'
TRACE_FILE['bzip2_base@ref']        = TRACE_FILE['bzip2_base@ref-1'];
TRACE_FILE['gcc_base@ref-3']        = trace_path + '/gcc/pin_traces/gcc.3.raw'
TRACE_FILE['gcc_base@ref-2']        = trace_path + '/gcc/pin_traces/gcc.2.raw'
TRACE_FILE['gcc_base@ref-1']        = trace_path + '/gcc/pin_traces/gcc.1.raw'
TRACE_FILE['gcc_base@ref']          = TRACE_FILE['gcc_base@ref-1'];
TRACE_FILE['mcf_base@ref-1']        = trace_path + '/mcf/pin_traces/mcf.1.raw'
TRACE_FILE['mcf_base@ref']          = TRACE_FILE['mcf_base@ref-1'];
TRACE_FILE['gobmk_base@ref-1']      = trace_path + '/gobmk/pin_traces/gobmk.1.raw'
TRACE_FILE['gobmk_base@ref']        = TRACE_FILE['gobmk_base@ref-1'];
TRACE_FILE['hmmer_base@ref-2']      = trace_path + '/hmmer/pin_traces/hmmer.2.raw'
TRACE_FILE['hmmer_base@ref-1']      = trace_path + '/hmmer/pin_traces/hmmer.1.raw'
TRACE_FILE['hmmer_base@ref']        = TRACE_FILE['hmmer_base@ref-1'];
TRACE_FILE['sjeng_base@ref-1']      = trace_path + '/sjeng/pin_traces/sjeng.1.raw'
TRACE_FILE['sjeng_base@ref']        = TRACE_FILE['sjeng_base@ref-1'];
TRACE_FILE['libquantum_base@ref-1'] = trace_path + '/libquantum/pin_traces/libquantum.1.raw'
TRACE_FILE['libquantum_base@ref']   = TRACE_FILE['libquantum_base@ref-1'];
TRACE_FILE['h264ref_base@ref-1']    = trace_path + '/h264ref/pin_traces/h264ref.1.raw'
TRACE_FILE['h264ref_base@ref']      = TRACE_FILE['h264ref_base@ref-1'];
TRACE_FILE['omnetpp_base@ref-1']    = trace_path + '/omnetpp/pin_traces/omnetpp.1.raw'
TRACE_FILE['omnetpp_base@ref']      = TRACE_FILE['omnetpp_base@ref-1'];
TRACE_FILE['astar_base@ref-1']      = trace_path + '/astar/pin_traces/astar.1.raw'
TRACE_FILE['astar_base@ref']        = TRACE_FILE['astar_base@ref-1'];
TRACE_FILE['xalancbmk_base@ref-1']  = trace_path + '/xalancbmk/pin_traces/xalancbmk.1.raw'
TRACE_FILE['xalancbmk_base@ref']    = TRACE_FILE['xalancbmk_base@ref-1'];

TRACE_FILE['bwaves_base@ref-1']     = trace_path + '/bwaves/pin_traces/bwaves.1.raw'
TRACE_FILE['bwaves_base@ref']       = TRACE_FILE['bwaves_base@ref-1'];
TRACE_FILE['gamess_base@ref-1']     = trace_path + '/gamess/pin_traces/gamess.1.raw'
TRACE_FILE['gamess_base@ref']       = TRACE_FILE['gamess_base@ref-1'];
TRACE_FILE['milc_base@ref-1']       = trace_path + '/milc/pin_traces/milc.1.raw'
TRACE_FILE['milc_base@ref']         = TRACE_FILE['milc_base@ref-1'];
TRACE_FILE['zeusmp_base@ref-1']     = trace_path + '/zeusmp/pin_traces/zeusmp.1.raw'
TRACE_FILE['zeusmp_base@ref']       = TRACE_FILE['zeusmp_base@ref-1'];
TRACE_FILE['gromacs_base@ref-1']    = trace_path + '/gromacs/pin_traces/gromacs.1.raw'
TRACE_FILE['gromacs_base@ref']      = TRACE_FILE['gromacs_base@ref-1'];
TRACE_FILE['cactusADM_base@ref-1']  = trace_path + '/cactusADM/pin_traces/cactusADM.1.raw'
TRACE_FILE['cactusADM_base@ref']    = TRACE_FILE['cactusADM_base@ref-1'];
TRACE_FILE['leslie3d_base@ref-1']   = trace_path + '/leslie3d/pin_traces/leslie3d.1.raw'
TRACE_FILE['leslie3d_base@ref']     = TRACE_FILE['leslie3d_base@ref-1'];
TRACE_FILE['namd_base@ref-1']       = trace_path + '/namd/pin_traces/namd.1.raw'
TRACE_FILE['namd_base@ref']         = TRACE_FILE['namd_base@ref-1'];
TRACE_FILE['dealII_base@ref-1']     = trace_path + '/dealII/pin_traces/dealII.1.raw'
TRACE_FILE['dealII_base@ref']       = TRACE_FILE['dealII_base@ref-1'];
TRACE_FILE['soplex_base@ref-1']     = trace_path + '/soplex/pin_traces/soplex.1.raw'
TRACE_FILE['soplex_base@ref']       = TRACE_FILE['soplex_base@ref-1'];
TRACE_FILE['povray_base@ref-1']     = trace_path + '/povray/pin_traces/povray.1.raw'
TRACE_FILE['povray_base@ref']       = TRACE_FILE['povray_base@ref-1'];
TRACE_FILE['calculix_base@ref-1']   = trace_path + '/calculix/pin_traces/calculix.1.raw'
TRACE_FILE['calculix_base@ref']     = TRACE_FILE['calculix_base@ref-1'];
TRACE_FILE['GemsFDTD_base@ref-1']   = trace_path + '/GemsFDTD/pin_traces/GemsFDTD.1.raw'
TRACE_FILE['GemsFDTD_base@ref']     = TRACE_FILE['GemsFDTD_base@ref-1'];
TRACE_FILE['tonto_base@ref-1']      = trace_path + '/tonto/pin_traces/tonto.1.raw'
TRACE_FILE['tonto_base@ref']        = TRACE_FILE['tonto_base@ref-1'];
TRACE_FILE['lbm_base@ref-1']        = trace_path + '/lbm/pin_traces/lbm.1.raw'
TRACE_FILE['lbm_base@ref']          = TRACE_FILE['lbm_base@ref-1'];
TRACE_FILE['wrf_base@ref-1']        = trace_path + '/wrf/pin_traces/wrf.1.raw'
TRACE_FILE['wrf_base@ref']          = TRACE_FILE['wrf_base@ref-1'];
TRACE_FILE['sphinx3_base@ref-1']     = trace_path + '/sphinx3/pin_traces/sphinx3.1.raw'
TRACE_FILE['sphinx3_base@ref']       = TRACE_FILE['sphinx3_base@ref-1'];


# Spec 2006 - Prefetch Base
trace_path = '/trace/1/spec2006/trace_blueprint/TRACE_Base'
TRACE_FILE['bzip2_b@ref-1']      = trace_path + '/bzip2/pin_traces/bzip2.1.raw'
TRACE_FILE['bzip2_b@ref']        = TRACE_FILE['bzip2_b@ref-1'];
TRACE_FILE['gcc_b@ref-1']        = trace_path + '/gcc/pin_traces/gcc.1.raw'
TRACE_FILE['gcc_b@ref']          = TRACE_FILE['gcc_b@ref-1'];
TRACE_FILE['mcf_b@ref-1']        = trace_path + '/mcf/pin_traces/mcf.1.raw'
TRACE_FILE['mcf_b@ref']          = TRACE_FILE['mcf_b@ref-1'];
TRACE_FILE['libquantum_b@ref-1'] = trace_path + '/libquantum/pin_traces/libquantum.1.raw'
TRACE_FILE['libquantum_b@ref']   = TRACE_FILE['libquantum_b@ref-1'];
TRACE_FILE['bwaves_b@ref-1']     = trace_path + '/bwaves/pin_traces/bwaves.1.raw'
TRACE_FILE['bwaves_b@ref']       = TRACE_FILE['bwaves_b@ref-1'];
TRACE_FILE['milc_b@ref-1']       = trace_path + '/milc/pin_traces/milc.1.raw'
TRACE_FILE['milc_b@ref']         = TRACE_FILE['milc_b@ref-1'];
TRACE_FILE['zeusmp_b@ref-1']     = trace_path + '/zeusmp/pin_traces/zeusmp.1.raw'
TRACE_FILE['zeusmp_b@ref']       = TRACE_FILE['zeusmp_b@ref-1'];
TRACE_FILE['cactusADM_b@ref-1']  = trace_path + '/cactusADM/pin_traces/cactusADM.1.raw'
TRACE_FILE['cactusADM_b@ref']    = TRACE_FILE['cactusADM_b@ref-1'];
TRACE_FILE['leslie3d_b@ref-1']   = trace_path + '/leslie3d/pin_traces/leslie3d.1.raw'
TRACE_FILE['leslie3d_b@ref']     = TRACE_FILE['leslie3d_b@ref-1'];
TRACE_FILE['soplex_b@ref-1']     = trace_path + '/soplex/pin_traces/soplex.1.raw'
TRACE_FILE['soplex_b@ref']       = TRACE_FILE['soplex_b@ref-1'];
TRACE_FILE['GemsFDTD_b@ref-1']   = trace_path + '/GemsFDTD/pin_traces/GemsFDTD.1.raw'
TRACE_FILE['GemsFDTD_b@ref']     = TRACE_FILE['GemsFDTD_b@ref-1'];
TRACE_FILE['lbm_b@ref-1']        = trace_path + '/lbm/pin_traces/lbm.1.raw'
TRACE_FILE['lbm_b@ref']          = TRACE_FILE['lbm_b@ref-1'];
TRACE_FILE['wrf_b@ref-1']        = trace_path + '/wrf/pin_traces/wrf.1.raw'
TRACE_FILE['wrf_b@ref']          = TRACE_FILE['wrf_b@ref-1'];
TRACE_FILE['sphinx3_b@ref-1']    = trace_path + '/sphinx3/pin_traces/sphinx3.1.raw'
TRACE_FILE['sphinx3_b@ref']      = TRACE_FILE['sphinx3_b@ref-1'];

trace_path = '/trace/1/spec2006/trace_others'
TRACE_FILE['astar_b@ref-1']    = trace_path + '/astar/pin_traces/astar.1.raw'
TRACE_FILE['astar_b@ref']      = TRACE_FILE['astar_b@ref-1'];
TRACE_FILE['xalancbmk_b@ref-1']    = trace_path + '/xalancbmk/pin_traces/xalancbmk.1.raw'
TRACE_FILE['xalancbmk_b@ref']      = TRACE_FILE['xalancbmk_b@ref-1'];


# Spec 2006 - Prefetch Base
trace_path = '/trace/1/spec2006/trace_blueprint/TRACE_sw'

TRACE_FILE['bzip2_p@ref-1']      = trace_path + '/bzip2/pin_traces/bzip2.1.raw'
TRACE_FILE['bzip2_p@ref']        = TRACE_FILE['bzip2_p@ref-1'];
TRACE_FILE['gcc_p@ref-1']        = trace_path + '/gcc/pin_traces/gcc.1.raw'
TRACE_FILE['gcc_p@ref']          = TRACE_FILE['gcc_p@ref-1'];
TRACE_FILE['mcf_p@ref-1']        = trace_path + '/mcf/pin_traces/mcf.1.raw'
TRACE_FILE['mcf_p@ref']          = TRACE_FILE['mcf_p@ref-1'];
TRACE_FILE['libquantum_p@ref-1'] = trace_path + '/libquantum/pin_traces/libquantum.1.raw'
TRACE_FILE['libquantum_p@ref']   = TRACE_FILE['libquantum_p@ref-1'];
TRACE_FILE['bwaves_p@ref-1']     = trace_path + '/bwaves/pin_traces/bwaves.1.raw'
TRACE_FILE['bwaves_p@ref']       = TRACE_FILE['bwaves_p@ref-1'];
TRACE_FILE['milc_p@ref-1']       = trace_path + '/milc/pin_traces/milc.1.raw'
TRACE_FILE['milc_p@ref']         = TRACE_FILE['milc_p@ref-1'];
TRACE_FILE['zeusmp_p@ref-1']     = trace_path + '/zeusmp/pin_traces/zeusmp.1.raw'
TRACE_FILE['zeusmp_p@ref']       = TRACE_FILE['zeusmp_p@ref-1'];
TRACE_FILE['cactusADM_p@ref-1']  = trace_path + '/cactusADM/pin_traces/cactusADM.1.raw'
TRACE_FILE['cactusADM_p@ref']    = TRACE_FILE['cactusADM_p@ref-1'];
TRACE_FILE['leslie3d_p@ref-1']   = trace_path + '/leslie3d/pin_traces/leslie3d.1.raw'
TRACE_FILE['leslie3d_p@ref']     = TRACE_FILE['leslie3d_p@ref-1'];
TRACE_FILE['soplex_p@ref-1']     = trace_path + '/soplex/pin_traces/soplex.1.raw'
TRACE_FILE['soplex_p@ref']       = TRACE_FILE['soplex_p@ref-1'];
TRACE_FILE['GemsFDTD_p@ref-1']   = trace_path + '/GemsFDTD/pin_traces/GemsFDTD.1.raw'
TRACE_FILE['GemsFDTD_p@ref']     = TRACE_FILE['GemsFDTD_p@ref-1'];
TRACE_FILE['lbm_p@ref-1']        = trace_path + '/lbm/pin_traces/lbm.1.raw'
TRACE_FILE['lbm_p@ref']          = TRACE_FILE['lbm_p@ref-1'];
TRACE_FILE['wrf_p@ref-1']        = trace_path + '/wrf/pin_traces/wrf.1.raw'
TRACE_FILE['wrf_p@ref']          = TRACE_FILE['wrf_p@ref-1'];
TRACE_FILE['sphinx3_p@ref-1']    = trace_path + '/sphinx3/pin_traces/sphinx3.1.raw'
TRACE_FILE['sphinx3_p@ref']      = TRACE_FILE['sphinx3_p@ref-1'];


# spec2006 - prefetch2
trace_path = '/trace/1/spec2006/trace_blueprint_unopt/trace_base'
TRACE_FILE['milc_bb@ref']       = trace_path + '/milc/pin_traces/milc.1.raw'
TRACE_FILE['mcf_bb@ref']        = trace_path + '/mcf/pin_traces/mcf.1.raw'
TRACE_FILE['lbm_bb@ref']        = trace_path + '/lbm/pin_traces/lbm.1.raw'
TRACE_FILE['libquantum_bb@ref'] = trace_path + '/libquantum/pin_traces/libquantum.1.raw'
TRACE_FILE['cactusADM_bb@ref']  = trace_path + '/cactusADM/pin_traces/cactusADM.1.raw'
TRACE_FILE['zeusmp_bb@ref']     = trace_path + '/zeusmp/pin_traces/zeusmp.1.raw'
TRACE_FILE['GemsFDTD_bb@ref']   = trace_path + '/GemsFDTD/pin_traces/GemsFDTD.1.raw'
TRACE_FILE['gcc_bb@ref']        = trace_path + '/gcc/pin_traces/gcc.1.raw'
TRACE_FILE['leslie3d_bb@ref']   = trace_path + '/leslie3d/pin_traces/leslie3d.1.raw'
TRACE_FILE['soplex_bb@ref']     = trace_path + '/soplex/pin_traces/soplex.1.raw'
TRACE_FILE['sphinx3_bb@ref']    = trace_path + '/sphinx3/pin_traces/sphinx3.1.raw'

trace_path = '/trace/1/spec2006/trace_blueprint_unopt/trace_sw'
TRACE_FILE['milc_pp@ref']       = trace_path + '/milc/pin_traces/milc.1.raw'
TRACE_FILE['mcf_pp@ref']        = trace_path + '/mcf/pin_traces/mcf.1.raw'
TRACE_FILE['lbm_pp@ref']        = trace_path + '/lbm/pin_traces/lbm.1.raw'
TRACE_FILE['libquantum_pp@ref'] = trace_path + '/libquantum/pin_traces/libquantum.1.raw'
TRACE_FILE['cactusADM_pp@ref']  = trace_path + '/cactusADM/pin_traces/cactusADM.1.raw'
TRACE_FILE['zeusmp_pp@ref']     = trace_path + '/zeusmp/pin_traces/zeusmp.1.raw'
TRACE_FILE['GemsFDTD_pp@ref']   = trace_path + '/GemsFDTD/pin_traces/GemsFDTD.1.raw'
TRACE_FILE['gcc_pp@ref']        = trace_path + '/gcc/pin_traces/gcc.1.raw'
TRACE_FILE['leslie3d_pp@ref']   = trace_path + '/leslie3d/pin_traces/leslie3d.1.raw'
TRACE_FILE['soplex_pp@ref']     = trace_path + '/soplex/pin_traces/soplex.1.raw'
TRACE_FILE['sphinx3_pp@ref']    = trace_path + '/sphinx3/pin_traces/sphinx3.1.raw'


trace_path = '/trace/1/benchmarks/spec2006/spec2006-tgen/Results/IND_NEW_TRACE3'
TRACE_FILE['bwaves_np@ref']     = trace_path + '/bwaves/pin_traces/bwaves.1.raw'
TRACE_FILE['milc_np@ref']       = trace_path + '/milc/pin_traces/milc.1.raw'
TRACE_FILE['mcf_np@ref']        = trace_path + '/mcf/pin_traces/mcf.1.raw'
TRACE_FILE['lbm_np@ref']        = trace_path + '/lbm/pin_traces/lbm.1.raw'
TRACE_FILE['libquantum_np@ref'] = trace_path + '/libquantum/pin_traces/libquantum.1.raw'
TRACE_FILE['cactusADM_np@ref']  = trace_path + '/cactusADM/pin_traces/cactusADM.1.raw'
TRACE_FILE['zeusmp_np@ref']     = trace_path + '/zeusmp/pin_traces/zeusmp.1.raw'
TRACE_FILE['GemsFDTD_np@ref']   = trace_path + '/GemsFDTD/pin_traces/GemsFDTD.1.raw'
TRACE_FILE['gcc_np@ref']        = trace_path + '/gcc/pin_traces/gcc.1.raw'
TRACE_FILE['leslie3d_np@ref']   = trace_path + '/leslie3d/pin_traces/leslie3d.1.raw'
TRACE_FILE['soplex_np@ref']     = trace_path + '/soplex/pin_traces/soplex.1.raw'
TRACE_FILE['sphinx3_np@ref']    = trace_path + '/sphinx3/pin_traces/sphinx3.1.raw'

trace_path = '/trace/1/benchmarks/spec2006/spec2006-tgen/Results/trace_target'
TRACE_FILE['bwaves_test@ref']     = trace_path + '/bwaves/pin_traces/bwaves.1.raw'
TRACE_FILE['milc_test@ref']       = trace_path + '/milc/pin_traces/milc.1.raw'
TRACE_FILE['mcf_test@ref']        = trace_path + '/mcf/pin_traces/mcf.1.raw'
TRACE_FILE['lbm_test@ref']        = trace_path + '/lbm/pin_traces/lbm.1.raw'
TRACE_FILE['libquantum_test@ref'] = trace_path + '/libquantum/pin_traces/libquantum.1.raw'
TRACE_FILE['cactusADM_test@ref']  = trace_path + '/cactusADM/pin_traces/cactusADM.1.raw'
TRACE_FILE['zeusmp_test@ref']     = trace_path + '/zeusmp/pin_traces/zeusmp.1.raw'
TRACE_FILE['GemsFDTD_test@ref']   = trace_path + '/GemsFDTD/pin_traces/GemsFDTD.1.raw'
TRACE_FILE['gcc_test@ref']        = trace_path + '/gcc/pin_traces/gcc.1.raw'
TRACE_FILE['leslie3d_test@ref']   = trace_path + '/leslie3d/pin_traces/leslie3d.1.raw'
TRACE_FILE['soplex_test@ref']     = trace_path + '/soplex/pin_traces/soplex.1.raw'
TRACE_FILE['sphinx3_test@ref']    = trace_path + '/sphinx3/pin_traces/sphinx3.1.raw'

trace_path = '/trace/1/benchmarks/spec2006/spec2006-tgen/Results/TRACE_BASE'
TRACE_FILE['bwaves_nb@ref']     = trace_path + '/bwaves/pin_traces/bwaves.1.raw'
TRACE_FILE['milc_nb@ref']       = trace_path + '/milc/pin_traces/milc.1.raw'
TRACE_FILE['mcf_nb@ref']        = trace_path + '/mcf/pin_traces/mcf.1.raw'
TRACE_FILE['lbm_nb@ref']        = trace_path + '/lbm/pin_traces/lbm.1.raw'
TRACE_FILE['libquantum_nb@ref'] = trace_path + '/libquantum/pin_traces/libquantum.1.raw'
TRACE_FILE['cactusADM_nb@ref']  = trace_path + '/cactusADM/pin_traces/cactusADM.1.raw'
TRACE_FILE['zeusmp_nb@ref']     = trace_path + '/zeusmp/pin_traces/zeusmp.1.raw'
TRACE_FILE['GemsFDTD_nb@ref']   = trace_path + '/GemsFDTD/pin_traces/GemsFDTD.1.raw'
TRACE_FILE['gcc_nb@ref']        = trace_path + '/gcc/pin_traces/gcc.1.raw'
TRACE_FILE['leslie3d_nb@ref']   = trace_path + '/leslie3d/pin_traces/leslie3d.1.raw'
TRACE_FILE['soplex_nb@ref']     = trace_path + '/soplex/pin_traces/soplex.1.raw'
TRACE_FILE['sphinx3_nb@ref']    = trace_path + '/sphinx3/pin_traces/sphinx3.1.raw'

trace_path = '/trace/1/benchmarks/spec2006/spec2006-tgen/Results/trace_target_opt'
TRACE_FILE['bwaves_opt@ref']     = trace_path + '/bwaves/pin_traces/bwaves.1.raw'
TRACE_FILE['milc_opt@ref']       = trace_path + '/milc/pin_traces/milc.1.raw'
TRACE_FILE['mcf_opt@ref']        = trace_path + '/mcf/pin_traces/mcf.1.raw'
TRACE_FILE['lbm_opt@ref']        = trace_path + '/lbm/pin_traces/lbm.1.raw'
TRACE_FILE['libquantum_opt@ref'] = trace_path + '/libquantum/pin_traces/libquantum.1.raw'
TRACE_FILE['cactusADM_opt@ref']  = trace_path + '/cactusADM/pin_traces/cactusADM.1.raw'
TRACE_FILE['zeusmp_opt@ref']     = trace_path + '/zeusmp/pin_traces/zeusmp.1.raw'
TRACE_FILE['GemsFDTD_opt@ref']   = trace_path + '/GemsFDTD/pin_traces/GemsFDTD.1.raw'
TRACE_FILE['gcc_opt@ref']        = trace_path + '/gcc/pin_traces/gcc.1.raw'
TRACE_FILE['leslie3d_opt@ref']   = trace_path + '/leslie3d/pin_traces/leslie3d.1.raw'
TRACE_FILE['soplex_opt@ref']     = trace_path + '/soplex/pin_traces/soplex.1.raw'
TRACE_FILE['sphinx3_opt@ref']    = trace_path + '/sphinx3/pin_traces/sphinx3.1.raw'

trace_path = '/trace/1/spec2006/trace_blueprint/s06npb_trace'
TRACE_FILE['bwaves_npb@ref']     = trace_path + '/bwaves/pin_traces/bwaves.1.raw'
TRACE_FILE['milc_npb@ref']       = trace_path + '/milc/pin_traces/milc.1.raw'
TRACE_FILE['mcf_npb@ref']        = trace_path + '/mcf/pin_traces/mcf.1.raw'
TRACE_FILE['lbm_npb@ref']        = trace_path + '/lbm/pin_traces/lbm.1.raw'
TRACE_FILE['libquantum_npb@ref'] = trace_path + '/libquantum/pin_traces/libquantum.1.raw'
TRACE_FILE['cactusADM_npb@ref']  = trace_path + '/cactusADM/pin_traces/cactusADM.1.raw'
TRACE_FILE['zeusmp_npb@ref']     = trace_path + '/zeusmp/pin_traces/zeusmp.1.raw'
TRACE_FILE['GemsFDTD_npb@ref']   = trace_path + '/GemsFDTD/pin_traces/GemsFDTD.1.raw'
TRACE_FILE['gcc_npb@ref']        = trace_path + '/gcc/pin_traces/gcc.1.raw'
TRACE_FILE['leslie3d_npb@ref']   = trace_path + '/leslie3d/pin_traces/leslie3d.1.raw'
TRACE_FILE['soplex_npb@ref']     = trace_path + '/soplex/pin_traces/soplex.1.raw'
TRACE_FILE['sphinx3_npb@ref']    = trace_path + '/sphinx3/pin_traces/sphinx3.1.raw'
TRACE_FILE['bzip2_npb@ref']      = trace_path + '/bzip2/pin_traces/bzip2.1.raw'

trace_path = '/trace/1/spec2006/trace_blueprint/s06nbb_trace'
TRACE_FILE['bwaves_nbb@ref']     = trace_path + '/bwaves/pin_traces/bwaves.1.raw'
TRACE_FILE['milc_nbb@ref']       = trace_path + '/milc/pin_traces/milc.1.raw'
TRACE_FILE['mcf_nbb@ref']        = trace_path + '/mcf/pin_traces/mcf.1.raw'
TRACE_FILE['lbm_nbb@ref']        = trace_path + '/lbm/pin_traces/lbm.1.raw'
TRACE_FILE['libquantum_nbb@ref'] = trace_path + '/libquantum/pin_traces/libquantum.1.raw'
TRACE_FILE['cactusADM_nbb@ref']  = trace_path + '/cactusADM/pin_traces/cactusADM.1.raw'
TRACE_FILE['zeusmp_nbb@ref']     = trace_path + '/zeusmp/pin_traces/zeusmp.1.raw'
TRACE_FILE['GemsFDTD_nbb@ref']   = trace_path + '/GemsFDTD/pin_traces/GemsFDTD.1.raw'
TRACE_FILE['gcc_nbb@ref']        = trace_path + '/gcc/pin_traces/gcc.1.raw'
TRACE_FILE['leslie3d_nbb@ref']   = trace_path + '/leslie3d/pin_traces/leslie3d.1.raw'
TRACE_FILE['soplex_nbb@ref']     = trace_path + '/soplex/pin_traces/soplex.1.raw'
TRACE_FILE['sphinx3_nbb@ref']    = trace_path + '/sphinx3/pin_traces/sphinx3.1.raw'
TRACE_FILE['bzip2_nbb@ref']      = trace_path + '/bzip2/pin_traces/bzip2.1.raw'


##! GCC trace
trace_path = '/trace/1/spec2006/trace_blueprint/gcc/gcc_trace_nosw'
TRACE_FILE['milc_gcc_n@ref']       = trace_path + '/milc/pin_traces/milc.1.raw'
TRACE_FILE['mcf_gcc_n@ref']        = trace_path + '/mcf/pin_traces/mcf.1.raw'
TRACE_FILE['lbm_gcc_n@ref']        = trace_path + '/lbm/pin_traces/lbm.1.raw'
TRACE_FILE['libquantum_gcc_n@ref'] = trace_path + '/libquantum/pin_traces/libquantum.1.raw'
TRACE_FILE['gcc_gcc_n@ref']        = trace_path + '/gcc/pin_traces/gcc.1.raw'
TRACE_FILE['soplex_gcc_n@ref']     = trace_path + '/soplex/pin_traces/soplex.1.raw'
TRACE_FILE['sphinx3_gcc_n@ref']    = trace_path + '/sphinx3/pin_traces/sphinx3.1.raw'
TRACE_FILE['bzip2_gcc_n@ref']      = trace_path + '/bzip2/pin_traces/bzip2.1.raw'

trace_path = '/trace/1/spec2006/trace_blueprint/gcc/gcc_trace_base'
TRACE_FILE['milc_gcc_b@ref']       = trace_path + '/milc/pin_traces/milc.1.raw'
TRACE_FILE['mcf_gcc_b@ref']        = trace_path + '/mcf/pin_traces/mcf.1.raw'
TRACE_FILE['lbm_gcc_b@ref']        = trace_path + '/lbm/pin_traces/lbm.1.raw'
TRACE_FILE['libquantum_gcc_b@ref'] = trace_path + '/libquantum/pin_traces/libquantum.1.raw'
TRACE_FILE['gcc_gcc_b@ref']        = trace_path + '/gcc/pin_traces/gcc.1.raw'
TRACE_FILE['soplex_gcc_b@ref']     = trace_path + '/soplex/pin_traces/soplex.1.raw'
TRACE_FILE['sphinx3_gcc_b@ref']    = trace_path + '/sphinx3/pin_traces/sphinx3.1.raw'
TRACE_FILE['bzip2_gcc_b@ref']      = trace_path + '/bzip2/pin_traces/bzip2.1.raw'

trace_path = '/trace/1/spec2006/trace_blueprint/gcc/gcc_trace_sw'
TRACE_FILE['milc_gcc_s@ref']       = trace_path + '/milc/pin_traces/milc.1.raw'
TRACE_FILE['mcf_gcc_s@ref']        = trace_path + '/mcf/pin_traces/mcf.1.raw'
TRACE_FILE['lbm_gcc_s@ref']        = trace_path + '/lbm/pin_traces/lbm.1.raw'
TRACE_FILE['libquantum_gcc_s@ref'] = trace_path + '/libquantum/pin_traces/libquantum.1.raw'
TRACE_FILE['gcc_gcc_s@ref']        = trace_path + '/gcc/pin_traces/gcc.1.raw'
TRACE_FILE['soplex_gcc_s@ref']     = trace_path + '/soplex/pin_traces/soplex.1.raw'
TRACE_FILE['sphinx3_gcc_s@ref']    = trace_path + '/sphinx3/pin_traces/sphinx3.1.raw'
TRACE_FILE['bzip2_gcc_s@ref']      = trace_path + '/bzip2/pin_traces/bzip2.1.raw'


trace_path = '/trace/1/spec2006/trace_blueprint/gcc44/gcc44_trace_nosw'
TRACE_FILE['milc_gcc44_n@ref']       = trace_path + '/milc/pin_traces/milc.1.txt'
TRACE_FILE['mcf_gcc44_n@ref']        = trace_path + '/mcf/pin_traces/mcf.1.txt'
TRACE_FILE['lbm_gcc44_n@ref']        = trace_path + '/lbm/pin_traces/lbm.1.txt'
TRACE_FILE['libquantum_gcc44_n@ref'] = trace_path + '/libquantum/pin_traces/libquantum.1.txt'
TRACE_FILE['gcc_gcc44_n@ref']        = trace_path + '/gcc/pin_traces/gcc.1.txt'
TRACE_FILE['soplex_gcc44_n@ref']     = trace_path + '/soplex/pin_traces/soplex.1.txt'
TRACE_FILE['sphinx3_gcc44_n@ref']    = trace_path + '/sphinx3/pin_traces/sphinx3.1.txt'
TRACE_FILE['bzip2_gcc44_n@ref']      = trace_path + '/bzip2/pin_traces/bzip2.1.txt'

trace_path = '/trace/1/spec2006/trace_blueprint/gcc44/gcc44_trace_base'
TRACE_FILE['milc_gcc44_b@ref']       = trace_path + '/milc/pin_traces/milc.1.txt'
TRACE_FILE['mcf_gcc44_b@ref']        = trace_path + '/mcf/pin_traces/mcf.1.txt'
TRACE_FILE['lbm_gcc44_b@ref']        = trace_path + '/lbm/pin_traces/lbm.1.txt'
TRACE_FILE['libquantum_gcc44_b@ref'] = trace_path + '/libquantum/pin_traces/libquantum.1.txt'
TRACE_FILE['gcc_gcc44_b@ref']        = trace_path + '/gcc/pin_traces/gcc.1.txt'
TRACE_FILE['soplex_gcc44_b@ref']     = trace_path + '/soplex/pin_traces/soplex.1.txt'
TRACE_FILE['sphinx3_gcc44_b@ref']    = trace_path + '/sphinx3/pin_traces/sphinx3.1.txt'
TRACE_FILE['bzip2_gcc44_b@ref']      = trace_path + '/bzip2/pin_traces/bzip2.1.txt'

trace_path = '/trace/1/spec2006/trace_blueprint/gcc44/gcc44_trace_sw'
TRACE_FILE['milc_gcc44_s@ref']       = trace_path + '/milc/pin_traces/milc.1.txt'
TRACE_FILE['mcf_gcc44_s@ref']        = trace_path + '/mcf/pin_traces/mcf.1.txt'
TRACE_FILE['lbm_gcc44_s@ref']        = trace_path + '/lbm/pin_traces/lbm.1.txt'
TRACE_FILE['libquantum_gcc44_s@ref'] = trace_path + '/libquantum/pin_traces/libquantum.1.txt'
TRACE_FILE['gcc_gcc44_s@ref']        = trace_path + '/gcc/pin_traces/gcc.1.txt'
TRACE_FILE['soplex_gcc44_s@ref']     = trace_path + '/soplex/pin_traces/soplex.1.txt'
TRACE_FILE['sphinx3_gcc44_s@ref']    = trace_path + '/sphinx3/pin_traces/sphinx3.1.txt'
TRACE_FILE['bzip2_gcc44_s@ref']      = trace_path + '/bzip2/pin_traces/bzip2.1.txt'


##! GCC PGO Base
trace_path = '/trace/1/spec2006/trace_blueprint/gcc_pgo/base'
TRACE_FILE['milc_gcc_pgo_b@ref']       = trace_path + '/milc/pin_traces/milc.1.txt'
TRACE_FILE['mcf_gcc_pgo_b@ref']        = trace_path + '/mcf/pin_traces/mcf.1.txt'
TRACE_FILE['lbm_gcc_pgo_b@ref']        = trace_path + '/lbm/pin_traces/lbm.1.txt'
TRACE_FILE['libquantum_gcc_pgo_b@ref'] = trace_path + '/libquantum/pin_traces/libquantum.1.txt'
TRACE_FILE['gcc_gcc_pgo_b@ref']        = trace_path + '/gcc/pin_traces/gcc.1.txt'
TRACE_FILE['soplex_gcc_pgo_b@ref']     = trace_path + '/soplex/pin_traces/soplex.1.txt'
TRACE_FILE['sphinx3_gcc_pgo_b@ref']    = trace_path + '/sphinx3/pin_traces/sphinx3.1.txt'
TRACE_FILE['bzip2_gcc_pgo_b@ref']      = trace_path + '/bzip2/pin_traces/bzip2.1.txt'


##! ICC PGO Base
trace_path = '/trace/1/spec2006/trace_blueprint/icc_pgo/base'
TRACE_FILE['bwaves_icc_pgo_b@ref']     = trace_path + '/bwaves/pin_traces/bwaves.1.txt'
TRACE_FILE['milc_icc_pgo_b@ref']       = trace_path + '/milc/pin_traces/milc.1.txt'
TRACE_FILE['mcf_icc_pgo_b@ref']        = trace_path + '/mcf/pin_traces/mcf.1.txt'
TRACE_FILE['lbm_icc_pgo_b@ref']        = trace_path + '/lbm/pin_traces/lbm.1.txt'
TRACE_FILE['libquantum_icc_pgo_b@ref'] = trace_path + '/libquantum/pin_traces/libquantum.1.txt'
TRACE_FILE['cactusADM_icc_pgo_b@ref']  = trace_path + '/cactusADM/pin_traces/cactusADM.1.txt'
TRACE_FILE['zeusmp_icc_pgo_b@ref']     = trace_path + '/zeusmp/pin_traces/zeusmp.1.txt'
TRACE_FILE['GemsFDTD_icc_pgo_b@ref']   = trace_path + '/GemsFDTD/pin_traces/GemsFDTD.1.txt'
TRACE_FILE['gcc_icc_pgo_b@ref']        = trace_path + '/gcc/pin_traces/gcc.1.txt'
TRACE_FILE['leslie3d_icc_pgo_b@ref']   = trace_path + '/leslie3d/pin_traces/leslie3d.1.txt'
TRACE_FILE['soplex_icc_pgo_b@ref']     = trace_path + '/soplex/pin_traces/soplex.1.txt'
TRACE_FILE['sphinx3_icc_pgo_b@ref']    = trace_path + '/sphinx3/pin_traces/sphinx3.1.txt'
TRACE_FILE['bzip2_icc_pgo_b@ref']      = trace_path + '/bzip2/pin_traces/bzip2.1.txt'




##! CDP
trace_path = '/trace/1/benchmarks/spec2006/spec2006-tgen/Results/s06_cdp_oracle'
TRACE_FILE['mcf_cdp@ref']        = trace_path + '/mcf/pin_traces/mcf.1.txt'
TRACE_FILE['gcc_cdp@ref']        = trace_path + '/gcc/pin_traces/gcc.1.txt'
TRACE_FILE['astar_cdp@ref']      = trace_path + '/astar/pin_traces/astar.1.txt'

trace_path = '/trace/1/ptx/gemv'
TRACE_FILE['gemv@ref']            = trace_path + '/bench_cuda_1drb/_Z10gpu_gemv__iimPKfS0_Pf_4/Trace.txt'
TRACE_FILE['gemv_pad@ref']        = trace_path + '/bench_cuda_1drb_pad/_Z23gpu_gemv__cuda_1drb_padiimPKfS0_Pf_4/Trace.txt'
TRACE_FILE['gemv_pref@ref']       = trace_path + '/bench_cuda_1drb_pad_pref_1/_Z28gpu_gemv__cuda_1drb_pad_prefiimPKfS0_Pf_4/Trace.txt'
TRACE_FILE['gemv_unroll@ref']     = trace_path + '/bench_cuda_1drb_pad_unroll/_Z30gpu_gemv__cuda_1drb_pad_unrolliimPKfS0_Pf_4/Trace.txt'



## SPEC 2000

trace_path = '/trace/x86/spec2000/'
TRACE_FILE['gzip-0@ref'] = trace_path + 'gzip/pin_traces/gzip.4.txt'
TRACE_FILE['gzip-1@ref'] = trace_path + 'gzip/pin_traces/gzip.1.txt'
TRACE_FILE['gzip-2@ref'] = trace_path + 'gzip/pin_traces/gzip.5.txt'
TRACE_FILE['gzip-3@ref'] = trace_path + 'gzip/pin_traces/gzip.2.txt'
TRACE_FILE['gzip-4@ref'] = trace_path + 'gzip/pin_traces/gzip.3.txt'
TRACE_FILE['vpr-0@ref'] = trace_path + 'vpr/pin_traces/vpr.1.txt'
TRACE_FILE['vpr-1@ref'] = trace_path + 'vpr/pin_traces/vpr.2.txt'
TRACE_FILE['gcc00-0@ref'] = trace_path + 'gcc/pin_traces/gcc.3.txt'
TRACE_FILE['gcc00-1@ref'] = trace_path + 'gcc/pin_traces/gcc.2.txt'
TRACE_FILE['gcc00-2@ref'] = trace_path + 'gcc/pin_traces/gcc.1.txt'
TRACE_FILE['gcc00-3@ref'] = trace_path + 'gcc/pin_traces/gcc.5.txt'
TRACE_FILE['mcf00-0@ref'] = trace_path + 'mcf/pin_traces/mcf.1.txt'
TRACE_FILE['crafty-0@ref'] = trace_path + 'crafty/pin_traces/crafty.1.txt'
TRACE_FILE['parser-0@ref'] = trace_path + 'parser/pin_traces/parser.1.txt'
TRACE_FILE['eon-0@ref'] = trace_path + 'eon/pin_traces/eon.2.txt'
TRACE_FILE['eon-1@ref'] = trace_path + 'eon/pin_traces/eon.1.txt'
TRACE_FILE['eon-2@ref'] = trace_path + 'eon/pin_traces/eon.3.txt'
TRACE_FILE['perlbmk-0@ref'] = trace_path + 'perlbmk/pin_traces/perlbmk.2.txt'
TRACE_FILE['perlbmk-1@ref'] = trace_path + 'perlbmk/pin_traces/perlbmk.5.txt'
TRACE_FILE['perlbmk-2@ref'] = trace_path + 'perlbmk/pin_traces/perlbmk.3.txt'
TRACE_FILE['perlbmk-3@ref'] = trace_path + 'perlbmk/pin_traces/perlbmk.4.txt'
TRACE_FILE['perlbmk-4@ref'] = trace_path + 'perlbmk/pin_traces/perlbmk.7.txt'
TRACE_FILE['gap-0@ref'] = trace_path + 'gap/pin_traces/gap.1.txt'
TRACE_FILE['vortex-0@ref'] = trace_path + 'vortex/pin_traces/vortex.1.txt'
TRACE_FILE['vortex-1@ref'] = trace_path + 'vortex/pin_traces/vortex.2.txt'
TRACE_FILE['vortex-2@ref'] = trace_path + 'vortex/pin_traces/vortex.3.txt'
TRACE_FILE['bzip200-0@ref'] = trace_path + 'bzip2/pin_traces/bzip2.1.txt'
TRACE_FILE['bzip200-1@ref'] = trace_path + 'bzip2/pin_traces/bzip2.3.txt'
TRACE_FILE['bzip200-2@ref'] = trace_path + 'bzip2/pin_traces/bzip2.2.txt'
TRACE_FILE['twolf-0@ref'] = trace_path + 'twolf/pin_traces/twolf.1.txt'


TRACE_FILE['wupwise-0@ref'] = trace_path + 'wupwise/pin_traces/wupwise.1.txt'
TRACE_FILE['swim-0@ref'] = trace_path + 'swim/pin_traces/swim.1.txt'
TRACE_FILE['mgrid-0@ref'] = trace_path + 'mgrid/pin_traces/mgrid.1.txt'
TRACE_FILE['applu-0@ref'] = trace_path + 'applu/pin_traces/applu.1.txt'
TRACE_FILE['mesa-0@ref'] = trace_path + 'mesa/pin_traces/mesa.1.txt'
TRACE_FILE['galgel-0@ref'] = trace_path + 'galgel/pin_traces/galgel.1.txt'
TRACE_FILE['art-0@ref'] = trace_path + 'art/pin_traces/art.1.txt'
TRACE_FILE['art-1@ref'] = trace_path + 'art/pin_traces/art.2.txt'
TRACE_FILE['equake-0@ref'] = trace_path + 'equake/pin_traces/equake.1.txt'
TRACE_FILE['facerec-0@ref'] = trace_path + 'facerec/pin_traces/facerec.1.txt'
TRACE_FILE['ammp-0@ref'] = trace_path + 'ammp/pin_traces/ammp.1.txt'
TRACE_FILE['lucas-0@ref'] = trace_path + 'lucas/pin_traces/lucas.1.txt'
TRACE_FILE['fma3d-0@ref'] = trace_path + 'fma3d/pin_traces/fma3d.1.txt'
TRACE_FILE['sixtrack-0@ref'] = trace_path + 'sixtrack/pin_traces/sixtrack.1.txt'
TRACE_FILE['apsi-0@ref'] = trace_path + 'apsi/pin_traces/apsi.1.txt'


## SPEC 2006 (New)
trace_path = '/trace/x86/spec2006/trace_simpoint/'

TRACE_FILE['perlbench-0@ref'] = trace_path + 'perlbench/pin_traces/perlbench.2.txt'
TRACE_FILE['perlbench-1@ref'] = trace_path + 'perlbench/pin_traces/perlbench.1.txt'
TRACE_FILE['perlbench-2@ref'] = trace_path + 'perlbench/pin_traces/perlbench.3.txt'
TRACE_FILE['bzip2-0@ref'] = trace_path + 'bzip2/pin_traces/bzip2.1.txt'
TRACE_FILE['bzip2-1@ref'] = trace_path + 'bzip2/pin_traces/bzip2.6.txt'
TRACE_FILE['bzip2-2@ref'] = trace_path + 'bzip2/pin_traces/bzip2.4.txt'
TRACE_FILE['bzip2-3@ref'] = trace_path + 'bzip2/pin_traces/bzip2.3.txt'
TRACE_FILE['bzip2-4@ref'] = trace_path + 'bzip2/pin_traces/bzip2.5.txt'
TRACE_FILE['bzip2-5@ref'] = trace_path + 'bzip2/pin_traces/bzip2.2.txt'
TRACE_FILE['gcc-0@ref'] = trace_path + 'gcc/pin_traces/gcc.8.txt'
TRACE_FILE['gcc-1@ref'] = trace_path + 'gcc/pin_traces/gcc.3.txt'
TRACE_FILE['gcc-2@ref'] = trace_path + 'gcc/pin_traces/gcc.4.txt'
TRACE_FILE['gcc-3@ref'] = trace_path + 'gcc/pin_traces/gcc.9.txt'
TRACE_FILE['gcc-4@ref'] = trace_path + 'gcc/pin_traces/gcc.2.txt'
TRACE_FILE['gcc-5@ref'] = trace_path + 'gcc/pin_traces/gcc.7.txt'
TRACE_FILE['gcc-6@ref'] = trace_path + 'gcc/pin_traces/gcc.1.txt'
TRACE_FILE['gcc-7@ref'] = trace_path + 'gcc/pin_traces/gcc.6.txt'
TRACE_FILE['gcc-8@ref'] = trace_path + 'gcc/pin_traces/gcc.5.txt'
TRACE_FILE['mcf-0@ref'] = trace_path + 'mcf/pin_traces/mcf.1.txt'
TRACE_FILE['gobmk-0@ref'] = trace_path + 'gobmk/pin_traces/gobmk.2.txt'
TRACE_FILE['gobmk-1@ref'] = trace_path + 'gobmk/pin_traces/gobmk.5.txt'
TRACE_FILE['gobmk-2@ref'] = trace_path + 'gobmk/pin_traces/gobmk.4.txt'
TRACE_FILE['gobmk-3@ref'] = trace_path + 'gobmk/pin_traces/gobmk.3.txt'
TRACE_FILE['gobmk-4@ref'] = trace_path + 'gobmk/pin_traces/gobmk.1.txt'
TRACE_FILE['hmmer-0@ref'] = trace_path + 'hmmer/pin_traces/hmmer.1.txt'
TRACE_FILE['hmmer-1@ref'] = trace_path + 'hmmer/pin_traces/hmmer.2.txt'
TRACE_FILE['sjeng-0@ref'] = trace_path + 'sjeng/pin_traces/sjeng.1.txt'
TRACE_FILE['libquantum-0@ref'] = trace_path + 'libquantum/pin_traces/libquantum.1.txt'
TRACE_FILE['h264ref-0@ref'] = trace_path + 'h264ref/pin_traces/h264ref.1.txt'
TRACE_FILE['h264ref-1@ref'] = trace_path + 'h264ref/pin_traces/h264ref.2.txt'
TRACE_FILE['h264ref-2@ref'] = trace_path + 'h264ref/pin_traces/h264ref.3.txt'
TRACE_FILE['omnetpp-0@ref'] = trace_path + 'omnetpp/pin_traces/omnetpp.1.txt'
TRACE_FILE['astar-0@ref'] = trace_path + 'astar/pin_traces/astar.2.txt'
TRACE_FILE['astar-1@ref'] = trace_path + 'astar/pin_traces/astar.1.txt'
TRACE_FILE['xalancbmk-0@ref'] = trace_path + 'xalancbmk/pin_traces/xalancbmk.1.txt'
## xalancbmk

TRACE_FILE['bwaves-0@ref'] = trace_path + 'bwaves/pin_traces/bwaves.1.txt'
TRACE_FILE['gamess-0@ref'] = trace_path + 'gamess/pin_traces/gamess.3.txt'
TRACE_FILE['gamess-1@ref'] = trace_path + 'gamess/pin_traces/gamess.2.txt'
TRACE_FILE['gamess-2@ref'] = trace_path + 'gamess/pin_traces/gamess.1.txt'
TRACE_FILE['milc-0@ref'] = trace_path + 'milc/pin_traces/milc.1.txt'
TRACE_FILE['zeusmp-0@ref'] = trace_path + 'zeusmp/pin_traces/zeusmp.1.txt'
TRACE_FILE['gromacs-0@ref'] = trace_path + 'gromacs/pin_traces/gromacs.1.txt'
TRACE_FILE['cactusADM-0@ref'] = trace_path + 'cactusADM/pin_traces/cactusADM.1.txt'
TRACE_FILE['leslie3d-0@ref'] = trace_path + 'leslie3d/pin_traces/leslie3d.1.txt'
TRACE_FILE['namd-0@ref'] = trace_path + 'namd/pin_traces/namd.1.txt'
#dealII
TRACE_FILE['dealII-0@ref'] = trace_path + 'dealII/pin_traces/dealII.1.txt'
TRACE_FILE['soplex-0@ref'] = trace_path + 'soplex/pin_traces/soplex.2.txt'
TRACE_FILE['soplex-1@ref'] = trace_path + 'soplex/pin_traces/soplex.1.txt'
TRACE_FILE['povray-0@ref'] = trace_path + 'povray/pin_traces/povray.1.txt'
TRACE_FILE['calculix-0@ref'] = trace_path + 'calculix/pin_traces/calculix.1.txt'
TRACE_FILE['GemsFDTD-0@ref'] = trace_path + 'GemsFDTD/pin_traces/GemsFDTD.1.txt'
TRACE_FILE['tonto-0@ref'] = trace_path + 'tonto/pin_traces/tonto.1.txt'
TRACE_FILE['lbm-0@ref'] = trace_path + 'lbm/pin_traces/lbm.1.txt'
TRACE_FILE['wrf-0@ref'] = trace_path + 'wrf/pin_traces/wrf.1.txt'
TRACE_FILE['sphinx3-0@ref'] = trace_path + 'sphinx3/pin_traces/sphinx3.1.txt'




# SPECOMP
omp_path = '/trace/1/specomp/8'


TRACE_FILE['wupwise_omp@ref-1'] = omp_path + '/wupwise.raw'
TRACE_FILE['wupwise_omp@ref']   = TRACE_FILE['wupwise_omp@ref-1'];
TRACE_FILE['swim_omp@ref-1']    = omp_path + '/swim.raw'
TRACE_FILE['swim_omp@ref']      = TRACE_FILE['swim_omp@ref-1'];
TRACE_FILE['mgrid_omp@ref-1']   = omp_path + '/mgrid.raw'
TRACE_FILE['mgrid_omp@ref']     = TRACE_FILE['mgrid_omp@ref-1'];
TRACE_FILE['applu_omp@ref-1']   = omp_path + '/applu.raw'
TRACE_FILE['applu_omp@ref']     = TRACE_FILE['applu_omp@ref-1'];
TRACE_FILE['equake_omp@ref-1']  = omp_path + '/equake.raw'
TRACE_FILE['equake_omp@ref']    = TRACE_FILE['equake_omp@ref-1'];
TRACE_FILE['apsi_omp@ref-1']    = omp_path + '/apsi.raw'
TRACE_FILE['apsi_omp@ref']      = TRACE_FILE['apsi_omp@ref-1'];
TRACE_FILE['gafort_omp@ref-1']  = omp_path + '/gafort.raw'
TRACE_FILE['gafort_omp@ref']    = TRACE_FILE['gafort_omp@ref-1'];
TRACE_FILE['fma3d_omp@ref-1']   = omp_path + '/fma3d.raw'
TRACE_FILE['fma3d_omp@ref']     = TRACE_FILE['fma3d_omp@ref-1'];
TRACE_FILE['art_omp@ref-1']     = omp_path + '/art.raw'
TRACE_FILE['art_omp@ref']       = TRACE_FILE['art_omp@ref-1'];
TRACE_FILE['ammp_omp@ref-1']    = omp_path + '/ammp.raw'
TRACE_FILE['ammp_omp@ref']      = TRACE_FILE['ammp_omp@ref-1'];


# Mult
mult_path = '/trace/1/mult/base'
TRACE_FILE['mgrid_b@ref-1']   = mult_path + '/mgrid/pin_traces/mgrid.raw'
TRACE_FILE['mgrid_b@ref']     = TRACE_FILE['mgrid_b@ref-1'];
TRACE_FILE['swim_b@ref-1']    = mult_path + '/swim/pin_traces/swim.raw'
TRACE_FILE['swim_b@ref']      = TRACE_FILE['swim_b@ref-1'];
TRACE_FILE['equake_b@ref-1']  = mult_path + '/equake/pin_traces/equake.raw'
TRACE_FILE['equake_b@ref']    = TRACE_FILE['equake_b@ref-1'];
TRACE_FILE['fma3d_b@ref-1']   = mult_path + '/fma3d/pin_traces/fma3d.raw'
TRACE_FILE['fma3d_b@ref']     = TRACE_FILE['fma3d_b@ref-1'];
TRACE_FILE['ft_A_b@ref-1']   = mult_path + '/ft/pin_traces/ft.A.raw'
TRACE_FILE['ft_A_b@ref']     = TRACE_FILE['ft_A_b@ref-1'];
TRACE_FILE['is_A_b@ref-1']   = mult_path + '/is/pin_traces/is.A.raw'
TRACE_FILE['is_A_b@ref']     = TRACE_FILE['is_A_b@ref-1'];

mult_sw_path = '/trace/1/mult/sw'
TRACE_FILE['mgrid_p@ref-1']   = mult_sw_path + '/mgrid/pin_traces/mgrid.raw'
TRACE_FILE['mgrid_p@ref']     = TRACE_FILE['mgrid_p@ref-1'];
TRACE_FILE['swim_p@ref-1']    = mult_sw_path + '/swim/pin_traces/swim.raw'
TRACE_FILE['swim_p@ref']      = TRACE_FILE['swim_p@ref-1'];
TRACE_FILE['equake_p@ref-1']  = mult_sw_path + '/equake/pin_traces/equake.raw'
TRACE_FILE['equake_p@ref']    = TRACE_FILE['equake_p@ref-1'];
TRACE_FILE['fma3d_p@ref-1']   = mult_sw_path + '/fma3d/pin_traces/fma3d.raw'
TRACE_FILE['fma3d_p@ref']     = TRACE_FILE['fma3d_p@ref-1'];
TRACE_FILE['ft_A_p@ref-1']   = mult_sw_path + '/ft/pin_traces/ft.A.raw'
TRACE_FILE['ft_A_p@ref']     = TRACE_FILE['ft_A_p@ref-1'];
TRACE_FILE['is_A_p@ref-1']   = mult_sw_path + '/is/pin_traces/is.A.raw'
TRACE_FILE['is_A_p@ref']     = TRACE_FILE['is_A_p@ref-1'];



trace_path = '/trace/ptx/cuda2.2'
TRACE_FILE['AlignedTypes@ref'] = trace_path + '/AlignedTypes/kernel_config.txt'
TRACE_FILE['SimpleTexture@ref'] = trace_path + '/SimpleTexture/kernel_config.txt'
TRACE_FILE['MersenneTwister@ref'] = trace_path + '/MersenneTwister/kernel_config.txt'
TRACE_FILE['Transpose@ref'] = trace_path + '/Transpose/kernel_config.txt'
TRACE_FILE['SobolQRNG@ref'] = trace_path + '/SobolQRNG/kernel_config.txt'
TRACE_FILE['MatrixMul@ref'] = trace_path + '/MatrixMul/kernel_config.txt'
TRACE_FILE['Template@ref'] = trace_path + '/Template/kernel_config.txt'
TRACE_FILE['BoxFilter@ref'] = trace_path + '/BoxFilter/kernel_config.txt'
TRACE_FILE['QuasirandomGenerator@ref'] = trace_path + '/QuasirandomGenerator/kernel_config.txt'
TRACE_FILE['Dct8x8@ref'] = trace_path + '/Dct8x8/kernel_config.txt'
TRACE_FILE['FastWalshTransform@ref'] = trace_path + '/FastWalshTransform/kernel_config.txt'
TRACE_FILE['CppIntegration@ref'] = trace_path + '/CppIntegration/kernel_config.txt'
TRACE_FILE['SobelFilter@ref'] = trace_path + '/SobelFilter/kernel_config.txt'
TRACE_FILE['AsyncAPI@ref'] = trace_path + '/AsyncAPI/kernel_config.txt'
TRACE_FILE['SimpleZeroCopy@ref'] = trace_path + '/SimpleZeroCopy/kernel_config.txt'
TRACE_FILE['ScalarProd@ref'] = trace_path + '/ScalarProd/kernel_config.txt'
TRACE_FILE['Eigenvalues@ref'] = trace_path + '/Eigenvalues/kernel_config.txt'
TRACE_FILE['Dxtc@ref'] = trace_path + '/Dxtc/kernel_config.txt'
TRACE_FILE['Mandelbrot@ref'] = trace_path + '/Mandelbrot/kernel_config.txt'
TRACE_FILE['BinomialOptions@ref'] = trace_path + '/BinomialOptions/kernel_config.txt'
TRACE_FILE['Nbody@ref'] = trace_path + '/Nbody/kernel_config.txt'
TRACE_FILE['SimpleStreams@ref'] = trace_path + '/SimpleStreams/kernel_config.txt'
TRACE_FILE['ImageDenoising@ref'] = trace_path + '/ImageDenoising/kernel_config.txt'
TRACE_FILE['BlackScholes@ref'] = trace_path + '/BlackScholes/kernel_config.txt'
TRACE_FILE['SimpleVoteIntrinsics@ref'] = trace_path + '/SimpleVoteIntrinsics/kernel_config.txt'
TRACE_FILE['Histogram64@ref'] = trace_path + '/Histogram64/kernel_config.txt'
TRACE_FILE['Bitonic@ref'] = trace_path + '/Bitonic/kernel_config.txt'
TRACE_FILE['DwtHaar1D@ref'] = trace_path + '/DwtHaar1D/kernel_config.txt'
TRACE_FILE['Reduction@ref'] = trace_path + '/Reduction/kernel_config.txt'
TRACE_FILE['ScanLargeArray@ref'] = trace_path + '/ScanLargeArray/kernel_config.txt'
TRACE_FILE['ConvolutionTexture@ref'] = trace_path + '/ConvolutionTexture/kernel_config.txt'
TRACE_FILE['ThreadFenceReduction@ref'] = trace_path + '/ThreadFenceReduction/kernel_config.txt'
TRACE_FILE['SimpleTemplates@ref'] = trace_path + '/SimpleTemplates/kernel_config.txt'
TRACE_FILE['Scan@ref'] = trace_path + '/Scan/kernel_config.txt'
TRACE_FILE['MonteCarlo@ref'] = trace_path + '/MonteCarlo/kernel_config.txt'
TRACE_FILE['TransposeNew@ref'] = trace_path + '/TransposeNew/kernel_config.txt'
TRACE_FILE['SimpleAtomicIntrinsics@ref'] = trace_path + '/SimpleAtomicIntrinsics/kernel_config.txt'
TRACE_FILE['BicubicTexture@ref'] = trace_path + '/BicubicTexture/kernel_config.txt'
TRACE_FILE['SimpleTexture3D@ref'] = trace_path + '/SimpleTexture3D/kernel_config.txt'
TRACE_FILE['Histogram256@ref'] = trace_path + '/Histogram256/kernel_config.txt'
TRACE_FILE['RecursiveGaussian@ref'] = trace_path + '/RecursiveGaussian/kernel_config.txt'
TRACE_FILE['ConvolutionSeparable@ref'] = trace_path + '/ConvolutionSeparable/kernel_config.txt'
TRACE_FILE['Clock@ref'] = trace_path + '/Clock/kernel_config.txt'

trace_path = '/trace/ptx/cuda3.2'
TRACE_FILE['MersenneTwister1@ref'] = trace_path + '/MersenneTwister/kernel_config.txt'
TRACE_FILE['SobolQRNG1@ref'] = trace_path + '/SobolQRNG/kernel_config.txt'
TRACE_FILE['QuasirandomGenerator1@ref'] = trace_path + '/QuasirandomGenerator/kernel_config.txt'
TRACE_FILE['Dct8x81@ref'] = trace_path + '/Dct8x8/kernel_config.txt'
TRACE_FILE['FastWalshTransform1@ref'] = trace_path + '/FastWalshTransform/kernel_config.txt'
TRACE_FILE['SobelFilter1@ref'] = trace_path + '/SobelFilter/kernel_config.txt'
TRACE_FILE['VolumeRender@ref'] = trace_path + '/VolumeRender/kernel_config.txt'
TRACE_FILE['ScalarProd1@ref'] = trace_path + '/ScalarProd/kernel_config.txt'
TRACE_FILE['Eigenvalues1@ref'] = trace_path + '/Eigenvalues/kernel_config.txt'
TRACE_FILE['Dxtc1@ref'] = trace_path + '/Dxtc/kernel_config.txt'
TRACE_FILE['BinomialOptions1@ref'] = trace_path + '/BinomialOptions/kernel_config.txt'
TRACE_FILE['Nbody1@ref'] = trace_path + '/Nbody/kernel_config.txt'
TRACE_FILE['ImageDenoising1@ref'] = trace_path + '/ImageDenoising/kernel_config.txt'
TRACE_FILE['BlackScholes1@ref'] = trace_path + '/BlackScholes/kernel_config.txt'
TRACE_FILE['DwtHaar1D@ref'] = trace_path + '/DwtHaar1D/kernel_config.txt'
TRACE_FILE['Reduction1@ref'] = trace_path + '/Reduction/kernel_config.txt'
TRACE_FILE['ConvolutionTexture1@ref'] = trace_path + '/ConvolutionTexture/kernel_config.txt'
TRACE_FILE['VectorAdd@ref'] = trace_path + '/VectorAdd/kernel_config.txt'
TRACE_FILE['Scan@ref'] = trace_path + '/Scan/kernel_config.txt'
TRACE_FILE['MonteCarlo1@ref'] = trace_path + '/MonteCarlo/kernel_config.txt'
TRACE_FILE['MergeSort@ref'] = trace_path + '/MergeSort/kernel_config.txt'
TRACE_FILE['RecursiveGaussian1@ref'] = trace_path + '/RecursiveGaussian/kernel_config.txt'
TRACE_FILE['ConvolutionSeparable1@ref'] = trace_path + '/ConvolutionSeparable/kernel_config.txt'
TRACE_FILE['Histogram641@ref'] = trace_path + '/Histogram/kernel_config64.txt'
TRACE_FILE['Histogram2561@ref'] = trace_path + '/Histogram/kernel_config256.txt'

trace_path = '/trace/ptx/rodinia'
TRACE_FILE['backPropagation@ref'] = trace_path + '/backPropagation/kernel_config.txt'
TRACE_FILE['cell@ref'] = trace_path + '/cell/kernel_config.txt'
TRACE_FILE['gaussian@ref'] = trace_path + '/gaussian/kernel_config.txt'
TRACE_FILE['hotspot@ref'] = trace_path + '/hotspot/kernel_config.txt'
TRACE_FILE['cfd@ref'] = trace_path + '/cfd/kernel_config.txt'
TRACE_FILE['heartwall@ref'] = trace_path + '/heartwall/kernel_config.txt'
TRACE_FILE['nearest-neighbor@ref'] = trace_path + '/nearest_neighbor/kernel_config.txt'
TRACE_FILE['dynproc@ref'] = trace_path + '/dynproc/kernel_config.txt'
TRACE_FILE['lud@ref'] = trace_path + '/lud/kernel_config.txt'
TRACE_FILE['bfs@ref'] = trace_path + '/bfs/kernel_config.txt'
TRACE_FILE['needlemanWunsch@ref'] = trace_path + '/needlemanWunsch/kernel_config.txt'

trace_path = '/trace/ptx/erc'
TRACE_FILE['RadixSort3@ref'] = trace_path + '/RadixSort3/kernel_config.txt'
TRACE_FILE['RayTracing@ref'] = trace_path + '/RayTracing/kernel_config.txt'
TRACE_FILE['SAD@ref'] = trace_path + '/SAD/kernel_config.txt'
TRACE_FILE['SHA1@ref'] = trace_path + '/SHA1/kernel_config.txt'
TRACE_FILE['AES@ref'] = trace_path + '/AES/kernel_config.txt'

trace_path = '/trace/ptx/parboil'
TRACE_FILE['lbm-gpu@ref'] = trace_path + '/lbm/kernel_config.txt'
TRACE_FILE['fft@ref'] = trace_path + '/fft/kernel_config.txt'
TRACE_FILE['mm@ref'] = trace_path + '/mm/kernel_config.txt'
TRACE_FILE['sad@ref'] = trace_path + '/sad/kernel_config.txt'
TRACE_FILE['stencil@ref'] = trace_path + '/stencil/kernel_config.txt'
TRACE_FILE['histo@ref'] = trace_path + '/histo/kernel_config.txt'
TRACE_FILE['tpacf@ref'] = trace_path + '/tpacf/kernel_config.txt'
TRACE_FILE['bfs1@ref'] = trace_path + '/bfs/kernel_config.txt'




trace_path = '/trace/1/merge/serial'
TRACE_FILE['merge-binomial@ref'] = trace_path + '/binomial/trace.txt'
TRACE_FILE['merge-convolve@ref'] = trace_path + '/convolve/trace.txt'
TRACE_FILE['merge-linear@ref']   = trace_path + '/linear/trace.txt'
TRACE_FILE['merge-sepia@ref']    = trace_path + '/sepia/trace.txt'
TRACE_FILE['merge-svm@ref']      = trace_path + '/svm/trace.txt'
TRACE_FILE['merge-fmm@ref']      = trace_path + '/fmm/trace.txt'

### NaCL TRACE 
trace_path = '/home/jlee663/NaCLWork/spec2006/trace_simpoint'
TRACE_FILE['bzip2-1@nacl'] = trace_path + '/bzip2/pin_traces_all/bzip2.1.txt' 
TRACE_FILE['bzip2-2@nacl'] = trace_path + '/bzip2/pin_traces_all/bzip2.2.txt' 
TRACE_FILE['bzip2-3@nacl'] = trace_path + '/bzip2/pin_traces_all/bzip2.3.txt' 
TRACE_FILE['bzip2-4@nacl'] = trace_path + '/bzip2/pin_traces_all/bzip2.4.txt' 
TRACE_FILE['bzip2-5@nacl'] = trace_path + '/bzip2/pin_traces_all/bzip2.5.txt' 
TRACE_FILE['bzip2-6@nacl'] = trace_path + '/bzip2/pin_traces_all/bzip2.6.txt' 
TRACE_FILE['gobmk@nacl'] = trace_path + '/gobmk/pin_traces_all/gobmk.1.txt' 
TRACE_FILE['gobmk-2@nacl'] = trace_path + '/gobmk/pin_traces_all/gobmk.2.txt' 
TRACE_FILE['gobmk-3@nacl'] = trace_path + '/gobmk/pin_traces_all/gobmk.3.txt' 
TRACE_FILE['gobmk-4@nacl'] = trace_path + '/gobmk/pin_traces_all/gobmk.4.txt' 
TRACE_FILE['gobmk-5@nacl'] = trace_path + '/gobmk/pin_traces_all/gobmk.5.txt' 
TRACE_FILE['lbm@nacl'] = trace_path + '/lbm/pin_traces_all/lbm.1.txt' 
TRACE_FILE['libquantum@nacl'] = trace_path + '/libquantum/pin_traces_all/libquantum.1.txt' 
TRACE_FILE['mcf@nacl'] = trace_path + '/mcf/pin_traces_all/mcf.1.txt' 
TRACE_FILE['milc@nacl'] = trace_path + '/milc/pin_traces_all/milc.1.txt' 
TRACE_FILE['namd@nacl'] = trace_path + '/namd/pin_traces_all/namd.1.txt' 
TRACE_FILE['specrand@nacl'] = trace_path + '/specrand/pin_traces_all/specrand.1.txt' 


if __name__ == '__main__':
  if sys.argv[1] in TRACE_FILE:
    print(TRACE_FILE[sys.argv[1]])
  else:
    print('TRACE_FILE[%s] does not exist' % sys.argv[1])



