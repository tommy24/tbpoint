#!/usr/bin/perl -w
# *************************************************************************************
# CVS          : $Id: graph_data,v 1.2 2008-08-21 02:12:38 hyesoon Exp $:
# *************************************************************************************

die "You need to set the SIMDIR environment variable to the scarab base directory.\n" unless $ENV{'SIMDIR'};
require ($ENV{'SIMDIR'} . "/tools/bench_common");


# ************************************************************************************
# Program usage 
# ************************************************************************************

$USAGE = "Usage: '$0 [ options ]'";

if (@ARGV && ($ARGV[0] eq "-h" || $ARGV[0] eq "-help")) {
    print(STDERR "$USAGE\n");
    print(STDERR "\n");

    print(STDERR "All '-' prefixed arguments are passed straight to get_data.\n");
    print(STDERR "\n");

    print(STDERR "graph_data options:\n");

    print(STDERR "\t+stdin       -- read input from standard input instead of calling get_data\n");
    print(STDERR "\t+ps          -- produce postscript output (instead of jgraph)\n");
    print(STDERR "\t+lpr         -- make postscript output printable\n");
    print(STDERR "\t+outfile     -- send output to a file\n");
    print(STDERR "\t+replace     -- replace output file, if it exists\n");
    print(STDERR "\t+autoview    -- automatically display graph (recommended)\n");

    print(STDERR "\t+title       -- specify the title (make sure to quote it)\n");
    print(STDERR "\t+renames     -- semicolon-separated list of names that map to experiments in order\n");
    print(STDERR "\t+xsize       -- specify x-axis size\n");
    print(STDERR "\t+ysize       -- specify y-axis size\n");
    print(STDERR "\t+xmax        -- specify x-axis maximum value\n");
    print(STDERR "\t+xmin         -- specify x-axis minimum value\n");
    print(STDERR "\t+ymin        -- specify y-axis minimum value\n");
    print(STDERR "\t+ymax        -- specify y-axis maximum value\n");
    print(STDERR "\t+yhash       -- specify y-axis hash value (passed to jgraph)\n");
    print(STDERR "\t+ylabel      -- specify y-axis label\n");
    print(STDERR "\t+lowleg      -- put legend on the bottom left of the graph\n");
    print(STDERR "\t+noleg       -- do not show legend\n");
    print(STDERR "\t+textblock   -- semicolor-separated list of strings to be put in the bottom right\n");
    
    print(STDERR "\t+yprec       -- specify y-axis label precision\n");
    print(STDERR "\t+mhash       -- specify minor hash interval\n");

    print(STDERR "\t+stack <num> -- build a stacked-bar graph with <num> rows per bar\n");
    print(STDERR "\t+noadd       -- a stacked-bar graph made from increasing data values instead of adding them together\n");
    print(STDERR "\t+slabel      -- use stack labeling for bars (assume all stack have same labels)\n");
    print(STDERR "\t+scolor      -- use stack coloring for bars (each stack uses same colors)\n");

    print(STDERR "\t+bw          -- generate a black-and-white graph\n");
    print(STDERR "\t+coffset     -- offset used to start color mapping (to coordinate between graphs)\n");
    print(STDERR "\t+colorstep   -- used for tweaking color mapping of bars\n");

    print(STDERR "\t+norotate    -- do not rotate the bar graph labels\n");
    print(STDERR "\t+xlabelfont  -- x-axis label font size \n");
    print(STDERR "\t+ylabelfont  -- y-axis label font size \n");
    print(STDERR "\t+xhashfont   -- x hash labels font size \n");
    print(STDERR "\t+yhashfont   -- y hash labels font size \n");
    print(STDERR "\t+hashfont    -- hash labels font size (both x and y)\n");
    print(STDERR "\t+legendfont  -- legend label font size \n");
    print(STDERR "\t+legpos      -- position of legend [x] [y] \n");
    print(STDERR "\t+legbox      -- legend box [size x] [size y] [pos x] [pos y] \n");
    
    print(STDERR "\t+xaxisdraw   -- x-axis draw at command \n");
    print(STDERR "\t+boxbar      -- draw a box in a bar graph \n");
    
    print(STDERR "\t+lineplot    -- draw a line graph \n");
    print(STDERR "\t+xscale      -- first column is the x-axis (lineplot) \n");
    print(STDERR "\t+x_renames   -- rename x-axis (lineplot) \n");
    print(STDERR "\t+xyswitch   -- switch x-axis and y-axis (lineplot) \n");
    print(STDERR "\t #           -- comments (ignore the line) \n");

    exit(1);
}


# ************************************************************************************
# Variables
# ************************************************************************************

@GET_DATA_ARGV	= ();
$STD_INPUT	= 0;
$PS_OUTPUT	= 0;
$LPR_OUTPUT	= 0;
$OUTFILE	= "";
$REPLACE	= 0;
$AUTO_VIEW	= 0;

$TITLE		= "";
@RENAMES	= ();
@X_RENAMES	= ();
$NAME_2K        = 0;
$X_AXIS_DRAW    = 0; 
$PRINT_DRAW_AT  = 0;
$X_MIN		= 0;
$X_MIN_SET      = 0; 
$X_MAX		= 100;
$X_SIZE		= 8.0;
$Y_SIZE		= 4.0;
$Y_LABEL	= "Instructions per Cycle";
$LOWER_LEGEND   = 0;
$NO_LEGEND      = 0;
@TEXT_BLOCK     = ();
$PRINT_DELTA	= 0;
$BOXBAR         = 0;

$STACK		= 1;
$NOADD_STACK	= 0;
$STACK_LABELING	= 0;
$STACK_COLORING	= 0;

$USE_COLOR	= 1;
$COLOR_STEP	= .15;

$NO_ROTATE      = 0;
$USE_PATTERN    = 0; 
$LEGEND_FONT_SIZE = 14;
$Y_LABEL_FONT_SIZE = 12;
$XHASH_LABEL_FONT_SIZE = 12; 
$YHASH_LABEL_FONT_SIZE = 12; 
@g_benches	= ();
@g_expts	= ();
@g_lines	= ();
$g_y_max	= 0;
$g_y_min	= 0;
@g_color_array	= ();
@g_bw_array	= ();
@g_pattern_array = (); 

$LINEPLOT       = 0;
$XSCALE         = 0; 
$XYSWITCH       = 0; 
$LEG_POS_SET    = 0;
$LEG_POS_X      = 0;
$LEG_POS_Y      = 0;
$LEG_BOX_SET    = 0; 
$LEG_BOX_SIZE_X = 0; 
$LEG_BOX_SIZE_Y = 0;
$LEG_BOX_POS_X  = 0; 
$LEG_BOX_POS_Y  = 0; 

@x_pos_array    = ();
@g_line_array   = ('solid', 'dotted','dashed','longdash','dotdash', 'dotdotdash', 'dotdotdashdash');
@g_mark_array   = ('box', 'box','circle', 'circle','triangle', 'triangle', 'x', 'cross');
@x_label_data   = ();
@y_label_data   = ();

# ************************************************************************************
# Parse command line
# ************************************************************************************

while (@ARGV) {
    $option = shift;

    if ($option eq "+stdin") {
	$STD_INPUT = 1;
    } elsif ($option eq "+ps") {
	$PS_OUTPUT = 1;
    } elsif ($option eq "+lpr") {
	$LPR_OUTPUT = 1;
    } elsif ($option eq "+outfile") {
	die "Option '$option' requires an argument.\n" unless @ARGV;
	$OUTFILE = shift;
    } elsif ($option eq "+replace") {
	$REPLACE = 1;
    } elsif ($option eq "+autoview") {
	$AUTO_VIEW = 1;
    } elsif ($option eq "+title") {
	die "Option '$option' requires an argument.\n" unless @ARGV;
	$TITLE = shift;
    } elsif ($option eq "+renames") {
	die "Option '$option' requires an argument.\n" unless @ARGV;
	my $line = shift;
	@RENAMES = split(/\s*;\s*/, $line);
    } elsif ($option eq "+x_renames") {
	die "Option '$option' requires an argument.\n" unless @ARGV;
	my $line = shift;
	@X_RENAMES = split(/\s*;\s*/, $line);
    } elsif ($option eq "+2k") {
	$NAME_2K = 1;
    } elsif ($option eq "+xsize") {
	die "Option '$option' requires an argument.\n" unless @ARGV;
	$X_SIZE = shift;
    } elsif ($option eq "+ysize") {
	die "Option '$option' requires an argument.\n" unless @ARGV;
	$Y_SIZE = shift;
    } elsif ($option eq "+xmax") {
	die "Option '$option' requires an argument.\n" unless @ARGV;
	$X_MAX = shift;
    } elsif ($option eq "+xmin") {
	die "Option '$option' requires an argument.\n" unless @ARGV;
	$X_MIN = shift;
	$X_MIN_SET = 1; 
    } elsif ($option eq "+ymin") {
	die "Option '$option' requires an argument.\n" unless @ARGV;
	$Y_MIN = shift;
    } elsif ($option eq "+ymax") {
	die "Option '$option' requires an argument.\n" unless @ARGV;
	$Y_MAX = shift;
    } elsif ($option eq "+yhash") {
	die "Option '$option' requires an argument.\n" unless @ARGV;
	$Y_HASH = shift;
    } elsif ($option eq "+yprec") {
	die "Option '$option' requires an argument.\n" unless @ARGV;
	$Y_PREC = shift;
    } elsif ($option eq "+mhash") {
	die "Option '$option' requires an argument.\n" unless @ARGV;
	$MHASH  = shift;
    } elsif ($option eq "+ylabel") {
	die "Option '$option' requires an argument.\n" unless @ARGV;
	$Y_LABEL = shift;	
    } elsif ($option eq "+xlabelfont") {
	die "Option '$option' requires an argument.\n" unless @ARGV;
	$X_LABEL_FONT = shift;
    } elsif ($option eq "+ylabelfont") {
	die "Option '$option' requires an argument.\n" unless @ARGV;
	$Y_LABEL_FONT_SIZE = shift;
    } elsif ($option eq "+hashfont" ) {
	die "Option '$option' requires an argument.\n" unless @ARGV;
	$XHASH_LABEL_FONT_SIZE = shift;
	$YHASH_LABEL_FONT_SIZE = $XHASH_LABEL_FONT_SIZE;
    } elsif ($option eq "+xhashfont") { 
	die "Option '$option' requires an argument.\n" unless @ARGV;
	$XHASH_LABEL_FONT_SIZE = shift;
    } elsif ($option eq "+yhashfont") {
	die "Option '$option' requires an argument.\n" unless @ARGV;
	$YHASH_LABEL_FONT_SIZE = shift;
    } elsif ($option eq "+legendfont") {
	die "Option '$option' requires an argument.\n" unless @ARGV;
	$LEGEND_FONT_SIZE = shift;
    } elsif ($option eq "+lowleg") {
	$LOWER_LEGEND = 1;
    } elsif ($option eq "+noleg") {
	$NO_LEGEND = 1;	
    } elsif ($option eq "+legpos") {
	$LEG_POS_SET = 1;
	die "Option '$option' requires an argument.\n" unless @ARGV;
	$LEG_POS_X = shift;
	die "Option '$option' requires an argument.\n" unless @ARGV;
	$LEG_POS_Y = shift; 
    } elsif ($option eq "+legbox") {
	$LEG_BOX_SET  = 1; 
	die "Option '$option' requires an argument.\n" unless @ARGV;
	$LEG_BOX_SIZE_X = shift;
	die "Option '$option' requires an argument.\n" unless @ARGV;
	$LEG_BOX_SIZE_Y = shift; 
	die "Option '$option' requires an argument.\n" unless @ARGV;
	$LEG_BOX_POS_X = shift;
	die "Option '$option' requires an argument.\n" unless @ARGV;
	$LEG_BOX_POS_Y = shift; 
    } elsif ($option eq "+textblock") {
	die "Option '$option' requires an argument.\n" unless @ARGV;
	my $line = shift;
	@TEXT_BLOCK = split(/\s*;\s*/, $line);
    } elsif ($option eq "+stack") {
	die "Option '$option' requires an argument.\n" unless @ARGV;
	$STACK = shift;
    } elsif ($option eq "+noadd") {
	$NOADD_STACK = 1;
    } elsif ($option eq "+slabel") {
	$STACK_LABELING = 1;
    } elsif ($option eq "+scolor") {
	$STACK_COLORING = 1;
    } elsif ($option eq "+bw") {
	$USE_COLOR = 0;
    } elsif ($option eq "+coffset") {
	die "Option '$option' requires an argument.\n" unless @ARGV;
	$COLOR_OFFSET = shift;
    } elsif ($option eq "+pattern") {
	$USE_PATTERN = 1; 
    } elsif ($option eq "+colorstep") {
	die "Option '$option' requires an argument.\n" unless @ARGV;
	$COLOR_STEP = shift;
	die "Invalid color step.\n" unless ($COLOR_STEP > 0 && $COLOR_STEP < .5);
    } elsif ($option eq "+norotate") {
	$NO_ROTATE = 1;
    } elsif ($option eq "+xaxisdraw") {
	$X_AXIS_DRAW = shift; 
	$PRINT_DRAW_AT = 1;
    } elsif ($option eq "+boxbar") {
	$BOXBAR    = 1; 
    } elsif ($option eq "+lineplot") {
	$LINEPLOT  = 1; 
    } elsif ($option eq "+xscale") {
	$XSCALE  = 1; 
    } elsif ($option eq "+xyswitch") {
	$XYSWITCH       = 1; 
    } elsif ($option =~ /^\+.*/) {
	die("Unrecognized option '$option'.\n");
    } elsif ($option eq "-pivot" || $option eq "-base") {
	$PRINT_DELTA = 1;
	push(@GET_DATA_ARGV, $option);
    } else {
	push(@GET_DATA_ARGV, $option);
    }
}

# param checking
if ($AUTO_VIEW) {
    if ($PS_OUTPUT || $LPR_OUTPUT) {
	print(STDERR "Warning: +autoview forces +ps on and +lpr off.\n");
    }
    $PS_OUTPUT  = 1;
    $LPR_OUTPUT = 0;
}


# ************************************************************************************
# Initialize  
# ************************************************************************************

init_colors();
if ($STD_INPUT) {
    open(INPUT, "-") || die;
} else {
    $cmd = "$ENV{SIMDIR}/bin/get_data " . join(" ", @GET_DATA_ARGV) . " -xxxx -widenames -nocolor";
    open(INPUT, "$cmd |") || die;
}

if ($OUTFILE) {
    die "Output file '$OUTFILE' already exists.\n" if (-f "$OUTFILE") && !$REPLACE;

    if ($PS_OUTPUT) {
	$jgraph_options = $LPR_OUTPUT ? "-P -L" : "";
	open(OUTPUT, "| jgraph $jgraph_options > $OUTFILE") || die;
    } else {
	open(OUTPUT, ">$OUTFILE") || die;
    }
} else {
    if ($AUTO_VIEW) {
	$tmp_file = ".graph_data.$$.ps";
	die "Temp output file '$tmp_file' already exists.\n" if -f "$tmp_file";
	open(OUTPUT, "| jgraph > $tmp_file") || die;
    } else {
	if ($PS_OUTPUT) {
	    $jgraph_options = $LPR_OUTPUT ? "-P -L" : "";
	    open(OUTPUT, "| jgraph $jgraph_options") || die;
	} else {
	    open(OUTPUT, ">-") || die;
	}
    }	    
}


# ************************************************************************************
# Process  
# ************************************************************************************

while ($line = <INPUT>) {
    chomp($line);
    my $suite = lc((split(/[ \t]+/, $line))[0]);

    die unless defined %SUITES; # stupid use to get rid of warning

    if (grep($suite eq $_, keys %SUITES, "custom")) {
	# start of suite
	my @line_split = split(/[ \t]+/, $line);
	my $ipc = (@line_split == 1) ? 1 : 0;
	if ($ipc && !$PRINT_DELTA) {
	    $Y_MIN  = 0 unless defined $Y_MIN;
	    $Y_HASH = 2 unless defined $Y_HASH ;
	    print_bar_graph();
	} elsif (!$ipc && $PRINT_DELTA) {
	    print_bar_graph();
	} 
    } 
}

close(INPUT);
close(OUTPUT);

if ($AUTO_VIEW) {
    if ($OUTFILE) {
	system("gv $OUTFILE\n");
    } else {
	system("gv $tmp_file\n");
	system("rm $tmp_file\n");
    }
}
     

# ************************************************************************************
# init_colors:

sub init_colors {
    my ($ii, $jj, $kk, $ll, $clr);

    # color array
    for ($ii = 0 ; $ii <= 1 ; $ii += $COLOR_STEP) {
  	for ($jj = 0 ; $jj <= 1 ; $jj += $COLOR_STEP) {
  	    for ($kk = 0 ; $kk <= 1 ; $kk += $COLOR_STEP) {
		for ($ll = 0 ; $ll <= 7 ; $ll++) {
		    @clr = ($ii, $jj, $kk);
		    if ($ll & 0x1) {
			$clr[0] += .5;
			$clr[0] -= 1 if $clr[0] > 1;
		    }
		    if ($ll & 0x2) {
			$clr[1] += .5;
			$clr[1] -= 1 if $clr[1] > 1;
		    }
		    if ($ll & 0x4) {
			$clr[2] += .5;
			$clr[2] -= 1 if $clr[2] > 1;
		    }
#   		    print("color " . join(" ", @clr), " \n");
		    push(@g_color_array, "color " . join(" ", @clr));
		}
  	    }
  	}
    }

    # black and white array
    for ($ii = $COLOR_STEP ; $ii <= 4 ; $ii += $COLOR_STEP) {
	if ($BOXBAR) {
	    push(@g_bw_array, "gray 0 fill $ii");
	    $jj = $ii + .5 - $COLOR_STEP;
	    $jj -= 1 if $jj > 1;
	    push(@g_bw_array, "gray 0 fill $jj");
	}
	else {
	    push(@g_bw_array, "gray $ii");
	    $jj = $ii + .5 - $COLOR_STEP;
	    $jj -= 1 if $jj > 1;
	    push(@g_bw_array, "gray $jj");
	}
    }

    for ($ii = 0 ; $ii <4 ; $ii++) {
	 $tmp_val = 135 - ($ii) *45;  
	# tt2  
	# $tmp_val = 180 - ($ii/2) *45;  
	# tt3 
	# $tmp_val = ($ii/2) *45;  
	if ($ii == 0 || $ii == 2 || $ii == 4 ) {
	    push(@g_pattern_array,  "pattern stripe $tmp_val ");
	}
	else {
	    push (@g_pattern_array, " ");
	}
    }

}


# ************************************************************************************
# print_bar_graph

sub print_bar_graph {
    $head_line = <INPUT>;
    chomp($head_line);
    @g_benches = split(/[ \t]+/, $head_line);
    die if (@g_benches <= 2);
    @g_benches = @g_benches[2..$#g_benches];
    $expt_count = 0;
    @g_lines = ();

    <INPUT> if $PRINT_DELTA;	# throw away baseline
    while (($line = <INPUT>) ne "\n") {
	chomp($line);
  	#   print("line:'$line'\n");
	@line_split = split(/[ \t]+/, $line);
	next if ($line_split[0]=~'#'); 
	push(@g_expts, $line_split[0]);
	@line_split = @line_split[2..$#line_split];
	push(@g_lines, join(" ", @line_split));
	die @line_split . " " . @g_benches if (@line_split!= @g_benches);
	$expt_count++;
    }

    die "Experiment count must be a multiple of -stack value.\n" unless $expt_count % $STACK == 0;

    for ($ii = 0 ; $ii < $expt_count ; $ii += $STACK) {
	my @vals = split(" ", $g_lines[$ii]);

	for ($jj = 0 ; $jj < @vals ; $jj++) {
	    if ($vals[$jj] !~ /^[x]+$/) {
		$col_min = $vals[$jj] if (!(defined $col_min) || $vals[$jj] < $col_min);
	    }
	}

	for ($jj = $ii + 1 ; $jj < $ii + $STACK ; $jj++) {
	    my @lvals = split(" ", $g_lines[$jj]);

	    for ($kk = 0 ; $kk < @lvals ; $kk++) {
		if ($lvals[$kk] !~ /^[x]+$/) {
		    if ($NOADD_STACK) {
			$vals[$kk] = $lvals[$kk];
		    } else {
			$vals[$kk] = $vals[$kk] + $lvals[$kk];
		    }
		} 
	    }
	}

	for ($jj = 0 ; $jj < @vals ; $jj++) {
	    if ($vals[$jj] !~ /^[x]+$/) {
 		$col_max = $vals[$jj] if (!(defined $col_max) || $vals[$jj] > $col_max);
	    }
	}
    }
    $col_max = 0 if (!(defined $col_max));
    $col_min = 0 if (!(defined $col_min));
#     print("col_max:$col_max  col_min:$col_min\n");

    # set Y axis min and max 10% beyond what was seen
    $g_y_min = $col_min - .10 * abs($col_min); 
    $g_y_max = $col_max + .10 * abs($col_max);
    
    if ($LINEPLOT) {
	emit_bar_graph_head();
	emit_line_graph_curves();
	emit_line_graph_hashes();
    }
    else { 
	emit_bar_graph_head();
	emit_bar_graph_curves();
	emit_bar_graph_hashes();
    }

    emit_leg_box() if ($LEG_BOX_SET);
    
}


# ************************************************************************************
# emit_graph_head:

sub emit_bar_graph_head {
    my $y_hash     = defined $Y_HASH ? $Y_HASH : ($g_y_max - $g_y_min) * .2;
    my $y_axis_min = defined $Y_MIN ? $Y_MIN : $g_y_min;
    my $y_axis_max = defined $Y_MAX ? $Y_MAX : $g_y_max;
    my $y_prec     = defined $Y_PREC ? $Y_PREC : 1;
    my $mhash      = defined $MHASH   ? $MHASH  : 0; 
    my $expt_width = 1.0 * $X_MAX / ($expt_count / $STACK * @g_benches + @g_benches);
    my $y_label    = $PRINT_DELTA ? "IPC Delta (%)" : $Y_LABEL;
    my $title_loc  = $y_axis_max + ($y_axis_max - $y_axis_min) * .075;
    my $legend_x   = $X_MAX + .03 * $X_MAX + .5 * $expt_width;
    my $legend_y   = $y_axis_max - .05 * ($y_axis_max - $y_axis_min);
    my $legend_onoff = $NO_LEGEND ? "off" : "on";

    if ($LOWER_LEGEND) {
	$legend_x = $X_MIN;
	$legend_y = $y_axis_min - .15 * ($y_axis_max - $y_axis_min);
    }
    if ($LEG_POS_SET) {
	$legend_x = $LEG_POS_X;
	$legend_y = $LEG_POS_Y;
    }
    

    print(OUTPUT "newgraph\n");
    print(OUTPUT "title y $title_loc fontsize 16 : $TITLE\n") if $TITLE;
    print(OUTPUT "legend $legend_onoff defaults x $legend_x y $legend_y hjl vjt fontsize $LEGEND_FONT_SIZE\n");
    print(OUTPUT "\n");
    emit_leg_box() if ($LEG_BOX_SET);
    print(OUTPUT "xaxis min $X_MIN max $X_MAX\n");
    print(OUTPUT "xaxis no_auto_hash_marks hash_labels fontsize $XHASH_LABEL_FONT_SIZE label fontsize 14 \n");
    print(OUTPUT "xaxis size $X_SIZE\n");
    print(OUTPUT "xaxis draw_at $X_AXIS_DRAW\n") if ($PRINT_DELTA || $PRINT_DRAW_AT);
    print(OUTPUT "\n");
    print(OUTPUT "yaxis min $y_axis_min max $y_axis_max\n");
    print(OUTPUT "yaxis hash $y_hash mhash $mhash hash_labels fontsize $YHASH_LABEL_FONT_SIZE label fontsize $Y_LABEL_FONT_SIZE : $y_label\n");
    print(OUTPUT "yaxis size $Y_SIZE\n");
    print(OUTPUT "yaxis precision $y_prec\n");
    print(OUTPUT "yaxis shash 0\n");
    print(OUTPUT "yaxis grid_lines \n");
    print(OUTPUT "\n");

    if (@TEXT_BLOCK) {
	my $textblock_x = $LOWER_LEGEND ? ($X_MAX - $X_MIN) * .7 : $legend_x ;
	my $textblock_y = $LOWER_LEGEND ? $legend_y : $y_axis_min; 
	my $vj = $LOWER_LEGEND ? "vjt" : "vjb";

	print(OUTPUT "newstring x $textblock_x y $textblock_y hjl $vj fontsize 14 : ");
	foreach (0..$#TEXT_BLOCK - 1) {
	    print(OUTPUT "$TEXT_BLOCK[$_]\\\n");
	}
	print(OUTPUT "$TEXT_BLOCK[$#TEXT_BLOCK]\n\n");
    }
}


# ************************************************************************************
# emit_graph_curves

sub emit_bar_graph_hashes {
    my ($y_axis_min)  = defined $Y_MIN ? $Y_MIN : $g_y_min;
    my ($y_axis_max)  = defined $Y_MAX ? $Y_MAX : $g_y_max;
    my ($y_label_pos) = $y_axis_min - ($y_axis_max - $y_axis_min) * .075;
    my ($expt_width)  = 1.0 * $X_MAX / ($expt_count / $STACK * @g_benches + @g_benches);
    my ($hash)	      = .5 * $expt_width + $expt_count / $STACK * $expt_width / 2.0;
    my ($xlabelfont)   = defined $X_LABEL_FONT ? $X_LABEL_FONT : 0; 

#      print("expt_width: $expt_width\n");
#      print("expt_count: $expt_count\n");

    foreach (@g_benches) {
	my $name = (split(/@/, $_))[0];
	if ($NAME_2K) {
	    $name =~ s/\_00$/_2k/;
	} else {
	    $name =~ s/\_00$//;
	}

  	printf(OUTPUT "xaxis hash_at %.5f\n", $hash, $hash);
	if ($xlabelfont) {
	    if ($NO_ROTATE) {
		printf(OUTPUT "newstring fontsize %d hjc x %.5f y %.5f : %s\n",
		       $xlabelfont, $hash, $y_label_pos, $name);
	    } else {
		printf(OUTPUT "newstring fontsize %d hjc x %.5f y %.5f rotate 45 : %s\n",
		       $xlabelfont, $hash, $y_label_pos, $name);
	    }
	}
	else {
	    if ($NO_ROTATE) {
		printf(OUTPUT "newstring hjc x %.5f y %.5f : %s\n",
		       $hash, $y_label_pos, $name);
	    } else {
		printf(OUTPUT "newstring hjc x %.5f y %.5f rotate 45 : %s\n",
		       $hash, $y_label_pos, $name);
	    }
	}
	
#  	printf(OUTPUT "xaxis hash_at %.5f hash_label at %.5f : ", $hash, $hash);
#  	printf(OUTPUT "%s\n", (split(/@|\_00/, $_))[0]);
	$hash += $expt_width * $expt_count / $STACK + $expt_width;
    }
    print(OUTPUT "\n");
}


# ************************************************************************************
# emit_graph_curves:

sub emit_bar_graph_curves {
    my $blank_val   = "0.0";
    my $expt_width  = 1.0 * $X_MAX / ($expt_count / $STACK * @g_benches + @g_benches);
    my $xdim	    = 0;
    my $start_color = defined($COLOR_OFFSET) ? $COLOR_OFFSET : 0;
    my $bar_num     = 0;

    for ($ii = 0 ; $ii < $expt_count ; $ii += $STACK) {
	my @vals = ();

	for ($jj = $ii ; $jj < $ii + $STACK ; $jj++) {
	    my @lvals = split(" ", $g_lines[$jj]);

	    if (@vals) {
		for ($kk = 0 ; $kk < @lvals ; $kk++) {
		    if ($lvals[$kk] =~ /^[x]+$/) {
			$vals[$kk] = $lvals[$kk];
		    } elsif ($vals[$kk] !~ /^[x]+$/) {
			if ($NOADD_STACK) {
			    print("Warning: values within stack $kk are not increasing.\n")
				unless $lvals[$kk] >= $vals[$kk];
			    $vals[$kk] = $lvals[$kk];
			} else {
			    $vals[$kk] += $lvals[$kk];
			}
		    }
		}
	    } else {
		@vals = @lvals;
	    }
	}

	for ($jj = $ii + $STACK - 1 ; $jj >= $ii ; $jj--) {
	    # this next line handles single value stacks by skipping the extra copies
# 	    next if ($jj != $ii && $g_expts[$jj] eq $g_expts[$ii]);		    

	    my $first	  = ($STACK_LABELING && $ii != 0) ? 0 : 1;
	    my $xdim	  = $expt_width + $ii / $STACK * $expt_width;
	    my $shade_num = ($STACK_COLORING && $STACK != 1) ? $start_color + $jj - $ii : $start_color + $bar_num;
	    my $color_str = $g_color_array[$shade_num % @g_color_array];
	    my $bw_str	  = $g_bw_array[$shade_num % @g_bw_array];
	    my $pattern_num = $g_pattern_array[$shade_num %@g_pattern_array]; 

	    foreach (@vals) {
		printf(OUTPUT "newcurve pts %.5f ", $xdim);
		printf(OUTPUT !/^[x]+$/ ? "$_" : "$blank_val");
		printf(OUTPUT " marktype xbar marksize %.5f ", $expt_width);
		print(OUTPUT "$color_str ") if ($USE_COLOR);
		print(OUTPUT "$bw_str ")    if (!$USE_COLOR);

		# print(OUTPUT " pattern stripe ")  if($USE_PATTERN);
		print(OUTPUT  "$pattern_num ")  if($USE_PATTERN);

		printf(OUTPUT "label : %s", defined($RENAMES[$jj]) ? $RENAMES[$jj] : $g_expts[$jj]) if ($first);
		print(OUTPUT "\n");
		$first = 0;
		$xdim += $expt_count / $STACK * $expt_width + $expt_width;
	    }

	    if ($NOADD_STACK) {
		my @lvals = split(" ", $g_lines[$jj - 1]);

		for ($kk = 0 ; $kk < @lvals ; $kk++) {
		    if ($vals[$kk] !~ /^[x]+$/) {
			$vals[$kk] = $lvals[$kk];
		    }
		}
	    } else {
		my @lvals = split(" ", $g_lines[$jj]);

		for ($kk = 0 ; $kk < @lvals ; $kk++) {
		    if ($vals[$kk] !~ /^[x]+$/) {
			$vals[$kk] -= $lvals[$kk];
		    }
		}
	    }
	    $bar_num++;
	}
    }
}

sub  emit_line_graph_curves() {
    my $blank_val   = "0.0";
    my $expt_width  = 1.0 * $X_MAX / ($expt_count / $STACK * @g_benches + @g_benches);
    my $xdim	    = 0;
    my $start_color = defined($COLOR_OFFSET) ? $COLOR_OFFSET : 0;
    my $bar_num     = 0;
    my $row_count   = 0; 
    my $column_count = 0; 
    
    for ($ii = 0 ; $ii < $expt_count ; $ii += $STACK) {
	my @vals = ();

	for ($jj = $ii ; $jj < $ii + $STACK ; $jj++) {
	    my @lvals = split(" ", $g_lines[$jj]);

	    if (@vals) {
		for ($kk = 0 ; $kk < @lvals ; $kk++) {
		    if ($lvals[$kk] =~ /^[x]+$/) {
			$vals[$kk] = $lvals[$kk];
		    } elsif ($vals[$kk] !~ /^[x]+$/) {
			if ($NOADD_STACK) {
			    print("Warning: values within stack $kk are not increasing.\n")
				unless $lvals[$kk] >= $vals[$kk];
			    $vals[$kk] = $lvals[$kk];
			} else {
			    $vals[$kk] += $lvals[$kk];
			}
		    }
		}
	    } else {
		@vals = @lvals;
	    }
	}
	## store the array into the data 
	$column_count=scalar @vals;
	for ($jj = 0; $jj < (scalar @vals) ; $jj++) {
	    $data[$row_count][$jj]=$vals[$jj];
	    # printf("storing ii:$ii jj:$jj  $data[$ii][$jj] \n");
	}
	$row_count++;
    }

    if ($XYSWITCH) {
	@x_label_data  = @g_benches;
	@y_label_data  = @g_expts; 
    } else {
	@x_label_data  = @g_expts;
	@y_label_data  = @g_benches; 
    }
    
    
    
    if (!$XSCALE) { 
	$x_min_data  = ($data[0][0]) if !($X_MIN_SET);
	$x_min_data  = ($X_MIN) if ($X_MIN_SET);
	$x_data_rage = ($data[$row_count-1][0]) - ($x_min_data);
    } 
    else {
	$x_min_data  = ($x_label_data[0]) if !($X_MIN_SET);
	$x_min_data  = ($X_MIN) if ($X_MIN_SET);
	$x_data_rage = ($x_label_data[(scalar @x_label_data) -1 ]) - ($x_min_data);
    }
    
    my $mark_index = 0; 
    my $jj_max = ($XYSWITCH) ?  $row_count : $column_count; 
    my $ii_max = ($XYSWITCH) ?  $column_count : $row_count; 
    die "the number of column should be greater than 1 " if ($ii_max <= 1) ;
    for ($jj = 0 ; $jj < $jj_max ; $jj++) {
	
	
	my $color_str = $g_color_array[$jj];

	printf(OUTPUT "newcurve \n");
	printf(OUTPUT "linetype $g_line_array[(($mark_index)/(scalar @g_mark_array))%(scalar @g_line_array)]\n");
	printf(OUTPUT "marktype  $g_mark_array[(($mark_index++) % (scalar @g_mark_array))] "); 
	if (($mark_index%2) ==1 ) {
	    printf(OUTPUT "fill 1 \n") if !($USE_COLOR); 
	    printf(OUTPUT "cfill $color_str ") if ($USE_COLOR); 
	}
	
	print(OUTPUT "$color_str ") if ($USE_COLOR);
	printf(OUTPUT "label : %s \n", defined($RENAMES[$jj]) ? $RENAMES[$jj] : $y_label_data[$jj]); 
	printf(OUTPUT "pts \n");
	
	for ($ii = 0 ; $ii < $ii_max ; $ii++) {
	    
	    $x_pos = ($X_MAX/($ii_max-1))*$ii if !($XSCALE);
	    $x_pos = ($X_MAX/$x_data_rage)*($x_label_data[$ii]-$x_min_data) if ($XSCALE);
		
	    $x_pos_array[$ii] = $x_pos;
	    
	    next if ($X_MIN_SET && ($x_pos < $X_MIN)); 
	    printf(OUTPUT "    $x_pos $data[$ii][$jj] \n") if (!$XYSWITCH);
	    printf(OUTPUT "    $x_pos $data[$jj][$ii] \n") if ($XYSWITCH);
	}
	printf(OUTPUT "\n\n\n");
    }

}

sub  emit_line_graph_hashes() {
    my ($y_axis_min)  = defined $Y_MIN ? $Y_MIN : $g_y_min;
    my ($y_axis_max)  = defined $Y_MAX ? $Y_MAX : $g_y_max;
    my ($y_label_pos) = $y_axis_min - ($y_axis_max - $y_axis_min) * .075;
    my ($expt_width)  = 1.0 * $X_MAX / ($expt_count / $STACK * @g_benches + @g_benches);
    # my ($hash)	      = .5 * $expt_width + $expt_count / $STACK * $expt_width / 2.0;
    my ($xlabelfont)   = defined $X_LABEL_FONT ? $X_LABEL_FONT : 0; 
    my $ii = 0; 
#      print("expt_width: $expt_width\n");
#      print("expt_count: $expt_count\n");

    foreach (@x_label_data) {
	my $name = (split(/@/, $_))[0];
	if ($NAME_2K) {
	    $name =~ s/\_00$/_2k/;
	} else {
	    $name =~ s/\_00$//;
	}
	    
	    
	$name =  defined($X_RENAMES[$ii]) ? $X_RENAMES[$ii] : $name; 
	
	my $hash =   $x_pos_array[$ii++];
	
  	printf(OUTPUT "xaxis hash_at %.5f\n", $hash, $hash);
	if ($xlabelfont) {
	    if ($NO_ROTATE) {
		printf(OUTPUT "newstring fontsize %d hjc x %.5f y %.5f : %s\n",
		       $xlabelfont, $hash, $y_label_pos, $name);
	    } else {
		printf(OUTPUT "newstring fontsize %d hjc x %.5f y %.5f rotate 45 : %s\n",
		       $xlabelfont, $hash, $y_label_pos, $name);
	    }
	}
	else {
	    if ($NO_ROTATE) {
		printf(OUTPUT "newstring hjc x %.5f y %.5f : %s\n",
		       $hash, $y_label_pos, $name);
	    } else {
		printf(OUTPUT "newstring hjc x %.5f y %.5f rotate 45 : %s\n",
		       $hash, $y_label_pos, $name);
	    }
	}
	
#  	printf(OUTPUT "xaxis hash_at %.5f hash_label at %.5f : ", $hash, $hash);
#  	printf(OUTPUT "%s\n", (split(/@|\_00/, $_))[0]);
	
    }
    print(OUTPUT "\n");
}

sub emit_leg_box()
{

    print (OUTPUT "newcurve gmarks 0 0 0 $LEG_BOX_SIZE_Y  $LEG_BOX_SIZE_X $LEG_BOX_SIZE_Y $LEG_BOX_SIZE_X 0 marktype general fill 1 pts $LEG_BOX_POS_X  $LEG_BOX_POS_Y \n");
}
