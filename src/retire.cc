/*
Copyright (c) <2012>, <Georgia Institute of Technology> All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted 
provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions 
and the following disclaimer.

Redistributions in binary form must reproduce the above copyright notice, this list of 
conditions and the following disclaimer in the documentation and/or other materials provided 
with the distribution.

Neither the name of the <Georgia Institue of Technology> nor the names of its contributors 
may be used to endorse or promote products derived from this software without specific prior 
written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR 
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY 
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
POSSIBILITY OF SUCH DAMAGE.
*/


/**********************************************************************************************
 * File         : retire.cc
 * Author       : Hyesoon Kim
 * Date         : 3/17/2008
 * CVS          : $Id: retire.cc 867 2009-11-05 02:28:12Z kacear $:
 * Description  : retirement logic
                  origial author: Jared W.Stark  imported from
 *********************************************************************************************/


///////////////////////////////////////////////////////////////////////////////////////////////
/// \page retire Retirement stage
///
/// This models retire (commit) stage in the processor pipeline. All instructions are retired 
/// in-order. However, basic execution is in the micro-op granularity. Thus, retirement 
/// should carefully handle this cases.
/// \li <c>Instruction termination condition</c> - All uops of an instruction retired in-order
/// \li <c>Thread termination condition</c> - Last uop of a thread
/// \li <c>Process termination condition</c> - # of thread terminated == # of thread created
///
/// \section retire_cpu CPU retirement stage
/// Check the front uop in the rob (in-order retirement).
///
/// \section retire_gpu GPU retirement stage
/// Since there are possibly many ready-to-retire uops from multiple threads. From all
/// ready-to-retire uops from all threads, we sort them based on the ready cycle (age).
///
/// \todo We need to check thread termination condition carefully.
///////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////
/// \page repeat Repeating traces
///
/// \section repeat_1 What is repeating traces?
/// When an application has been terminated, run this application again
///
/// \section repeat_2 Why do we need to repeat traces?
/// In typical multi-programmed workloads (not multi-threaded), whan an application is 
/// terminated earlier than other applications, typically we keep running early-terminated 
/// application until the last application is terminated.
///
/// \section repeat_3 How to enable repeating trace?
/// There are two ways to enable repeating traces.
/// \li Multi-programmed workloads - set <b>*m_simBase->m_knobs->KNOB_REPEAT_TRACE 1</b>
/// \li Single-application - set <b>*m_simBase->m_knobs->KNOB_REPEAT_TRACE 1</b> and 
/// <b>set *m_simBase->m_knobs->KNOB_REPEAT_TRACE_N positive number</b>
///////////////////////////////////////////////////////////////////////////////////////////////


#include "bug_detector.h"
#include "core.h"
#include "frontend.h"
#include "process_manager.h"
#include "retire.h"
#include "rob.h"
#include "rob_smc.h"
#include "uop.h"

#include "config.h"

#include "knob.h"
#include "debug_macros.h"
#include "statistics.h"

#include "all_knobs.h"

#define DEBUG(args...)   _DEBUG(*m_simBase->m_knobs->KNOB_DEBUG_RETIRE_STAGE, ## args)


// retire_c constructor
retire_c::retire_c(RETIRE_INTERFACE_PARAMS(), macsim_c* simBase) : RETIRE_INTERFACE_INIT() 
{
  m_simBase = simBase;

  m_retire_running      = false;
  m_total_insts_retired = 0;

  RETIRE_CONFIG();

  if (m_knob_ptx_sim)
    m_knob_width = 1000;
}


// retire_c destructor
retire_c::~retire_c()
{
}


// run_a_cycle : try to commit (retire) instructions every cycle
// Check front ROB (reorder buffer) entry and see whehther it is completed
// If there are multiple completed uops, commit until pipeline width
void retire_c::run_a_cycle()
{
  // check whether retire stage is running
  if (!m_retire_running) {
    return;
  }

  m_cur_core_cycle = m_simBase->m_core_cycle[m_core_id];
  core_c *core = m_simBase->m_core_pointers[m_core_id];
    
  vector<uop_c*>* uop_list = NULL;
  unsigned int uop_list_index = 0;
  if (m_knob_ptx_sim && *m_simBase->m_knobs->KNOB_GPU_SCHED) {
    // GPU : many retireable uops from multiple threads. Get entire retireable uops
    uop_list = m_gpu_rob->get_n_uops_in_ready_order(m_knob_width, m_cur_core_cycle);
  }


  // retire instructions : all micro-ops within an inst. need to be retired for an inst.
  for (int count = 0; count < m_knob_width; ++count) {
    uop_c* cur_uop;
    rob_c* rob;

    // we need to handle retirement for x86 and ptx separately
    
    // retirement logic for GPU
    if (m_knob_ptx_sim && *m_simBase->m_knobs->KNOB_GPU_SCHED) {
      // GPU : many retireable uops from multiple threads. Get entire retireable uops
      if (uop_list_index == uop_list->size()) {
        uop_list->clear();
        break;
      }

      cur_uop = uop_list->at(uop_list_index++);

      rob = m_gpu_rob->get_thread_rob(cur_uop->m_thread_id);
      rob->pop();
    }
    // retirement logic for x86 simulation
    else {
      rob = m_rob;

      // rob is empty;
      if (rob->entries() == 0) {
        break;
      }

      cur_uop = rob->front();

      // uop cannot be retired
      if (!cur_uop->m_done_cycle || cur_uop->m_done_cycle > m_cur_core_cycle || cur_uop->m_exec_cycle == 0) {
        break;
      }

      rob->pop();
      POWER_CORE_EVENT(m_core_id, POWER_REORDER_BUF_R);
      POWER_CORE_EVENT(m_core_id, POWER_INST_COMMIT_SEL_LOGIC_R);
    }    
    
    // all uops belong to previous instruction have been retired : inst_count++
    // nagesh - why are we marking the instruction as retired when the uop
    // marked BOM is retired? shouldn't we do that when the EOM is retired?
    // nagesh - ISTR that I tried changing and something failed - not 100% 
    // sure though : (jaekyu) I think this is the matter of the design. we can update
    // everything from the first uop of an instruction.
    if (cur_uop->m_isitBOM) {
      if (cur_uop->m_uop_type >= UOP_FCF && cur_uop->m_uop_type <= UOP_FCMOV) {
        STAT_EVENT(FP_OPS_TOT);
        STAT_CORE_EVENT(cur_uop->m_core_id, FP_OPS);
      }

      ++m_insts_retired[cur_uop->m_thread_id];
      ++m_total_insts_retired;
      ++m_period_inst_count;

      if (*KNOB(KNOB_SIMPOINT_BBV_ENABLED) != 0) { 
        if (m_simBase->m_unit_warp_insts_retired == 0) {
          m_simBase->simpoint_bbv_profile << "TB"; 
          for (map<unsigned int, bool>::iterator it =  m_simBase->curBlockList.begin(); it !=  m_simBase->curBlockList.end(); it ++) {
            m_simBase->simpoint_bbv_profile << ":" << it->first;
          }
          m_simBase->simpoint_bbv_profile << endl;
        }
      }

      //////////////////////////////////////////////////
      // Generate PC to BB mapping      
      int bb_id;
      int block_id = cur_uop->m_block_id;

      if (*KNOB(KNOB_PC_BB_ENABLED) != 0) {
        if (m_simBase->PCBBMap.find(cur_uop->m_pc) != m_simBase->PCBBMap.end()) {
          m_simBase->curBB[cur_uop->m_thread_id] = bb_id = m_simBase->PCBBMap[cur_uop->m_pc];        
        } else {
          bb_id = m_simBase->curBB[cur_uop->m_thread_id];        
        }

        m_simBase->AllPCBBMap[cur_uop->m_pc] = bb_id;
        // cout << "retired " << cur_uop->m_pc << " " << bb_id << " "
        //      << (cur_uop->m_cf_type != NOT_CF) << endl;
        // }
      
        // cout << "cur uop " << cur_uop->m_thread_id << endl;
        // cout << "PC " << cur_uop->m_pc << " " << bb_id << endl;      
            
        // if (cur_uop->m_thread_id == m_simBase->monitored_thread &&      
        //     cur_uop->m_cf_type != NOT_CF) {
        if (cur_uop->m_cf_type != NOT_CF) {        
        
          // cout << "target: " << cur_uop->m_pc << " " << cur_uop->m_target_addr
          //      << endl;

          if (m_simBase->PCBBMap.find(cur_uop->m_target_addr) == m_simBase->PCBBMap.end()) {
            m_simBase->PCBBMap[cur_uop->m_target_addr] = m_simBase->maxBB;
            m_simBase->maxBB ++;                        
          }
          if (m_simBase->PCBBMap.find(cur_uop->m_pc + 8) == m_simBase->PCBBMap.end()) {
            m_simBase->PCBBMap[cur_uop->m_pc + 8] = m_simBase->maxBB;
            m_simBase->maxBB ++;            
          }
               
        }
      } else {
        bb_id = m_simBase->PC_to_BB_map[m_simBase->bench_name][cur_uop->m_pc];
      }
      
      // Get Active Threads
      int active_threads = 0;
      //uint32_t mask = ~cur_uop->m_active_mask;
      uint32_t mask = cur_uop->m_active_mask;
      while (mask > 0) {
        if (mask & 0x1  == 1)
          active_threads ++;
        mask >>= 1;        
      }
      //////////////////////////////////////////////////      
      
      // int pc_index = (cur_uop->m_pc - 8000) >> 3;
      // int bb_id = 0;
      // int block_id = cur_uop->m_block_id;

      // map<string, map<unsigned int, unsigned int>>::iterator it;
      // for (it = m_simBase->PC_to_BB_map.begin(); it != m_simBase->PC_to_BB_map.end(); it ++) {
      //   // cout << "pc_index " << pc_index << endl; 
      //   assert(it->second.find(pc_index) != it->second.end());          
      //   bb_id = it->second[pc_index];          
      //   break;        
      // }

      // Initialization of Counters
      if (m_simBase->MEM_counter.find(bb_id) == m_simBase->MEM_counter.end()) {
        bb_info new_bb_info;
        memset(&new_bb_info, 0, sizeof(bb_info));
        m_simBase->MEM_counter[bb_id] = new_bb_info;
      }
      
      if (m_simBase->BB_counter.find(bb_id) == m_simBase->BB_counter.end()) {
        bb_info new_bb_info;
        memset(&new_bb_info, 0, sizeof(bb_info));
        m_simBase->BB_counter[bb_id] = new_bb_info;
      }
      
      if (m_simBase->tblocks[block_id].diverge_info.find(bb_id) == m_simBase->tblocks[block_id].diverge_info.end()) {
        bb_info new_bb_info;
        memset(&new_bb_info, 0, sizeof(bb_info));
        m_simBase->tblocks[block_id].diverge_info[bb_id] = new_bb_info;
      }

      // instruction counting statistics
      if (m_simBase->doneStablePhase) {
        m_simBase->acc_cycles += (double)(1.0 / m_simBase->StablePhaseIPC);
        m_simBase->m_prof_cycles_retired += (double)(1.0 / m_simBase->StablePhaseIPC);
        m_simBase->acc_warp_insts += 1;
        m_simBase->m_prof_insts_retired += 1;
        m_simBase->skipped_insts += 1;

        // L1/L2 tracking
        // if (cur_uop->m_mem_type == MEM_LD_GM || cur_uop->m_mem_type == MEM_LD_LM || cur_uop->m_mem_type == MEM_LD) {        
        if (cur_uop->m_num_child_uops == 0) {
          m_simBase->acc_l1_accesses ++;
          m_simBase->acc_l1_misses += m_simBase->StableL1MissRate * 1.0;
          m_simBase->acc_l2_accesses += 1.0 * m_simBase->StableL1L2Rate;
          m_simBase->acc_l2_misses += 1.0 * m_simBase->StableL1L2Rate * m_simBase->StableL2MissRate;
        } else {
          map<Addr, bool> uop_mem_map;          
          for (int ii = 0; ii < cur_uop->m_num_child_uops; ++ii) {
            uop_c* child_op = cur_uop->m_child_uops[ii];
            Addr line_addr = (child_op->m_vaddr & ~(127));
            uop_mem_map[line_addr] = true;
            m_simBase->MEMFOOT_counter.mem_foots[line_addr] = true;
            m_simBase->tblocks[block_id].memfoot_info.mem_foots[line_addr] = true;
          }          
          
          m_simBase->acc_l1_accesses += uop_mem_map.size();
          m_simBase->acc_l1_misses += m_simBase->StableL1MissRate * uop_mem_map.size();
          m_simBase->acc_l2_accesses += uop_mem_map.size() * m_simBase->StableL1L2Rate;
          m_simBase->acc_l2_misses += uop_mem_map.size() * m_simBase->StableL1L2Rate * m_simBase->StableL2MissRate;
        }          
        // }
      } else {
        m_simBase->acc_warp_insts += 1;
        m_simBase->m_prof_insts_retired += 1;        
        
        m_simBase->BB_counter[bb_id].insts += active_threads;
        m_simBase->BB_counter[bb_id].warp_insts += 1;        
        m_simBase->m_unit_insts_retired += active_threads;
        m_simBase->m_unit_warp_insts_retired += 1;
        m_simBase->tblocks[block_id].diverge_info[bb_id].warp_insts += 1;
        m_simBase->tblocks[block_id].diverge_info[bb_id].insts += active_threads;
        m_simBase->warp_insts += 1;

        int slotId = -1;      
        for (int i = 0; i < core->get_max_threads_per_core(); i ++)
          if (core->threadSlot[i] == cur_uop->m_thread_id)
            slotId = i;      

        assert(slotId != -1);
        if (cur_uop->m_mem_type == MEM_LD_GM || cur_uop->m_mem_type == MEM_LD_LM || cur_uop->m_mem_type == MEM_LD) {
          m_simBase->warp_mem_insts ++;
        }
      
        // Mem Counters increase      
        if (cur_uop->m_mem_type == MEM_LD_GM || cur_uop->m_mem_type == MEM_LD_LM || cur_uop->m_mem_type == MEM_LD) {        
        
          m_simBase->m_prof_mem_insts ++;
          m_simBase->tblocks[block_id].diverge_ratio ++;
          m_simBase->tblocks[block_id].diverge_info[bb_id].mem_insts += 1;        
        
          if (cur_uop->m_num_child_uops == 0) {
            Addr line_addr = (cur_uop->m_vaddr & ~(127));
            m_simBase->m_prof_mem_uops ++;
            if (cur_uop->m_mem_type == MEM_LD_GM || cur_uop->m_mem_type == MEM_LD_LM || cur_uop->m_mem_type == MEM_LD) {
              m_simBase->warp_mem_reqs += 1;
            }
            m_simBase->MEM_counter[bb_id].mem_reqs ++;          
            m_simBase->MEMFOOT_counter.mem_foots[line_addr] = true;
            m_simBase->tblocks[block_id].memfoot_info.mem_foots[line_addr] = true;
            m_simBase->tblocks[block_id].mem_reqs ++;
            m_simBase->tblocks[block_id].diverge_info[bb_id].mem_reqs += 1;

            if (*KNOB(KNOB_MEM_PROF_ENABLED) != 0) {
              if (m_core_id == 0)
                m_simBase->mem_profile << slotId << " " << core->memGap[slotId] << " 1 " << m_cur_core_cycle << endl;
            }
            core->memGap[slotId] = 0;        
          
          } else {
            // m_simBase->m_prof_mem_uops += cur_uop->m_num_child_uops;
            // m_simBase->MEM_counter[bb_id] += cur_uop->m_num_child_uops;
            map<Addr, bool> uop_mem_map;          
            for (int ii = 0; ii < cur_uop->m_num_child_uops; ++ii) {
              uop_c* child_op = cur_uop->m_child_uops[ii];
              Addr line_addr = (child_op->m_vaddr & ~(127));
              uop_mem_map[line_addr] = true;
              m_simBase->MEMFOOT_counter.mem_foots[line_addr] = true;
              m_simBase->tblocks[block_id].memfoot_info.mem_foots[line_addr] = true;
            }          
          
            m_simBase->m_prof_mem_uops += uop_mem_map.size();
            if (cur_uop->m_mem_type == MEM_LD_GM || cur_uop->m_mem_type == MEM_LD_LM || cur_uop->m_mem_type == MEM_LD) {
              m_simBase->warp_mem_reqs += uop_mem_map.size();
            }
            m_simBase->MEM_counter[bb_id].mem_reqs += uop_mem_map.size();
            m_simBase->tblocks[block_id].mem_reqs += uop_mem_map.size();
            m_simBase->tblocks[block_id].diverge_info[bb_id].mem_reqs += uop_mem_map.size();

            if (*KNOB(KNOB_MEM_PROF_ENABLED) != 0) {
              if (m_core_id == 0)
                m_simBase->mem_profile << slotId << " " << core->memGap[slotId] << " " << uop_mem_map.size() << " " << m_cur_core_cycle << endl;
            }
            core->memGap[slotId] = 0;        
          }
        }            
        assert(m_simBase->tblocks.find(block_id) != m_simBase->tblocks.end());
        m_simBase->tblocks[block_id].block_uops += active_threads;
        m_simBase->tblocks[block_id].block_instructions += 1;

        core->memGap[slotId] += 1;      
      }
      
      // Record each sample unit results
      // cout << "retired " << m_simBase->m_unit_insts_retired << " " << *KNOB(KNOB_SAMPLE_UNIT_SIZE) << endl;
      if (m_simBase->sample_unit_size != 0 && m_simBase->m_unit_warp_insts_retired >= m_simBase->sample_unit_size) {
      // if (m_simBase->sample_unit_size != 0 && m_simBase->m_unit_insts_retired >= m_simBase->sample_unit_size) {
        double interval_CPI, kernel_CPI, l1_rate, l2_rate, diverge_ratio;          
        m_simBase->m_kernel_cycles_retired += m_simBase->m_unit_cycles_retired;
        m_simBase->m_kernel_insts_retired += m_simBase->m_unit_insts_retired;
        m_simBase->m_kernel_warp_insts_retired += m_simBase->m_unit_warp_insts_retired;
        m_simBase->m_kernel_l1_access += m_simBase->m_prof_l1_access;
        m_simBase->m_kernel_l2_access += m_simBase->m_prof_l2_access;
        m_simBase->m_kernel_l1_misses += m_simBase->m_prof_l1_misses;
        m_simBase->m_kernel_l2_misses += m_simBase->m_prof_l2_misses;
        m_simBase->m_kernel_mem_insts += m_simBase->m_prof_mem_insts;
        m_simBase->m_kernel_mem_uops += m_simBase->m_prof_mem_uops;

        if (*KNOB(KNOB_IDEAL_BBV_ENABLED) != 0)
          m_simBase->GenIdealBBVs(false, &m_simBase->ideal_bbv_profile);

        if (*KNOB(KNOB_SIMPOINT_BBV_ENABLED) != 0)
          m_simBase->GenIdealBBVs(false, &m_simBase->simpoint_bbv_profile);
          
        m_simBase->BB_counter.clear();
        m_simBase->MEM_counter.clear();
        m_simBase->MEMFOOT_counter.mem_foots.clear();        
            
        interval_CPI = (double)m_simBase->m_unit_cycles_retired / (double)m_simBase->m_unit_insts_retired;
        kernel_CPI = (double)m_simBase->m_kernel_cycles_retired / (double)m_simBase->m_kernel_insts_retired;          
        m_simBase->ideal_bbv_profile << "Unit: " << interval_CPI << " "
                               << kernel_CPI << " "
                               << m_simBase->m_unit_cycles_retired << endl;

        if (m_simBase->m_prof_l1_access == 0)
          m_simBase->m_prof_l1_access = 1;
        if (m_simBase->m_prof_l2_access == 0)
          m_simBase->m_prof_l2_access = 1;
            
        l1_rate = (double)m_simBase->m_prof_l1_misses / (double)m_simBase->m_prof_l1_access;
        l2_rate = (double)m_simBase->m_prof_l2_misses / (double)m_simBase->m_prof_l2_access;
        diverge_ratio = (double)m_simBase->m_prof_mem_uops / (double)m_simBase->m_prof_mem_insts;
        
        if (*KNOB(KNOB_SIMPOINT_BBV_ENABLED) != 0)
          m_simBase->simpoint_bbv_profile << "Diverge_Ratio: " << diverge_ratio 
                                          << " L1 rate: " << l1_rate << " accesses: " <<  m_simBase->m_prof_l1_access 
                                          << " L2 rate: " << l2_rate << " accesses: " <<  m_simBase->m_prof_l2_access 
                                          << endl;
        
        if (*KNOB(KNOB_SAMPLING_ENABLED) == true) {
          if (m_simBase->tbpoints.size() > 0) {
            int phaseId = m_simBase->tbpoints.front().first;
            m_simBase->phaseCPI[phaseId] = interval_CPI;
            m_simBase->phaseInsts[phaseId] = m_simBase->m_unit_insts_retired;
            m_simBase->phaseL1Rate[phaseId] = l1_rate;
            m_simBase->phaseL2Rate[phaseId] = l2_rate;
            cout << "phaseId " << phaseId << " " << interval_CPI << " "
                 << m_simBase->tbpoints.size()<< endl;
            m_simBase->tbpoints.erase(m_simBase->tbpoints.begin());
          }
        }        
        
        m_simBase->m_unit_cycles_retired = 0;
        m_simBase->m_unit_insts_retired = 0;
        m_simBase->m_unit_warp_insts_retired = 0;
        m_simBase->m_prof_l1_misses = 0;
        m_simBase->m_prof_l1_access = 0;
        m_simBase->m_prof_l2_misses = 0;
        m_simBase->m_prof_l2_access = 0;
        m_simBase->m_prof_mem_insts = 0;
        m_simBase->m_prof_mem_uops = 0;
        m_simBase->m_tb_retired_counts = 0;
        m_simBase->m_tot_active_threads = 0;
        m_simBase->m_tot_block_num = 0;
      }
      /////////////////////////////////////////////////      

      STAT_CORE_EVENT(cur_uop->m_core_id, INST_COUNT);
      STAT_CORE_EVENT(cur_uop->m_core_id, IPC);
      POWER_CORE_EVENT(cur_uop->m_core_id, POWER_PIPELINE);
      STAT_EVENT(INST_COUNT_TOT);
      STAT_EVENT(IPC_TOT);
    }

    STAT_CORE_EVENT(cur_uop->m_core_id, UOP_COUNT);
    STAT_EVENT(UOP_COUNT_TOT);

    // GPU : barrier
    if (m_knob_ptx_sim && cur_uop->m_bar_type == BAR_FETCH) {
      frontend_c *frontend = core->get_frontend();
      frontend->synch_thread(cur_uop->m_block_id, cur_uop->m_thread_id);
    }


    // Terminate thread : current uop is last uop of a thread, so we can retire a thread now
    thread_s* thread_trace_info = core->get_trace_info(cur_uop->m_thread_id);
    process_s *process = thread_trace_info->m_process;
    if (cur_uop->m_last_uop || m_insts_retired[cur_uop->m_thread_id] >= *m_simBase->m_knobs->KNOB_MAX_INSTS) {
      core->m_thread_reach_end[cur_uop->m_thread_id] = true;
      ++core->m_num_thread_reach_end;
      if (!core->m_thread_finished[cur_uop->m_thread_id]) {
        DEBUG("core_id:%d thread_id:%d terminated\n", m_core_id, cur_uop->m_thread_id);

        // terminate thread
        m_simBase->m_process_manager->terminate_thread(m_core_id, thread_trace_info, \
            cur_uop->m_thread_id, cur_uop->m_block_id);

        // disable current thread's fetch engine
        if (!core->m_fetch_ended[cur_uop->m_thread_id]) {
          core->m_fetch_ended[cur_uop->m_thread_id] = true;
          core->m_fetching_thread_num--;
        }

        // all threads in an application have been retired. Thus, we can retire an appliacation
        if (process->m_no_of_threads_terminated == process->m_no_of_threads_created) {
          if (process->m_current_vector_index == process->m_applications.size() 
            || (*m_simBase->m_ProcessorStats)[INST_COUNT_TOT].getCount() >= *KNOB(KNOB_MAX_INSTS1)) {
            update_stats(process);
            m_simBase->m_process_manager->terminate_process(process);
            if (m_simBase->m_process_count_without_repeat == 0) {
              m_simBase->m_repeat_done = true;
            }
            repeat_traces(process);
          }
          else {
            m_simBase->m_process_manager->terminate_process(process);
          }

          // schedule new threads
          m_simBase->m_process_manager->sim_thread_schedule(true);
        }

        // schedule new threads
        m_simBase->m_process_manager->sim_thread_schedule(false);
      }
    } // terminate_thread


    // update number of retired uops
    ++m_uops_retired[cur_uop->m_thread_id];

    DEBUG("core_id:%d thread_id:%d retired_insts:%lld uop->inst_num:%lld uop_num:%lld " 
        "done_cycle:%lld\n",
        m_core_id, cur_uop->m_thread_id, m_insts_retired[cur_uop->m_thread_id], 
        cur_uop->m_inst_num, cur_uop->m_uop_num, cur_uop->m_done_cycle);

    // free uop
    for (int ii = 0; ii < cur_uop->m_num_child_uops; ++ii) {
			if (*m_simBase->m_knobs->KNOB_BUG_DETECTOR_ENABLE)
	      m_simBase->m_bug_detector->deallocate(cur_uop->m_child_uops[ii]);
      m_uop_pool->release_entry(cur_uop->m_child_uops[ii]->free());
    }

    if (*m_simBase->m_knobs->KNOB_BUG_DETECTOR_ENABLE)
			m_simBase->m_bug_detector->deallocate(cur_uop);

    delete [] cur_uop->m_child_uops;
    m_uop_pool->release_entry(cur_uop->free());
  
    // release physical registers
    if (cur_uop->m_req_lb) {
      rob->dealloc_lb();
    }
    if (cur_uop->m_req_sb) {
      rob->dealloc_sb();
    }
    if (cur_uop->m_req_int_reg) {
      rob->dealloc_int_reg();
    }
    if (cur_uop->m_req_fp_reg) {
      rob->dealloc_fp_reg();
    }
  }

  if (m_core_id == 0) {
    m_simBase->m_core0_inst_count = m_insts_retired[0];
  }
}


// When a new thread has been scheduled, reset data
void retire_c::allocate_retire_data(int tid)
{
  m_insts_retired[tid] = 0;
  m_uops_retired[tid]  = 0;
}


void retire_c::start()
{
  m_retire_running = true;
}


void retire_c::stop()
{
  m_retire_running = false;
}


bool retire_c::is_running()
{
  return m_retire_running;
}



#if 0
// return number of retired instructions per thread
inline Counter retire_c::get_instrs_retired(int thread_id) 
{ 
  return m_insts_retired[thread_id]; 
}
#endif


// return number of retired uops per thread
Counter retire_c::get_uops_retired(int thread_id) 
{ 
  return m_uops_retired[thread_id]; 
}


// return total number of retired instructions
Counter retire_c::get_total_insts_retired() 
{ 
  return m_total_insts_retired; 
}


// whan an application is completed, update corresponding stats
void retire_c::update_stats(process_s* process)
{
  core_c* core = m_simBase->m_core_pointers[m_core_id];

  // repeating traces in case of running multiple applications
  // TOCHECK I will get back to this later
  if (*KNOB(KNOB_REPEAT_TRACE) && process->m_repeat < *KNOB(KNOB_REPEAT_TRACE_N) &&
      core->get_core_type() == "ptx") {
    if ((process->m_repeat+1) == *m_simBase->m_knobs->KNOB_REPEAT_TRACE_N) {
      --m_simBase->m_process_count_without_repeat;
      STAT_EVENT_N(CYC_COUNT_PTX, CYCLE);
      report("application " << process->m_process_id << " terminated " 
          << "(" << process->m_applications[process->m_current_vector_index-1] 
          << "," << process->m_repeat << ") at " << CYCLE);
    }
  } 
  else {
    if (process->m_repeat == 0) {
      if (core->get_core_type() == "ptx") {
        STAT_EVENT_N(CYC_COUNT_PTX, CYCLE);
      }
      else {
        STAT_EVENT_N(CYC_COUNT_X86, CYCLE);
      }
      --m_simBase->m_process_count_without_repeat;
      report("----- application " << process->m_process_id << " terminated (" 
          << process->m_applications[process->m_current_vector_index-1] 
          << "," << process->m_repeat << ") at " << CYCLE);
    }
  }
}
          

// repeat (terminated) trace, if necessary
void retire_c::repeat_traces(process_s* process)
{
  if ((*KNOB(KNOB_REPEAT_TRACE) || (*KNOB(KNOB_REPEAT_TRACE) && *KNOB(KNOB_REPEAT_TRACE_N) > 0)) && 
      m_simBase->m_process_count_without_repeat > 0) {
    // create duplicate process once previous one is terminated
    m_simBase->m_process_manager->create_process(process->m_kernel_config_name, process->m_repeat+1, 
        process->m_orig_pid);
    STAT_EVENT(NUM_REPEAT);
  }
}
