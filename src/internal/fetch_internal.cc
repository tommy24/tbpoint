/**********************************************************************************************
 * File         : internal/fetch_internal.cc
 * Author       : Jaekyu Lee
 * Date         : 1/27/2011 
 * SVN          : $Id: frontend.h 915 2009-11-20 19:13:07Z kacear $:
 * Description  : various fetch policies (not seen to public)
 *********************************************************************************************/


#include "../uop.h"
#include "../frontend.h"
#include "../statistics.h"
#include "../debug_macros.h"
#include "../core.h"
#include "../process_manager.h"
#include "fetch_internal.h"
#include "../fetch_factory.h"

#include "../all_knobs.h"


#define DEBUG(args...)   _DEBUG(*m_simBase->m_knobs->KNOB_DEBUG_FRONT_STAGE, ## args)
#define DEBUG_CORE(m_core_id, args...)       \
  if (m_core_id == *m_simBase->m_knobs->KNOB_DEBUG_CORE_ID) {     \
    _DEBUG(*m_simBase->m_knobs->KNOB_DEBUG_FRONT_STAGE, ## args); \
  }
#define DEBUG_SWPREF(args...) _DEBUG(*m_simBase->m_knobs->KNOB_DEBUG_SWPREF, ## args)


///////////////////////////////////////////////////////////////////////////////////////////////
// wrapper function to link internal fetch policies
///////////////////////////////////////////////////////////////////////////////////////////////
frontend_c *fetch_factory_internal(FRONTEND_INTERFACE_PARAMS(), macsim_c* simBase) {
  frontend_c *new_fe = new fetch_internal_c(FRONTEND_INTERFACE_ARGS(), simBase);

  return new_fe;
}




///////////////////////////////////////////////////////////////////////////////////////////////
// Singleton fetch_internal_wrapper
///////////////////////////////////////////////////////////////////////////////////////////////
fetch_internal_wrapper_c fetch_internal_wrapper_c::Singleton;


///////////////////////////////////////////////////////////////////////////////////////////////
// fetch_internal_wrapper constructor
// To register all modules
///////////////////////////////////////////////////////////////////////////////////////////////
fetch_internal_wrapper_c::fetch_internal_wrapper_c()
{
  fetch_factory_c::get()->register_class("fair", fetch_factory_internal);
  fetch_factory_c::get()->register_class("lrf", fetch_factory_internal);
  fetch_factory_c::get()->register_class("icount", fetch_factory_internal);
  fetch_factory_c::get()->register_class("prirr", fetch_factory_internal);
  fetch_factory_c::get()->register_class("fairrr", fetch_factory_internal);
  fetch_factory_c::get()->register_class("maxrr", fetch_factory_internal);
  fetch_factory_c::get()->register_class("blk_until_load", fetch_factory_internal);
  fetch_factory_c::get()->register_class("all", fetch_factory_internal);
  fetch_factory_c::get()->register_class("random", fetch_factory_internal);
  fetch_factory_c::get()->register_class("fairperiod", fetch_factory_internal);
  fetch_factory_c::get()->register_class("bar", fetch_factory_internal);
  fetch_factory_c::get()->register_class("mem", fetch_factory_internal);
  fetch_factory_c::get()->register_class("mem_bar", fetch_factory_internal);
  fetch_factory_c::get()->register_class("mem_bar_new", fetch_factory_internal);
  fetch_factory_c::get()->register_class("rr_mem_bar_new", fetch_factory_internal);
  fetch_factory_c::get()->register_class("pred_longest_first", fetch_factory_internal);
  fetch_factory_c::get()->register_class("pred_longest_first_new", fetch_factory_internal);
  fetch_factory_c::get()->register_class("reconv_long", fetch_factory_internal);
  fetch_factory_c::get()->register_class("rr_block", fetch_factory_internal);
  fetch_factory_c::get()->register_class("row_hit", fetch_factory_internal);
}


///////////////////////////////////////////////////////////////////////////////////////////////
// fetch_internal_wrapper_c destructor
///////////////////////////////////////////////////////////////////////////////////////////////
fetch_internal_wrapper_c::~fetch_internal_wrapper_c()
{
}


///////////////////////////////////////////////////////////////////////////////////////////////
// fetch_internal_c constructor
///////////////////////////////////////////////////////////////////////////////////////////////
fetch_internal_c::fetch_internal_c(FRONTEND_INTERFACE_PARAMS(), macsim_c* simBase) 
: frontend_c(FRONTEND_INTERFACE_ARGS(), simBase)
{
  m_simBase = simBase;

  m_dec_scheduled_thread_num         = 0;
  m_dec_rr_freq                      = *m_simBase->m_knobs->KNOB_DEC_RR_FREQ;
  m_dec_fetch_arbiter                = 0;
  m_rr_type                          = 0;
  m_rr_counter                       = 1;
  if (m_dec_rr_freq < 2) {
    m_dec_rr_freq = 2;
  }
  m_prev_unique_scheduled_thread_num = 0;
  m_block_arbiter                    = 0;
  m_MT_stop_fair                     = *m_simBase->m_knobs->KNOB_MT_STOP_FAIR_INIT; 
  m_prev_bid                         = -1;
  m_prev_rowid                       = -1;
}


///////////////////////////////////////////////////////////////////////////////////////////////
// fetch_internal_c destructor
///////////////////////////////////////////////////////////////////////////////////////////////
fetch_internal_c::~fetch_internal_c()
{
}


///////////////////////////////////////////////////////////////////////////////////////////////
// fetch policy
///////////////////////////////////////////////////////////////////////////////////////////////
int fetch_internal_c::fetch()
{
  static bool fetch_internal_init = false;

  if (!fetch_internal_init) {
    fetch_internal_init = false;
  
    string policy = m_simBase->m_knobs->KNOB_FETCH_POLICY->getValue();

    if (policy == "fair") 
      MT_fetch_scheduler = &fetch_internal_c::fetch_fair;
    else if (policy == "lrf") 
      MT_fetch_scheduler = &fetch_internal_c::fetch_lrf;
    else if (policy == "icount") 
      MT_fetch_scheduler = &fetch_internal_c::fetch_icount;
    else if (policy == "prirr") 
      MT_fetch_scheduler = &fetch_internal_c::fetch_prirr;
    else if (policy == "fairrr") 
      MT_fetch_scheduler = &fetch_internal_c::fetch_fairrr;
    else if (policy == "maxrr") 
      MT_fetch_scheduler = &fetch_internal_c::fetch_maxrr;
    else if (policy == "blk_until_load") 
      MT_fetch_scheduler = &fetch_internal_c::fetch_blk_until_load;
    else if (policy == "all") 
      MT_fetch_scheduler = &fetch_internal_c::fetch_all;
    /* hyesoon's trial */
    else if (policy == "random") 
      MT_fetch_scheduler = &fetch_internal_c::fetch_random;
//    else if (policy == "fairperiod") 
//      MT_fetch_scheduler = &fetch_internal_c::fetch_fairperiod;
    else if (policy == "bar") 
      MT_fetch_scheduler = &fetch_internal_c::fetch_bar;
    else if (policy == "mem") 
      MT_fetch_scheduler = &fetch_internal_c::fetch_mem;
    else if (policy == "mem_bar") 
      MT_fetch_scheduler = &fetch_internal_c::fetch_mem_bar;
    else if (policy == "mem_bar_new") 
      MT_fetch_scheduler = &fetch_internal_c::fetch_mem_bar_new;
    else if (policy == "rr_mem_bar_new") 
      MT_fetch_scheduler = &fetch_internal_c::fetch_rr_mem_bar_new;
//    else if (policy == "pred_longest_first") 
//      MT_fetch_scheduler = &fetch_internal_c::fetch_pred_longest_first;
    else if (policy == "pred_longest_first_new") 
      MT_fetch_scheduler = &fetch_internal_c::fetch_pred_longest_first_new;
    else if (policy == "reconv_long") 
      MT_fetch_scheduler = &fetch_internal_c::fetch_reconv_long;
    else if (policy == "rr_block") 
      MT_fetch_scheduler = &fetch_internal_c::fetch_rr_block;
    else if (policy == "row_hit") 
      MT_fetch_scheduler = &fetch_internal_c::fetch_row_hit;
    else
      MT_fetch_scheduler = &fetch_internal_c::fetch_roundrobin;
  }

  return (*this.*MT_fetch_scheduler)();
}





///////////////////////////////////////////////////////////////////////////////////////////////
// beyond this point, add fetch policies


///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
int fetch_internal_c::fetch_roundrobin()
{
  int try_again = 1;
  //int fetch_id = 0;
  int fetch_id = -1;
  int arbiter_fetch_id = -1;
  bool can_fetch;

  DEBUG("m_core_id:%d m_running_thread_num:%d m_fetching_thread_num:%d "
        "m_unique_scheduled_thread_num:%d \n",m_core_id, m_running_thread_num,
        m_fetching_thread_num, m_unique_scheduled_thread_num);

  int max_try = m_unique_scheduled_thread_num - m_last_terminated_tid;
  
  while (m_fetching_thread_num && try_again && try_again <= max_try) {
    // find next fetch-able thread by round-robin fashion
    fetch_id      = m_fetch_arbiter % m_unique_scheduled_thread_num;
    m_fetch_arbiter = (m_fetch_arbiter + 1) % m_unique_scheduled_thread_num;
    if (m_fetch_arbiter < m_last_terminated_tid) {
      m_fetch_arbiter = m_last_terminated_tid;
    }


    if (m_core->m_fetch_ended[fetch_id] || 
        m_core->m_thread_reach_end[fetch_id] || 
        (*m_simBase->m_knobs->KNOB_NO_FETCH_ON_ICACHE_MISS && !check_fetch_ready(fetch_id))) {
      try_again++;
      continue;
    }


    frontend_s* fetch_data = m_core->get_trace_info(fetch_id)->m_fetch_data;
    if (fetch_data!= NULL && fetch_data->m_fetch_blocked) {
      DEBUG("m_core_id:%d thread_id:%d fetch_blocked\n",
          m_core_id, fetch_id);
      try_again++;
      continue;
    }


    can_fetch = true;
    if (m_knob_ptx_sim) {
      if (*m_simBase->m_knobs->KNOB_MT_NO_FETCH_BR) {
        if (!check_br_ready(fetch_id)) {
          can_fetch = false;
          DEBUG("m_core_id:%d thread_id:%d br not ready\n",
              m_core_id, fetch_id);
          if (try_again == 1) {
            STAT_EVENT(FETCH_THREAD_SKIP_BR_WAIT);
          }
        }
      }

      if (*m_simBase->m_knobs->KNOB_FETCH_ONLY_LOAD_READY) {
        if (!check_load_ready(fetch_id)) {
          can_fetch = false;
          DEBUG("m_core_id:%d thread_id:%d load not ready\n",
              m_core_id, fetch_id);
          if (try_again == 1) {
            STAT_EVENT(FETCH_THREAD_SKIP_LD_WAIT);
          }
        }
      }
    }

    if (can_fetch) {
      m_ready_thread_available = true;
      try_again = 0; 
    } 
    else {
      try_again++;
    }
  }

  if (try_again > max_try) {
    fetch_id = arbiter_fetch_id;
  }

  DEBUG_CORE(m_core_id, 
             "m_core_id:%d try_agin:%d thread_id:%d arbiter_fetch_id:%d\n",
             m_core_id, try_again, fetch_id, arbiter_fetch_id);

  return fetch_id;
}


///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
int fetch_internal_c::fetch_fair(void)
{
  Counter min_fetched_inst = ~0;
  int min_fetched_thread = -1;
  //int default_thread = -1;
  int count = 1;
  bool can_fetch;

  m_core = m_core;
  for (int thread_id = m_last_terminated_tid; thread_id < m_unique_scheduled_thread_num; thread_id++)
  {
    if ((m_core->m_fetch_ended[thread_id]) || (m_core->m_thread_reach_end[thread_id]) ||
    		(*m_simBase->m_knobs->KNOB_NO_FETCH_ON_ICACHE_MISS && !check_fetch_ready(thread_id))) {
      continue;
    }
    frontend_s* fetch_data = m_core->get_trace_info(thread_id)->m_fetch_data;
    if (fetch_data != NULL && fetch_data->m_fetch_blocked) {
      continue;
    }

    can_fetch = true;
    if (m_knob_ptx_sim && *m_simBase->m_knobs->KNOB_MT_NO_FETCH_BR) {
      if (!check_br_ready(thread_id)) {
        can_fetch = false;
        if (count == 1) {
          STAT_EVENT(FETCH_THREAD_SKIP_BR_WAIT); 
        }
        count++;
      }
    }

    if (m_knob_ptx_sim && *m_simBase->m_knobs->KNOB_FETCH_ONLY_LOAD_READY) {
      if (!check_load_ready(thread_id)) {
        can_fetch = false;
        if (count == 1) {
          STAT_EVENT(FETCH_THREAD_SKIP_LD_WAIT); 
        }
        count++; 
      }
    }
			
    if (can_fetch) {
      m_ready_thread_available = true;
    }
    if (can_fetch && m_core->m_inst_fetched[thread_id] < min_fetched_inst)	{
      min_fetched_thread = thread_id;	
      min_fetched_inst = m_core->m_inst_fetched[thread_id];
    }
  }

  return min_fetched_thread;
}


///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
int fetch_internal_c::fetch_lrf(void)
{
  cout << "fetch_lrf\n";
  Counter last_fetch_cycle = ~0;
  //int default_thread = -1;
  int lrf_thread = -1;
  int count = 1;
  bool can_fetch;

  for (int thread_id = m_last_terminated_tid; thread_id < m_unique_scheduled_thread_num; thread_id++) {
    if ((m_core->m_fetch_ended[thread_id]) || (m_core->m_thread_reach_end[thread_id])) {
      continue;
    }


    frontend_s* fetch_data = m_core->get_trace_info(thread_id)->m_fetch_data;
    if (fetch_data != NULL && fetch_data->m_fetch_blocked) {
      continue;
    }
    can_fetch = true;
    //stat logic is wrong - nagesh
    if (m_knob_ptx_sim && *m_simBase->m_knobs->KNOB_MT_NO_FETCH_BR) {
      if (!check_br_ready(thread_id)) {
        can_fetch = false;
        //printf("(%ld) core id %d fetch id %d cannot fetch bec br_not_ready\n", core->get_cycle_count(), m_core_id, thread_id);
        if (count == 1) {
          STAT_EVENT(FETCH_THREAD_SKIP_BR_WAIT); 
        }
        count++;
      }
    }
    if (m_knob_ptx_sim && *m_simBase->m_knobs->KNOB_FETCH_ONLY_LOAD_READY) {
      if (!check_load_ready(thread_id)) {
        can_fetch = false;
        //printf("(%ld) core id %d fetch id %d cannot fetch bec ld_not_ready\n", core->get_cycle_count(), m_core_id, thread_id);
        if (count == 1) {
          STAT_EVENT(FETCH_THREAD_SKIP_LD_WAIT); 
        }
        count++; 
      }
    }

    if (can_fetch) {
      m_ready_thread_available = true;
    }
    if (can_fetch && m_core->m_last_fetch_cycle[thread_id] < last_fetch_cycle) {
      lrf_thread = thread_id;	
      last_fetch_cycle = m_core->m_last_fetch_cycle[thread_id];
    }
  }
  return lrf_thread;
}


///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
int fetch_internal_c::fetch_icount(void)
{
  Counter min_ops_to_be_dispatched = ~0;
  int min_ops_thread = -1;
  int count = 1;
  bool can_fetch;

  for (int thread_id = m_last_terminated_tid; thread_id < m_unique_scheduled_thread_num; thread_id++) {
    if ((m_core->m_fetch_ended[thread_id]) || (m_core->m_thread_reach_end[thread_id])) {
      continue;
    }

    frontend_s* fetch_data = m_core->get_trace_info(thread_id)->m_fetch_data;
    if (fetch_data != NULL && fetch_data->m_fetch_blocked) {
      continue;
    }
    can_fetch = true;
    //stat logic is wrong - nagesh
    if (m_knob_ptx_sim && *m_simBase->m_knobs->KNOB_MT_NO_FETCH_BR) {
      if (!check_br_ready(thread_id)) {
        can_fetch = false;
        if (count == 1) {
          STAT_EVENT(FETCH_THREAD_SKIP_BR_WAIT); 
        }
        count++;
      }
    }
    if (m_knob_ptx_sim && *m_simBase->m_knobs->KNOB_FETCH_ONLY_LOAD_READY) {
      if (!check_load_ready(thread_id)) {
        can_fetch = false;
        if (count == 1) {
          STAT_EVENT(FETCH_THREAD_SKIP_LD_WAIT); 
        }
        count++; 
      }
    }

    if (can_fetch) {
      m_ready_thread_available = true;
    }
    if (can_fetch && m_core->m_ops_to_be_dispatched[thread_id] < min_ops_to_be_dispatched){
      min_ops_thread = thread_id;	
      min_ops_to_be_dispatched = m_core->m_ops_to_be_dispatched[thread_id];
    }
  }
  return min_ops_thread;
}


///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
int fetch_internal_c::fetch_prirr(void)
{
    int try_again = 1;
    int fetch_id = 0;
    int arbiter_fetch_id = -1;
    bool can_fetch;

    DEBUG("m_core_id:%d m_running_thread_num:%d m_fetching_thread_num:%d m_unique_scheduled_thread_num:%d \n",
          m_core_id, m_running_thread_num, m_fetching_thread_num, m_unique_scheduled_thread_num);

    while (m_fetching_thread_num && try_again && (try_again <= m_unique_scheduled_thread_num)) {
      fetch_id = (m_fetch_arbiter++)%m_unique_scheduled_thread_num;

      if (m_core->m_fetch_ended[fetch_id] || m_core->m_thread_reach_end[fetch_id]) {
        DEBUG("m_core_id:%d thread_finished[%d]:%d thread_reach_end[%d]:%d \n",
              m_core_id, fetch_id, (m_core)->m_thread_finished[fetch_id] ,
              fetch_id, (m_core->m_thread_reach_end[fetch_id]));

        try_again++;
        continue;
      }

      frontend_s* fetch_data = m_core->get_trace_info(fetch_id)->m_fetch_data;
      if (fetch_data != NULL && fetch_data->m_fetch_blocked) {
        DEBUG("m_core_id:%d thread_finished[%d]:%d thread_reach_end[%d]:%d \n",
              m_core_id, fetch_id, (m_core)->m_thread_finished[fetch_id] ,
              fetch_id, (m_core->m_thread_reach_end[fetch_id]));
        try_again++;
        continue;
      }
      can_fetch = true;

      //not sure about stats - nagesh
      if (m_knob_ptx_sim && *m_simBase->m_knobs->KNOB_MT_NO_FETCH_BR) {
        if (!check_br_ready(fetch_id)) {
          can_fetch = false;
          if (try_again == 1) {
        	  STAT_EVENT(FETCH_THREAD_SKIP_BR_WAIT);
          }
        }

      }
      if (m_knob_ptx_sim && *m_simBase->m_knobs->KNOB_FETCH_ONLY_LOAD_READY) {
        if (!check_load_ready(fetch_id)) {
          can_fetch = false;
          if (try_again == 1) {
        	  STAT_EVENT(FETCH_THREAD_SKIP_LD_WAIT);
          }
        }
      }

      if (can_fetch) {
        m_ready_thread_available = true;
      }
      if (can_fetch && m_core->m_inst_fetched[fetch_id] < m_core->m_max_inst_fetched) {
        try_again = 0;
      }
      else if (can_fetch && m_core->m_inst_fetched[fetch_id] ==
    		   m_core->m_max_inst_fetched && arbiter_fetch_id == -1) {
        arbiter_fetch_id = fetch_id;
      }
      else {
        try_again++;
      }

    }
    if (try_again > m_unique_scheduled_thread_num) {
    	fetch_id = arbiter_fetch_id;
    }

    DEBUG_CORE(m_core_id, "m_core_id:%d try_agin:%d fetch_id:%d"
    		   " arbiter_fetch_id:%d \n", m_core_id, try_again,
    		   fetch_id, arbiter_fetch_id);
    return fetch_id;
}


///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
int fetch_internal_c::fetch_fairrr(void)
{
  Counter min_fetched_inst = ~0;
  int min_fetched_thread = -1;
  int default_thread = -1;
  int count = 1;
  bool can_fetch;

  if (0) {
    printf("%lld", min_fetched_inst);
  }

  for (int thread_id = m_last_terminated_tid;
		   thread_id < m_unique_scheduled_thread_num; thread_id++) {
    if ((m_core->m_fetch_ended[thread_id]) ||
        (m_core->m_thread_reach_end[thread_id])) {
      continue;
    }

    frontend_s* fetch_data = m_core->get_trace_info(thread_id)->m_fetch_data;
    if (fetch_data != NULL && fetch_data->m_fetch_blocked) {
      continue;
    }

    can_fetch = true;
    //stat logic is wrong - nagesh
    if (m_knob_ptx_sim && *m_simBase->m_knobs->KNOB_MT_NO_FETCH_BR) {
      if (!check_br_ready(thread_id)) {
        can_fetch = false;
        //printf("(%ld) core id %d fetch id %d cannot fetch bec br_not_ready\n", core->get_cycle_count(), m_core_id, thread_id);
        if (count == 1) {
          STAT_EVENT(FETCH_THREAD_SKIP_BR_WAIT); 
        }
        count++;
      }
    }
    if (m_knob_ptx_sim && *m_simBase->m_knobs->KNOB_FETCH_ONLY_LOAD_READY) {
      if (!check_load_ready(thread_id)) {
        can_fetch = false;
        if (count == 1) {
          STAT_EVENT(FETCH_THREAD_SKIP_LD_WAIT); 
        }
        count++; 
      }
    }

    if (can_fetch) {
      m_ready_thread_available = true;
    }
    if (can_fetch && m_core->m_inst_fetched[thread_id] <
    		m_core->m_max_inst_fetched) {
      min_fetched_thread = thread_id;	
      min_fetched_inst = m_core->m_inst_fetched[thread_id];
    }
    else if (can_fetch && m_core->m_inst_fetched[thread_id] ==
    		m_core->m_max_inst_fetched && default_thread == -1) {
      default_thread = thread_id;
    }
  }

  if (min_fetched_thread == -1) {
    min_fetched_thread = default_thread;
  }

  return min_fetched_thread;
}


///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
int fetch_internal_c::fetch_maxrr(void)
{
  Counter max_fetched_inst = 0;
  int max_fetched_thread = -1;
  int default_thread = -1;
  int count = 1;
  bool can_fetch;

  if (0) { // to prevent compilation error, jaekyu (11-3-2009)
    printf("%lld", max_fetched_inst);
  }

  for (int thread_id = m_last_terminated_tid;
		   thread_id < m_unique_scheduled_thread_num; thread_id++) {
    if ((m_core->m_fetch_ended[thread_id]) || (m_core->m_thread_reach_end[thread_id])) {
      continue;
    }

    frontend_s* fetch_data = m_core->get_trace_info(thread_id)->m_fetch_data;
    if (fetch_data != NULL && fetch_data->m_fetch_blocked) {
      continue;
    }

    can_fetch = true;
    //stat logic is wrong - nagesh
    if (m_knob_ptx_sim && *m_simBase->m_knobs->KNOB_MT_NO_FETCH_BR) {
      if (!check_br_ready(thread_id)) {
        can_fetch = false;
        if (count == 1) {
          STAT_EVENT(FETCH_THREAD_SKIP_BR_WAIT); 
        }
        count++;
      }
    }
    if (m_knob_ptx_sim && *m_simBase->m_knobs->KNOB_FETCH_ONLY_LOAD_READY) {
      if (!check_load_ready(thread_id)) {
        can_fetch = false;
        if (count == 1) {
          STAT_EVENT(FETCH_THREAD_SKIP_LD_WAIT); 
        }
        count++; 
      }
    }

    if (can_fetch) {
      m_ready_thread_available = true;
    }
    if (can_fetch && m_core->m_inst_fetched[thread_id] == m_core->m_max_inst_fetched) {
      max_fetched_thread = thread_id;	
      max_fetched_inst = m_core->m_max_inst_fetched;
    }
    else if (can_fetch && m_core->m_inst_fetched[thread_id] < m_core->m_max_inst_fetched && default_thread == -1) {
      default_thread = thread_id;
    }
  }

  if (max_fetched_thread == -1) {
    max_fetched_thread = default_thread;
  }

  return max_fetched_thread;
}


///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
int fetch_internal_c::fetch_blk_until_load(void)
{
  int thread_to_fetch = -1;
  int thread_id; 
  int block_index;
  int rr_index;
  int base_tid;

  for (thread_id = m_last_terminated_tid;
	   thread_id < m_unique_scheduled_thread_num; thread_id++) {
    if ((m_core->m_fetch_ended[thread_id]) || (m_core->m_thread_reach_end[thread_id]) ||
    	!check_load_ready(thread_id) || !check_br_ready(thread_id)) {
      continue;
    }
    frontend_s* fetch_data = m_core->get_trace_info(thread_id)->m_fetch_data;
    if (fetch_data != NULL && fetch_data->m_fetch_blocked) {
      continue;
    }
    break;
  }

  if (thread_id == m_unique_scheduled_thread_num) {
    return -1;
  }

  block_index = thread_id / m_simBase->m_no_threads_per_block;
  if (m_rr_index.find(block_index) == m_rr_index.end())
    m_rr_index[block_index] = 0;
  rr_index = m_rr_index[block_index];

  base_tid = block_index * m_simBase->m_no_threads_per_block;

  for (int i = 0; i < m_simBase->m_no_threads_per_block; i++) {
    thread_id = base_tid + rr_index;
    rr_index = (rr_index + 1) % m_simBase->m_no_threads_per_block;

    if ((m_core->m_fetch_ended[thread_id]) || (m_core->m_thread_reach_end[thread_id]) ||
         !check_load_ready(thread_id) || !check_br_ready(thread_id)) {
      continue;
    }

    frontend_s* fetch_data = m_core->get_trace_info(thread_id)->m_fetch_data;
    if (fetch_data != NULL && fetch_data->m_fetch_blocked) {
      continue;
    }

    thread_to_fetch = thread_id;
    break;
  }
  m_rr_index[block_index] = rr_index;

  return thread_to_fetch;
}


///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
int fetch_internal_c::fetch_all(void)
{
  Counter max_fetched_inst = 0;
  int max_fetched_thread = -1;
  int default_thread = -1;

  for (int thread_id = m_last_terminated_tid;
		   thread_id < m_unique_scheduled_thread_num; thread_id++) {
    if ((m_core->m_fetch_ended[thread_id]) || (m_core->m_thread_reach_end[thread_id]) ||
    	 !check_load_ready(thread_id) || !check_br_ready(thread_id)) {
      continue;
    }

    frontend_s* fetch_data = m_core->get_trace_info(thread_id)->m_fetch_data;
    if (fetch_data != NULL && fetch_data->m_fetch_blocked) {
      continue;
    }

    if (default_thread == -1) {
      default_thread = thread_id;
    }

    if (max_fetched_inst <
    		m_core->get_trace_info(thread_id)->m_sections.front()->m_section_length) {
      max_fetched_inst =
    		m_core->get_trace_info(thread_id)->m_sections.front()->m_section_length;
      max_fetched_thread = thread_id;
    }
  }

  if (max_fetched_thread == -1) {
    return default_thread;
  }

  return max_fetched_thread;
}


///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
int fetch_internal_c::fetch_random(void)
{
  // round robin first
  // m_icache miss skip to other threads
  // l2 miss skip to other threads  (GPU_MODE)
  bool can_fetch = false; 
	    
  int fetch_id; 
  int try_again = 1; 
  float random; 

  DEBUG("m_core_id:%d m_running_thread_num:%d m_fetching_thread_num:%d m_unique_scheduled_thread_num:%d \n",
        m_core_id, m_running_thread_num, m_fetching_thread_num, m_unique_scheduled_thread_num);

  while(m_fetching_thread_num && try_again && (try_again <= m_unique_scheduled_thread_num)) {
    random = ((float)rand())/RAND_MAX;
    fetch_id = (int)(random*m_unique_scheduled_thread_num)%m_unique_scheduled_thread_num;

    if (((m_core)->m_fetch_ended[fetch_id]) || (m_core->m_thread_reach_end[fetch_id]) ||
    		(*m_simBase->m_knobs->KNOB_NO_FETCH_ON_ICACHE_MISS && !check_fetch_ready(fetch_id))) {

    	DEBUG("m_core_id:%d thread_finished[%d]:%d thread_reach_end[%d]:%d \n",
            m_core_id, fetch_id, (m_core)->m_thread_finished[fetch_id] , 
            fetch_id, (m_core->m_thread_reach_end[fetch_id]));
		try_again++;
        continue;
    }

    frontend_s* fetch_data = m_core->get_trace_info(fetch_id)->m_fetch_data;
    if (fetch_data != NULL && fetch_data->m_fetch_blocked) {
      DEBUG("m_core_id:%d thread_finished[%d]:%d thread_reach_end[%d]:%d \n",
            m_core_id, fetch_id, (m_core)->m_thread_finished[fetch_id] , 
            fetch_id, (m_core->m_thread_reach_end[fetch_id]));
		    
      try_again++;
      continue;
    }
		
    can_fetch = true;
		
    //not sure about stats - nagesh
    if (m_knob_ptx_sim && *m_simBase->m_knobs->KNOB_MT_NO_FETCH_BR) {
      if (!check_br_ready(fetch_id)) {
        can_fetch = false;
        if (try_again == 1) {
        	STAT_EVENT(FETCH_THREAD_SKIP_BR_WAIT);
        }
      }
		    
    }
    if (m_knob_ptx_sim && *m_simBase->m_knobs->KNOB_FETCH_ONLY_LOAD_READY) {
      if (!check_load_ready(fetch_id)) {
        can_fetch = false;
        if (try_again == 1) STAT_EVENT(FETCH_THREAD_SKIP_LD_WAIT); 
      }
    }

    if (can_fetch) {
      m_ready_thread_available = true;
      try_again = 0; 
    }
    else {
      try_again++;
    }
  }
  if (try_again > m_unique_scheduled_thread_num)  {
    float random = ((float)rand())/RAND_MAX;
    fetch_id = (int)(random*m_unique_scheduled_thread_num)%m_unique_scheduled_thread_num; 
  }
	    
  DEBUG_CORE(m_core_id, "m_core_id:%d try_agin:%d fetch_id:%d random:%0.2f "
		     "m_unique_scheduled_thread_num:%d  \n",
             m_core_id, try_again, fetch_id, random,m_unique_scheduled_thread_num);

  return fetch_id;
}


///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
/*
int fetch_internal_c::fetch_fairperiod(void)
{    
  Counter min_fetched_inst = ~0;
  int min_fetched_thread = -1;
  //int default_thread = -1;
  int max_fetched_thread = -1; 
  Counter max_fetched_inst = 0; 
  int count = 1;
  bool can_fetch;
  static Counter last_fair_period_chk_time; 
  static int fair_check_freq = 0; 

		
  if (g_simulation_cycle > (last_fair_period_chk_time + *m_simBase->m_knobs->KNOB_FETCH_FAIR_PERIOD)) {
		    
    fair_check_freq++; 
    bool start_fair =!(check_stop_fair()); 

    if (*m_simBase->m_knobs->KNOB_FETCH_FAIR_MERGE) { 
			
      long merge_count = read_merge_count((int)m_core_id, -1);
      reset_merge_b_count();
      if(merge_count> *m_simBase->m_knobs->KNOB_FETCH_FAIR_MERGE_TH) {
        start_fair = true; 
        STAT_EVENT(SET_START_FAIR_MERGE);
      }
      else {
        start_fair = false; 
        STAT_EVENT(SET_STOP_FAIR_MERGE);
      }
      DEBUG("read_merge_count:%lld stop_fair:%d m_core_id:%d FAIRPERIOD\n", merge_count, check_stop_fair(), m_core_id); 
    }
    if (*m_simBase->m_knobs->KNOB_FETCH_FAIR_TSHARE) { 
      if (!(fair_check_freq%*m_simBase->m_knobs->KNOB_FETCH_FAIR_TSHARE_FREQ)) {
			    
        int share_count = global_check_b_hit(0, -1); 
        // int share_count = global_check_b_hit(m_core_id, -1); 
        // reset_b_hit_count(m_core_id);
        reset_b_hit_count(0);
			    
        if (share_count > *m_simBase->m_knobs->KNOB_FETCH_FAIR_TSHARE_TH) {
          start_fair = true; 
          STAT_EVENT(SET_START_FAIR_TSHARE);
        }
        else if(!*m_simBase->m_knobs->KNOB_FETCH_FAIR_MERGE) {
          start_fair = false; 			    
          STAT_EVENT(SET_STOP_FAIR_TSHARE);
        }
        else {
          STAT_EVENT(SET_STOP_FAIR_TSHARE);
        }
        DEBUG("read_tshare_count:%lld m_core_id:%d stop_fair:%d FAIRPERIOD\n",
        	  share_count, m_core_id, check_stop_fair());
      }
			
    }
		    
    if (start_fair) { 
      set_start_fair(); 
      STAT_EVENT(SET_START_FAIR); 
    }
    else { 
      set_stop_fair(); 
      STAT_EVENT(SET_STOP_FAIR);
    }
		    
    last_fair_period_chk_time = g_simulation_cycle; 
  }
  STAT_EVENT(FAIR_FETCH_FAIR+check_stop_fair()); 
  for (int ii = m_last_terminated_tid; ii < m_unique_scheduled_thread_num; ii++)  {

    int thread_id = (m_fetch_arbiter+ii)%m_unique_scheduled_thread_num;
    if ((m_core->m_fetch_ended[thread_id]) || (m_core->m_thread_reach_end[thread_id]) ||
    	(*m_simBase->m_knobs->KNOB_NO_FETCH_ON_ICACHE_MISS && !check_fetch_ready(thread_id))) {
    	continue;
    }

    frontend_s* fetch_data = m_core->get_trace_info(thread_id)->m_fetch_data;
    if (fetch_data != NULL && fetch_data->m_fetch_blocked) {
      continue;
    }


    can_fetch = true;
    //stat logic is wrong - nagesh
    if (m_knob_ptx_sim && *m_simBase->m_knobs->KNOB_MT_NO_FETCH_BR) {
      if (!check_br_ready(thread_id)) {
        can_fetch = false;
        if (count == 1) {
          STAT_EVENT(FETCH_THREAD_SKIP_BR_WAIT); 
        }
        count++;
      }
    }
    if (m_knob_ptx_sim && *m_simBase->m_knobs->KNOB_FETCH_ONLY_LOAD_READY) {
      if (!check_load_ready(thread_id)) {
        can_fetch = false;
        if (count == 1) {
          STAT_EVENT(FETCH_THREAD_SKIP_LD_WAIT); 
        }
        count++; 
      }
    }

			
    if (can_fetch) {
      m_ready_thread_available = true;
    }

    // fair among ready threads ... 
    // want to have round robin for asymmetric, no merging cases ...

    if (!check_stop_fair()) {
      if (can_fetch && m_core->m_inst_fetched[thread_id] < min_fetched_inst) {
        min_fetched_thread = thread_id;	
        min_fetched_inst = m_core->m_inst_fetched[thread_id];
      }
			    
      if (can_fetch && m_core->m_inst_fetched[thread_id] > max_fetched_inst) {
        max_fetched_thread = thread_id;	
        max_fetched_inst = m_core->m_inst_fetched[thread_id];
      }
    }
			    
    else if (can_fetch) {
			    
      m_fetch_arbiter=m_fetch_arbiter + ii + 1; 
			    
      DEBUG("m_core_id:%d ii:%d fetch_arbiter:%d fetch_id:%d  min_fetched_inst:%d "
    		"max_fetched_inst:%d check_stop_fair:%d FAIRPERIOD\n", m_core_id, ii,
    		m_fetch_arbiter, thread_id, min_fetched_inst, max_fetched_inst,
    		check_stop_fair());

      return thread_id;  
    }
}

  if (check_stop_fair()) {
	  min_fetched_thread=-1;
  }
  else {
    m_fetch_arbiter++;
  }
		

  DEBUG("m_core_id:%d fetch_arbiter:%d fetch_id:%d min_fetched_inst:%d  max_fetched_inst:%d check_stop_fair:%d FAIRPERIOD\n",
        m_core_id, m_fetch_arbiter, min_fetched_thread, min_fetched_inst, max_fetched_inst, check_stop_fair()); 
		
  return min_fetched_thread;
}
*/


///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
int fetch_internal_c::fetch_bar(void)
{
  Counter max_fetched_inst = 0;
  int max_fetched_thread = -1;
  int default_thread = -1;

  for (int thread_id = m_last_terminated_tid; thread_id < m_unique_scheduled_thread_num; thread_id++)  {
    if ((m_core->m_fetch_ended[thread_id]) || (m_core->m_thread_reach_end[thread_id]) ||
        !check_load_ready(thread_id) || !check_br_ready(thread_id)) {
      continue;
    }

    frontend_s* fetch_data = m_core->get_trace_info(thread_id)->m_fetch_data;
    if (fetch_data != NULL && fetch_data->m_fetch_blocked) {
      continue;
    }

    if (default_thread == -1) {
      default_thread = thread_id;
    }

    if (max_fetched_inst <
    		m_core->get_trace_info(thread_id)->m_bar_sections.front()->m_section_length) {
      max_fetched_inst =
    		  m_core->get_trace_info(thread_id)->m_bar_sections.front()->m_section_length;
      max_fetched_thread = thread_id;
    }
  }

  if (max_fetched_thread == -1) {
    return default_thread;
  }

  return max_fetched_thread;
}

int fetch_internal_c::fetch_mem(void)
{
  Counter max_fetched_inst = 0;
  int max_fetched_thread = -1;
  int default_thread = -1;

  for (int thread_id = m_last_terminated_tid; thread_id < m_unique_scheduled_thread_num; thread_id++) {
    if ((m_core->m_fetch_ended[thread_id]) || (m_core->m_thread_reach_end[thread_id]) ||
    	 !check_load_ready(thread_id) || !check_br_ready(thread_id)) {
      continue;
    }

    frontend_s* fetch_data = m_core->get_trace_info(thread_id)->m_fetch_data;
    if (fetch_data != NULL && fetch_data->m_fetch_blocked) {
      continue;
    }

    if (default_thread == -1) {
      default_thread = thread_id;
    }

    if (max_fetched_inst <
    		m_core->get_trace_info(thread_id)->m_mem_sections.front()->m_section_length) {
      max_fetched_inst =
    		m_core->get_trace_info(thread_id)->m_mem_sections.front()->m_section_length;
      max_fetched_thread = thread_id;
    }
  }

  if (max_fetched_thread == -1) {
    return default_thread;
  }

  return max_fetched_thread;
}


///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
int fetch_internal_c::fetch_mem_bar(void)
{
  Counter max_fetched_inst = 0;
  int max_fetched_thread = -1;
  int default_thread = -1;

  for (int thread_id = m_last_terminated_tid; thread_id < m_unique_scheduled_thread_num; thread_id++) {
    if ((m_core->m_fetch_ended[thread_id]) || (m_core->m_thread_reach_end[thread_id]) ||
    	 !check_load_ready(thread_id) || !check_br_ready(thread_id)) {
      continue;
    }

    frontend_s* fetch_data = m_core->get_trace_info(thread_id)->m_fetch_data;
    if (fetch_data != NULL && fetch_data->m_fetch_blocked) {
      continue;
    }
    if (default_thread == -1) {
      default_thread = thread_id;
    }

    if (max_fetched_inst <
    		m_core->get_trace_info(thread_id)->m_mem_bar_sections.front()->m_section_length) {
      max_fetched_inst =
    		  m_core->get_trace_info(thread_id)->m_mem_bar_sections.front()->m_section_length;
      max_fetched_thread = thread_id;
    }
  }

  if (max_fetched_thread == -1) {
    return default_thread;
  }
  return max_fetched_thread;
}


int fetch_internal_c::fetch_mem_bar_new(void)
{
  Counter max_inst = 0;
  int max_mem_inst = -1;
  int next_fetch_thread = -1;
  int default_thread = -1;

  Counter inst;
  int mem_inst;

  for (int thread_id = m_last_terminated_tid; thread_id < m_unique_scheduled_thread_num; ++thread_id) {
    if ((m_core->m_fetch_ended[thread_id]) || (m_core->m_thread_reach_end[thread_id]) ||
        !check_load_ready(thread_id) || !check_br_ready(thread_id)) {
      continue;
    }
    frontend_s* fetch_data = m_core->get_trace_info(thread_id)->m_fetch_data;
    if (fetch_data != NULL && fetch_data->m_fetch_blocked)
      continue;

    if (default_thread == -1) {
      default_thread = thread_id;
    }

    inst =
         m_core->get_trace_info(thread_id)->m_bar_sections.front()->m_section_length;
    mem_inst =
    	 m_core->get_trace_info(thread_id)->m_mem_for_bar_sections.front()->m_section_length;

    if (max_mem_inst < mem_inst || (max_mem_inst == mem_inst && max_inst < inst)) {
      max_mem_inst = mem_inst;
      max_inst = inst;
      next_fetch_thread = thread_id;
    }
  }

  if (next_fetch_thread == -1)  {
    return default_thread;
  }
  return next_fetch_thread;
}


///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
int fetch_internal_c::fetch_rr_mem_bar_new(void)
{
#if 0
  int block_index = 0;
  int iter_count = 0;
  int default_thread = -1;

  int mem_inst;
  Counter inst;
  int max_mem_inst = -1;
  Counter max_inst = 0;
  int next_fetch_thread = -1;

  while (iter_count < MAX_NUM_BLOCKS) {
    iter_count++;

    block_index = m_block_arbiter;
    m_block_arbiter = (m_block_arbiter + 1) % MAX_NUM_BLOCKS;

    if (m_core->m_assigned_blocks[block_index] != -1 &&
    	!g_block_schedule_info[m_core->m_assigned_blocks[block_index]]->m_retired) {
      /* using MIN2 because for first cycle, trace of only one thread is pre-read */
      for (int thread_id = block_index * g_no_threads_per_block;
    		   thread_id < MIN2(((block_index + 1) * g_no_threads_per_block),
    				   m_unique_scheduled_thread_num); thread_id++){
        if ((m_core->m_fetch_ended[thread_id]) || (m_core->m_thread_reach_end[thread_id]) ||
        	!check_load_ready(thread_id) || !check_br_ready(thread_id)) {
          continue;
        }
        frontend_s* fetch_data = m_core->get_trace_info(thread_id)->m_fetch_data;
        if (fetch_data != NULL && fetch_data->m_fetch_blocked) {
          continue;
        }
        if (default_thread == -1) {
          default_thread = thread_id;
        }

        inst = m_core->get_trace_info(thread_id)->m_bar_sections.front()->m_section_length;
        mem_inst =
        	m_core->get_trace_info(thread_id)->m_mem_for_bar_sections.front()->m_section_length;

        if (max_mem_inst < mem_inst || (max_mem_inst == mem_inst && max_inst < inst)) {
          max_mem_inst = mem_inst;
          max_inst = inst;
          next_fetch_thread = thread_id;
        }
      }
    }

    if (next_fetch_thread != -1) {
      break;
    }
  }

  return next_fetch_thread;
#endif
  return 0;
}


///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
/*
int fetch_internal_c::fetch_pred_longest_first(void)
{
  // first time m_unique_scheduled_thread_num = 1
  if (pred_first_time) {
    pred_first_time = 0;
    m_prev_unique_scheduled_thread_num = m_unique_scheduled_thread_num;
    m_fetch_arbiter++;
    return 0;
  }

  if (m_prev_unique_scheduled_thread_num != m_unique_scheduled_thread_num) {
    m_prev_unique_scheduled_thread_num = m_unique_scheduled_thread_num;
    m_dec_scheduled_thread_num = m_unique_scheduled_thread_num;
    m_dec_fetch_arbiter = 0;
    m_rr_counter = 1;
  }


  int no_threads;
  int arbiter_id;
  if (m_rr_counter != 0) {
    no_threads = m_unique_scheduled_thread_num;
    arbiter_id = m_fetch_arbiter;
  }
  else {
    no_threads = m_dec_scheduled_thread_num;
    arbiter_id = m_dec_fetch_arbiter;
  }

  if (0) { // to prevent compilation error, jaekyu (11-3-2009)
    printf("%d %d", no_threads, arbiter_id);
  }

  bool dec_first;
  if (m_rr_counter != 0) {
    dec_first = false;
  }
  else {
    dec_first = true;
  }

  bool rr_fetch_id_after_dec_scheduled_thread_num;

  if (m_fetch_arbiter >= m_dec_scheduled_thread_num) {
    rr_fetch_id_after_dec_scheduled_thread_num = true;
  }
  else {
    rr_fetch_id_after_dec_scheduled_thread_num = false;
  }

  int count = 0;
  int thread_id = -1;
  int fetch_thread_id = -1;
  while (m_fetching_thread_num && count < m_unique_scheduled_thread_num) {
    if (m_rr_counter != 0) {
      thread_id = m_fetch_arbiter;
      m_fetch_arbiter = (m_fetch_arbiter + 1) % m_unique_scheduled_thread_num;
      if (m_fetch_arbiter == 0) {
        m_rr_counter = (m_rr_counter + 1) % m_dec_rr_freq;
      }
    }
    else {
      thread_id = m_dec_fetch_arbiter;
      m_dec_fetch_arbiter = (m_dec_fetch_arbiter + 1) % m_dec_scheduled_thread_num;
      if (m_dec_fetch_arbiter == 0) {
        m_rr_counter = (m_rr_counter + 1) % m_dec_rr_freq;
        if (dec_first) {
          count = 0;
        }
        else {
          if (rr_fetch_id_after_dec_scheduled_thread_num) {
            m_fetch_arbiter = m_dec_scheduled_thread_num;
          }
        }

        m_dec_scheduled_thread_num--;
        if (m_dec_scheduled_thread_num == 0) {
          m_dec_scheduled_thread_num = m_unique_scheduled_thread_num;
        }
      }

    }

    if ((m_core->m_fetch_ended[thread_id]) || (m_core->m_thread_reach_end[thread_id]) ||
         !check_load_ready(thread_id) || !check_br_ready(thread_id)) {
      count++;
      continue;
    }
    
    frontend_s* fetch_data = m_core->get_trace_info(thread_id)->m_fetch_data;
    if (fetch_data != NULL && fetch_data->m_fetch_blocked) {
      count++;
      continue;
    }
    else {
      fetch_thread_id = thread_id;
      break;
    }
  }
  return fetch_thread_id;
}
*/


///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
int fetch_internal_c::fetch_pred_longest_first_new(void)
{
  int thread_id;
  int fetch_thread_id = -1;
  int count = 0;

  if (m_prev_unique_scheduled_thread_num != m_unique_scheduled_thread_num) {
    m_prev_unique_scheduled_thread_num = m_unique_scheduled_thread_num;
    m_dec_scheduled_thread_num = m_unique_scheduled_thread_num;
    m_dec_fetch_arbiter = 0;
    m_rr_counter = 1;
  }

  if (m_rr_counter != 0) {
    while (m_fetching_thread_num && count < m_unique_scheduled_thread_num) {
      thread_id = m_fetch_arbiter;
      m_fetch_arbiter = (m_fetch_arbiter + 1) % m_unique_scheduled_thread_num;

      if ((m_core->m_fetch_ended[thread_id]) || (m_core->m_thread_reach_end[thread_id]) ||
    	   !check_load_ready(thread_id) || !check_br_ready(thread_id)) {
        count++;
        continue;
      }

      frontend_s* fetch_data = m_core->get_trace_info(thread_id)->m_fetch_data;
      if (fetch_data != NULL && fetch_data->m_fetch_blocked) {
        count++;
        continue;
      }
      else {
        fetch_thread_id = thread_id;
        break;
      }
    }
  }
  else {
    while (m_fetching_thread_num && count < m_dec_scheduled_thread_num) {
      thread_id = m_dec_fetch_arbiter;
      m_dec_fetch_arbiter = (m_dec_fetch_arbiter + 1) % m_dec_scheduled_thread_num;

      if (m_dec_fetch_arbiter == 0) {
        m_dec_scheduled_thread_num--;
        if (m_dec_scheduled_thread_num == 0) {
          m_dec_scheduled_thread_num = m_unique_scheduled_thread_num;
        }
      }

      if ((m_core->m_fetch_ended[thread_id]) || (m_core->m_thread_reach_end[thread_id]) ||
    	   !check_load_ready(thread_id) || !check_br_ready(thread_id))  {
        count++;
        continue;
      }


      frontend_s* fetch_data = m_core->get_trace_info(thread_id)->m_fetch_data;
      if (fetch_data != NULL && fetch_data->m_fetch_blocked) {
        count++;
        continue;
      }
      else {
        fetch_thread_id = thread_id;
        break;
      }
    }

    if (fetch_thread_id == -1) {
      while (m_fetching_thread_num && count < m_unique_scheduled_thread_num) {
        thread_id = m_dec_fetch_arbiter + 1;
        if (thread_id < m_unique_scheduled_thread_num) {
          if ((m_core->m_fetch_ended[thread_id]) || (m_core->m_thread_reach_end[thread_id]) ||
        		  !check_load_ready(thread_id) || !check_br_ready(thread_id)) {
            count++;
            continue;
          }

          frontend_s* fetch_data = m_core->get_trace_info(thread_id)->m_fetch_data;
          if (fetch_data != NULL && fetch_data->m_fetch_blocked) {
            count++;
            continue;
          }
          else {
            fetch_thread_id = thread_id;
            break;
          }
        }
      }
    }
  }

  m_rr_counter = (m_rr_counter + 1) % m_dec_rr_freq;
  return fetch_thread_id;
}


///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
int fetch_internal_c::fetch_reconv_long(void)
{
  int thread_id;
  int fetch_thread_id = -1;
  int count = 0;

  while (m_fetching_thread_num && count < m_unique_scheduled_thread_num) {
    thread_id = m_fetch_arbiter;


    if ((m_core->m_fetch_ended[thread_id]) || (m_core->m_thread_reach_end[thread_id])
        || !check_load_ready(thread_id)
        || !check_br_ready(thread_id) || (*m_simBase->m_knobs->KNOB_NO_FETCH_ON_ICACHE_MISS &&
        !check_fetch_ready(thread_id))) {
      count++;
      //default_thread = thread_id;
      //fetch_thread_id = thread_id;//to be deleted
      m_fetch_arbiter = (m_fetch_arbiter + 1) % m_unique_scheduled_thread_num;
      continue;
    }

    frontend_s* fetch_data = m_core->get_trace_info(thread_id)->m_fetch_data;
    if (fetch_data != NULL && fetch_data->m_fetch_blocked) {
      count++;
      continue;
    }
    else {
      fetch_thread_id = thread_id;
      break;
    }
  }

  if (fetch_thread_id != -1) {
    frontend_s* fetch_data = m_core->get_trace_info(fetch_thread_id)->m_fetch_data;
    if (fetch_data->m_reconv_data.m_work_counter) {
      fetch_data->m_extra_fetch++;

      if (fetch_data->m_extra_fetch == 3) {
        fetch_data->m_reconv_data.m_work_counter--;
        fetch_data->m_extra_fetch = 0;
        m_fetch_arbiter = (m_fetch_arbiter + 1) % m_unique_scheduled_thread_num;
      }
    }
    else {
      m_fetch_arbiter = (m_fetch_arbiter + 1) % m_unique_scheduled_thread_num;
    }
    return fetch_thread_id;
  }
  else {
    return fetch_thread_id;
  }
}


///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
int fetch_internal_c::fetch_rr_block(void)
{
#if 0
  int block_index = 0;
  int iter_count = 0;

  int next_fetch_thread = -1;

  while (iter_count < MAX_NUM_BLOCKS) {
    iter_count++;

    block_index = m_block_arbiter;
    m_block_arbiter = (m_block_arbiter + 1) % MAX_NUM_BLOCKS;

    if (m_core->m_assigned_blocks[block_index] != -1 &&
        !g_block_schedule_info[m_core->m_assigned_blocks[block_index]]->m_retired) {
      /* using MIN2 because for first cycle, trace of only one thread is pre-read */
      for (int thread_id = block_index * g_no_threads_per_block;
    		   thread_id < MIN2(((block_index + 1) * g_no_threads_per_block),
    				   m_unique_scheduled_thread_num); thread_id++) {
        if ((m_core->m_fetch_ended[thread_id]) || (m_core->m_thread_reach_end[thread_id])
            || !check_load_ready(thread_id)
            || !check_br_ready(thread_id)
            || (*m_simBase->m_knobs->KNOB_NO_FETCH_ON_ICACHE_MISS && !check_fetch_ready(thread_id))) {
          continue;
        }
        frontend_s* fetch_data = m_core->get_trace_info(thread_id)->m_fetch_data;
        if (fetch_data != NULL && fetch_data->m_fetch_blocked) {
          continue;
        }

        next_fetch_thread = thread_id;
        break;
      }
    }

    if (next_fetch_thread != -1) {
      break;
    }
  }

  return next_fetch_thread;
#endif
  return 0;
}


///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
int fetch_internal_c::fetch_row_hit(void)
{
  int count = 0;
  int row_hit_arbiter = m_fetch_arbiter;
  int fetch_id = 0; 

  while (count < m_unique_scheduled_thread_num) {
    count++;
    fetch_id = row_hit_arbiter;
    if ((m_core->m_fetch_ended[fetch_id]) || (m_core->m_thread_reach_end[fetch_id])
        || !check_load_ready(fetch_id)
        || !check_br_ready(fetch_id)
        || (*m_simBase->m_knobs->KNOB_NO_FETCH_ON_ICACHE_MISS && !check_fetch_ready(fetch_id))) {
      continue;
    }

    frontend_s* fetch_data = m_core->get_trace_info(fetch_id)->m_fetch_data;
    if (fetch_data != NULL && fetch_data->m_fetch_blocked) {
      continue;
    }
    if (m_prev_bid != -1 && fetch_data->m_next_bid != -1) {
      if (m_prev_bid == fetch_data->m_next_bid && m_prev_rowid == fetch_data->m_next_rowid) {
        return fetch_id;
      }
    }
    row_hit_arbiter = (row_hit_arbiter + 1) % m_unique_scheduled_thread_num;
  }

  int try_again = 1; 
  int arbiter_fetch_id = -1; 
  bool can_fetch;

  while(m_fetching_thread_num && try_again && (try_again <= m_unique_scheduled_thread_num)) {
    fetch_id = (m_fetch_arbiter++)%m_unique_scheduled_thread_num; 
    if (((m_core)->m_fetch_ended[fetch_id]) || (m_core->m_thread_reach_end[fetch_id])) {
      try_again++;
      continue; 
    }
    
    frontend_s* fetch_data = m_core->get_trace_info(fetch_id)->m_fetch_data;
    if (fetch_data != NULL && fetch_data->m_fetch_blocked) {
      try_again++;
      continue;
    }

    can_fetch = true;

    if (*m_simBase->m_knobs->KNOB_MT_NO_FETCH_BR) {
      if (!check_br_ready(fetch_id)) {
        can_fetch = false;
        if (try_again == 1) {
        	STAT_EVENT(FETCH_THREAD_SKIP_BR_WAIT);
        }
      }

    }
    if (*m_simBase->m_knobs->KNOB_FETCH_ONLY_LOAD_READY) {
      if (!check_load_ready(fetch_id)) {
        can_fetch = false;
        if (try_again == 1) STAT_EVENT(FETCH_THREAD_SKIP_LD_WAIT); 
      }
    }
    if (can_fetch) {
      m_ready_thread_available = true;
      try_again = 0; 
    }
    else {
      try_again++;
    }
  }
  if (try_again > m_unique_scheduled_thread_num) {
	  fetch_id = arbiter_fetch_id;
  }
  return fetch_id; 
}

