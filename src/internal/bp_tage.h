/*
Copyright (c) <2012>, <Georgia Institute of Technology> All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted 
provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions 
and the following disclaimer.

Redistributions in binary form must reproduce the above copyright notice, this list of 
conditions and the following disclaimer in the documentation and/or other materials provided 
with the distribution.

Neither the name of the <Georgia Institue of Technology> nor the names of its contributors 
may be used to endorse or promote products derived from this software without specific prior 
written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR 
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY 
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
POSSIBILITY OF SUCH DAMAGE.
*/


/**********************************************************************************************
 * File         : bp_tage.h 
 * Author       : Dilan Manatunga 
 * Date         : 
 * SVN          : 
 * Description  : tage branch predictor 
                  created from tage source code 
 *********************************************************************************************/

#ifndef BP_TAGE_H_INCLUDED 
#define BP_TAGE_H_INCLUDED 


#include "../global_defs.h"
#include "../bp.h"
#include <bitset>

#define MAXHIST 131
#define MINHIST 5

typedef bitset < MAXHIST > history_t;
///////////////////////////////////////////////////////////////////////////////////////////////
/// \brief TAGE branch predictor
///
/// TAGE branch predictor class
///////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Cyclic shift register for folding a long global history into a smaller number of bits
 */
class folded_history
{
  public:

    void init(macsim_c* simBase, int32 original_length, int32 compressed_length);

    void update(history_t h);
  
  public:
    macsim_c* m_simBase;
    uns32 comp;
    int32 CLENGTH;
    int32 OLENGTH;
    int32 OUTPOINT;
};

/**
 * Bimodal table entry
 */ 
class bentry
{
  public:
    /**
     * Bimodal entry constuctor
     */
    bentry();

  public:
    int8 pred;
    int8 hyst;
};

/**
 * Global table entry
 */
class gentry
{
  public:
    /**
     * Global entry constructor
     */
    gentry();
         
  public:
    int8 ctr;
    uns16 tag;
    int8 ubit;  
};


class bp_tage_c : public bp_dir_base_c 
{
  public:
    /**
     * TAGE BP constructor
     */
    bp_tage_c(macsim_c* simBase);

    /**
     * TAGE BP destructor
     */
    ~bp_tage_c(void) {} 

    /**
     * Predict a branch instruction
     */
    uns8 pred(uop_c *uop);
    
    /**
     * Update branch predictor when a branch instruction is resolved.
     */
    void update(uop_c *uop);
    
    /**
     * Called to recover the bp when a misprediction is realized 
     */
    void recover(recovery_info_c* recovery_info); 

  private:
    /**
     * The number of history lengths to use (equals number of global entry tables)
     */
    uns32 NHIST; 
    /**
     * The number of bits in a tag 
     */
    uns32 TBITS;
    /**
     * The number of bits in counter
     */
    int32 CBITS;
    /**
     * The size of the index for base table - log(BASE_TABLE_SIZE)
     */
    uns32 LOGB;
    /**
     * The size of the index for global entry table - LOG(TAGGED_TABLE_SIZE)
     */
    uns32 LOGG; 
    /**
     * Base bimodal table
     */
    bentry *btable;
    /**
     * Array of global entry tables (array length equals number of components used
     */
    gentry **gtable;
    /**
     * Array for storing the length of the history for each component
     */
    int32 *hist_lens;
    /**
     * Global path history 
     */
    history_t ghist;

    int32 PWIN;
    int32 TICK;
    int32 phist;
    folded_history *ch_i;
    folded_history **ch_t;

    /**
     * Uop index for bimodal table
     */
    int32 BI;
    /**
     * Uop indicies for global tables
     */
    int32* GI;
    /**
     * Seed for pseudo random generator table
     */
    int32 Seed;
  private:
    /**
     * Private constructor
     * Do not implement
     */
    bp_tage_c(const bp_tage_c& rhs);

    /**
     * Overridden operator =
     */
    const bp_tage_c& operator=(const bp_tage_c& rhs);
    
    /**
     * Computes index to bimodal table
     */ 
    int32 bindex(Addr pc);
    /**
     * Computes index to global entry table
     */
    int32 gindex(Addr pc, int32 bank);
    /**
     * Function to mix path history for gindex 
     */
    int32 f(int32 a, int32 size, int32 bank);
    /**
     * Function to compute tag value in global entry tables
     */
    uns16 gtag(Addr pc, int32 bank);
    /**
     *  up-down saturating counter
     */
    void ctrupdate (int8 &ctr, uns8 taken, int32 nbits);
    /**
     * Identify a prediction for given pc address
     */
    uns8 read_prediction(Addr pc, int32 &bank, uns8 &altpred);
    /**
     * Get bimodal prediction
     */
    uns8 getbim(Addr pc);
    /**
     * Update the bimodal predictor
     */
    void baseupdate(Addr pc, uns8 Taken);
    /**
     * Simple pseudo random number generator based on linear feedback shift register
     */
    int32 MYRANDOM();
};

#endif // BP_TAGE_H_INCLUDED 

