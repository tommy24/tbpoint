/**********************************************************************************************
 * File         : internal/cache_internal.h
 * Author       : Jaekyu Lee
 * Date         : 1/27/2011 
 * SVN          : $Id: frontend.h 915 2009-11-20 19:13:07Z kacear $:
 * Description  : various dram policies (not seen to public)
 *********************************************************************************************/


#ifndef CACHE_INTERNAL_H
#define CACHE_INTERNAL_H


#include "../global_defs.h"
#include "../global_types.h"
#include "../dram.h"


// To transparently link internal modules, we need to have
// 1. wrapper class to cache_internal_c
// 2. singleton wrapper entry
class cache_internal_wrapper_c
{
  public:
    cache_internal_wrapper_c();
    ~cache_internal_wrapper_c();

    static cache_internal_wrapper_c Singleton;
};

#endif
