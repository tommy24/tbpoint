/*
Copyright (c) <2012>, <Georgia Institute of Technology> All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted 
provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions 
and the following disclaimer.

Redistributions in binary form must reproduce the above copyright notice, this list of 
conditions and the following disclaimer in the documentation and/or other materials provided 
with the distribution.

Neither the name of the <Georgia Institue of Technology> nor the names of its contributors 
may be used to endorse or promote products derived from this software without specific prior 
written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR 
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY 
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
POSSIBILITY OF SUCH DAMAGE.
*/


/* ********************************************************************************************
 * File         : bp_tage.cc
 * Author       : Dilan Manatunga 
 * Date         :  
 * SVN          : 
 * Description  : tage branch predictor
 *********************************************************************************************/


#include <cstdlib>
#include <math.h>
#include "../bp.h"
#include "bp_tage.h"
#include "../utils.h"
#include "../debug_macros.h"
#include "../assert_macros.h"
#include "../uop.h"

#include "../all_knobs.h"

#define DEBUG(args...)   _DEBUG(*m_simBase->m_knobs->KNOB_DEBUG_BP_DIR, ## args) 


///////////////////////////////////////////////////////////////////////////////////////////////


// bp_tage_c constructor
bp_tage_c::bp_tage_c(macsim_c* simBase) : bp_dir_base_c(simBase)
{
  // Based on KNOB value, initialize component settings
  switch (*KNOB(KNOB_BP_TAGE_NUM_COMPONENTS)) {
    case 5:
      NHIST = 4;
      LOGB = 13;
      LOGG = LOGB  - 3;
      TBITS = 9;
      break;
    case 8:
      NHIST = 7;
      LOGB = 13;
      LOGG = LOGB - 4;
      TBITS = 12;
      break;
    case 14:
      NHIST = 13;
      LOGB = 13;
      LOGG = LOGB - 5;
      TBITS = 15;
      break;
    default:
      // Error state
      break;
  }
  CBITS = 3; // TODO: Consider switching to KNOB value

  ghist = 0; // Initialize global history to 0
 
  // Identify lengths for history to use to index each global entry table
  // History lengths ordered from largest history length to smallest
  hist_lens = new int32[NHIST]; 
  hist_lens[0] = MAXHIST - 1;
  hist_lens[NHIST - 1] = MINHIST;
  for (int ii = 1; ii < NHIST - 1; ii++) {
    // history lengths computed by variation of following formula:
    //      L(i) = (int) (L(1) * pow(a, i-1) + 0.5)
    hist_lens[NHIST - 1 - ii] = (int32) (((double) MINHIST *
      pow((double) (MAXHIST - 1) / (double) MINHIST,
          (double) (ii) / (double) ((NHIST - 1)))) + 0.5);
  }

  ch_i = new folded_history[NHIST];
  for (int ii = NHIST - 1; ii >= 0; ii--) {
    assert(hist_lens[ii] < MAXHIST); // Check that history length below maximum history len
    ch_i[ii].init(m_simBase, hist_lens[ii], LOGG);
  }

  ch_t = new folded_history*[2];
  for (int ii = 0; ii < 2; ii++) {
    ch_t[ii] = new folded_history[NHIST];
  }

  for (int ii = 0; ii < NHIST; ii++) {
    ch_t[0][ii].init(m_simBase, ch_i[ii].OLENGTH, TBITS - ((ii + (NHIST & 1)) / 2));
    ch_t[1][ii].init(m_simBase, ch_i[ii].OLENGTH, TBITS - ((ii + (NHIST & 1)) / 2) - 1);
  }

  // Intialize base bimodal table
  btable  = new bentry[1 << LOGB];
  
  // Initialize global entry tables 
  gtable = new gentry*[NHIST];
  for (int ii = 0; ii < NHIST; ii++) {
    gtable[ii] = new gentry[1 << LOGG];
  }

  GI = new int32[NHIST];
}

// branch prediction
uns8 bp_tage_c::pred (uop_c *uop)
{
  Addr pc = uop->m_pc; 
  
  int32 bank;
  uns8 pred, altpred;

  uop->m_uop_info.m_tage_GI = new int32[NHIST];
  // Compute indices for bimodal table and global entry table 
  for (int ii = 0; ii < NHIST; ii++) {
    GI[ii] = gindex(pc, ii);
    // Store indices in uop to propogate to update
    uop->m_uop_info.m_tage_GI[ii] = GI[ii];
  }
  BI = bindex(pc);
  
  pred = read_prediction(pc, bank,  altpred);
  // Save information for update
  uop->m_uop_info.m_tage_pred = pred;
  uop->m_uop_info.m_tage_altpred = altpred;
  uop->m_uop_info.m_tage_bank = bank;
  
  DEBUG("Predicting core:%d thread_id:%d uop_num:%s addr:%s pred:%d  dir:%d\n", 
        uop->m_core_id, uop->m_thread_id, unsstr64(uop->m_uop_num), hexstr64s(pc), 
        pred, uop->m_dir);

  return pred;
}


// update branch predictor
void bp_tage_c::update (uop_c *uop)
{
  Addr pc       = uop->m_pc; 
  uns8 taken    = uop->m_dir;
  int32 bank    = uop->m_uop_info.m_tage_bank;
  uns8 pred     = uop->m_uop_info.m_tage_pred;
  uns8 altpred  = uop->m_uop_info.m_tage_altpred;
    
  int32 NRAND = MYRANDOM();
  // Compute indices for bimodal table and global entry table 
  for (int ii = 0; ii < NHIST; ii++) {
    GI[ii] = uop->m_uop_info.m_tage_GI[ii];
  }
  BI = bindex(pc);
  delete uop->m_uop_info.m_tage_GI; // Delete allcoated indices

  bool ALLOC = ((pred != taken) & (bank > 0));

  if (bank < NHIST) {
    uns8 locpred = (gtable[bank][GI[bank]].ctr >= 0);
    
    bool PseudoNewAlloc = (abs(2 & gtable[bank][GI[bank]].ctr + 1) == 1)
                          && (gtable[bank][GI[bank]].ubit == 0);
    if (PseudoNewAlloc) {
      if (locpred == taken) {
        // If the provide component was delivering the correct prediction;
        // no nned to allocate a new entry even if the overall prediction
        // was false.
        ALLOC = false;
      }
      
      // See section 3.2.4
      if (locpred != altpred) {
        if (altpred == taken) {
          if (PWIN < 7) {
            PWIN++;
          }
        } else if (PWIN > -8) {
          PWIN--;
        }
      }
    }
  }

  if (ALLOC) {
    int8 min = 3;
    for (int ii = 0; ii < bank; ii++) {
      if (gtable[ii][GI[ii]].ubit < min) {
        min = gtable[ii][GI[ii]].ubit;
      }
    }

    if (min > 0) {
      // No Unuseful entry to allocate: age all possible targets, but do not allocate
      for (int ii = bank - 1; ii >= 0; ii--) {
        gtable[ii][GI[ii]].ubit--;
      }
    } else {
      // YES: allocate one entry, but apply some randomness
      // bank I is twice more probable than bank I-1
      int32 Y = NRAND & ((1 << (bank - 1)) - 1);
      int32 X = bank - 1;
      while ((Y & 1) != 0) {
        X--;
        Y >>= 1;
      }

      for (int ii = X; ii >= 0; ii--) {
        int T = ii;
        if ((gtable[T][GI[T]].ubit == min)) {
          gtable[T][GI[T]].tag = gtag(pc, T);
          gtable[T][GI[T]].ctr = (taken) ? 0 : -1;
          gtable[T][GI[T]].ubit = 0;
          break;
        }
      }
    }
  }

  TICK++;
  if ((TICK & ((1 << 18) - 1)) == 0) {
    int X = (TICK >> 18) & 1;
    if ((X & 1) == 0) {
      X = 2;
    }

    for (int ii = 0; ii < NHIST; ii++) {
      for (int jj = 0; jj < (1 << LOGG); jj++) {
        gtable[ii][jj].ubit = gtable[ii][jj].ubit & X;
      }
    }
  }

  // update the counter that provided the prediction and only this counter
  if (bank < NHIST) {
    ctrupdate(gtable[bank][GI[bank]].ctr, taken, CBITS);
  } else {
    baseupdate(pc, taken);
  }
  
  // update the ubit counter
  if ((pred != altpred)) {
    assert(bank < NHIST);
    if (pred == taken) {
      if (gtable[bank][GI[bank]].ubit < 3) {
        gtable[bank][GI[bank]].ubit++;
      } 
    } else {
      if (gtable[bank][GI[bank]].ubit > 0) {
        gtable[bank][GI[bank]].ubit--;
      }
    }
  }

  ghist = (ghist << 1);
  // TODO: Current TAGE implementation includes unconditional branches in path
  if (taken) {
    ghist |= (history_t) 1; 
  }

  phist = (phist << 1) + (pc & 1);
  phist = (phist & ((1 << 16) - 1));
  for (int ii = 0; ii < NHIST; ii++) {
    ch_i[ii].update(ghist);
    ch_t[0][ii].update(ghist);
    ch_t[1][ii].update(ghist);
  }
}


// recovery from branch-mis prediction
void bp_tage_c::recover(recovery_info_c *recovery_info)
{
}

// Compute index to bimodal table
int32 bp_tage_c::bindex(Addr pc) 
{
  return (pc & ((1 << LOGB) - 1));
}

// Compute index to global table
int32 bp_tage_c::gindex(Addr pc, int32 bank) 
{
  int32 index;
  if (hist_lens[bank] >= 16) {
    index = pc ^ (pc >> ((LOGG - (NHIST - bank - 1)))) ^ 
              ch_i[bank].comp ^ f(phist, 16, bank);
  } else {
    index = pc ^ (pc >> (LOGG - NHIST + bank + 1)) ^ 
              ch_i[bank].comp ^ f(phist, hist_lens[bank], bank);
  }

  return (index & ((1 << (LOGG)) - 1));
}

// Mix path history function
int32 bp_tage_c::f(int32 a, int32 size, int32 bank) 
{
  int32 a1, a2;

  a = a & ((1 << size) - 1);
  a1 = (a & ((1 << LOGG) - 1));
  a2 = (a >> LOGG);
  a2 = ((a2 << bank) & ((1 << LOGG) - 1)) + (a2 >> (LOGG - bank));
  a = a1 ^ a2;
  a = ((a << bank) & ((1 << LOGG) - 1)) + (a >> (LOGG - bank));
  return (a);
}

// Global Table entry tag computation
uns16 bp_tage_c::gtag(Addr pc, int32 bank) 
{
  int32 tag = pc ^ ch_t[0][bank].comp ^ (ch_t[1][bank].comp << 1);
  return (tag & ((1 << (TBITS - ((bank + (NHIST & 1)) / 2))) - 1));
}

// up-down saturating counter
void bp_tage_c::ctrupdate (int8 &ctr, uns8 taken, int32 nbits) 
{
  if (taken) {
    if (ctr < ((1 << (nbits - 1)) - 1)) {
      ctr++;
    }
  } else {
    if (ctr > -(1 << (nbits - 1))) {
      ctr--;
    }
  }
}

// Identify a prediction for given pc address
uns8 bp_tage_c::read_prediction(Addr pc, int32 &bank, uns8 &altpred) 
{
  bank = NHIST;
  int32 altbank = NHIST;
  {
    // Find the first global entry table with a matching entry
    for (int ii = 0; ii < NHIST; ii++) {
      if (gtable[ii][GI[ii]].tag == gtag(pc, ii)) {
        bank = ii;
        break;
      }
    }
    // Find the alternate global entry value with a matching entry
    for (int ii = bank + 1; ii < NHIST; ii++) {
      if (gtable[ii][GI[ii]].tag == gtag(pc, ii)) {
        altbank = ii;
        break;
      }
    }

    // If we found matching entry in a global entry table
    if (bank < NHIST) {
      // If we found matching entry for alternate prediction
      if (altbank < NHIST) {
        altpred = (gtable[altbank][GI[altbank]].ctr >= 0);
      } else {
        // Did not find matching alternate entry (ctr > -(1  so consult bimodal table
        altpred = getbim(pc);
      }
      // If the entry is recognized as a newly allocated entry and counter
      // PWIN is negative use the alternate prediction (see section 3.2.4)
      if ((PWIN < 0) || (abs(2 * gtable[bank][GI[bank]].ctr + 1) != 1)
          || (gtable[bank][GI[bank]].ubit != 0)) {
        return (gtable[bank][GI[bank]].ctr >= 0);
      } else {
        return (altpred);
      }
    } else {
      // Did not find matching entry in any global table, so consult bimodal table
      altpred = getbim(pc);
      return altpred;
    }
  }
}

// Get bimodal prediction
uns8 bp_tage_c::getbim(Addr pc) 
{
  return (btable[BI].pred > 0);
}

// Update the bimodal predictor
void bp_tage_c::baseupdate(Addr pc, uns8 Taken)
{
  if (Taken == getbim(pc)) {
    if (Taken) {
      if (btable[BI].pred) {
        btable[BI >> 2].hyst = 1;
      }
    } else {
      if (!btable[BI].pred) {
        btable[BI >> 2].hyst = 0;
      }
    }
  } else {
    int inter = (btable[BI].pred << 1) + btable[BI >> 2].hyst;
    if (Taken) {
      if (inter < 3) {
        inter += 1;
      }
    } else {
      if (inter > 0) {
        inter--;
      }
    }
    btable[BI].pred = inter >> 1;
    btable[BI >> 2].hyst = (inter & 1);
  }
}

// Simple pseudo random number generator based on linear feedback shift register
int32 bp_tage_c::MYRANDOM()
{
  Seed = ((1 << 2 * NHIST) + 1) * Seed + 0xf3f531;
  Seed = (Seed & ((1 << (2 * (NHIST))) - 1));
  return (Seed);
}

// folded_history init
void folded_history::init(macsim_c* simBase, int32 original_length, int32 compressed_length)
{
  m_simBase = simBase;
  comp = 0;
  OLENGTH = original_length;
  CLENGTH = compressed_length;
  OUTPOINT = OLENGTH % CLENGTH;
  assert(OLENGTH < MAXHIST);
}

// folded_history update
void folded_history::update(history_t h)
{
  assert((comp >> CLENGTH) == 0);
  comp = (comp << 1) | h[0];
  comp ^= h[OLENGTH] << OUTPOINT;
  comp ^= (comp >> CLENGTH);
  comp &= (1 << CLENGTH) - 1;
}

// bentry constructor
bentry::bentry()
{
  pred = 0;
  hyst = 1;
}

// gentry constructor
gentry::gentry()
{
  ctr = 0;
  tag = 0;
  ubit = 0;
}


