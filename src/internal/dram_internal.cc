/**********************************************************************************************
 * File         : internal/dram_internal.cc
 * Author       : HPArch
 * Date         : 1/27/2011 
 * SVN          : $Id: frontend.h 915 2009-11-20 19:13:07Z kacear $:
 * Description  : various dram policies (not seen to public)
 *********************************************************************************************/


#include <cmath>

#include "dram_internal.h"
#include "../dram.h"
//#include "../dram_factory.h"

#include "../debug_macros.h"
#include "../factory_class.h"
#include "../memreq_info.h"

#include "../all_knobs.h"

///////////////////////////////////////////////////////////////////////////////////////////////


// wrapper function to link internal dram policies
dram_controller_c *dram_factory_internal(macsim_c* m_simBase) {
  string policy = m_simBase->m_knobs->KNOB_DRAM_SCHEDULING_POLICY->getValue();
  dram_controller_c* ptr;
  if (policy == "test") {
    ptr = new example_dc_c (m_simBase);
  }
  else if (policy == "hetero") {
    ptr = new hetero_dc_c(m_simBase);
  }
  else if (policy == "SJF") {
    ptr = new sjf_dc_c(m_simBase);
  }
  else if (policy == "ASJF") {
    ptr = new asjf_dc_c(m_simBase);
  }

  return ptr;
}


///////////////////////////////////////////////////////////////////////////////////////////////


// Singleton dram_internal_wrapper
dram_internal_wrapper_c dram_internal_wrapper_c::Singleton;


// dram_internal_wrapper constructor
// To register all modules
dram_internal_wrapper_c::dram_internal_wrapper_c()
{
  dram_factory_c::get()->register_class("test", dram_factory_internal);
  dram_factory_c::get()->register_class("hetero", dram_factory_internal);
  dram_factory_c::get()->register_class("SJF", dram_factory_internal);
  dram_factory_c::get()->register_class("ASJF", dram_factory_internal);
}


dram_internal_wrapper_c::~dram_internal_wrapper_c()
{
}


///////////////////////////////////////////////////////////////////////////////////////////////


example_dc_c::sort_func::sort_func(example_dc_c* parent)
{
  m_parent = parent;
}


bool example_dc_c::sort_func::operator()(drb_entry_s* req_a, drb_entry_s* req_b)
{
  // to implement
  return false;
}


example_dc_c::example_dc_c(macsim_c* simBase) : dram_controller_c(simBase)
{
  m_sort = new sort_func(this);
}


example_dc_c::~example_dc_c()
{
  delete m_sort;
}


drb_entry_s* example_dc_c::schedule(list<drb_entry_s*>* buffer)
{
  buffer->sort(*m_sort);

  return buffer->front();
}

///////////////////////////////////////////////////////////////////////////////////////////////


hetero_dc_c::sort_func::sort_func(hetero_dc_c* parent)
{
  m_parent = parent;
}


bool hetero_dc_c::sort_func::operator()(drb_entry_s* req_a, drb_entry_s* req_b)
{
  if (req_a->m_req->m_ptx == false && req_b->m_req->m_ptx == true)
    return true;
  else if (req_a->m_req->m_ptx == true && req_b->m_req->m_ptx == false)
    return false;
  else {
    int bid = req_a->m_bid;
    int current_rid = m_parent->m_current_rid[bid];
    if (req_a->m_rid == current_rid && req_b->m_rid != current_rid)
      return true;
    else if (req_a->m_rid != current_rid && req_b->m_rid == current_rid)
      return false;
    else
      return req_a->m_timestamp < req_b->m_timestamp;
  }
}


hetero_dc_c::hetero_dc_c(macsim_c* simBase) : dram_controller_c(simBase)
{
  m_sort = new sort_func(this);
}


hetero_dc_c::~hetero_dc_c()
{
  delete m_sort;
}


drb_entry_s* hetero_dc_c::schedule(list<drb_entry_s*>* buffer)
{
  buffer->sort(*m_sort);

  return buffer->front();
}


///////////////////////////////////////////////////////////////////////////////////////////////
sjf_dc_c::sort_func::sort_func(sjf_dc_c* parent)
{
  m_parent = parent;
}


bool sjf_dc_c::sort_func::operator()(drb_entry_s* req_a, drb_entry_s* req_b)
{
  int bid = req_a->m_bid;
  int current_rid = m_parent->m_current_rid[bid];

  int index_a;
  int index_b;

  if (req_a->m_read || *m_parent->m_simBase->m_knobs->KNOB_USE_INCOMING_TID_CID_FOR_WB) {
    index_a = req_a->m_core_id * 100000 + req_a->m_thread_id;
  } else {
    index_a = *m_parent->m_simBase->m_knobs->KNOB_NUM_SIM_CORES * 100000;
  }

  if (req_b->m_read || *m_parent->m_simBase->m_knobs->KNOB_USE_INCOMING_TID_CID_FOR_WB) {
    index_b = req_b->m_core_id * 100000 + req_b->m_thread_id;
  } else {
    index_b = *m_parent->m_simBase->m_knobs->KNOB_NUM_SIM_CORES * 100000;
  }

  if (*m_parent->m_simBase->m_knobs->KNOB_USE_INCOMING_TID_CID_FOR_WB) {

    if (m_parent->m_req_count[bid][index_a] < m_parent->m_req_count[bid][index_b]) {
      return true;
    } 
    else if (m_parent->m_req_count[bid][index_a] > m_parent->m_req_count[bid][index_b]) {
      return false;
    } 
    else {
      if (req_a->m_rid == current_rid && req_b->m_rid != current_rid) {
        return true;
      } 
      else if (req_a->m_rid != current_rid && req_b->m_rid == current_rid) {
        return false;
      } 
      else {
        return req_a->m_timestamp < req_b->m_timestamp;
      }
    }

  } 
  else {
    if (req_a->m_read && !req_b->m_read) {
      return true;
    }
    else if (!req_a->m_read && req_b->m_read) {
      return false;
    }
    else {

      if (m_parent->m_req_count[bid][index_a] < m_parent->m_req_count[bid][index_b]) {
        return true;
      } 
      else if (m_parent->m_req_count[bid][index_a] > m_parent->m_req_count[bid][index_b]) {
        return false;
      } 
      else {
        if (req_a->m_rid == current_rid && req_b->m_rid != current_rid) {
          return true;
        } 
        else if (req_a->m_rid != current_rid && req_b->m_rid == current_rid) {
          return false;
        } 
        else {
          return req_a->m_timestamp < req_b->m_timestamp;
        }
      }
    }
  }

}


sjf_dc_c::sjf_dc_c(macsim_c* simBase) : dram_controller_c(simBase)
{
  m_sort = new sort_func(this);
  m_req_count = new unordered_map<int, int>[*simBase->m_knobs->KNOB_DRAM_NUM_BANKS];
}


sjf_dc_c::~sjf_dc_c()
{
  delete m_sort;
  delete []m_req_count;
}


drb_entry_s* sjf_dc_c::schedule(list<drb_entry_s*>* buffer)
{
  buffer->sort(*m_sort);

  return buffer->front();
}

void sjf_dc_c::on_insert(mem_req_s* req, int bid, int rid, int cid)
{
  int index;

  if (req->m_type != MRT_WB || *m_simBase->m_knobs->KNOB_USE_INCOMING_TID_CID_FOR_WB) {
    index = req->m_core_id * 100000 + req->m_thread_id;
  } else {
    index = *m_simBase->m_knobs->KNOB_NUM_SIM_CORES * 100000;
  }

  unordered_map<int, int>::iterator itr = m_req_count[bid].find(index);
  unordered_map<int, int>::iterator end = m_req_count[bid].end();

  if (itr != end) {
    itr->second++;
  } else {
    m_req_count[bid].insert(pair<int, int>(index, 1));
  }
}

void sjf_dc_c::on_complete(drb_entry_s* req)
{
  int index;

  if (req->m_read || *m_simBase->m_knobs->KNOB_USE_INCOMING_TID_CID_FOR_WB) {
    index = req->m_core_id * 100000 + req->m_thread_id;
  } else {
    index = *m_simBase->m_knobs->KNOB_NUM_SIM_CORES * 100000;
  }

  unordered_map<int, int>::iterator itr = m_req_count[req->m_bid].find(index);
  unordered_map<int, int>::iterator end = m_req_count[req->m_bid].end();

  if (itr->second == 1) {
    m_req_count[req->m_bid].erase(itr);
  } else {
    itr->second--;
  }
}

///////////////////////////////////////////////////////////////////////////////////////////////
asjf_dc_c::sort_func::sort_func(asjf_dc_c* parent)
{
  m_parent = parent;
}


bool asjf_dc_c::sort_func::operator()(drb_entry_s* req_a, drb_entry_s* req_b)
{
  int bid = req_a->m_bid;
  int current_rid = m_parent->m_current_rid[bid];


  if (!*m_parent->m_simBase->m_knobs->KNOB_USE_INCOMING_TID_CID_FOR_WB) {
    if (req_a->m_read && !req_b->m_read) {
      return false;
    }
    else if (!req_a->m_read && req_b->m_read) {
      return true;
    }
    else if (req_a->m_read && req_b->m_read) {
      if (req_a->m_rid == current_rid && req_b->m_rid != current_rid) {
        return true;
      }
      else if (req_a->m_rid == current_rid && req_b->m_rid != current_rid) {
        return false;
      }
      else {
        return req_a->m_timestamp < req_b->m_timestamp;
      }
    }
  }

  int index_a;
  int index_b;

  if (req_a->m_read || *m_parent->m_simBase->m_knobs->KNOB_USE_INCOMING_TID_CID_FOR_WB) {
    index_a = req_a->m_core_id * 100000 + req_a->m_thread_id;
  } else {
    index_a = *m_parent->m_simBase->m_knobs->KNOB_NUM_SIM_CORES * 100000;
  }

  if (req_b->m_read || *m_parent->m_simBase->m_knobs->KNOB_USE_INCOMING_TID_CID_FOR_WB) {
    index_b = req_b->m_core_id * 100000 + req_b->m_thread_id;
  } else {
    index_b = *m_parent->m_simBase->m_knobs->KNOB_NUM_SIM_CORES * 100000;
  }

  int threads_a = m_parent->get_core_active_threads(req_a->m_core_id);
  int threads_b = m_parent->get_core_active_threads(req_b->m_core_id);

  if (threads_a < threads_b) {
    return true;
  }
  else if (threads_a > threads_b) {
    return false;
  }

  double redn_a;
  double redn_b;
  double alpha = *m_parent->m_simBase->m_knobs->KNOB_ASJF_ALPHA;
  double m = *m_parent->m_simBase->m_knobs->KNOB_ASJF_M;

  if (req_a->m_rid == current_rid) {
    redn_a = pow(m * m_parent->m_req_count[bid][index_a], alpha) 
      - pow(m * m_parent->m_req_count[bid][index_a] - m, alpha);
  }
  else {
    redn_a = pow(m * m_parent->m_req_count[bid][index_a], alpha) 
      - pow(m * m_parent->m_req_count[bid][index_a] - 1.0, alpha);
  }

  if (req_b->m_rid == current_rid) {
    redn_b = pow(m * m_parent->m_req_count[bid][index_b], alpha) 
      - pow(m * m_parent->m_req_count[bid][index_b] - m, alpha);
  }
  else {
    redn_b = pow(m * m_parent->m_req_count[bid][index_b], alpha) 
      - pow(m * m_parent->m_req_count[bid][index_b] - 1.0, alpha);
  }

  if (redn_a > redn_b) {
    return true;
  }
  else if (redn_a < redn_b) {
    return false;
  }
  else {
    if (req_a->m_rid == current_rid && req_a->m_rid != current_rid) {
      return true;
    }
    else if (req_a->m_rid == current_rid && req_a->m_rid != current_rid) {
      return false;
    }
    else {
      return req_a->m_timestamp < req_b->m_timestamp;
    }

  }
}


asjf_dc_c::asjf_dc_c(macsim_c* simBase) : dram_controller_c(simBase)
{
  m_sort = new sort_func(this);
  m_req_count = new unordered_map<int, int>[*simBase->m_knobs->KNOB_DRAM_NUM_BANKS];
}


asjf_dc_c::~asjf_dc_c()
{
  delete m_sort;
  delete []m_req_count;
}


drb_entry_s* asjf_dc_c::schedule(list<drb_entry_s*>* buffer)
{
  buffer->sort(*m_sort);

  return buffer->front();
}

void asjf_dc_c::on_insert(mem_req_s* req, int bid, int rid, int cid)
{
  int index;

  if (req->m_type != MRT_WB || *m_simBase->m_knobs->KNOB_USE_INCOMING_TID_CID_FOR_WB) {
    index = req->m_core_id * 100000 + req->m_thread_id;
  } else {
    index = *m_simBase->m_knobs->KNOB_NUM_SIM_CORES * 100000;
  }

  unordered_map<int, int>::iterator itr = m_req_count[bid].find(index);
  unordered_map<int, int>::iterator end = m_req_count[bid].end();

  if (itr != end) {
    itr->second++;
  } else {
    m_req_count[bid].insert(pair<int, int>(index, 1));
  }
}

void asjf_dc_c::on_complete(drb_entry_s* req)
{
  int index;

  if (req->m_read || *m_simBase->m_knobs->KNOB_USE_INCOMING_TID_CID_FOR_WB) {
    index = req->m_core_id * 100000 + req->m_thread_id;
  } else {
    index = *m_simBase->m_knobs->KNOB_NUM_SIM_CORES * 100000;
  }

  unordered_map<int, int>::iterator itr = m_req_count[req->m_bid].find(index);
  unordered_map<int, int>::iterator end = m_req_count[req->m_bid].end();

  if (itr->second == 1) {
    m_req_count[req->m_bid].erase(itr);
  } else {
    itr->second--;
  }
}

int asjf_dc_c::get_core_active_threads(int core_id) {
  assert(core_id < *m_simBase->m_knobs->KNOB_NUM_SIM_CORES);

  return m_simBase->m_core_pointers[core_id]->m_running_thread_num;
}

///////////////////////////////////////////////////////////////////////////////////////////////
