/**********************************************************************************************
 * File         : internal/fetch_internal.h
 * Author       : Jaekyu Lee
 * Date         : 1/27/2011 
 * SVN          : $Id: frontend.h 915 2009-11-20 19:13:07Z kacear $:
 * Description  : various fetch policies (not seen to public)
 *********************************************************************************************/


#ifndef FETCH_INTERNAL_H
#define FETCH_INTERNAL_H


#include "../global_defs.h"
#include "../global_types.h"
#include "../frontend.h"


// To transparently link internal modules, we need to have
// 1. wrapper class to fetch_internal_c
// 2. singleton wrapper entry
class fetch_internal_wrapper_c
{
  public:
    fetch_internal_wrapper_c();
    ~fetch_internal_wrapper_c();

    static fetch_internal_wrapper_c Singleton;
};


//! Internal fetch policies
class fetch_internal_c : public frontend_c
{
  public:
    fetch_internal_c(FRONTEND_INTERFACE_PARAMS(), macsim_c* simBase);
    virtual ~fetch_internal_c();

    int fetch(void); 

    /*! \fn int fetch_roundrobin()
     *  \brief Function for roundrobin fetch scheduling
     *  \param void
     *  \return int - Thread to be fetched
     */
    int fetch_roundrobin();

    /*! \fn int fetch_fair()
     *  \brief Function for Fair fetch scheduling
     *  \param void
     *  \return int - Thread to be fetched
     */
    int fetch_fair();

    /*! \fn int fetch_lrf()
     *  \brief Function for Least Recently fetched scheduling
     *  \param void
     *  \return int - Thread to be fetched
     */
    int fetch_lrf();

    /*! \fn int fetch_icount()
     *  \brief Function for fetch_icount scheduling
     *  \param void
     *  \return int - Thread to be fetched
     */
    int fetch_icount();

    /*! \fn int fetch_prirr()
     *  \brief Function for fetch_prirr scheduling
     *  \param void
     *  \return int - Thread to be fetched
     */
    int fetch_prirr();

    /*! \fn int fetch_fairrr()
     *  \brief Function for fetch_fairrr scheduling
     *  \param void
     *  \return int - Thread to be fetched
     */
    int fetch_fairrr();

    /*! \fn int fetch_maxrr()
     *  \brief Function for fetch_maxrr scheduling
     *  \param void
     *  \return int - Thread to be fetched
     */
    int fetch_maxrr();

    /*! \fn int fetch_blk_until_load()
     *  \brief Function for fetch_blk_until_load scheduling
     *  \param void
     *  \return int - Thread to be fetched
     */
    int fetch_blk_until_load();

    /*! \fn int fetch_all()
     *  \brief Function for fetch_alll scheduling
     *  \param void
     *  \return int - Thread to be fetched
     */
    int fetch_all();

    /*! \fn int fetch_random()
     *  \brief Function for fetch_random scheduling
     *  \param void
     *  \return int - Thread to be fetched
     */
    int fetch_random();

    /*! \fn int fetch_fairperiod()
     *  \brief Function for fetch_fairperiod scheduling
     *  \param void
     *  \return int - Thread to be fetched
     */
    int fetch_fairperiod();

    /*! \fn int fetch_bar()
     *  \brief Function for fetch_bar scheduling
     *  \param void
     *  \return int - Thread to be fetched
     */
    int fetch_bar();

    /*! \fn int fetch_mem()
     *  \brief Function for fetch_mem scheduling
     *  \param void
     *  \return int - Thread to be fetched
     */
    int fetch_mem();

    /*! \fn int fetch_mem_bar()
     *  \brief Function for fetch_mem_bar scheduling
     *  \param void
     *  \return int - Thread to be fetched
     */
    int fetch_mem_bar();

    /*! \fn int fetch_mem_bar_new()
     *  \brief Function for fetch_mem_bar_new scheduling
     *  \param void
     *  \return int - Thread to be fetched
     */
    int fetch_mem_bar_new();

    /*! \fn int fetch_rr_mem_bar_new()
     *  \brief Function for fetch_rr_mem_bar_new scheduling
     *  \param void
     *  \return int - Thread to be fetched
     */
    int fetch_rr_mem_bar_new();

    /*! \fn int fetch_pred_longest_first()
     *  \brief Function for fetch_pred_longest_first scheduling
     *  \param void
     *  \return int - Thread to be fetched
     */
    int fetch_pred_longest_first();

    /*! \fn int fetch_pred_longest_first_new()
     *  \brief Function for fetch_pred_longest_first_new scheduling
     *  \param void
     *  \return int - Thread to be fetched
     */
    int fetch_pred_longest_first_new();

    /*! \fn int fetch_reconv_long()
     *  \brief Function for fetch_reconv_long scheduling
     *  \param void
     *  \return int - Thread to be fetched
     */
    int fetch_reconv_long();

    /*! \fn int fetch_rr_block()
     *  \brief Function for Fetch Round Robin scheduling
     *  \param void
     *  \return int - Thread to be fetched
     */
    int fetch_rr_block();

    /*! \fn int fetch_row_hit()
     *  \brief Function for Row hit fetch scheduling
     *  \param void
     *  \return int - Thread to be fetched
     */
    int fetch_row_hit();

  public:
    int m_prev_bid;
    int m_prev_rowid;

  private:
    // do not implement
    fetch_internal_c();

    // current policy
    int (fetch_internal_c::*MT_fetch_scheduler)(void);

    Counter           m_dec_fetch_arbiter;
    int               m_dec_scheduled_thread_num;
    int               m_dec_rr_freq;
    int               m_rr_type;
    int               m_rr_counter;
    int               m_block_arbiter;
    int               m_prev_unique_scheduled_thread_num;
    bool              m_MT_stop_fair;

    unordered_map<int, int> m_rr_index;

    macsim_c* m_simBase;         /**< macsim_c base class for simulation globals */
   
};


#endif


