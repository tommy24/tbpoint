/**********************************************************************************************
 * File         : internal/pref_internal.cc
 * Author       : Jaekyu Lee
 * Date         : 1/27/2011 
 * SVN          : $Id: frontend.h 915 2009-11-20 19:13:07Z kacear $:
 * Description  : various fetch policies (not seen to public)
 *********************************************************************************************/


#include "../pref_factory.h"
#include "pref_internal.h"
#include "pref_stream.h"
#include "pref_ghb.h"
#include "pref_2dc.h"
#include "pref_phase.h"
#include "pref_stridepc.h"

///////////////////////////////////////////////////////////////////////////////////////////////
// wrapper function to link internal fetch policies
///////////////////////////////////////////////////////////////////////////////////////////////
void pref_factory_internal(vector<pref_base_c *> &pref_table, hwp_common_c *hcc, 
                           Unit_Type type, macsim_c* simBase)
{
  pref_base_c *pref_stream = new pref_stream_c(hcc, type, simBase);
  pref_base_c *pref_ghb = new pref_ghb_c(hcc, type, simBase);
  pref_base_c *pref_2dc = new pref_2dc_c(hcc, type, simBase);
  pref_base_c *pref_phase = new pref_phase_c(hcc, type, simBase);
  pref_base_c *pref_stridepc = new pref_stridepc_c(hcc, type, simBase);

  pref_table.push_back(pref_stream);
  pref_table.push_back(pref_ghb);
  pref_table.push_back(pref_2dc);
  pref_table.push_back(pref_phase);
  pref_table.push_back(pref_stridepc);
}




///////////////////////////////////////////////////////////////////////////////////////////////
// Singleton fetch_internal_wrapper
///////////////////////////////////////////////////////////////////////////////////////////////
pref_internal_wrapper_c pref_internal_wrapper_c::Singleton;



///////////////////////////////////////////////////////////////////////////////////////////////
// fetch_internal_wrapper constructor
// To register all modules
///////////////////////////////////////////////////////////////////////////////////////////////
pref_internal_wrapper_c::pref_internal_wrapper_c()
{
  pref_factory_c::get()->register_class(pref_factory_internal);
}
