/**********************************************************************************************
 * File         : internal/pref_internal.h
 * Author       : Jaekyu Lee
 * Date         : 1/27/2011 
 * SVN          : $Id: frontend.h 915 2009-11-20 19:13:07Z kacear $:
 * Description  : various prefetchers (not seen to public)
 *********************************************************************************************/


#ifndef PREF_INTERNAL_H
#define PREF_INTERNAL_H


#include "../global_defs.h"
#include "../global_types.h"


// To transparently link internal modules, we need to have
// 1. wrapper class to fetch_internal_c
// 2. singleton wrapper entry
class pref_internal_wrapper_c
{
  public:
    pref_internal_wrapper_c();
    ~pref_internal_wrapper_c() {}

    static pref_internal_wrapper_c Singleton;
};

#endif

