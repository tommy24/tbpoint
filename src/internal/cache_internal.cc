/**********************************************************************************************
 * File         : internal/cache_internal.cc
 * Author       : Jaekyu Lee
 * Date         : 1/27/2011 
 * SVN          : $Id: frontend.h 915 2009-11-20 19:13:07Z kacear $:
 * Description  : various dram policies (not seen to public)
 *********************************************************************************************/


#include "cache_internal.h"
#include "../debug_macros.h"
#include "../factory_class.h"
#include "../memreq_info.h"
#include "../all_knobs.h"
#include "../memory.h"
#include "../statistics.h"
#include "tadip.h"
#include "rrip.h"
#include "ucp.h"

///////////////////////////////////////////////////////////////////////////////////////////////


#define CACHE_CONSTRUCTOR_TEMPLATE \
  *KNOB(KNOB_L3_NUM_SET), *KNOB(KNOB_L3_ASSOC), *KNOB(KNOB_L3_LINE_SIZE), \
  sizeof(dcache_data_s), *KNOB(KNOB_L3_NUM_BANK), false, 0, CACHE_DL1, false, m_simBase


// wrapper function to link internal dram policies
cache_c *cache_factory_internal(macsim_c* m_simBase) {
  string llc_type = KNOB(KNOB_LLC_TYPE)->getValue();
  cache_c* llc;

  if (llc_type == "rrip")       llc = new cache_rrip_c("llc_rrip", CACHE_CONSTRUCTOR_TEMPLATE);
  else if (llc_type == "tadip") llc = new cache_tadip_c("llc_tadip", CACHE_CONSTRUCTOR_TEMPLATE);
  else if (llc_type == "ucp")   llc = new cache_ucp_c("llc_ucp", CACHE_CONSTRUCTOR_TEMPLATE);
 
  return llc;
}


///////////////////////////////////////////////////////////////////////////////////////////////


// Singleton cache_internal_wrapper
cache_internal_wrapper_c cache_internal_wrapper_c::Singleton;


// cache_internal_wrapper constructor
// To register all modules
cache_internal_wrapper_c::cache_internal_wrapper_c()
{
  llc_factory_c::get()->register_class("tadip", cache_factory_internal);
  llc_factory_c::get()->register_class("rrip", cache_factory_internal);
  llc_factory_c::get()->register_class("ucp", cache_factory_internal);
}


cache_internal_wrapper_c::~cache_internal_wrapper_c()
{
}



