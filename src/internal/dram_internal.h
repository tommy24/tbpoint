/**********************************************************************************************
 * File         : internal/dram_internal.h
 * Author       : Jaekyu Lee
 * Date         : 1/27/2011 
 * SVN          : $Id: frontend.h 915 2009-11-20 19:13:07Z kacear $:
 * Description  : various dram policies (not seen to public)
 *********************************************************************************************/


#ifndef DRAM_INTERNAL_H
#define DRAM_INTERNAL_H


#include "../global_defs.h"
#include "../global_types.h"
#include "../dram.h"


// To transparently link internal modules, we need to have
// 1. wrapper class to dram_internal_c
// 2. singleton wrapper entry
class dram_internal_wrapper_c
{
  public:
    dram_internal_wrapper_c();
    ~dram_internal_wrapper_c();

    static dram_internal_wrapper_c Singleton;
};


class example_dc_c : public dram_controller_c
{
  class sort_func {
    public:
      bool operator() (drb_entry_s* req_a, drb_entry_s* req_b);
      sort_func(example_dc_c*);

      example_dc_c* m_parent;
  };

  friend class sort_func;

  public:
    example_dc_c(macsim_c* simBase);
    ~example_dc_c();

    drb_entry_s* schedule(list<drb_entry_s*> *);

  private:
    class sort_func* m_sort;
};

class hetero_dc_c : public dram_controller_c
{
  class sort_func {
    public:
      bool operator() (drb_entry_s* req_a, drb_entry_s* req_b);
      sort_func(hetero_dc_c*);

      hetero_dc_c* m_parent;
  };

  friend class sort_func;

  public:
    hetero_dc_c(macsim_c* simBase);
    ~hetero_dc_c();

    drb_entry_s* schedule(list<drb_entry_s*> *);

  private:
    class sort_func* m_sort;
};

/////////////////////////////////////////////////////////////////////////
class sjf_dc_c : public dram_controller_c
{
  class sort_func {
    public:
      bool operator() (drb_entry_s* req_a, drb_entry_s* req_b);
      sort_func(sjf_dc_c*);

      sjf_dc_c* m_parent;
  };

  friend class sort_func;

  public:
    sjf_dc_c(macsim_c* simBase);
    ~sjf_dc_c();

    drb_entry_s* schedule(list<drb_entry_s*> *);

  private:
    class sort_func* m_sort;
    unordered_map<int, int> *m_req_count;

    void on_insert(mem_req_s* req, int bid, int rid, int cid);
    void on_complete(drb_entry_s* req);
};

/////////////////////////////////////////////////////////////////////////
class asjf_dc_c : public dram_controller_c
{
  class sort_func {
    public:
      bool operator() (drb_entry_s* req_a, drb_entry_s* req_b);
      sort_func(asjf_dc_c*);

      asjf_dc_c* m_parent;
  };

  friend class sort_func;

  public:
    asjf_dc_c(macsim_c* simBase);
    ~asjf_dc_c();

    drb_entry_s* schedule(list<drb_entry_s*> *);

  private:
    class sort_func* m_sort;
    unordered_map<int, int> *m_req_count;

    void on_insert(mem_req_s* req, int bid, int rid, int cid);
    void on_complete(drb_entry_s* req);

    int get_core_active_threads(int core_id);
};

/////////////////////////////////////////////////////////////////////////

#endif 



