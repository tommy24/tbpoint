/**********************************************************************************************
 * File         : ucp.cc
 * Author       : Jaekyu Lee
 * Date         : 04/26/2011 
 * SVN          : $Id: cache.h,
 * Description  : UCP (Qureshi and Patt MICRO06)
 *********************************************************************************************/


#include <cmath>
#include <cassert>

#include "ucp.h"
#include "../cache.h"
#include "../debug_macros.h"
#include "../assert_macros.h"
#include "../all_knobs.h"


#define DEBUG(args...) _DEBUG(*KNOB(KNOB_DEBUG_CACHE_LIB, ## args)
#define DEBUG_MEM(args...) _DEBUG(*KNOB(KNOB_DEBUG_MEM_TRACE, ## args)


// constructor
cache_ucp_c::cache_ucp_c(string name, uns num_set, uns assoc, uns line_size, 
    uns data_size, uns bank_num, bool cache_by_pass, int core_id, Cache_Type cache_type_info, 
    bool enable_partition, macsim_c* simBase) : cache_c(name, num_set, assoc, line_size, data_size, bank_num,
      cache_by_pass, core_id, cache_type_info, enable_partition, 1, 0, simBase)
{
  // assume 32 sets for 1 SDM
  m_modulo = num_set / 32;
  m_num_way = assoc;
  m_num_application = *KNOB(KNOB_UCP_CACHE_NUM_APPLICATION);
  m_partition_period = *KNOB(KNOB_UCP_CACHE_PARTITION_PERIOD);

  for (int ii = 0; ii < m_max_application; ++ii) {
    m_way_counter[ii] = new int[assoc];
    fill_n(m_way_counter[ii], assoc, 0); 
  }

  for (int ii = 0; ii < m_max_application; ++ii) {
    m_atd[ii] = new list<Addr>[32];
    for (int jj = 0; jj < 32; ++jj) {
      for (int kk = 0; kk < assoc; ++kk) {
        m_atd[ii][jj].push_back(0);
      }
    }
  }

  for (int ii = 0; ii < m_max_application; ++ii) {
    m_num_entry[ii] = new int[num_set];
    fill_n(m_num_entry[ii], num_set, 0);
  }

  // initially assign equal amount
  for (int ii = 0; ii < m_max_application; ++ii) {
    m_quota[ii] = assoc / m_num_application;
  }

  // tocheck
  //m_last_partition_cycle = 0;
  m_last_partition_cycle = 10000 - m_partition_period;
  
  m_access_count_by_type[2] = {0};
  m_total_access_count = 0;
  m_access_ratio = 0.0;
}


// destructor
cache_ucp_c::~cache_ucp_c()
{
}


// find an entry to be replaced based on the policy
cache_entry_c* cache_ucp_c::find_replacement_line(int set, int appl_id) 
{
  int index = -1;
  Counter min_lru = m_simBase->m_simulation_cycle + 1;
  if (appl_id == -1) {
    if (*KNOB(KNOB_CACHE_USE_PSEUDO_LRU)) {
      while (1) {
        for (int ii = 0; ii < m_assoc; ++ii) {
          cache_entry_c* line = &(m_set[set]->m_entry[ii]);
          if (!line->m_valid || line->m_last_access_time == 0) {
            return &(m_set[set]->m_entry[ii]);
          }
        }

        for (int ii = 0; ii < m_assoc; ++ii) {
          cache_entry_c* line = &(m_set[set]->m_entry[ii]);
          line->m_last_access_time = 0;
        }
      }
    }
    else {
      for (int ii = 0; ii < m_assoc; ++ii) {
        cache_entry_c* line = &(m_set[set]->m_entry[ii]);
        // find invalid or LRU entry
        if (line->m_valid != true) {
          index = ii;
          break;
        }

        if (line->m_last_access_time < min_lru) {
          index = ii;
          min_lru = line->m_last_access_time;
        }
      }
    }
  }
  else {
    // evict a block from other applications
    if (m_num_entry[appl_id][set] < m_quota[appl_id]) {
      if (*KNOB(KNOB_CACHE_USE_PSEUDO_LRU)) {
        while (1) {
          for (int ii = 0; ii < m_assoc; ++ii) {
            cache_entry_c* line = &(m_set[set]->m_entry[ii]);
            if (!line->m_valid || (line->m_appl_id != appl_id && line->m_last_access_time == 0)) {
              return &(m_set[set]->m_entry[ii]);
            }
          }

          for (int ii = 0; ii < m_assoc; ++ii) {
            cache_entry_c* line = &(m_set[set]->m_entry[ii]);
            if (line->m_appl_id != appl_id) {
              line->m_last_access_time = 0;
            }
          }
        }
      }
      else {
        for (int ii = 0; ii < m_assoc; ++ii) {
          cache_entry_c* line = &(m_set[set]->m_entry[ii]);
          if (line->m_valid != true) {
            index = ii;
            break;
          }

          if (line->m_appl_id != appl_id && line->m_last_access_time < min_lru) {
            index = ii;
            min_lru = line->m_last_access_time;
          }
        }
      }
    }
    // evict a block from own
    else {
      if (*KNOB(KNOB_CACHE_USE_PSEUDO_LRU)) {
        while (1) {
          for (int ii = 0; ii < m_assoc; ++ii) {
            cache_entry_c* line = &(m_set[set]->m_entry[ii]);
            if (!line->m_valid || (line->m_appl_id == appl_id && line->m_last_access_time == 0)) {
              return &(m_set[set]->m_entry[ii]);
            }
          }

          for (int ii = 0; ii < m_assoc; ++ii) {
            cache_entry_c* line = &(m_set[set]->m_entry[ii]);
            if (line->m_appl_id == appl_id) {
              line->m_last_access_time = 0;
            }
          }
        }
      }
      else {
        for (int ii = 0; ii < m_assoc; ++ii) {
          cache_entry_c* line = &(m_set[set]->m_entry[ii]);
          if (line->m_appl_id == appl_id && line->m_last_access_time < min_lru) {
            index = ii;
            min_lru = line->m_last_access_time;
          }
        }
      }
    }
  }

  ASSERT(index != -1);

  return &(m_set[set]->m_entry[index]);
}


// initialize a cache line
void cache_ucp_c::initialize_cache_line(cache_entry_c *ins_line, Addr tag, Addr addr, 
    int appl_id, bool gpuline, int set, bool skip) 
{
  ins_line->m_valid            = true;
  ins_line->m_tag              = tag;
  ins_line->m_base             = (addr & ~m_offset_mask);
  ins_line->m_access_counter   = 0;
  ins_line->m_pref             = false;
  ins_line->m_appl_id          = appl_id;
  ins_line->m_gpuline          = gpuline;
  ins_line->m_last_access_time = m_simBase->m_simulation_cycle;
  ins_line->m_skip             = skip;

  ++m_num_entry[appl_id][set];
}


void cache_ucp_c::update_cache_on_access(Addr line_addr, int set, int appl_id)
{
  if (m_total_access_count == 1000) {
    if (m_access_count_by_type[0] == 0) {
      m_access_ratio = 1000.0;
    }
    else {
      m_access_ratio = static_cast<float>(1.0 * m_access_count_by_type[1] / m_access_count_by_type[0]);
    }
    m_total_access_count = 0;
  }

  if (set % m_modulo == 0) {
    int set_index = set / m_modulo;
    bool hit = false;
    int count = 0;
    for (auto I = m_atd[appl_id][set_index].begin(), E = m_atd[appl_id][set_index].end(); I != E; ++I) {
      if ((*I) == line_addr) {
        ++m_way_counter[appl_id][count];
        hit = true;
        break;
      }
      ++count;
    }


    if (!hit) {
      m_atd[appl_id][set_index].pop_back();
    }
    else {
      m_atd[appl_id][set_index].remove(line_addr);
    }
    m_atd[appl_id][set_index].push_front(line_addr);
  } 

  if (m_last_partition_cycle + m_partition_period <= m_simBase->m_simulation_cycle) {
    update_partition();
  }

  ++m_total_access_count;
}


// update application partition every *KNOB(KNOB_UCP_CACHE_PARTITION_PERIOD cycle
// use lookahead algorithm
void cache_ucp_c::update_partition(void)
{
  // assign at least 1 way
  fill_n(m_quota, m_max_application, 1);
  int count = m_num_way - m_num_application;


  if (*KNOB(KNOB_UCP_CACHE_FOR_GPU) == true && m_access_ratio >= 2.0) {
    for (int ii = 0; ii < m_num_application; ++ii) {
      if (m_simBase->m_PCL->get_appl_type(ii) != true)
        continue;

      for (int jj = 0; jj < m_num_way; ++jj) {
        m_way_counter[ii][jj] /= *KNOB(KNOB_UCP_CACHE_ACCESS_CONTROL);
        //m_way_counter[ii][jj] /= m_access_ratio;
      }
    }
  }


  int skip_cache[m_max_application] = {false};
  int skip_count = 0;
  for (int ii = 0; ii < m_num_application; ++ii) {
    // GPU && PSEL = 0
    if (*KNOB(KNOB_UCP_CACHE_FOR_GPU)) {
      if (m_simBase->m_PCL->get_appl_type(ii) == true && m_simBase->m_PCL->get_psel_mask() == false) {
        skip_cache[ii] = true;
        ++skip_count;
      }
    }

    if (*KNOB(KNOB_CACHE_FOR_STREAM_CPU)) {
      if (m_simBase->m_PCL->get_appl_type(ii) == false) {
        skip_cache[ii] = true;
        ++skip_count;
      }
    }
  }


  if (skip_count == m_num_application) {
    for (int ii = 0; ii < m_num_application; ++ii) {
      skip_cache[ii] = false;
    }
  }
  
  int total_hit[m_max_application] = {0};
  for (int ii = 0; ii < m_num_application; ++ii) {
    total_hit[ii] = m_way_counter[ii][0];
  }


  int random_index = m_simBase->m_simulation_cycle % m_num_application;
  bool skip = true;
  while (count > 0) {
    float max = -1.0;
    int max_appl_id = -1;
    int max_way = -1;
    int max_total_hit = 0;

    bool cpu_skip = false;
    // emulate GPU interference
    if (!*KNOB(KNOB_UCP_CACHE_FOR_GPU) && 
        rand() % 100 >= *KNOB(KNOB_UCP_CACHE_CPU_INTERFERENCE))
      cpu_skip = true;

    for (int ii = random_index; ii < random_index + m_num_application; ++ii) {
      int appl_id = ii % m_num_application;
      int sum = 0;


      // skip gpu application partitioning
      // skip : when there is not hit anymore for cpus, include gpu as well
      if (*KNOB(KNOB_UCP_CACHE_FOR_GPU) == true && skip && skip_cache[appl_id] == true)
        continue;


      // running streaming CPU application, so skip it
      if (*KNOB(KNOB_CACHE_FOR_STREAM_CPU) && m_simBase->m_PCL->get_appl_type(appl_id) == false)
        continue;


      // emulate GPU interference
      if (m_simBase->m_PCL->get_appl_type(appl_id) == false && cpu_skip == true)
        continue;


      // try to give less space to gpu
      if (*KNOB(KNOB_UCP_CACHE_FOR_GPU) && 
          m_simBase->m_PCL->get_appl_type(appl_id) == true && 
          rand() % 100 >= *KNOB(KNOB_UCP_CACHE_GPU_DROP_PROBABILITY))
        continue;


      // try to give less space if access rate is much higher
      if (*KNOB(KNOB_UCP_CACHE_FOR_GPU) &&
          m_simBase->m_PCL->get_appl_type(appl_id) == true &&
          m_access_ratio >= 3.0 &&
          rand() % 100 >= 50)
        continue;


      // set limitation for gpu in lookahead partitioning algorithm
      int max_lookup = count;
      if (*KNOB(KNOB_UCP_CACHE_FOR_GPU) == true && 
          m_simBase->m_PCL->get_appl_type(appl_id) == true && 
          count >= *KNOB(KNOB_UCP_CACHE_GPU_MAX_PARTITION_LOOKUP)) {
        max_lookup = *KNOB(KNOB_UCP_CACHE_GPU_MAX_PARTITION_LOOKUP);
      }

      for (int jj = m_quota[appl_id]; jj < m_quota[appl_id] + max_lookup; ++jj) {
        sum += m_way_counter[appl_id][jj];
        float marginal_utility = static_cast<float>(1.0* sum / (jj - m_quota[appl_id] + 1));
        if (marginal_utility > max) {
          max = marginal_utility;
          max_appl_id = appl_id;
          max_way = jj - m_quota[appl_id] + 1;
          max_total_hit = total_hit[appl_id];
          assert(max_way > 0);
        }
        // usual : give it to high total_hit (GPU)
        // ucp-g : give it evenly
        else if (marginal_utility == max && skip && total_hit[appl_id] > max_total_hit) {
          assert(skip == true);
          max = marginal_utility;
          max_appl_id = appl_id;
          max_way = jj - m_quota[appl_id] + 1;
          max_total_hit = total_hit[appl_id];
          assert(max_way > 0);
        }
      }
    }
    
    // If CPU does not have more hits, stop allocating to CPU, give it to GPUs.
    if (*KNOB(KNOB_UCP_CACHE_FOR_GPU) && max == 0.0 && skip == true) {
      skip = false;
      continue;
    }

    if (max_appl_id == -1)
      continue;

    int current_quota = m_quota[max_appl_id];
    for (int ii = current_quota; ii < current_quota + max_way; ++ii) {
      total_hit[max_appl_id] += m_way_counter[max_appl_id][ii];
    }
    //cout << max << " " << max_appl_id << " " << max_way << "\n";
    m_quota[max_appl_id] += max_way;
    count -= max_way;
    random_index++;
  }

  // halve the counter values
  for (int ii = 0; ii < m_num_application; ++ii) {
//    fprintf(g_mystderr, "%s appl%d %d at %lld\n", m_name.c_str(), ii, m_quota[ii], m_simBase->m_simulation_cycle);
    for (int jj = 0; jj < m_num_way; ++jj) {
      m_way_counter[ii][jj] /= 2;
    }
  }

  m_last_partition_cycle = m_simBase->m_simulation_cycle;
}

void cache_ucp_c::update_set_on_replacement(Addr tag, int appl_id, int set, bool gpuline)
{
  if (gpuline) {
    --m_num_gpu_line;
    --m_set[set]->m_num_gpu_line;
  }
  else {
    --m_num_cpu_line;
    --m_set[set]->m_num_cpu_line;
  }
  --m_num_entry[appl_id][set];
}

