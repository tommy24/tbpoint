/**********************************************************************************************
 * File         : internal/bp_internal.cc
 * Author       : Jaekyu Lee
 * Date         : 1/27/2011 
 * SVN          : $Id: frontend.h 915 2009-11-20 19:13:07Z kacear $:
 * Description  : various bp policies (not seen to public)
 *********************************************************************************************/


#include "../uop.h"
#include "../bp.h"
#include "../bp_gshare.h"
#include "../debug_macros.h"
#include "../factory_class.h"
#include "bp_internal.h"
#include "all_knobs.h"
#include "bp_tage.h"


#define DEBUG(args...)   _DEBUG(*m_simBase->m_knobs->KNOB_DEBUG_BP_DIR, ## args) 
#define PHT_INIT_VALUE                     (0x1 << *m_simBase->m_knobs->KNOB_PHT_CTR_BITS - 1) /* weakly taken */
#define COOK_HIST_BITS(hist,len,untouched) ((uns32)(hist) >> (32 - (len) + (untouched)) << (untouched))
#define COOK_ADDR_BITS(addr,len,shift)     (((uns32)(addr) >> (shift)) & (N_BIT_MASK((len))))
#define SAT_INC(val, max)               ((val) == (max) ? (max) : (val) + 1)
#define SAT_DEC(val, min)               ((val) == (min) ? (min) : (val) - 1)
#define PERCEPTRON_INIT_VALUE   0  /* mid point */
#define PERCEPTRON_HASH(addr)                  ( addr % *m_simBase->m_knobs->KNOB_PERCEPTRON_ENTRIES )
#define PERCEPTRON_THRESHOLD    (int32)((*m_simBase->m_knobs->KNOB_PERCEPTRON_THRESH_OVRD)? *m_simBase->m_knobs->KNOB_PERCEPTRON_THRESH_OVRD : ( 1.93*(*m_simBase->m_knobs->KNOB_BP_HIST_LENGTH) + 14)) 
#define MAX_WEIGHT		((1<<(*m_simBase->m_knobs->KNOB_PERCEPTRON_CTR_BITS-1))-1)
#define MIN_WEIGHT		(-(MAX_WEIGHT+1))


///////////////////////////////////////////////////////////////////////////////////////////////
// wrapper function to link internal bp policies
///////////////////////////////////////////////////////////////////////////////////////////////
bp_dir_base_c *bp_factory_internal(macsim_c* m_simBase) {
  string bp_class = m_simBase->m_knobs->KNOB_BP_DIR_MECH->getValue();
  bp_dir_base_c* ptr;
  if (bp_class == "perceptron") {
    ptr = new bp_perceptron_c (m_simBase);
  }
  else if (bp_class == "gselect") {
    ptr = new bp_gselect_c (m_simBase);
  }
  else if (bp_class == "gshare_pcGHR") {
    ptr = new bp_gshare_pcGHR_c (m_simBase);
  }
  else if (bp_class == "gshare_advanced") {
    ptr = new bp_gshare_advanced_c (m_simBase);
  }
  else if (bp_class == "bimodal") {
    ptr = new bp_bimodal_c (m_simBase);
  }
  else if (bp_class == "gshare_mtGHR") {
    ptr = new bp_gshare_mtGHR_c (m_simBase);
  }
  else if (bp_class == "always_taken") {
    ptr = new bp_always_taken_c(m_simBase);
  }
  else if (bp_class == "tage") {
    ptr = new bp_tage_c(m_simBase);
  }

  return ptr;
}




///////////////////////////////////////////////////////////////////////////////////////////////
// Singleton bp_internal_wrapper
///////////////////////////////////////////////////////////////////////////////////////////////
bp_internal_wrapper_c bp_internal_wrapper_c::Singleton;


///////////////////////////////////////////////////////////////////////////////////////////////
// bp_internal_wrapper constructor
// To register all modules
///////////////////////////////////////////////////////////////////////////////////////////////
bp_internal_wrapper_c::bp_internal_wrapper_c()
{
  bp_factory_c::get()->register_class("perceptron", bp_factory_internal);
  bp_factory_c::get()->register_class("gselect", bp_factory_internal);
  bp_factory_c::get()->register_class("gshare_pcGHR", bp_factory_internal);
  bp_factory_c::get()->register_class("gshare_advanced", bp_factory_internal);
  bp_factory_c::get()->register_class("bimodal", bp_factory_internal);
  bp_factory_c::get()->register_class("gshare_mtGHR", bp_factory_internal);
  bp_factory_c::get()->register_class("always_taken", bp_factory_internal);
  bp_factory_c::get()->register_class("tage", bp_factory_internal);
}


/**************************************************************************************/
/* bp_always_taken_init:  */
bp_always_taken_c::bp_always_taken_c(macsim_c* simBase) : bp_dir_base_c(simBase)
{
}

/**************************************************************************************/
/* bp_always_taken_pred:  */
uns8 bp_always_taken_c::pred(uop_c *uop)
{
  return 1;
}

/**************************************************************************************/
/* bp_always_taken_update:  */
void bp_always_taken_c::update(uop_c *uop)
{
}

/**************************************************************************************/
/* bp_always_taken_recover:  */
void bp_always_taken_c::recover(recovery_info_c *)
{
}


///////////////////////////////////////////////////////////////////////////////////////////////
// bp_internal_wrapper_c destructor
///////////////////////////////////////////////////////////////////////////////////////////////
bp_internal_wrapper_c::~bp_internal_wrapper_c()
{
}


bp_perceptron_c::bp_perceptron_c(macsim_c* simBase) : bp_dir_base_c(simBase)
{
    
  uns ii;   
  this->m_pt = (Perceptron *)malloc(sizeof(Perceptron) * (*m_simBase->m_knobs->KNOB_PERCEPTRON_ENTRIES));   
  for (ii = 0 ; ii < *m_simBase->m_knobs->KNOB_PERCEPTRON_ENTRIES  ; ii++) {   
    uns jj;   
    m_pt[ii].m_weights = (int32 *)malloc(sizeof(int32) * (*m_simBase->m_knobs->KNOB_BP_HIST_LENGTH + 1));    
    for(jj=0 ; jj < (*m_simBase->m_knobs->KNOB_BP_HIST_LENGTH + 1) ; jj++) {   
      m_pt[ii].m_weights[jj] = PERCEPTRON_INIT_VALUE;   
    }   
  }   
}

uns8 bp_perceptron_c::pred (uop_c *uop)
{
  Addr   addr   = uop->m_pc; 
  uns64 hist    = m_global_hist_64;
  uns32 index   = PERCEPTRON_HASH(addr);
  uns8  pred    = 0;
  int32 output  = 0;
  uns ii;
  uns64 mask;
  Perceptron *p;
  int32 *w;

  /* get pointers to that perceptron and its weights */

  p = &(m_pt[index]);
  w = &(p->m_weights[0]);

  /* initialize the output to the bias weight, and bump the pointer
   * to the weights
   */

  output = *(w++);

  /* find the (rest of the) dot product of the history register
   * and the perceptron weights.  note that, instead of actually
   * doing the expensive multiplies, we simply add a weight when the
   * corresponding branch in the history register is taken, or
   * subtract a weight when the branch is not taken.  this also lets
   * us use binary instead of bipolar logic to represent the history
   * register
   */
  for (mask=((uns64)1)<<63,ii=0; ii<*m_simBase->m_knobs->KNOB_HIST_LENGTH; ii++,mask>>=1,w++) {
    if (!!(hist & mask)) {
      output += *w;
	  }
    else {
      output += -(*w);
     }
  }

  /* record the various values needed to update the predictor */
    
  pred        =  (output < 0)? 0 : 1 ;
  uop->m_uop_info.m_pred_global_hist_64 = m_global_hist_64;
  this->m_global_hist_64 >>= 1;
  uop->m_recovery_info.m_global_hist_64 = (this->m_global_hist_64) | (((uns64)uop->m_dir) << 63);
  this->m_global_hist_64 |= (((uns64)pred) << 63);

  // For other predictors which use 32bit global history 
  uop->m_uop_info.m_pred_global_hist = m_global_hist;
  this->m_global_hist >>=1;
  uop->m_recovery_info.m_global_hist = (this->m_global_hist) | (uop->m_dir<<31);
  this->m_global_hist |= (pred << 31);
  uop->m_uop_info.m_perceptron_output = output ;
  DEBUG("perceptron_global_hist:%s -> %s index:%u pred:%d oracle_dir:%d \n",
        hexstr64(uop->m_uop_info.m_pred_global_hist_64),
        hexstr64(this->m_global_hist_64), index, pred, uop->m_dir);
  return pred;
}

void bp_perceptron_c::update (uop_c *uop)
{
  Addr addr     = uop->m_pc; 
  uns64 hist    = uop->m_uop_info.m_pred_global_hist_64;
  uns32 index   = PERCEPTRON_HASH(addr);          
  int32 output  = uop->m_uop_info.m_perceptron_output ;
  int y;
  uns64 mask;
  int32 * w;
  uns ii;

  /* if the output of the perceptron predictor is outside of
   * the range [-THETA,THETA] *and* the prediction was correct,
   * then we don't need to adjust the weights
   */
    
  if (output > PERCEPTRON_THRESHOLD) {
    y = 1;
	}
  else if (output < -PERCEPTRON_THRESHOLD) {
    y = 0;
	}
  else {
    y = 2;
	}
  if (y == 1 && uop->m_dir) {
    return;
	}
  if (y == 0 && !(uop->m_dir)) { 
    return;
	}
    
  /* w is a pointer to the first weight (the bias weight) */
  w = &(this->m_pt[index].m_weights[0]);
    
  /* if the branch was taken, increment the bias weight,
   * else decrement it, with saturating arithmetic
   */

  if (uop->m_dir) {
    (*w)++;
	}
  else {
    (*w)--;
	}
  if (*w > MAX_WEIGHT) {
    *w = MAX_WEIGHT;
	}
  if (*w < MIN_WEIGHT) {
    *w = MIN_WEIGHT;
	}
    
  /* now w points to the next weight */
  w++;

  /* for each weight and corresponding bit in the history register... */
  for (mask=((uns64)1)<<63,ii=0; ii<*m_simBase->m_knobs->KNOB_BP_HIST_LENGTH; ii++,mask>>=1,w++) {
	
    /* if the i'th bit in the history positively correlates
     * with this branch outcome, increment the corresponding 
     * weight, else decrement it, with saturating arithmetic
     */
	
    if (!!(hist & mask) == uop->m_dir) {
      (*w)++;
      if (*w > MAX_WEIGHT){
	  *w = MAX_WEIGHT;
      }
	}
 	else {
      (*w)--;
      if (*w < MIN_WEIGHT) {
	  *w = MIN_WEIGHT;
	  }
    }
  }
  DEBUG("perceptron update *w:%d index:%d \n", *w, index); 
}

void bp_perceptron_c::recover (recovery_info_c *recovery_info)
{
  m_global_hist= recovery_info->m_global_hist;
  m_global_hist_64 = recovery_info->m_global_hist_64;
}

/**************************************************************************************/
/* Bimodal Branch Predictor: bp_bimodal_c */
bp_bimodal_c::bp_bimodal_c(macsim_c* simBase) : bp_dir_base_c(simBase)
{
  m_pht = (uns8 *)malloc(sizeof(uns8) * (0x1 << *m_simBase->m_knobs->KNOB_BP_HIST_LENGTH));
  for (uns ii = 0; ii < (0x1 << *m_simBase->m_knobs->KNOB_BP_HIST_LENGTH); ii++) {
    m_pht[ii] = PHT_INIT_VALUE;
  }
}

bp_bimodal_c::~bp_bimodal_c()
{
  free(m_pht);
}

uns8 bp_bimodal_c::pred(uop_c *uop)
{
  uns32 pht_index = COOK_ADDR_BITS(uop->m_pc, *m_simBase->m_knobs->KNOB_BP_HIST_LENGTH, 2);
  uns8 counter = m_pht[pht_index];
  return (counter >> (*m_simBase->m_knobs->KNOB_PHT_CTR_BITS - 1)) & 0x1;
}

void bp_bimodal_c::update(uop_c *uop)
{
  uns32 pht_index = COOK_ADDR_BITS(uop->m_pc, *m_simBase->m_knobs->KNOB_BP_HIST_LENGTH, 2);
  uns8 counter = m_pht[pht_index];
  if (uop->m_dir) {
    m_pht[pht_index] = SAT_INC(counter, N_BIT_MASK(*m_simBase->m_knobs->KNOB_PHT_CTR_BITS));
	}
  else {
    m_pht[pht_index] = SAT_DEC(counter, 0);
   }
}

void bp_bimodal_c::recover(recovery_info_c *recovery_info) {}
/**************************************************************************************/


/**************************************************************************************/
/* gselect Branch Predictor: bp_gselect_c: */
bp_gselect_c::bp_gselect_c(macsim_c* simBase) : bp_dir_base_c(simBase)
{
  m_global_hist = 0;
  m_global_hist_64 = 0;
  m_pht = (uns8 *)malloc(sizeof(uns8) * (0x1 << (2 * *m_simBase->m_knobs->KNOB_BP_HIST_LENGTH)));
  for (uns ii = 0; ii < (0x1 << (2 * *m_simBase->m_knobs->KNOB_BP_HIST_LENGTH)); ii++) {
    m_pht[ii] = PHT_INIT_VALUE;
   }
}

bp_gselect_c::~bp_gselect_c()
{
  free(m_pht);
}

uns8 bp_gselect_c::pred(uop_c *uop)
{
  Addr addr = uop->m_pc;
  uns32 addr_lower = COOK_ADDR_BITS(addr, *m_simBase->m_knobs->KNOB_BP_HIST_LENGTH, 2);
  uop->m_uop_info.m_pred_global_hist = m_global_hist;
  uns32 pred;
  if (*m_simBase->m_knobs->KNOB_BP_HIST_HIGH_BIT_MOST_RECENT) {    
  } 
  else {
    uns32 hist = m_global_hist & N_BIT_MASK(*m_simBase->m_knobs->KNOB_BP_HIST_LENGTH);
    uns32 pht_index = (hist << *m_simBase->m_knobs->KNOB_BP_HIST_LENGTH) | addr_lower;
    uns8 counter = m_pht[pht_index]; 
    pred = (counter >> (*m_simBase->m_knobs->KNOB_PHT_CTR_BITS - 1)) & 0x1;
    uop->m_recovery_info.m_global_hist = (m_global_hist << 1) | uop->m_dir;
    if (*m_simBase->m_knobs->KNOB_ADD_PRED_TO_BP_HIST) {
      m_global_hist = (m_global_hist << 1) | pred;
	 }
  }
  return pred;
}

void bp_gselect_c::update(uop_c *uop)
{
  Addr addr = uop->m_pc;
  uns32 addr_lower = COOK_ADDR_BITS(addr, *m_simBase->m_knobs->KNOB_BP_HIST_LENGTH, 2);
  uns32 pht_index;
  if (*m_simBase->m_knobs->KNOB_BP_HIST_HIGH_BIT_MOST_RECENT) {
  }
  else {
    uns32 hist = uop->m_uop_info.m_pred_global_hist & N_BIT_MASK(*m_simBase->m_knobs->KNOB_BP_HIST_LENGTH);
    pht_index = (hist << *m_simBase->m_knobs->KNOB_BP_HIST_LENGTH) | addr_lower;
    if (!*m_simBase->m_knobs->KNOB_ADD_PRED_TO_BP_HIST) {
      m_global_hist = (m_global_hist << 1) | (uop->m_dir);
	}
  }

  uns8 counter = m_pht[pht_index];
  if (uop->m_dir) {
  	m_pht[pht_index] = SAT_INC(counter, N_BIT_MASK(*m_simBase->m_knobs->KNOB_PHT_CTR_BITS));
	}
   else {
  	m_pht[pht_index] = SAT_DEC(counter, 0);
	}
}

void bp_gselect_c::recover(recovery_info_c* recovery_info)
{
  if (*m_simBase->m_knobs->KNOB_ADD_PRED_TO_BP_HIST) {
    m_global_hist = recovery_info->m_global_hist;
    m_global_hist_64 = recovery_info->m_global_hist_64;
  }
}
/**************************************************************************************/

/**************************************************************************************/
/* gshare Branch Predictor (Dilan's implementation): bp_gshare_advanced_c: */
bp_gshare_advanced_c::bp_gshare_advanced_c(macsim_c* simBase) : bp_dir_base_c(simBase)
{
  m_global_hist = 0;
  m_global_hist_64 = 0;
  m_pht = (uns8 *)malloc(sizeof(uns8) * (0x1 << *m_simBase->m_knobs->KNOB_BP_HIST_LENGTH));
  for (uns ii = 0; ii < (0x1 << *m_simBase->m_knobs->KNOB_BP_HIST_LENGTH); ii++) {
    m_pht[ii] = PHT_INIT_VALUE;
	}
}

bp_gshare_advanced_c::~bp_gshare_advanced_c()
{
  free(m_pht);
}

uns8 bp_gshare_advanced_c::pred(uop_c *uop)
{
  Addr addr = uop->m_pc;
  uns32 addr_lower = COOK_ADDR_BITS(addr, *m_simBase->m_knobs->KNOB_BP_HIST_LENGTH, 2);
  uop->m_uop_info.m_pred_global_hist = m_global_hist;  
  uns8 pred;
  if (*m_simBase->m_knobs->KNOB_BP_HIST_HIGH_BIT_MOST_RECENT) {
  }
  else {
    uns32 hist = m_global_hist & N_BIT_MASK(*m_simBase->m_knobs->KNOB_BP_HIST_LENGTH);
    uns32 pht_index = addr_lower ^ hist; 
    uns8 counter = m_pht[pht_index]; 
    pred = (counter >> (*m_simBase->m_knobs->KNOB_PHT_CTR_BITS - 1)) & 0x1;
    uop->m_recovery_info.m_global_hist = (m_global_hist << 1) | uop->m_dir;
    if (*m_simBase->m_knobs->KNOB_ADD_PRED_TO_BP_HIST) {
      m_global_hist = (m_global_hist << 1) | pred;
     } 
  }
  return pred;
}

void bp_gshare_advanced_c::update(uop_c *uop)
{
  Addr addr = uop->m_pc;
  uns32 addr_lower = COOK_ADDR_BITS(addr, *m_simBase->m_knobs->KNOB_BP_HIST_LENGTH, 2);
  uns32 hist;
  if (*m_simBase->m_knobs->KNOB_BP_HIST_HIGH_BIT_MOST_RECENT) {
  }
  else {
    hist = (uop->m_uop_info.m_pred_global_hist) & N_BIT_MASK(*m_simBase->m_knobs->KNOB_BP_HIST_LENGTH);
    if (!*m_simBase->m_knobs->KNOB_ADD_PRED_TO_BP_HIST)
      m_global_hist = (m_global_hist << 1) | (uop->m_dir);
  }
  uns32 pht_index = addr_lower ^ hist; // Exclusive or the bits to identify the PHT index
  uns8 counter = m_pht[pht_index];
  // Change the state of the bimodal counter based on whether the branch was taken or not taken
  if (uop->m_dir) {
    m_pht[pht_index] = SAT_INC(counter, N_BIT_MASK(*m_simBase->m_knobs->KNOB_PHT_CTR_BITS));
   }
  else {
    m_pht[pht_index] = SAT_DEC(counter, 0);
	}
}

void bp_gshare_advanced_c::recover(recovery_info_c *recovery_info)
{
  if (*m_simBase->m_knobs->KNOB_ADD_PRED_TO_BP_HIST) {
    m_global_hist = recovery_info->m_global_hist;
    m_global_hist_64 = recovery_info->m_global_hist_64;
  }
}
/**************************************************************************************/

/**************************************************************************************/
/* gshare Branch Predictor with PC GHR thread switching policy: bp_gshare_pcGHR_c: */
bp_gshare_pcGHR_c::bp_gshare_pcGHR_c(macsim_c* simBase) : bp_dir_base_c(simBase)
{
  m_current_thread_id = -1;
  // Initialize Global History
  m_global_hist = 0;
  m_global_hist_64 = 0;
  // Allocate the Pattern-History Table that will contain the bimodal counters.
  // The size of the Pattern-History Table is determined by doubling the number of bits 
  // used in the global history register
  m_pht = (uns8 *)malloc(sizeof(uns8) * (0x1 << *m_simBase->m_knobs->KNOB_BP_HIST_LENGTH));
  // Initialize the Pattern-History Table values to the initial value
  for (uns ii = 0; ii < (0x1 << *m_simBase->m_knobs->KNOB_BP_HIST_LENGTH); ii++) {
    m_pht[ii] = PHT_INIT_VALUE;
	}
}

bp_gshare_pcGHR_c::~bp_gshare_pcGHR_c()
{
  free(m_pht);
}

uns8 bp_gshare_pcGHR_c::pred(uop_c *uop)
{
  Addr addr = uop->m_pc;
  if ((m_current_thread_id == -1) || (m_current_thread_id != uop->m_thread_id)) {
    m_current_thread_id = uop->m_thread_id;
    m_global_hist = addr;
  }
   
  uop->m_uop_info.m_pred_global_hist = m_global_hist;
  // Get the lower n bits of the PC address (ignoring the least two significant bits of the pc)
  uns32 addr_lower = COOK_ADDR_BITS(addr, *m_simBase->m_knobs->KNOB_BP_HIST_LENGTH, 2);
  uns8 pred;
  if (*m_simBase->m_knobs->KNOB_BP_HIST_HIGH_BIT_MOST_RECENT) {
    // TODO: Global History stored where most recent branch outcome at highest-bit  
  }
  else {
    // Get the lower n bits of the global history
    uns32 hist = m_global_hist & N_BIT_MASK(*m_simBase->m_knobs->KNOB_BP_HIST_LENGTH);
    uns32 pht_index = addr_lower ^ hist; // Exclusive or the bits to identify the PHT index
    uns8 counter = m_pht[pht_index]; // Get out the corresponding bimodal counter
    // Identify whether counter predicts taken or not taken. This is done by checking
    // whether the highest order bit of the counter is 1 or 0
    pred = (counter >> (*m_simBase->m_knobs->KNOB_PHT_CTR_BITS - 1)) & 0x1;
    uop->m_recovery_info.m_global_hist = (m_global_hist << 1) | uop->m_dir;
    uop->m_recovery_info.m_thread_id = m_current_thread_id;
    if (*m_simBase->m_knobs->KNOB_ADD_PRED_TO_BP_HIST) {
      m_global_hist = (m_global_hist << 1) | pred;
	 }
  }
  return pred;
}

void bp_gshare_pcGHR_c::update(uop_c *uop)
{
  Addr addr = uop->m_pc;
  uns32 addr_lower = COOK_ADDR_BITS(addr, *m_simBase->m_knobs->KNOB_BP_HIST_LENGTH, 2);
  uns32 hist;
  if (*m_simBase->m_knobs->KNOB_BP_HIST_HIGH_BIT_MOST_RECENT) {
    // TODO: Global History stored where most recent branch outcome at highest-bit 
  }
  else {
    hist = (uop->m_uop_info.m_pred_global_hist) & N_BIT_MASK(*m_simBase->m_knobs->KNOB_BP_HIST_LENGTH);
    if (!*m_simBase->m_knobs->KNOB_ADD_PRED_TO_BP_HIST) {
      if (m_current_thread_id == uop->m_thread_id) {
        m_global_hist = (m_global_hist << 1) | (uop->m_dir);
	   }
    }
  }
  uns32 pht_index = addr_lower ^ hist; // Exclusive or the bits to identify the PHT index
  uns8 counter = m_pht[pht_index];
  // Change the state of the bimodal counter based on whether the branch was taken or not taken
  if (uop->m_dir) {
    m_pht[pht_index] = SAT_INC(counter, N_BIT_MASK(*m_simBase->m_knobs->KNOB_PHT_CTR_BITS));
  }
  else {
    m_pht[pht_index] = SAT_DEC(counter, 0);
  }
}

void bp_gshare_pcGHR_c::recover(recovery_info_c *recovery_info)
{
  if ((*m_simBase->m_knobs->KNOB_ADD_PRED_TO_BP_HIST) && (m_current_thread_id == recovery_info->m_thread_id)) {
    m_global_hist = recovery_info->m_global_hist;
    m_global_hist_64 = recovery_info->m_global_hist_64;
  }
}
/**************************************************************************************/

/**************************************************************************************/
/* gshare Branch Predictor with saving of thread branch history: bp_gshare_mtGHR_c: */
bp_gshare_mtGHR_c::bp_gshare_mtGHR_c(macsim_c* simBase) : bp_dir_base_c(simBase)
{
  m_current_thread_id = -1;
  // Initialize Global History
  m_global_hist = 0;
  m_global_hist_64 = 0;
  // Allocate the Pattern-History Table that will contain the bimodal counters.
  // The size of the Pattern-History Table is determined by doubling the number of bits 
  // used in the global history register
  m_pht = (uns8 *)malloc(sizeof(uns8) * (0x1 << *m_simBase->m_knobs->KNOB_BP_HIST_LENGTH));
  // Initialize the Pattern-History Table values to the initial value
  for (uns ii = 0; ii < (0x1 << *m_simBase->m_knobs->KNOB_BP_HIST_LENGTH); ii++) {
    m_pht[ii] = PHT_INIT_VALUE;
   }

  if (!*m_simBase->m_knobs->KNOB_PERFECT_GHR_SWITCHING) {
    m_mtGHR_table = (uns32 *)malloc(sizeof(uns32) * (0x1 << *m_simBase->m_knobs->KNOB_MT_GHR_INDEX_LENGTH));
    for (uns ii = 0; ii < (0x1 << *m_simBase->m_knobs->KNOB_MT_GHR_INDEX_LENGTH); ii++) {
      m_mtGHR_table[ii] = 0;
    }
  }
}

bp_gshare_mtGHR_c::~bp_gshare_mtGHR_c()
{
  free(m_pht);
  if (!*m_simBase->m_knobs->KNOB_PERFECT_GHR_SWITCHING) { 
    free(m_mtGHR_table);
   }
}

uns8 bp_gshare_mtGHR_c::pred(uop_c *uop)
{
  Addr addr = uop->m_pc;
  if (m_current_thread_id == -1) {
     m_current_thread_id = uop->m_thread_id;
     m_global_hist = 0;
  }
  else if (m_current_thread_id != uop->m_thread_id) {
    if (*m_simBase->m_knobs->KNOB_PERFECT_GHR_SWITCHING) {
      m_mtGHR_map[m_current_thread_id] = m_global_hist;
      m_current_thread_id = uop->m_thread_id;
      m_global_hist = m_mtGHR_map[m_current_thread_id];   
    }
    else {
      m_mtGHR_table[m_current_thread_id & N_BIT_MASK(*m_simBase->m_knobs->KNOB_MT_GHR_INDEX_LENGTH)] = m_global_hist;
      m_current_thread_id = uop->m_thread_id;
      m_global_hist = m_mtGHR_table[m_current_thread_id & N_BIT_MASK(*m_simBase->m_knobs->KNOB_MT_GHR_INDEX_LENGTH)];
    }
  }
    
  uop->m_uop_info.m_pred_global_hist = m_global_hist;
  // Get the lower n bits of the PC address (ignoring the least two significant bits of the pc)
  uns32 addr_lower = COOK_ADDR_BITS(addr, *m_simBase->m_knobs->KNOB_BP_HIST_LENGTH, 2);

  uns8 pred;
  if (*m_simBase->m_knobs->KNOB_BP_HIST_HIGH_BIT_MOST_RECENT) {
    // TODO: Global History stored where most recent branch outcome at highest-bit  
  }
  else {
    // Get the lower n bits of the global history
    uns32 hist = m_global_hist & N_BIT_MASK(*m_simBase->m_knobs->KNOB_BP_HIST_LENGTH);
    uns32 pht_index = addr_lower ^ hist; // Exclusive or the bits to identify the PHT index
    uns8 counter = m_pht[pht_index]; // Get out the corresponding bimodal counter
    // Identify whether counter predicts taken or not taken. This is done by checking
    // whether the highest order bit of the counter is 1 or 0
    pred = (counter >> (*m_simBase->m_knobs->KNOB_PHT_CTR_BITS - 1)) & 0x1;

    uop->m_recovery_info.m_global_hist = (m_global_hist << 1) | uop->m_dir;
    uop->m_recovery_info.m_thread_id = m_current_thread_id;
    if (*m_simBase->m_knobs->KNOB_ADD_PRED_TO_BP_HIST) {
      m_global_hist = (m_global_hist << 1) | pred;
	 }
  }
  return pred;
}

void bp_gshare_mtGHR_c::update(uop_c *uop)
{
  Addr addr = uop->m_pc;
  uns32 addr_lower = COOK_ADDR_BITS(addr, *m_simBase->m_knobs->KNOB_BP_HIST_LENGTH, 2);
  uns32 hist;
  if (*m_simBase->m_knobs->KNOB_BP_HIST_HIGH_BIT_MOST_RECENT) {
    // TODO: Global History stored where most recent branch outcome at highest-bit 
  }
  else {
    hist = (uop->m_uop_info.m_pred_global_hist) & N_BIT_MASK(*m_simBase->m_knobs->KNOB_BP_HIST_LENGTH);
    if (!*m_simBase->m_knobs->KNOB_ADD_PRED_TO_BP_HIST) {
      if (m_current_thread_id == uop->m_thread_id) {
        m_global_hist = (m_global_hist << 1) | (uop->m_dir);
      }
      else if (!*m_simBase->m_knobs->KNOB_UPDATE_CURRENT_HIST_ONLY) {
        if (*m_simBase->m_knobs->KNOB_PERFECT_GHR_SWITCHING) {
          m_mtGHR_map[uop->m_thread_id] = m_mtGHR_map[uop->m_thread_id] << 1 | (uop->m_dir);
        }
        else {
          m_mtGHR_map[uop->m_thread_id & N_BIT_MASK(*m_simBase->m_knobs->KNOB_MT_GHR_INDEX_LENGTH)] 
              = m_mtGHR_map[uop->m_thread_id & N_BIT_MASK(*m_simBase->m_knobs->KNOB_MT_GHR_INDEX_LENGTH)] << 1 | (uop->m_dir);
        }        
      }
    }
  }
  uns32 pht_index = addr_lower ^ hist; // Exclusive or the bits to identify the PHT index
  uns8 counter = m_pht[pht_index];
  // Change the state of the bimodal counter based on whether the branch was taken or not taken
  if (uop->m_dir) {
    m_pht[pht_index] = SAT_INC(counter, N_BIT_MASK(*m_simBase->m_knobs->KNOB_PHT_CTR_BITS));
  }
  else {
    m_pht[pht_index] = SAT_DEC(counter, 0);\
  }
}

void bp_gshare_mtGHR_c::recover(recovery_info_c *recovery_info)
{
  if (*m_simBase->m_knobs->KNOB_ADD_PRED_TO_BP_HIST) {
    if (m_current_thread_id == recovery_info->m_thread_id) {
      m_global_hist = recovery_info->m_global_hist;
      m_global_hist_64 = recovery_info->m_global_hist_64;
    }
    else if (!*m_simBase->m_knobs->KNOB_UPDATE_CURRENT_HIST_ONLY) {
      if (*m_simBase->m_knobs->KNOB_PERFECT_GHR_SWITCHING) {
        m_mtGHR_map[recovery_info->m_thread_id] = recovery_info->m_global_hist;
      }
      else {
        m_mtGHR_map[recovery_info->m_thread_id & N_BIT_MASK(*m_simBase->m_knobs->KNOB_MT_GHR_INDEX_LENGTH)] 
          = recovery_info->m_global_hist;
      }
    }
  }
}
/**************************************************************************************/


