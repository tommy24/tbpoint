/**********************************************************************************************
 * File         : internal/bp_internal.h
 * Author       : Jaekyu Lee
 * Date         : 1/27/2011 
 * SVN          : $Id: frontend.h 915 2009-11-20 19:13:07Z kacear $:
 * Description  : various bp policies (not seen to public)
 *********************************************************************************************/


#ifndef FETCH_INTERNAL_H
#define FETCH_INTERNAL_H


#include "../global_defs.h"
#include "../global_types.h"
#include "../bp.h"


// To transparently link internal modules, we need to have
// 1. wrapper class to bp_internal_c
// 2. singleton wrapper entry
class bp_internal_wrapper_c
{
  public:
    bp_internal_wrapper_c();
    ~bp_internal_wrapper_c();

    static bp_internal_wrapper_c Singleton;
};


// Always Taken Predictor
class bp_always_taken_c : public bp_dir_base_c
{
    private:
    bp_always_taken_c(const bp_always_taken_c& rhs);
    const bp_always_taken_c& operator=(const bp_always_taken_c& rhs);

    public:
    bp_always_taken_c(macsim_c* simBase);
    ~bp_always_taken_c(void);

    uns8 pred(uop_c *uop);
    void update(uop_c *uop);
    void recover(recovery_info_c *);
};

// Bimodal Branch Predictor
class bp_bimodal_c : public bp_dir_base_c
{
    private:
    bp_bimodal_c(const bp_bimodal_c& rhs);
    const bp_bimodal_c& operator=(const bp_bimodal_c& rhs);

    public:
    bp_bimodal_c(macsim_c* simBase);
    ~bp_bimodal_c(void);

    uns8 pred(uop_c *uop);
    void update(uop_c *uop);
    void recover(recovery_info_c *);
};


// gselect Branch Predictor
class bp_gselect_c : public bp_dir_base_c
{
    private:
    bp_gselect_c(const bp_gselect_c& rhs);
    const bp_gselect_c& operator=(const bp_gselect_c& rhs);

    public:
    bp_gselect_c(macsim_c* simBase);
    ~bp_gselect_c(void);

    uns8 pred(uop_c *uop);
    void update(uop_c *uop);
    void recover(recovery_info_c *);
};


// gshare (advanced implementation) Branch Predictor
class bp_gshare_advanced_c : public bp_dir_base_c
{
    private:
    bp_gshare_advanced_c(const bp_gshare_advanced_c& rhs);
    const bp_gshare_advanced_c& operator=(const bp_gshare_advanced_c& rhs);

    public:
    bp_gshare_advanced_c(macsim_c* simBase);
    ~bp_gshare_advanced_c(void);

    uns8 pred(uop_c *uop);
    void update(uop_c *uop);
    void recover(recovery_info_c *);
};


// gshare Branch Predictor with pc GHR thread switching policy 
class bp_gshare_pcGHR_c : public bp_dir_base_c
{
    private:
    bp_gshare_pcGHR_c(const bp_gshare_pcGHR_c& rhs);
    const bp_gshare_pcGHR_c& operator=(const bp_gshare_pcGHR_c& rhs);

    public:
    bp_gshare_pcGHR_c(macsim_c* simBase);
    ~bp_gshare_pcGHR_c(void);
    uns8 pred(uop_c *uop);
    void update(uop_c *uop);
    void recover(recovery_info_c *);
    int m_current_thread_id;
};


// gshare Branch Predictor with pc GHR thread switching policy 
class bp_gshare_mtGHR_c : public bp_dir_base_c
{
    private:
    bp_gshare_mtGHR_c(const bp_gshare_mtGHR_c& rhs);
    const bp_gshare_mtGHR_c& operator=(const bp_gshare_mtGHR_c& rhs);

    public:
    int m_current_thread_id;
	map<int, uns32> m_mtGHR_map;
	uns32* m_mtGHR_table;

    bp_gshare_mtGHR_c(macsim_c* simBase);
    ~bp_gshare_mtGHR_c(void);
    uns8 pred(uop_c *uop);
    void update(uop_c *uop);
    void recover(recovery_info_c *);
};



typedef struct per_data_s{
  int *m_weights;
} per_data_s;

typedef struct per_state_s{
  char m_dummy_counter;
  int m_prediction;
  int m_output;
  unsigned long long int m_history;
  per_data_s *m_perc;
} per_state_s;


typedef struct Perceptron_struct{ 
  int32 *m_weights;
} Perceptron; 

class bp_perceptron_c : public bp_dir_base_c 
{
  private:
  bp_perceptron_c(const bp_perceptron_c& rhs);
  const bp_perceptron_c& operator=(const bp_perceptron_c& rhs);
  Perceptron *m_pt;

  public:
  bp_perceptron_c(macsim_c* simBase);
  ~bp_perceptron_c(void) {} 

  uns8 pred(uop_c *uop);
  void update(uop_c *uop);
  void recover(recovery_info_c *); 
};

#endif // BP_DIR_MECH_H_INCLUDED 



