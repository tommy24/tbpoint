/*
Copyright (c) <2012>, <Georgia Institute of Technology> All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted 
provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions 
and the following disclaimer.

Redistributions in binary form must reproduce the above copyright notice, this list of 
conditions and the following disclaimer in the documentation and/or other materials provided 
with the distribution.

Neither the name of the <Georgia Institue of Technology> nor the names of its contributors 
may be used to endorse or promote products derived from this software without specific prior 
written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR 
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY 
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
POSSIBILITY OF SUCH DAMAGE.
*/


/**********************************************************************************************
 * File         : process_manager.cc
 * Author       : Jaekyu Lee
 * Date         : 1/6/2011
 * SVN          : $Id: main.cc,v 1.26 2008-09-21 00:02:54 kacear Exp $:
 * Description  : process manager (creation, termination, scheduling, ...)
 *                combine sim_process and sim_thread_schedule
 *********************************************************************************************/


///////////////////////////////////////////////////////////////////////////////////////////////
/// \page process Process management
///
/// In process, we manage each application as a process which consists of one or more
/// threads (for multi-threaded CPU or GPU applications).
///
/// \section process_core Core Partition
///
/// \section process_queue Queues
/// We maintain two separate queues for the simulation; thread queue and block queue. Thread
/// queue has CPU traces and block queue has GPU traces. Especially, block queue consists of
/// thread list. Each list in the block queue will have threads from the same thread block.
/// \see process_manager_c::m_thread_queue
/// \see process_manager_c::m_block_queue
///
/// \section process_schedule Thread Scheduling
/// When a core becomes available, based on the core type, it pulls out a thread from 
/// the corresponding queue. Each core has maximum number of threads that can run concurrently.
/// 
/// \section process_storage Storage Management
/// Since each thread requires its own data structure and GPU simulation will have huge number
/// of threads, GPU simulation will require huge memory consumption. However, since there are
/// limited number of threads at a certain time, we just need to maintain data structure for
/// those active threads. In each thread/block queue, it will have dummy thread structures.
/// When a thread is actually scheduled to the core, its data structure will be allocated.
/// 
/// \todo We need to carefully handle core allocation process. multiple x86s, multi-threaded..
///////////////////////////////////////////////////////////////////////////////////////////////


#include <iostream>
#include <fstream>
#include <cstring>
#include <sstream>

#include "assert_macros.h"
#include "knob.h"
#include "core.h"
#include "utils.h"
#include "statistics.h"
#include "frontend.h"
#include "process_manager.h"
#include "pref_common.h"
#include "trace_read.h"

#include "debug_macros.h"

#include "all_knobs.h"

///////////////////////////////////////////////////////////////////////////////////////////////


#define BLOCK_ID_SHIFT 16 
#define THREAD_ID_MASK 0xFFF 
#define BLOCK_ID_MOD 16384 

#define DEBUG(args...) _DEBUG(*m_simBase->m_knobs->KNOB_DEBUG_SIM_THREAD_SCHEDULE, ## args)

///////////////////////////////////////////////////////////////////////////////////////////////


// thread_stat_s constructor
thread_stat_s::thread_stat_s()
{
  m_thread_id          = 0;
  m_unique_thread_id   = 0;
  m_block_id           = 0;
  m_thread_sched_cycle = 0;
  m_thread_fetch_cycle = 0;
  m_thread_end_cycle   = 0;
}

////////////////////////////////////////////////////////////////////////////////
//  process_manager_c() - constructor
//   m_thread_queue - contains the list of unassigned threads (from all 
//  applications) that are ready to be launched
//   m_block_queue - contains the list of unassigned blocks (from all 
//  applications) that are ready to be launched
////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
//  process_s() - constructor
////////////////////////////////////////////////////////////////////////////////
process_s::process_s()
{
  m_process_id = 0;
  m_orig_pid = 0;
  m_max_block = 0;
  m_thread_start_info = NULL;
  m_thread_trace_info = NULL;
  m_no_of_threads = 0;
  m_no_of_threads_created = 0;
  m_no_of_threads_terminated = 0;
  m_core_pool = NULL;
  m_ptx = false;
  m_repeat = 0;
  m_current_file_name_base = "";
  m_kernel_config_name = "";
  m_current_vector_index = 0;
  m_inst_count_tot = 0;
  m_block_count          = 0;
}


////////////////////////////////////////////////////////////////////////////////
//  process_s() - constructor
////////////////////////////////////////////////////////////////////////////////
process_s::~process_s()
{
}


////////////////////////////////////////////////////////////////////////////////
//  thread_s() - constructor
////////////////////////////////////////////////////////////////////////////////
thread_s::thread_s(macsim_c* simBase)
{
  m_simBase         = simBase;
  m_fetch_data      = new frontend_s; 
  int buf_ele_size  = (CPU_TRACE_SIZE > GPU_TRACE_SIZE) ? CPU_TRACE_SIZE : GPU_TRACE_SIZE;
  m_buffer          = new char[1000 * buf_ele_size];
  m_prev_trace_info = NULL;
  m_next_trace_info = NULL;

  for (int ii = 0; ii < MAX_PUP; ++ii) {
    m_trace_uop_array[ii] = new trace_uop_s;
    if (static_cast<string>(*m_simBase->m_knobs->KNOB_FETCH_POLICY) == "row_hit") {
      m_next_trace_uop_array[ii] = new trace_uop_s;
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
//  thread_s() - destructor
////////////////////////////////////////////////////////////////////////////////
thread_s::~thread_s()
{
#if 0
  delete m_fetch_data;
  delete[] m_buffer;
  delete m_prev_trace_info;
  delete m_next_trace_info;
  
  for (int ii = 0; ii < MAX_PUP; ++ii) {
    delete m_trace_uop_array[ii];
    if (static_cast<string>(*m_simBase->m_knobs->KNOB_FETCH_POLICY) == "row_hit") {
      delete m_next_trace_uop_array[ii];
    }
  }
#endif
}

////////////////////////////////////////////////////////////////////////////////
//  block_schedule_info_s() - constructor
////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////


// block_schedule_info_s constructor
block_schedule_info_s::block_schedule_info_s()
{
  m_start_to_fetch        = false;
  m_dispatched_core_id    = -1;
  m_retired               = false;
  m_dispatched_thread_num = 0;
  m_retired_thread_num    = 0;
  m_dispatch_done         = false;
  m_trace_exist           = false;
  m_total_thread_num      = 0;
}


block_schedule_info_s::~block_schedule_info_s()
{
}


///////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//  process_manager_c() - constructor
//   m_thread_queue - contains the list of unassigned threads (from all 
//  applications) that are ready to be launched
//   m_block_queue - contains the list of unassigned blocks (from all 
//  applications) that are ready to be launched
////////////////////////////////////////////////////////////////////////////////
process_manager_c::process_manager_c(macsim_c* simBase)
{
  //base simulation reference
  m_simBase = simBase;

  // allocate queues
  m_thread_queue = new list<thread_trace_info_node_s *>;
  m_block_queue  = new unordered_map<int, list<thread_trace_info_node_s *> *>;
  m_inst_hash_pool = new pool_c<hash_c<inst_info_s> >(1, "inst_hash_pool");
}


////////////////////////////////////////////////////////////////////////////////
//  process_manager_c() - destructor
////////////////////////////////////////////////////////////////////////////////
process_manager_c::~process_manager_c()
{
  // deallocate thread queue
  m_thread_queue->clear();
  delete m_thread_queue;

  // deallocate block queue
  m_block_queue->clear();
  delete m_block_queue;

//  delete m_inst_hash_pool;
}


////////////////////////////////////////////////////////////////////////////////
//  process_manager_c::create_thread_node()
//   called for each thread/warp when it becomes ready to be launched (started);
//  allocates a node for the thread/warp and add its to m_thread_queue (for x86)
//  or m_block_queue (for ptx)
////////////////////////////////////////////////////////////////////////////////
void process_manager_c::create_thread_node(process_s* process, int tid, bool main)
{
  // create a new thread node
  thread_trace_info_node_s* node = m_simBase->m_trace_node_pool->acquire_entry();

  // initialize the node
  node->m_trace_info_ptr = NULL;
  node->m_process        = process;
  node->m_tid            = tid;
  node->m_main           = main;
  node->m_ptx            = process->m_ptx;

  // create a new thread start information
  thread_start_info_s *start_info = &(process->m_thread_start_info[tid]);

  // TODO (jaekyu, 8-2-2010)
  // check block id assignment policy

  // block id assignment in case of multiple applications
  int block_id = start_info->m_thread_id >> BLOCK_ID_SHIFT; 
  node->m_block_id = m_simBase->m_block_id_mapper->find(process->m_process_id, 
      block_id + process->m_kernel_block_start_count[process->m_current_vector_index-1]);

  // new block has been executed.
  if (node->m_block_id == -1) {
    node->m_block_id = m_simBase->m_block_id_mapper->insert(process->m_process_id, 
        block_id + process->m_kernel_block_start_count[process->m_current_vector_index-1]);

    // increase total block count
    ++process->m_block_count;
  }

  // update process block list
  process->m_block_list[node->m_block_id] = true;


  // add a new node to m_thread_queue (for x86) or m_block_queue (for ptx)
  if (process->m_ptx == true) 
    insert_block(node);
  else  
    insert_thread(node);

  // increase number of active threads
  ++m_simBase->m_num_active_threads;
}


////////////////////////////////////////////////////////////////////////////////
//  process_manager_c::create_thread_node()
//   creates a process and opens the trace for the main thread of the process
////////////////////////////////////////////////////////////////////////////////
int process_manager_c::create_process(string appl)	
{
  return create_process(appl, 0, 0);
}


// Creates a process and opens the trace for the main thread of the process
int process_manager_c::create_process(string appl, int repeat, int pid)
{
  ///
  /// Create a process and setup id and other fields
  ///

  // create a new process
  process_s* process = new process_s;

  // create new instruction hash table to reduce cost of decoding trace instructions
  string name;
  stringstream sstr;

  sstr << "inst_info_" << m_simBase->m_process_count;
  sstr >> name;
  
  hash_c<inst_info_s>* new_inst_hash = m_inst_hash_pool->acquire_entry();
  m_simBase->m_inst_info_hash[m_simBase->m_process_count]  = new_inst_hash;


  // process data structure setup
  process->m_repeat             = repeat;
  process->m_process_id         = m_simBase->m_process_count++;
  process->m_kernel_config_name = appl;

  // keep original process id in case of the repeated application
  if (repeat == 0) {
    ++m_simBase->m_process_count_without_repeat;
    process->m_orig_pid = process->m_process_id;
  }
  else {
    process->m_orig_pid = pid;
  }

  m_simBase->m_sim_processes[process->m_orig_pid] = process; 

  
  ///
  /// Get configurations from the file
  ///
  // Get benchmark name
  /////////////////////////////////
  char path[1000];  
  strcpy(path, appl.c_str());  
  char *token;
  char prev_token[100];
  token = strtok(path, "/");  
  while ((token = strtok(NULL, "/")) != NULL &&
         (strcmp(token, "kernel_config.txt"))) {
    strcpy(prev_token, token);
  }
  string bench_str(prev_token);
  cout << "bench_str " << bench_str << endl;
  m_simBase->bench_name = bench_str;
  /////////////////////////////////


  // Setup file name of process trace
  unsigned int dot_location = appl.find_last_of(".");
  if (dot_location == string::npos)
    ASSERTM(0, "file(%s) formats should be <appl_name>.<extn>\n", appl.c_str());

  // open trace configuration file
  ifstream trace_config_file;
  trace_config_file.open(appl.c_str(), ifstream::in);

  // open trace configuration file
  if (trace_config_file.fail()) {
    STAT_EVENT(FILE_OPEN_ERROR);
    ASSERTM(0, "filename:%s cannot be opened\n", appl.c_str());
  }
  
  // Read the first line of trace configuration file
  // TYPE
  string trace_type;
  if (!(trace_config_file >> trace_type))
    ASSERTM(0, "error reading from file:%s", appl.c_str());

  int trace_ver = -1;
  if (trace_type != "x86") {
    if (!(trace_config_file >> trace_ver) || trace_ver != 131) {
      ASSERTM(0, "this version of the simulator supports only version 1.31 of the GPU traces\n");
    }
  }
    
  int thread_count;
  if (!(trace_config_file >> thread_count)) {
    ASSERTM(0, "error reading from file:%s", appl.c_str());
  }

  // To support new trace version : each kernel has own configuration and some applications
  // may have multiple kernels
  if (thread_count == -1) {
    string kernel_directory;
    while (trace_config_file >> kernel_directory) {
      string kernel_path = appl.substr(0, appl.find_last_of('/'));
      kernel_path += kernel_directory.substr(kernel_directory.rfind('/', 
            kernel_directory.find_last_of('/')-1), kernel_directory.length());
      // When a trace directory is moved to the different path,
      // since everything is coded in absolute path, this will cause problem.
      // By taking relative path here, the problem can be solved
      // process->m_applications.push_back(kernel_path);
      process->m_applications.push_back(kernel_directory);
      process->m_kernel_block_start_count.push_back(0);
    }
  }
  else {
    process->m_applications.push_back(appl);
    process->m_kernel_block_start_count.push_back(0);
  }

  trace_config_file.close();
    
  // setup core pool
  if (trace_type == "ptx" || trace_type == "newptx") {
    process->m_ptx = true;
    process->m_core_pool = &m_simBase->m_ptx_core_pool;

    // most basic check to ensure that offsets of members in structure that we
    // use for reading traces (which contains one extra field compared to the
    // structure used when generating traces) match with the structure used when
    // generating traces 
    if (*KNOB(KNOB_TRACE_USES_64_BIT_ADDR)) {
      assert(sizeof(trace_info_gpu_s) == (sizeof(trace_info_gpu_small_s) + sizeof(uint64_t)));
    }
    else {
      assert(sizeof(trace_info_gpu_s) == (sizeof(trace_info_gpu_small_s) + sizeof(uint32_t)));
    }
  }
  else {
    process->m_ptx = false;
    process->m_core_pool = &m_simBase->m_x86_core_pool;
  }

  // now we set up a new process to execute it
  setup_process(process);

  return 0;
}


// setup a process to run. Each process has vector which holds all sub-applications.
// (GPU with multiple kernel calls). 
void process_manager_c::setup_process(process_s* process)
{
  // create profile files
  ///////////////////////////////////////////////////////
  string tb_bbv_path;
  string ideal_bbv_path;
  string simpoint_bbv_path;
  string mem_prof_path;
  string model_bbv_path;
  string sampling_bbv_path;
  string pc_bb_path;

  string macperf_path = string(getenv("MACPERF_ROOT"));  

  if (m_simBase->start_tracking == false) {    
    
    if (*KNOB(KNOB_SAMPLING_ENABLED) == true) {
      KNOB(KNOB_IDEAL_BBV_ENABLED)->setValue(0);
      KNOB(KNOB_TB_BBV_ENABLED)->setValue(0);
      KNOB(KNOB_MODEL_BBV_ENABLED)->setValue(0);
      m_simBase->GetTBPoint();      
    }

    if (*KNOB(KNOB_INTRA_SAMPLING_ENABLED) == true) {
      KNOB(KNOB_IDEAL_BBV_ENABLED)->setValue(0);
      KNOB(KNOB_TB_BBV_ENABLED)->setValue(0);
      KNOB(KNOB_MODEL_BBV_ENABLED)->setValue(0);
      m_simBase->GetInterPoint();
      m_simBase->GetIntraPoint();
      m_simBase->GetIntraWeight();
    }
        
    
    if (*KNOB(KNOB_TB_BBV_ENABLED) != 0) {      
      tb_bbv_path = macperf_path + "/inputs/input_bbvs/TB/" + m_simBase->bench_name.c_str() + ".tb";
      
      string cmd = "mkdir -p " + tb_bbv_path;
      int ret = system(cmd.c_str());
      m_simBase->tb_bbv_profile.open(tb_bbv_path, ofstream::out);
    }
  
    if (*KNOB(KNOB_IDEAL_BBV_ENABLED) != 0) {            
      string expName = *m_simBase->m_knobs->KNOB_EXP;
      string hwName = *m_simBase->m_knobs->KNOB_HWCONFIG;            
      ideal_bbv_path = macperf_path + "/inputs/input_bbvs/ideal/" + expName + "/" + hwName + "/";
      
      string cmd = "mkdir -p " + ideal_bbv_path;
      int ret = system(cmd.c_str());
      ideal_bbv_path += m_simBase->bench_name + ".tb";
      m_simBase->ideal_bbv_profile.open(ideal_bbv_path, ofstream::out);
    }

    if (*KNOB(KNOB_SIMPOINT_BBV_ENABLED) != 0) {            
      string expName = *m_simBase->m_knobs->KNOB_EXP;
      string hwName = *m_simBase->m_knobs->KNOB_HWCONFIG;            
      simpoint_bbv_path = macperf_path + "/inputs/input_bbvs/simpoint-BBV/" + expName + "/" + hwName + "/";
      
      string cmd = "mkdir -p " + simpoint_bbv_path;
      int ret = system(cmd.c_str());
      simpoint_bbv_path += m_simBase->bench_name + ".tb";
      m_simBase->simpoint_bbv_profile.open(simpoint_bbv_path, ofstream::out);
    }

    if (*KNOB(KNOB_MODEL_BBV_ENABLED) != 0) {
      string expName = *m_simBase->m_knobs->KNOB_EXP;
      string hwName = *m_simBase->m_knobs->KNOB_HWCONFIG;            
      model_bbv_path = macperf_path + "/inputs/input_bbvs/model/" + expName + "/" + hwName + "/";
      
      string cmd = "mkdir -p " + model_bbv_path;
      int ret = system(cmd.c_str());
      model_bbv_path += m_simBase->bench_name + ".tb";
      m_simBase->model_bbv_profile.open(model_bbv_path, ofstream::out);
    }

    if (*KNOB(KNOB_INTRA_SAMPLING_ENABLED) != 0) {
      string expName = *m_simBase->m_knobs->KNOB_EXP;
      string hwName = *m_simBase->m_knobs->KNOB_HWCONFIG;            
      sampling_bbv_path = macperf_path + "/inputs/input_bbvs/sampling/" + expName + "/" + hwName + "/";
      
      string cmd = "mkdir -p " + sampling_bbv_path;
      int ret = system(cmd.c_str());
      sampling_bbv_path += m_simBase->bench_name + ".tb";
      m_simBase->sampling_bbv_profile.open(sampling_bbv_path, ofstream::out);
    }

    if (*KNOB(KNOB_MEM_PROF_ENABLED) != 0) {            
      string expName = *m_simBase->m_knobs->KNOB_EXP;
      string hwName = *m_simBase->m_knobs->KNOB_HWCONFIG;            
      mem_prof_path = macperf_path + "/inputs/input_bbvs/mem_profiles/" + expName + "/" + hwName + "/";
      
      string cmd = "mkdir -p " + mem_prof_path;
      int ret = system(cmd.c_str());
      mem_prof_path += m_simBase->bench_name + ".mem";
      m_simBase->mem_profile.open(mem_prof_path, ofstream::out);
    }    
    
    m_simBase->start_tracking = true;
  }
    
  ///////////////////////////////////////////////////////
  report("setup_process:" << process->m_orig_pid << " " 
      << process->m_applications[process->m_current_vector_index]
      << " current_index:" << process->m_current_vector_index << " (" 
      << process->m_applications.size() << ")");

  ASSERT(process->m_current_vector_index < process->m_applications.size());

  // Each block within an application (across multiple kernels) has unique id
  process->m_kernel_block_start_count[process->m_current_vector_index] = 
    process->m_block_count;

  // trace file name
  string trace_info_file_name = process->m_applications[process->m_current_vector_index++];

  unsigned int dot_location = trace_info_file_name.find_last_of(".");
  if (dot_location == string::npos)
    ASSERTM(0, "file(%s) formats should be <appl_name>.<extn>\n", 
        trace_info_file_name.c_str());

  // get the base name of current application (without extension)
  process->m_current_file_name_base = trace_info_file_name.substr(0, dot_location);


  // open TRACE_CONFIG file
  ifstream trace_config_file;
  trace_config_file.open(trace_info_file_name.c_str(), ifstream::in);
  if (trace_config_file.fail()) {
    STAT_EVENT(FILE_OPEN_ERROR);
    ASSERTM(0, "trace_config_file:%s\n", trace_info_file_name.c_str());
  }
  
  // -------------------------------------------
  // TRACE_CONFIG Format
  // -------------------------------------------
  // Trace version
  // #Threads | Trace Type | (Optional Fields)
  // 1st Thread ID    | #Instructions
  // 2nd Thread ID    | #Instructions
  // ....
  // nth Thread ID    | #Instructions
  
  // read the first line of TRACE_CONFIG file 
  // X86 Traces : (#Threads | x86)
  // GPU Traces (OLD) : (#Warps | ptx)
  // GPU Traces (NEW) : (#Warps | newptx | #MaxBlocks per Core)
  
  string trace_type;
  if (!(trace_config_file >> trace_type)) 
    ASSERTM(0, "error reading from file:%s", trace_info_file_name.c_str());

  
  int trace_ver = -1;
  if (trace_type != "x86") {
    if (!(trace_config_file >> trace_ver) || trace_ver != 131) {
      ASSERTM(0, "this version of the simulator supports only version 1.31 of the GPU traces\n");
    }
  }
  
  // get occupancy
  if (trace_type == "ptx") {
    process->m_max_block = *m_simBase->m_knobs->KNOB_MAX_BLOCK_PER_CORE;    
  }
  if (trace_type == "newptx") {
    if (!(trace_config_file >> process->m_max_block))
      ASSERTM(0, "error reading from file:%s", trace_info_file_name.c_str());
    trace_type = "ptx";
    if (*m_simBase->m_knobs->KNOB_MAX_BLOCK_PER_CORE_SUPER > 0) {
      process->m_max_block = *m_simBase->m_knobs->KNOB_MAX_BLOCK_PER_CORE_SUPER;      
    }
  }

  // process->m_max_block = 100;
  
  // get thread count
  int thread_count;
  if (!(trace_config_file >> thread_count)) 
    ASSERTM(0, "error reading from file:%s", trace_info_file_name.c_str());

  
  // # thread_count > 0
  if (thread_count <= 0) 
    ASSERTM(0, "invalid thread count:%d", thread_count);

  report("thread_count:" << thread_count);


  // create data structures
  thread_stat_s *new_stat = new thread_stat_s[thread_count];
  m_simBase->m_thread_stats[process->m_process_id] = new_stat;

  m_simBase->m_all_threads += thread_count;

  process->m_no_of_threads     = thread_count;	
  process->m_thread_start_info = new thread_start_info_s[thread_count];	
  process->m_thread_trace_info = new thread_s*[thread_count];	

  if (process->m_thread_start_info == NULL || process->m_thread_trace_info == NULL)	{
    ASSERTM(0, "unable to allocate memory\n");
  }

  // read each thread's information (thread id, # of starting instruction (spawn))
  // This should start to read the first field of the second line in TRACE_CONFIG.
  for (int ii = 0; ii < thread_count; ++ii) { 
    if (!(trace_config_file >> process->m_thread_start_info[ii].m_thread_id 
          >> process->m_thread_start_info[ii].m_inst_count)) { 
      ASSERTM(0, "error reading from file:%s ii:%d\n", trace_info_file_name.c_str(), ii);
    }
  }

  // close TRACE_CONFIG file
  trace_config_file.close();


  // GPU simulation
  if (true == process->m_ptx) { 
    string path = process->m_current_file_name_base;
    path += "_info.txt";

    // read trace information file
    ifstream trace_lengths_file(path.c_str());
    if (!trace_lengths_file.good())
      ASSERTM(0, "could not open file: %s\n", path.c_str());

    // FIXME
    Counter inst_count_tot = 0; 
    Counter inst_count;
    Counter thread_id;

    for (int ii = 0; ii < thread_count; ++ii) {
      trace_lengths_file >> thread_id >> inst_count;
      inst_count_tot += inst_count;
    }
    trace_lengths_file.close();

    process->m_inst_count_tot = inst_count_tot;


    // FIXME
    // TODO (jaekyu, 1-30-2009)
    // fix this
    
    // Calculate the number of threads (warps) per block 
    // Currently, (n * 65536) is assigned to the first global warp ID for the nth block.
    m_simBase->m_no_threads_per_block = 0;
    for (int ii = 0; ii < thread_count; ++ii) { 
      if (process->m_thread_start_info[ii].m_thread_id < 100)
        ++m_simBase->m_no_threads_per_block;
      else
        break;
    }

    queue<int> *core_pool = process->m_core_pool;

    // Allocate cores to this application (bi-directonal)
    // get maximum allowed
    if (*m_simBase->m_knobs->KNOB_MAX_NUM_CORE_PER_APPL == 0) {
      while (!core_pool->empty()) {
        int core_id = core_pool->front();
        core_pool->pop();

        process->m_core_list[core_id] = true;
        m_simBase->m_core_pointers[core_id]->init();
        m_simBase->m_core_pointers[core_id]->add_application(0, process);
      }
    }
    // get limited (*m_simBase->m_knobs->KNOB_MAX_NUM_CORE_PER_APPL) number of cores
    else {
      int count = 0;
      while (!core_pool->empty() && count < *m_simBase->m_knobs->KNOB_MAX_NUM_CORE_PER_APPL) {
        int core_id = core_pool->front();
        core_pool->pop();

        process->m_core_list[core_id] = true;
        m_simBase->m_core_pointers[core_id]->init();
        m_simBase->m_core_pointers[core_id]->add_application(0, process);

        ++count;
      }
    }
  }


  // TODO (jaekyu, 1-30-2009)
  // FIXME
  if (trace_type == "ptx" && *KNOB(KNOB_BLOCKS_TO_SIMULATE)) {
    if ((*KNOB(KNOB_BLOCKS_TO_SIMULATE) * m_simBase->m_no_threads_per_block) < 
        static_cast<unsigned int>(thread_count)) { 
      uns temp = thread_count;
      thread_count = *KNOB(KNOB_BLOCKS_TO_SIMULATE) * m_simBase->m_no_threads_per_block;

      //print the thread count of the application at the beginning
      REPORT("new thread count %d\n", thread_count);	
      process->m_no_of_threads = thread_count;	//assign thread_count to the number of threads of the process

      m_simBase->m_all_threads += thread_count;

      if (*KNOB(KNOB_SIMULATE_LAST_BLOCKS)) {
        for (int i = 0; i < thread_count; ++i) {
          process->m_thread_start_info[i].m_thread_id = 
            process->m_thread_start_info[temp - thread_count + i].m_thread_id;
          process->m_thread_start_info[i].m_inst_count = 
            process->m_thread_start_info[temp - thread_count + i].m_inst_count;
        }
      }
    }
  }

  // Initialize stats (this will be used to determine process termination
  process->m_no_of_threads_created    = 1;
  process->m_no_of_threads_terminated = 0;

  // Insert the main thread to the pool
  create_thread_node(process, 0, true);

  if (process->m_ptx) {
    for (int tid = 1; tid < thread_count; ++tid) {
      if (process->m_thread_start_info[tid].m_inst_count == 0) {
        create_thread_node(process, process->m_no_of_threads_created++, false);
      }
      else {
        break;
      }
    }

    m_simBase->kernel_launch_counter ++;          
  }

  // tommy24 - Reset sampling unit counting
  ////////////////////////////////////////
  if (*KNOB(KNOB_MODEL_BBV_ENABLED) != 0 || *KNOB(KNOB_INTRA_SAMPLING_ENABLED) != 0
      || *KNOB(KNOB_SAMPLING_ENABLED) != 0 || *KNOB(KNOB_IDEAL_BBV_ENABLED) != 0
      || *KNOB(KNOB_SIMPOINT_BBV_ENABLED) != 0) {
    double interval_CPI, kernel_CPI, kernel_warp_IPC, l1_rate, l2_rate, kernel_l1_rate, kernel_l2_rate, diverge_ratio, kernel_diverge_ratio;
    
    m_simBase->m_kernel_cycles_retired += m_simBase->m_unit_cycles_retired;
    m_simBase->m_kernel_insts_retired += m_simBase->m_unit_insts_retired;
    m_simBase->m_kernel_warp_insts_retired += m_simBase->m_unit_warp_insts_retired;
    m_simBase->m_kernel_l1_access += m_simBase->m_prof_l1_access;
    m_simBase->m_kernel_l2_access += m_simBase->m_prof_l2_access;
    m_simBase->m_kernel_l1_misses += m_simBase->m_prof_l1_misses;
    m_simBase->m_kernel_l2_misses += m_simBase->m_prof_l2_misses;
    m_simBase->m_kernel_mem_insts += m_simBase->m_prof_mem_insts;
    m_simBase->m_kernel_mem_uops += m_simBase->m_prof_mem_uops;
    
        
    //if (m_simBase->m_unit_insts_retired != 0 && m_simBase->m_kernel_insts_retired != 0) {
    if (m_simBase->m_kernel_insts_retired != 0) {      
      
      interval_CPI = (double)m_simBase->m_unit_cycles_retired / (double)m_simBase->m_unit_insts_retired;
      kernel_CPI = (double)m_simBase->m_kernel_cycles_retired / (double)m_simBase->m_kernel_insts_retired;
      kernel_warp_IPC = (double)m_simBase->m_kernel_warp_insts_retired / (double)m_simBase->m_kernel_cycles_retired;

      l1_rate = (double)m_simBase->m_prof_l1_misses / (double)m_simBase->m_prof_l1_access;
      l2_rate = (double)m_simBase->m_prof_l2_misses / (double)m_simBase->m_prof_l2_access;
      diverge_ratio = (double)m_simBase->m_prof_mem_uops / (double)m_simBase->m_prof_mem_insts;

      kernel_l1_rate = (double)m_simBase->m_kernel_l1_misses / (double)m_simBase->m_kernel_l1_access;
      kernel_l2_rate = (double)m_simBase->m_kernel_l2_misses / (double)m_simBase->m_kernel_l2_access;
      kernel_diverge_ratio = (double)m_simBase->m_kernel_mem_uops / (double)m_simBase->m_kernel_mem_insts;
      
      if (*KNOB(KNOB_IDEAL_BBV_ENABLED) != 0) {
        m_simBase->GenIdealBBVs(true, &m_simBase->ideal_bbv_profile);
        double acc_thread_IPC = (float)m_simBase->acc_insts / (float)m_simBase->acc_cycles;
        double acc_IPC = (float)m_simBase->acc_warp_insts / (float)m_simBase->acc_cycles;
        double total_IPC = (float)m_simBase->m_prof_insts_retired / (float)m_simBase->m_prof_cycles_retired;
        m_simBase->ideal_bbv_profile << "Kernel_IPC_W " << acc_IPC << " " << acc_thread_IPC << " " << m_simBase->acc_warp_insts
                                     << " " << m_simBase->acc_cycles << endl;        
        m_simBase->ideal_bbv_profile << "Total_IPC_W " << total_IPC << " " << m_simBase->m_prof_insts_retired << " " << m_simBase->m_prof_cycles_retired << endl;                                     

        m_simBase->ideal_bbv_profile << "Kernel_Diverge_Ratio: " << kernel_diverge_ratio 
                                     << " L1 rate: " << kernel_l1_rate << " accesses: " <<  m_simBase->m_kernel_l1_access 
                                     << " L2 rate: " << kernel_l2_rate << " accesses: " <<  m_simBase->m_kernel_l2_access 
                                     << endl;
      
        m_simBase->ideal_bbv_profile << "Kernel_Sample_Ratio "
                                     << 1 - (float)m_simBase->skipped_insts / (float)m_simBase->acc_warp_insts
                                     << " " << m_simBase->acc_warp_insts 
                                     << " " << m_simBase->acc_warp_insts - m_simBase->skipped_insts
                                     << endl;          
      }

      if (*KNOB(KNOB_SIMPOINT_BBV_ENABLED) != 0) {
        m_simBase->GenIdealBBVs(true, &m_simBase->simpoint_bbv_profile);
        double acc_thread_IPC = (float)m_simBase->acc_insts / (float)m_simBase->acc_cycles;
        double acc_IPC = (float)m_simBase->acc_warp_insts / (float)m_simBase->acc_cycles;
        double total_IPC = (float)m_simBase->m_prof_insts_retired / (float)m_simBase->m_prof_cycles_retired;
        m_simBase->simpoint_bbv_profile << "Kernel_IPC_W " << acc_IPC << " " << acc_thread_IPC << " " << m_simBase->acc_warp_insts
                                     << " " << m_simBase->acc_cycles << endl;        
        m_simBase->simpoint_bbv_profile << "Total_IPC_W " << total_IPC << " " << m_simBase->m_prof_insts_retired << " " << m_simBase->m_prof_cycles_retired << endl;                                     

        m_simBase->simpoint_bbv_profile << "Kernel_Diverge_Ratio: " << kernel_diverge_ratio 
                                     << " L1 rate: " << kernel_l1_rate << " accesses: " <<  m_simBase->m_kernel_l1_access 
                                     << " L2 rate: " << kernel_l2_rate << " accesses: " <<  m_simBase->m_kernel_l2_access 
                                     << endl;
      
        m_simBase->simpoint_bbv_profile << "Kernel_Sample_Ratio "
                                     << 1 - (float)m_simBase->skipped_insts / (float)m_simBase->acc_warp_insts
                                     << " " << m_simBase->acc_warp_insts 
                                     << " " << m_simBase->acc_warp_insts - m_simBase->skipped_insts
                                     << endl;          
      }

      if (*KNOB(KNOB_MODEL_BBV_ENABLED) != 0) {
        m_simBase->GenIdealBBVs(true, &m_simBase->model_bbv_profile);
        double acc_thread_IPC = (float)m_simBase->acc_insts / (float)m_simBase->acc_cycles;
        double acc_IPC = (float)m_simBase->acc_warp_insts / (float)m_simBase->acc_cycles;
        double total_IPC = (float)m_simBase->m_prof_insts_retired / (float)m_simBase->m_prof_cycles_retired;
        m_simBase->model_bbv_profile << "Kernel_IPC_W " << acc_IPC << " " << acc_thread_IPC << " " << m_simBase->acc_warp_insts
                                     << " " << m_simBase->acc_cycles << endl;
        m_simBase->model_bbv_profile << "Total_IPC_W " << total_IPC << " " << m_simBase->m_prof_insts_retired << " " << m_simBase->m_prof_cycles_retired << endl;                                     

        m_simBase->model_bbv_profile << "Kernel_Diverge_Ratio: " << kernel_diverge_ratio 
                                     << " L1 rate: " << kernel_l1_rate << " accesses: " <<  m_simBase->m_kernel_l1_access 
                                     << " L2 rate: " << kernel_l2_rate << " accesses: " <<  m_simBase->m_kernel_l2_access 
                                     << endl;
      
        m_simBase->model_bbv_profile << "Kernel_Sample_Ratio "
                                     << 1 - (float)m_simBase->skipped_insts / (float)m_simBase->acc_warp_insts
                                     << " " << m_simBase->acc_warp_insts
                                     << " " << m_simBase->acc_warp_insts - m_simBase->skipped_insts
                                     << endl;          
      }

      if (*KNOB(KNOB_INTRA_SAMPLING_ENABLED) != 0) {
        m_simBase->GenIdealBBVs(true, &m_simBase->sampling_bbv_profile);
        double acc_thread_IPC = (float)m_simBase->acc_insts / (float)m_simBase->acc_cycles;
        double acc_CPI = (float)m_simBase->acc_cycles / (float)m_simBase->acc_warp_insts;
        double total_IPC = (float)m_simBase->m_prof_insts_retired / (float)m_simBase->m_prof_cycles_retired;

        int kernelPhase = m_simBase->kernelPhaseID[m_simBase->curKernelPhaseKernel];
        int phaseId = m_simBase->kernelPhaseID[m_simBase->curKernelPhaseKernel];
        double PhaseWeight = ((double)m_simBase->kernelPhaseWeight[phaseId] / (double)m_simBase->totalKernelPhaseWeight);
        
        m_simBase->curKernelPhaseWeight = PhaseWeight;
        m_simBase->curKernelPhaseID = phaseId;
        m_simBase->totalKernelPhaseCPI += m_simBase->curKernelPhaseWeight * acc_CPI;
        m_simBase->totalKernelPhaseL1Accesses += m_simBase->curKernelPhaseWeight * m_simBase->acc_l1_accesses;
        m_simBase->totalKernelPhaseL1Misses += m_simBase->curKernelPhaseWeight * m_simBase->acc_l1_misses;
        m_simBase->totalKernelPhaseL2Accesses += m_simBase->curKernelPhaseWeight * m_simBase->acc_l2_accesses;
        m_simBase->totalKernelPhaseL2Misses += m_simBase->curKernelPhaseWeight * m_simBase->acc_l2_misses;
        
        m_simBase->sampling_bbv_profile << "KernelPhaseIPC " << acc_CPI << " " << PhaseWeight << endl;        
        
        m_simBase->sampling_bbv_profile << "Kernel_IPC_W " << 1.0 / acc_CPI << " " << acc_thread_IPC << " " << m_simBase->acc_warp_insts
                                        << " " << m_simBase->acc_cycles << endl;
        m_simBase->sampling_bbv_profile << "Total_IPC_W " << total_IPC << " " << m_simBase->m_prof_insts_retired << " " << m_simBase->m_prof_cycles_retired << endl;

        m_simBase->sampling_bbv_profile << "Kernel_L1_Rate " << m_simBase->acc_l1_misses/m_simBase->acc_l1_accesses << " " << m_simBase->acc_l1_accesses << endl;
        m_simBase->sampling_bbv_profile << "Kernel_L2_Rate " << m_simBase->acc_l2_misses/m_simBase->acc_l2_accesses << " " << m_simBase->acc_l2_accesses << endl;        

        m_simBase->sampling_bbv_profile << "Kernel_Diverge_Ratio: " << kernel_diverge_ratio 
                                        << " L1 rate: " << kernel_l1_rate << " accesses: " <<  m_simBase->m_kernel_l1_access 
                                        << " L2 rate: " << kernel_l2_rate << " accesses: " <<  m_simBase->m_kernel_l2_access 
                                        << endl;
      
        m_simBase->sampling_bbv_profile << "Kernel_Sample_Ratio "
                                        << 1 - (float)m_simBase->skipped_insts / (float)m_simBase->acc_warp_insts
                                        << " " << m_simBase->acc_warp_insts
                                        << " " << m_simBase->acc_warp_insts - m_simBase->skipped_insts
                                        << endl;          

        m_simBase->sampling_bbv_profile << "Kernel_Weight " << m_simBase->curKernelPhaseWeight
                                        << " "
                                        << m_simBase->curKernelPhaseID
                                        << endl;
      }

      m_simBase->BB_counter.clear();
      m_simBase->MEM_counter.clear();
      m_simBase->MEMFOOT_counter.mem_foots.clear();      

      if (*KNOB(KNOB_SAMPLING_ENABLED) == true) {
        if (m_simBase->tbpoints.size() > 0) {
          int phaseId = m_simBase->tbpoints.front().first;
          m_simBase->phaseCPI[phaseId] = interval_CPI;
          m_simBase->phaseInsts[phaseId] = m_simBase->m_unit_insts_retired;
          m_simBase->phaseL1Rate[phaseId] = l1_rate;
          m_simBase->phaseL2Rate[phaseId] = l2_rate;
          m_simBase->tbpoints.erase(m_simBase->tbpoints.begin());
          // cout << "Not phaseId " << phaseId << " "
          //      << m_simBase->m_unit_insts_retired << endl;
        }
      }
    }
    
    m_simBase->m_prof_l1_misses = 0;
    m_simBase->m_prof_l1_access = 0;
    m_simBase->m_prof_l2_misses = 0;
    m_simBase->m_prof_l2_access = 0;
    m_simBase->m_prof_mem_insts = 0;
    m_simBase->m_prof_mem_uops = 0;
    m_simBase->m_unit_cycles_retired = 0;
    m_simBase->m_unit_insts_retired = 0;
    m_simBase->m_unit_warp_insts_retired = 0;
    m_simBase->m_kernel_cycles_retired = 0;
    m_simBase->m_kernel_insts_retired = 0;
    m_simBase->m_kernel_warp_insts_retired = 0;
    m_simBase->m_kernel_l1_misses = 0;
    m_simBase->m_kernel_l1_access = 0;
    m_simBase->m_kernel_l2_misses = 0;
    m_simBase->m_kernel_l2_access = 0;
    m_simBase->m_kernel_mem_insts = 0;
    m_simBase->m_kernel_mem_uops = 0;
    m_simBase->m_tb_retired_counts = 0;
    m_simBase->m_tot_active_threads = 0;
    m_simBase->m_tot_block_num = 0;
    m_simBase->acc_warp_insts = 0;
    m_simBase->acc_insts = 0;
    m_simBase->acc_cycles = 0;
    m_simBase->acc_l1_accesses = 0;
    m_simBase->acc_l1_misses = 0;
    m_simBase->acc_l2_accesses = 0;
    m_simBase->acc_l2_misses = 0;
    m_simBase->skipped_insts = 0;
    m_simBase->isStablePhase = false;
    m_simBase->doneStablePhase = false;
    m_simBase->epoch_count = 0;
    m_simBase->prevIPC = 0;
    m_simBase->monitored_count = 0;
    m_simBase->monitored_block = -1;
  }

  // tommy24 - show kernel occupancy
  // bound by no. of threads  
  int max_threads = *m_simBase->m_knobs->KNOB_MAX_THREADS_PER_CORE;
  int max_tbs_by_threads = int((double)max_threads / (double)m_simBase->m_no_threads_per_block + 0.5);
  int max_tbs_by_blocks = process->m_max_block;  
  int max_tbs = (max_tbs_by_blocks < max_tbs_by_threads) ? max_tbs_by_blocks : max_tbs_by_threads;
  // printf("max_tbs_by_threads %d max_tbs_by_blocks %d\n", max_threads, process->m_max_block);
  if (*KNOB(KNOB_IDEAL_BBV_ENABLED) != 0) {
    m_simBase->ideal_bbv_profile << "Kernel " << process->m_applications[process->m_current_vector_index - 1] << " occupancy: " << max_tbs << " threads: " << m_simBase->m_no_threads_per_block << " blocks started" << endl;
  }

  if (*KNOB(KNOB_SIMPOINT_BBV_ENABLED) != 0) {
    m_simBase->simpoint_bbv_profile << "Kernel " << process->m_applications[process->m_current_vector_index - 1] << " occupancy: " << max_tbs << " threads: " << m_simBase->m_no_threads_per_block << " blocks started" << endl;
  }

  if (*KNOB(KNOB_MODEL_BBV_ENABLED) != 0) {
    m_simBase->model_bbv_profile << "Kernel " << process->m_applications[process->m_current_vector_index - 1] << " occupancy: " << max_tbs << " threads: " << m_simBase->m_no_threads_per_block << " blocks started" << endl;    
  }

  if (*KNOB(KNOB_INTRA_SAMPLING_ENABLED) != 0) {
    m_simBase->sampling_bbv_profile << "Kernel " << process->m_applications[process->m_current_vector_index - 1] << " occupancy: " << max_tbs << " threads: " << m_simBase->m_no_threads_per_block << " blocks started" << endl;    
  }
  
  if (*KNOB(KNOB_TB_BBV_ENABLED) != 0) {
    m_simBase->tb_bbv_profile << "Kernel " << process->m_applications[process->m_current_vector_index - 1] << " occupancy: " << max_tbs << " threads: " << m_simBase->m_no_threads_per_block << " blocks started" << endl;
  }
  
  if (*KNOB(KNOB_MEM_PROF_ENABLED) != 0) {
    m_simBase->mem_profile << "Kernel " << process->m_applications[process->m_current_vector_index - 1] << " occupancy: " << max_tbs << " threads: " << m_simBase->m_no_threads_per_block << " started" << endl;
  }  

  cout << "Kernel " << process->m_applications[process->m_current_vector_index - 1] << " occupancy: " << max_tbs << " threads: " << m_simBase->m_no_threads_per_block << " blocks started" << endl;
  m_simBase->tblocks.clear();

  // Get Current Kernel Name
  m_simBase->current_kernel_name = process->m_applications[process->m_current_vector_index - 1];
  size_t kernel_name_begin = m_simBase->current_kernel_name.rfind('Z', m_simBase->current_kernel_name.find_last_of('/') - 1) - 1;
  size_t kernel_name_end = m_simBase->current_kernel_name.find_last_of('/');
  m_simBase->current_kernel_name = m_simBase->current_kernel_name.substr(kernel_name_begin, kernel_name_end - kernel_name_begin);

  cout << "kernel Name " << m_simBase->current_kernel_name << endl;  
  m_simBase->GetSampleUnitSize(max_tbs);
  m_simBase->curKernelPhaseKernel = process->m_current_vector_index - 1;
  ////////////////////////////////////////

  if (*KNOB(KNOB_PC_BB_ENABLED) != 0) {
    pc_bb_path = macperf_path + "/inputs/input_bbvs/PCBBMAP/" + m_simBase->bench_name.c_str() + ".tb";    
    m_simBase->pc_bb_profile.open(pc_bb_path, ofstream::out);
    
    for (map<unsigned int, unsigned int>::iterator it = m_simBase->AllPCBBMap.begin(); it != m_simBase->AllPCBBMap.end(); it ++) {
      m_simBase->pc_bb_profile << it->first << " " << it->second << endl;
    }

    m_simBase->pc_bb_profile.close();
    m_simBase->curBB.clear();
  } else {    
    m_simBase->GetPCBBMapping(m_simBase->bench_name);  
  }
}


// terminate a process
bool process_manager_c::terminate_process(process_s* process)
{
  delete []process->m_thread_start_info;
  delete []process->m_thread_trace_info;
  process->m_block_list.clear();
  
  // TODO (jaekyu, 2-3-2010)
  // We may need to change this using pool_c
  m_simBase->m_inst_info_hash[process->m_process_id]->clear();

  // deallocate data structures
  thread_stat_s* thread_stat_data_to_delete = m_simBase->m_thread_stats[process->m_process_id];
  m_simBase->m_thread_stats.erase(process->m_process_id);
  delete[] thread_stat_data_to_delete;


  // Since there are more kernels within an application, we need to finish these
  // before terminate this application
  if (process->m_current_vector_index < process->m_applications.size() &&
      (*m_simBase->m_ProcessorStats)[INST_COUNT_TOT].getCount() < *KNOB(KNOB_MAX_INSTS1) &&
      (*KNOB(KNOB_KERNELS_TO_SIMULATE) == 0 || (m_simBase->kernel_launch_counter < *KNOB(KNOB_KERNELS_TO_SIMULATE)))) {

    // cout << "nono" << endl;
    setup_process(process);
    return false;
  }

  m_simBase->m_block_id_mapper->delete_table(process->m_process_id);

  // release allocated cores
  for (auto I = process->m_core_list.begin(), E = process->m_core_list.end(); I != E; ++I) {
    int core_id = (*I).first;
    process->m_core_pool->push(core_id); 

    m_simBase->m_core_pointers[core_id]->delete_application(process->m_process_id);
  }
  process->m_core_list.clear();
    
  hash_c<inst_info_s>* inst_info_hash = m_simBase->m_inst_info_hash[process->m_process_id];
  m_simBase->m_inst_info_hash.erase(process->m_process_id);
  inst_info_hash->clear();
  m_inst_hash_pool->release_entry(inst_info_hash);


  stringstream sstr;
  string ext;
  sstr << "." << process->m_process_id;
  sstr >> ext;

  int delta;
  if (m_appl_cyccount_info.find(process->m_orig_pid) == m_appl_cyccount_info.end()) {
    delta = static_cast<int>(CYCLE);
  }
  else {
    delta = static_cast<int>(CYCLE - m_appl_cyccount_info[process->m_orig_pid]);
  }
  m_appl_cyccount_info[process->m_orig_pid] = CYCLE;


  STAT_EVENT_N(APPL_CYC_COUNT0 + process->m_orig_pid, delta);
  STAT_EVENT(APPL_CYC_COUNT_BASE0 + process->m_orig_pid);

  // FIXME (jaekyu, 4-2-2010)
  // need to handle repeat_trace_n > 0 cases
  if (process->m_repeat == 0) {
    m_simBase->m_ProcessorStats->saveStats(ext);
  }

  return true;
}


// try to give a unique thread id
// counter to assign unique thread_ids to threads/warps
static int global_unique_thread_id = 0;


// create a new thread (actually, a thread has been created when create_thread_node()
// has been called. However, when a thread is actually scheduled, we allocate and initialize
// data in a thread.
thread_s *process_manager_c::create_thread(process_s* process, int tid, bool main)
{
  thread_s* trace_info = m_simBase->m_thread_pool->acquire_entry(m_simBase);
  process->m_thread_trace_info[tid] = trace_info;

  //TODO - nbl (apr-17-2013): use pools
  if (process->m_ptx) {
    trace_info->m_prev_trace_info = new trace_info_gpu_s;
    trace_info->m_next_trace_info = new trace_info_gpu_s;
  }
  else {
    trace_info->m_prev_trace_info = new trace_info_cpu_s;
    trace_info->m_next_trace_info = new trace_info_cpu_s;
  }
  thread_start_info_s* start_info = &process->m_thread_start_info[tid];

  int block_id = start_info->m_thread_id >> BLOCK_ID_SHIFT;

  // original block id
  trace_info->m_orig_block_id = block_id;
  trace_info->m_block_id = m_simBase->m_block_id_mapper->find(process->m_process_id, block_id + 
      process->m_kernel_block_start_count[process->m_current_vector_index-1]);


  // FIXME
  //trace_info->m_orig_thread_id = start_info->m_thread_id;
  //trace_info->m_unique_thread_id = global_unique_thread_id++;
  trace_info->m_unique_thread_id = (start_info->m_thread_id) % BLOCK_ID_MOD; 
  trace_info->m_orig_thread_id = global_unique_thread_id++;

  // set up trace file name
  stringstream sstr;
  sstr << process->m_current_file_name_base << "_" << start_info->m_thread_id << ".raw";

  string filename = "";
  sstr >> filename;

  // open trace file  
  trace_info->m_trace_file = gzopen(filename.c_str(), "r");
  if (trace_info->m_trace_file == NULL) 
    ASSERTM(0, "error opening trace file:%s\n", filename.c_str());

  trace_info->m_file_opened      = true;
  trace_info->m_trace_ended      = false;
  trace_info->m_ptx              = process->m_ptx; 
  trace_info->m_buffer_index     = 0;
  trace_info->m_buffer_index_max = 0;
  trace_info->m_buffer_exhausted = true;
  trace_info->m_inst_count       = 0;
  trace_info->m_uop_count        = 0; 
  trace_info->m_process          = process;	
  trace_info->m_main_thread      = main;
  trace_info->m_temp_inst_count  = 0;
  trace_info->m_temp_uop_count   = 0;
  trace_info->m_thread_init      = true;
  trace_info->m_num_sending_uop  = 0;
  trace_info->m_bom              = true;
  trace_info->m_eom              = true;

  trace_info->m_fetch_data->init();

  return trace_info;
}


// terminate a thread
int process_manager_c::terminate_thread(int core_id, thread_s* trace_info, int thread_id, 
    int b_id)	
{
  core_c* core = m_simBase->m_core_pointers[core_id];

  ASSERT(core->m_running_thread_num); 

  int  block_id = trace_info->m_block_id; 
  --core->m_running_thread_num;
  --m_simBase->running_thread_num;

  // m_simBase->sampling_bbv_profile << "running thread " << m_simBase->running_thread_num << " " << block_id
  //                                 << endl;

  if (m_simBase->monitored_thread == thread_id) {
    // cout << "monitored " << m_simBase->monitored_thread << endl;
    m_simBase->monitored_thread = -1;
  }

  int slotId = -1;
  for (int i = 0; i < core->threadSlot.size(); i ++)
    if (core->threadSlot[i] == thread_id) {
      core->threadSlot[i] = -1;      

      if (i == 0 && core_id == 0)
        m_simBase->mem_profile << "terminate thread " << thread_id <<endl;
      
      slotId = i;
      break;
    }    
    
  core->warp_issue_list.erase(thread_id);
  core->warp_alloc_list.erase(thread_id);
  core->warp_fetch_list.erase(thread_id);
  // if (core_id == 0)
  //   cout << "remove " << thread_id << endl;

  // All threads have been terminated in a core. Mark core as ended.
  if (core->m_running_thread_num == 0)
    m_simBase->m_core_end_trace[core_id] = true;

  // Mark thread terminated
  core->m_thread_finished[thread_id] = 1; 

  // final heartbeat for the thread
  core->final_heartbeat(thread_id);

  // deallocate data structures
  core->deallocate_thread_data(thread_id);

  DEBUG("core_id:%d terminated block: %d thread - %d running_thread_num:%d block_id:%d \n", 
      core_id, trace_info->m_block_id, trace_info->m_thread_id, core->m_running_thread_num, 
      b_id);

  // update number of terminated thread for an application
  ++trace_info->m_process->m_no_of_threads_terminated;

  // decrement number of active threads
  --m_simBase->m_num_active_threads;

  // GPU simulation
  if (trace_info->m_ptx == true) { 
    int t_process_id = trace_info->m_process->m_process_id;
    int t_thread_id  = trace_info->m_unique_thread_id;
    m_simBase->m_thread_stats[t_process_id][t_thread_id].m_thread_end_cycle = 
      core->get_cycle_count();

    block_schedule_info_s* block_info = m_simBase->m_block_schedule_info[block_id];
    ++block_info->m_retired_thread_num;

    DEBUG("core_id:%d block_id:%d block_retired_thread_num:%d block_total_thread_num:%d "
        "running_block_num:%d\n",
        core_id, block_id, block_info->m_retired_thread_num, block_info->m_total_thread_num, 
        core->m_running_block_num); 


    // BLOCK RETIRE : all threads in a block have been retired, so the block is retired now
    if (block_info->m_retired_thread_num == block_info->m_total_thread_num) {

      // tommy24 - output thread block info
      //////////////////////////////////////////////      
      if (*KNOB(KNOB_TB_BBV_ENABLED) != 0) {        
        m_simBase->m_tb_retired_counts ++;
        m_simBase->m_retired_counts ++;
        assert(m_simBase->tblocks.find(block_id) != m_simBase->tblocks.end());
        m_simBase->tblocks[block_id].finish_time = m_simBase->m_prof_cycles_retired;
        m_simBase->tblocks[block_id].instructions = m_simBase->m_prof_insts_retired - m_simBase->tblocks[block_id].instructions;
        m_simBase->tblocks[block_id].CPI = (double)(m_simBase->tblocks[block_id].finish_time - m_simBase->tblocks[block_id].start_time) / (double)m_simBase->tblocks[block_id].instructions;
        m_simBase->tblocks[block_id].retired = true;      
        m_simBase->tblocks[block_id].diverge_ratio = m_simBase->tblocks[block_id].mem_reqs / m_simBase->tblocks[block_id].diverge_ratio;      
      
        map<int, tblock_info>::iterator it = m_simBase->tblocks.begin();
        while (it != m_simBase->tblocks.end()){
          // tommy24 - show retired blocks in order
          if (!it->second.retired) break;
          m_simBase->GenTBBBVs(it->first);          
          m_simBase->tblocks.erase(it ++);
        }
      }

      m_simBase->curBlockList.erase(block_id);
      if (m_simBase->monitored_block == block_id) {
        // m_simBase->model_bbv_profile << "Retired " << block_id << " core " << core_id << " cycle " << core->get_cycle_count() << endl;
        // cout << "Retired " << block_id << " core " << core_id << " cycle " << core->get_cycle_count() << endl;
        m_simBase->monitored_count ++;
        m_simBase->monitored_block = -1;
      }
      // cout << "Retired " << block_id << " core " << core_id << " cycle " << core->get_cycle_count() << endl;
     
      if ((*KNOB(KNOB_MODEL_BBV_ENABLED) != 0 || *KNOB(KNOB_INTRA_SAMPLING_ENABLED) != 0)
          && m_simBase->monitored_count == *KNOB(KNOB_MODEL_SIZE_RATIO)) {        
        double interval_CPI, kernel_CPI, l1_rate, l2_rate, diverge_ratio;          
        m_simBase->m_kernel_cycles_retired += m_simBase->m_unit_cycles_retired;
        m_simBase->m_kernel_insts_retired += m_simBase->m_unit_insts_retired;
        m_simBase->m_kernel_warp_insts_retired += m_simBase->m_unit_warp_insts_retired;
        m_simBase->m_kernel_l1_access += m_simBase->m_prof_l1_access;
        m_simBase->m_kernel_l2_access += m_simBase->m_prof_l2_access;
        m_simBase->m_kernel_l1_misses += m_simBase->m_prof_l1_misses;
        m_simBase->m_kernel_l2_misses += m_simBase->m_prof_l2_misses;
        m_simBase->m_kernel_mem_insts += m_simBase->m_prof_mem_insts;
        m_simBase->m_kernel_mem_uops += m_simBase->m_prof_mem_uops;
        
        if (*KNOB(KNOB_MODEL_BBV_ENABLED) != 0)
          m_simBase->GenIdealBBVs(false, &m_simBase->model_bbv_profile);
        else
          m_simBase->GenIdealBBVs(false, &m_simBase->sampling_bbv_profile);
        // m_simBase->GetProfiling();        
        // m_simBase->MathModel();
        m_simBase->BB_counter.clear();
        m_simBase->MEM_counter.clear();
        m_simBase->MEMFOOT_counter.mem_foots.clear();        
            
        interval_CPI = (double)m_simBase->m_unit_cycles_retired / (double)m_simBase->m_unit_insts_retired;
        kernel_CPI = (double)m_simBase->m_kernel_cycles_retired / (double)m_simBase->m_kernel_insts_retired;          
        // m_simBase->model_bbv_profile << "Unit: " << interval_CPI << " "
        //                              << kernel_CPI << " "
        //                              << m_simBase->m_unit_cycles_retired << endl;                

        if (m_simBase->m_prof_l1_access == 0)
          m_simBase->m_prof_l1_access = 1;
        if (m_simBase->m_prof_l2_access == 0)
          m_simBase->m_prof_l2_access = 1;
        
        l1_rate = (double)m_simBase->m_prof_l1_misses / (double)m_simBase->m_prof_l1_access;
        l2_rate = (double)m_simBase->m_prof_l2_misses / (double)m_simBase->m_prof_l2_access;
        diverge_ratio = (double)m_simBase->m_prof_mem_uops / (double)m_simBase->m_prof_mem_insts;

        if (*KNOB(KNOB_MODEL_BBV_ENABLED) != 0) {
          m_simBase->model_bbv_profile << "Diverge_Ratio: " << diverge_ratio
                                       << " L1 rate: " << l1_rate << " accesses: "
                                       <<  m_simBase->m_prof_l1_access 
                                       << " L2 rate: " << l2_rate << " accesses: "
                                       <<  m_simBase->m_prof_l2_access 
                                       << endl;
        } else {
          m_simBase->sampling_bbv_profile << "Diverge_Ratio: " << diverge_ratio
                                          << " L1 rate: " << l1_rate << " accesses: "
                                          <<  m_simBase->m_prof_l1_access 
                                          << " L2 rate: " << l2_rate << " accesses: "
                                          <<  m_simBase->m_prof_l2_access 
                                          << endl;
        }
            
        // cout << "Diverge ratio: " << diverge_ratio 
        //      << " L1 rate: " << l1_rate << " accesses: " <<  m_simBase->m_prof_l1_access 
        //      << " L2 rate: " << l2_rate << " accesses: " <<  m_simBase->m_prof_l2_access 
        //      << endl;
        
        m_simBase->m_unit_cycles_retired = 0;
        m_simBase->m_unit_insts_retired = 0;
        m_simBase->m_unit_warp_insts_retired = 0;
        m_simBase->m_prof_l1_misses = 0;
        m_simBase->m_prof_l1_access = 0;
        m_simBase->m_prof_l2_misses = 0;
        m_simBase->m_prof_l2_access = 0;
        m_simBase->m_prof_mem_insts = 0;
        m_simBase->m_prof_mem_uops = 0;
        m_simBase->m_tb_retired_counts = 0;
        m_simBase->m_tot_active_threads = 0;
        m_simBase->m_tot_block_num = 0;
        m_simBase->monitored_count = 0;
        m_simBase->AMAT = 0;
        m_simBase->AMAT_base = 0;
      }      
      //////////////////////////////////////////////
      
      block_info->m_retired = true; 
      --core->m_running_block_num;
      --m_simBase->running_block_num;
      // cout << "@" << core->get_cycle_count() << " running block decrease!! " << m_simBase->running_block_num << endl;

      // deallocate block information
      block_schedule_info_s* block_schedule_info = m_simBase->m_block_schedule_info[block_id];
      m_simBase->m_block_schedule_info.erase(block_id);
      delete block_schedule_info;

      trace_info->m_process->m_block_list.erase(block_id);
      list<thread_trace_info_node_s*> *block_queue = (*m_block_queue)[block_id];
      m_block_queue->erase(block_id);
      block_queue->clear();
      delete block_queue;


      // stats
      STAT_EVENT_N(AVG_BLOCK_EXE_CYCLE, CYCLE - block_info->m_sched_cycle);
      STAT_EVENT(AVG_BLOCK_EXE_CYCLE_BASE);

      ++m_simBase->m_total_retired_block;
      if (*m_simBase->m_knobs->KNOB_MAX_BLOCKS_TO_SIMULATE > 0 && 
          m_simBase->m_total_retired_block >= *m_simBase->m_knobs->KNOB_MAX_BLOCKS_TO_SIMULATE) { 
        m_simBase->m_end_simulation = true;
      }
    }


    // FIXME TONAGESH
    
    // delete all synchronization information
    section_info_s* info;
    while (trace_info->m_sections.size() > 0) { 
      info = trace_info->m_sections.front();

      trace_info->m_sections.pop_front();
      m_simBase->m_section_pool->release_entry(info);
    }

    while (trace_info->m_bar_sections.size() > 0) { 
      info = trace_info->m_bar_sections.front();

      trace_info->m_bar_sections.pop_front();
      m_simBase->m_section_pool->release_entry(info);
    }

    while (trace_info->m_mem_sections.size() > 0) { 
      info = trace_info->m_mem_sections.front();

      trace_info->m_mem_sections.pop_front();
      m_simBase->m_section_pool->release_entry(info);
    }

    while (trace_info->m_mem_bar_sections.size() > 0) { 
      info = trace_info->m_mem_bar_sections.front();

      trace_info->m_mem_bar_sections.pop_front();
      m_simBase->m_section_pool->release_entry(info);
    }

    while (trace_info->m_mem_for_bar_sections.size() > 0) { 
      info = trace_info->m_mem_for_bar_sections.front();

      trace_info->m_mem_for_bar_sections.pop_front();
      m_simBase->m_section_pool->release_entry(info);
    }

    ASSERT(core->m_running_block_num >= 0); 
  }


  // TODO - nbl (apr-17-2013): use pools
  if (trace_info->m_process->m_ptx) {
    trace_info_gpu_s *temp = static_cast<trace_info_gpu_s*>(trace_info->m_prev_trace_info);
    delete temp;
    temp = static_cast<trace_info_gpu_s*>(trace_info->m_next_trace_info);
    delete temp;
  }
  else {
    trace_info_cpu_s *temp = static_cast<trace_info_cpu_s*>(trace_info->m_prev_trace_info);
    delete temp;
    temp = static_cast<trace_info_cpu_s*>(trace_info->m_next_trace_info);
    delete temp;
  }

  // release thread_trace_info to the pool
  m_simBase->m_thread_pool->release_entry(trace_info);


  // if there are remaining threads, schedule it
  if (m_simBase->m_num_waiting_dispatched_threads)
    sim_thread_schedule(false);  

  return 0;
}


// insert a new thread
void process_manager_c::insert_thread(thread_trace_info_node_s *incoming)
{
  ++m_simBase->m_num_waiting_dispatched_threads;
  m_thread_queue->push_back(incoming);
}


// insert a new thread block
void process_manager_c::insert_block(thread_trace_info_node_s *incoming)
{
  ++m_simBase->m_num_waiting_dispatched_threads;
  int block_id = incoming->m_block_id; 
  if (m_simBase->m_block_schedule_info.find(block_id) == m_simBase->m_block_schedule_info.end()) {
    block_schedule_info_s* block_schedule_info = new block_schedule_info_s;
    m_simBase->m_block_schedule_info[block_id] = block_schedule_info;
  }

  ++m_simBase->m_block_schedule_info[block_id]->m_total_thread_num;
  m_simBase->m_block_schedule_info[block_id]->m_trace_exist = true; 

  DEBUG("block_schedule_info[%d].trace_exist:%d\n", 
      block_id, m_simBase->m_block_schedule_info[block_id]->m_trace_exist); 

  if (m_block_queue->find(block_id) == m_block_queue->end()) {
    list<thread_trace_info_node_s *> *new_list = new list<thread_trace_info_node_s *>;
    (*m_block_queue)[block_id] = new_list;
  }
  (*m_block_queue)[block_id]->push_back(incoming);
}


// fetch a new thread
thread_trace_info_node_s *process_manager_c::fetch_thread(void)
{
  if (m_thread_queue->empty()) {
    return NULL;
  }

  ASSERT(m_simBase->m_num_waiting_dispatched_threads > 0);

  --m_simBase->m_num_waiting_dispatched_threads;

  thread_trace_info_node_s *front = m_thread_queue->front();
  m_thread_queue->pop_front();

  return front;
}


// get a thread from a block 
// fetch_block is a misnomer - it actually fetches a warp from the specified block
thread_trace_info_node_s *process_manager_c::fetch_block(int block_id)
{
  list<thread_trace_info_node_s *> *block_list = (*m_block_queue)[block_id];
  if (block_list->empty()) {
    return NULL;
  }

  ASSERT(m_simBase->m_num_waiting_dispatched_threads > 0);

  --m_simBase->m_num_waiting_dispatched_threads;

  thread_trace_info_node_s *front = block_list->front();
  block_list->pop_front();

  return front;
} 


// schedule a thread
// assigns a new thread/block to a core if the number of live threads/blocks
// on the core are fewer than the maximum allowed
void process_manager_c::sim_thread_schedule(bool initial)
{
  thread_trace_info_node_s* trace_to_run;

  // assign the fetched thread to that core's active queue
  // for (int core_id = 0; core_id < *KNOB(KNOB_NUM_SIM_CORES); ++core_id)  {
  for (int core_id = 0; core_id < *KNOB(KNOB_NUM_SIM_SMALL_CORES); ++core_id)  {
    core_c* core = m_simBase->m_core_pointers[core_id];

    if (*KNOB(KNOB_ROUTER_PLACEMENT) == 1 &&
        core->get_core_type() != "ptx" &&
        (core_id < *KNOB(KNOB_CORE_ENABLE_BEGIN) || core_id > *KNOB(KNOB_CORE_ENABLE_END))) 
        continue;

    if (core->m_running_thread_num < core->get_max_threads_per_core()) {
      // schedule a thread to x86 core
      if (core->get_core_type() != "ptx") {
        // fetch a new thread
        trace_to_run = fetch_thread();
        if (trace_to_run != NULL) {
          // create a new thread
          trace_to_run->m_trace_info_ptr = create_thread(trace_to_run->m_process, 
              trace_to_run->m_tid, trace_to_run->m_main);

          // unique thread num of a core
          int unique_scheduled_thread_num = core->m_unique_scheduled_thread_num;          
          trace_to_run->m_trace_info_ptr->m_thread_id = unique_scheduled_thread_num; 

          // add a new application to the core
          core->add_application(unique_scheduled_thread_num, trace_to_run->m_process);
           
          // add a new thread trace information
          core->create_trace_info(unique_scheduled_thread_num, trace_to_run->m_trace_info_ptr); 

          DEBUG("schedule: core %d will run thread id %d unique_thread_id:%d \n", 
              core_id, trace_to_run->m_trace_info_ptr->m_thread_id, 
              trace_to_run->m_trace_info_ptr->m_unique_thread_id);

          // set flag for the simulation
          m_simBase->m_core_end_trace[core_id] = false;
          m_simBase->m_sim_end[core_id]        = false;
          m_simBase->m_core_started[core_id]   = true; 

          // release the node entry
          m_simBase->m_trace_node_pool->release_entry(trace_to_run);
        }
      }
      // GPU simulation
      else if (core->get_core_type() == "ptx") { 
        // get currently fetching id
        int prev_fetching_block_id = core->m_fetching_block_id;

        // get block id
        int block_id = sim_schedule_thread_block(core_id, initial); 

        // no thread to schedule
        if (block_id == -1) 
          continue;

        // find a new thread
        trace_to_run = fetch_block(block_id);

        // try to schedule as many threads as possible in the same block
        while (trace_to_run != NULL) { 
          //create a new thread
          trace_to_run->m_trace_info_ptr = create_thread(trace_to_run->m_process, 
              trace_to_run->m_tid, trace_to_run->m_main);

          // increment dispatched thread number of a block
          ++m_simBase->m_block_schedule_info[block_id]->m_dispatched_thread_num;

          // unique thread num of a core
          int unique_scheduled_thread_num = core->m_unique_scheduled_thread_num;
          trace_to_run->m_trace_info_ptr->m_thread_id = unique_scheduled_thread_num;          

          // add a new application to the core
          core->add_application(unique_scheduled_thread_num, trace_to_run->m_process);

          // add a new thread trace information
          core->create_trace_info(unique_scheduled_thread_num, trace_to_run->m_trace_info_ptr);

          core->warp_issue_list[unique_scheduled_thread_num] = 0;
          core->warp_alloc_list[unique_scheduled_thread_num] = 0;
          core->warp_fetch_list[unique_scheduled_thread_num] = 0;

          for (int i = 0; i < core->threadSlot.size(); i ++)
            if (core->threadSlot[i] == -1) {
              core->threadSlot[i] = unique_scheduled_thread_num;
              break;
            }
          // if (core_id == 0)
          //   printf("unique %d\n", unique_scheduled_thread_num);

          //m_simBase->m_trace_reader->pre_read_trace(trace_to_run->m_trace_info_ptr);
          // Increase warp launch count
          STAT_EVENT(WARPS_LAUNCHED);
          
          // set flag for the simulation
          m_simBase->m_core_end_trace[core_id] = false;
          m_simBase->m_sim_end[core_id]        = false;
          m_simBase->m_core_started[core_id]   = true; 

          // for thread start end cycles
          uint32_t unique_thread_id = trace_to_run->m_trace_info_ptr->m_unique_thread_id;
          uint32_t process_id       = trace_to_run->m_trace_info_ptr->m_process->m_process_id;
          ASSERT(unique_thread_id < m_simBase->m_all_threads);
          
          thread_stat_s* thread_stat = &m_simBase->m_thread_stats[process_id][unique_thread_id]; 

          thread_stat->m_unique_thread_id   = unique_thread_id; 
          thread_stat->m_block_id           = block_id;	   
          thread_stat->m_thread_sched_cycle = core->get_cycle_count();

          // cout << "thread " << unique_scheduled_thread_num << endl;
          if (m_simBase->monitored_thread == -1)
            m_simBase->monitored_thread = unique_scheduled_thread_num;

          if (core->m_running_thread_num == core->get_max_threads_per_core()) 
            break;
          
          // release the node entry
          m_simBase->m_trace_node_pool->release_entry(trace_to_run);

          // try to schedule other threads in the same block
          prev_fetching_block_id = core->m_fetching_block_id;
          block_id = sim_schedule_thread_block(core_id, initial);
          if (block_id == -1) 
            break;

          // fetch a new thread
          trace_to_run = fetch_block(block_id);          
        }
      }
    }
  }
}


// assign a new block to the core
// get the block id of the warp to be next assigned to the core
// assignment of warps from a block doesn't happen in one go, it is done one 
// warp at a time, hence this function is called once for each warp. however, 
// all warps of a block get assigned in the same cycle (TODO: make it 1 call)
int process_manager_c::sim_schedule_thread_block(int core_id, bool initial) 
{  
  core_c* core          = m_simBase->m_core_pointers[core_id];
  int new_block_id      = -1; 
  int fetching_block_id = core->m_fetching_block_id; 
  DEBUG("core:%d fetching_block_id:%d total_thread_num:%d dispatched_thread_num:%d "
      "running_block_num:%d block_retired:%d \n",
      core_id, fetching_block_id, 
      (m_simBase->m_block_schedule_info[fetching_block_id]->m_total_thread_num), 
      (m_simBase->m_block_schedule_info[fetching_block_id]->m_dispatched_thread_num), 
      core->m_running_block_num, m_simBase->m_block_schedule_info[fetching_block_id]->m_retired);

  // cout << "3 fetching_block " << fetching_block_id << endl;
  // All threads from the currently serveced block should be scheduled before a new block
  // executes. Check currently fetching block has other threads to schedule
  if ((fetching_block_id != -1) && 
      m_simBase->m_block_schedule_info.find(fetching_block_id) != m_simBase->m_block_schedule_info.end() &&
      !(m_simBase->m_block_schedule_info[fetching_block_id]->m_retired)) {
    // cout << "2 fetching_block " << fetching_block_id << " " << core_id << endl;
    if (m_simBase->m_block_schedule_info[fetching_block_id]->m_total_thread_num > 
        m_simBase->m_block_schedule_info[fetching_block_id]->m_dispatched_thread_num) {      
      DEBUG("fetching_continue block_id:%d total_thread_num:%d dispatched_thread_num:%d \n", 
            fetching_block_id, m_simBase->m_block_schedule_info[fetching_block_id]->m_total_thread_num, 
            m_simBase->m_block_schedule_info[fetching_block_id]->m_dispatched_thread_num);
      return fetching_block_id; 
    }
  }
  // if (fetching_block_id != -1)
  //   cout << "fetching_block " << fetching_block_id
  //        << " " << m_simBase->m_block_schedule_info[fetching_block_id]->m_dispatched_thread_num << endl;

  // All threads from previous block have been schedule. Thus, need to find a new block
  int appl_id = core->get_appl_id();
  int max_block_per_core = m_simBase->m_sim_processes[appl_id]->m_max_block;

  // If the core already has maximum blocks, do not fetch more  
  if ((core->m_running_block_num + 1) > max_block_per_core) 
    return -1;
  
  bool block_found = true;
  process_s* process = m_simBase->m_sim_processes[appl_id];
  for (auto I = process->m_block_list.begin(), E = process->m_block_list.end(); I != E; ++I) {    
    int block_id = (*I).first;

    if (*KNOB(KNOB_SAMPLING_ENABLED) == true) {
      bool matched = false;
      
      if (m_simBase->tbpoints.size() > 0) {
        // Remove finished block from tb list      
        int phaseId = m_simBase->tbpoints.front().first;
        vector<int> tblist = m_simBase->tbpoints.front().second;
        
        // cout << "phaseId " << m_simBase->tbpoints.size() << endl;        
            
        if (fetching_block_id != -1) {        
          for (int i = 0; i < tblist.size(); i ++) {            
            if (fetching_block_id == tblist[i]) {
              tblist.erase(tblist.begin() + i);
              m_simBase->tbpoints.front().second = tblist;
              break;
            }          
          }
          m_simBase->finishedBlocks[fetching_block_id] = true;
        }

        // Remove issued blocks from tb list
        for (int i = 0; i < tblist.size(); i ++) 
          if (m_simBase->finishedBlocks.find(tblist[i]) != m_simBase->finishedBlocks.end()) {
            tblist.erase(tblist.begin() + i);
            m_simBase->tbpoints.front().second = tblist;
            break;
          }

        // Check if all blocks in list are issued                  
        if (tblist.empty() == false) {
          for (int i = 0; i < tblist.size(); i ++)
            if (tblist[i] > m_simBase->max_block_id)
              m_simBase->max_block_id = tblist[i];
          
          if (block_id < m_simBase->max_block_id) {
            for (int i = 0; i < tblist.size(); i ++) 
              if (block_id == tblist[i]) {
                matched = true;                
                break;
              } 
          } else {
            matched = true;
          }
          
        } else {          
          if (block_id > m_simBase->max_block_id)
            matched = true;          
        }       
        
        // printf("%ld %d %d\n", m_simBase->epoch_map[epoch_id].size(), block_id, m_simBase->epoch_min_block);        
      }
      
      if (!matched && process->m_block_list.size() > 1) continue;
    } // end of sampling_enabled

    if (*KNOB(KNOB_INTRA_SAMPLING_ENABLED) == true) {
      int i;
      
      if (m_simBase->doneStablePhase) {
        i = m_simBase->curBlockId;

        // cout << "curBlock " << m_simBase->curBlockId << endl;
        while (i < m_simBase->tbtype.size() && m_simBase->tbtype[i] != m_simBase->tbtype[m_simBase->curBlockId])
          i ++;

        if (i == m_simBase->tbtype.size()) {
          if (process->m_current_vector_index < process->m_applications.size()) {
            // printf("RESET 1!\n");
            setup_process(process);
            sim_thread_schedule(true);
          }
          return -1;
          
        } else if (m_simBase->curBlockList.size() != 0) {
          continue;
        } else if (block_id > m_simBase->curBlockId &&
                   m_simBase->tbtype[block_id] == m_simBase->tbtype[m_simBase->curBlockId]) {
          m_simBase->acc_cycles += ((double)m_simBase->tbweight[block_id] / (double)m_simBase->StablePhaseIPC);
          m_simBase->m_prof_cycles_retired += ((double)m_simBase->tbweight[block_id] / (double)m_simBase->StablePhaseIPC);
          m_simBase->acc_warp_insts += m_simBase->tbweight[block_id];
          m_simBase->m_prof_insts_retired += m_simBase->tbweight[block_id];
          // cout << "Skip Insts " << m_simBase->tbweight[block_id] << " Cycles "
          //      << (m_simBase->tbweight[block_id] / m_simBase->StablePhaseIPC) << " IPC "
          //      << m_simBase->StablePhaseIPC << " TBID "
          //      << block_id << endl;

          m_simBase->acc_l1_accesses += m_simBase->memweight[block_id];
          m_simBase->acc_l1_misses += m_simBase->StableL1MissRate * m_simBase->memweight[block_id];
          m_simBase->acc_l2_accesses += m_simBase->memweight[block_id] * m_simBase->StableL1L2Rate;
          m_simBase->acc_l2_misses += m_simBase->memweight[block_id] * m_simBase->StableL1L2Rate * m_simBase->StableL2MissRate;
          
          m_simBase->sampling_bbv_profile << "Skip Insts " << m_simBase->tbweight[block_id] << " Cycles "
                                          << (m_simBase->tbweight[block_id] / m_simBase->StablePhaseIPC) << " IPC "
                                          << m_simBase->StablePhaseIPC << " TBID "
                                          << block_id << " PhaseID " << m_simBase->tbtype[block_id]
                                          << endl;
          
          m_simBase->skipped_insts += m_simBase->tbweight[block_id];
          // m_simBase->curBlockId = block_id;
          process->m_block_list.erase(block_id);
          continue;
        }
      }      
      
      if (block_id < m_simBase->curBlockId)
        continue;
    }
    
    if (initial && !*m_simBase->m_knobs->KNOB_ASSIGN_BLOCKS_GREEDILY_INITIALLY) {
      int min_core_id;
      if (*m_simBase->m_knobs->KNOB_MAX_NUM_CORE_PER_APPL == 0) {
        min_core_id = 0;
      }
      else 
      {
        min_core_id = process->m_core_list.begin()->first;
      }
      int num_core_per_appl = process->m_core_list.size();

      if ((block_id  - m_simBase->m_block_id_mapper->find(process->m_process_id, process->m_kernel_block_start_count[process->m_current_vector_index - 1])) % num_core_per_appl != (core_id - min_core_id)) {        
        continue;
      }
    }

    // printf("X block_id %d\n", block_id);
    // this block was not fetched yet and has traces to schedule
    if (!(m_simBase->m_block_schedule_info[block_id]->m_start_to_fetch) && 
        m_simBase->m_block_schedule_info[block_id]->m_trace_exist) {
      block_found = true;
      new_block_id = block_id;
      break; 
    }	     
  } // end of for loop

  // no block found to schedule
  if (new_block_id == -1) {
    if ((*KNOB(KNOB_SAMPLING_ENABLED) == true || *KNOB(KNOB_INTRA_SAMPLING_ENABLED))
        && m_simBase->running_block_num == 0) {

      if (process->m_current_vector_index < process->m_applications.size()) {        
        // double acc_IPC = (float)m_simBase->acc_warp_insts / (float)m_simBase->acc_cycles;
        // int kernelPhase = m_simBase->kernelPhaseID[process->m_current_vector_index - 1];
        int phaseId = m_simBase->kernelPhaseID[process->m_current_vector_index - 1];
        // double PhaseWeight = ((double)m_simBase->kernelPhaseWeight[phaseId] / (double)m_simBase->totalKernelPhaseWeight);
        
        // m_simBase->curKernelPhaseWeight = PhaseWeight;
        // m_simBase->curKernelPhaseID = phaseId;
        // m_simBase->totalKernelPhaseIPC += PhaseWeight * acc_IPC;
        // m_simBase->sampling_bbv_profile << "KernelPhaseIPC " << acc_IPC << " " << PhaseWeight << endl;

        // This phase is finished        
        m_simBase->kernelPhaseDone[phaseId] = true;
        m_simBase->totalKernelInsts += (m_simBase->acc_warp_insts - m_simBase->skipped_insts);        
        printf("RESET! %d %d\n", process->m_current_vector_index, m_simBase->kernelPhaseWeight[phaseId]);       
        while (m_simBase->kernelPhaseDone[phaseId] == true) {          
          if (process->m_current_vector_index >= process->m_applications.size())
            break;
          phaseId = m_simBase->kernelPhaseID[process->m_current_vector_index];
          process->m_current_vector_index ++;        
        } 
        
        if (m_simBase->kernelPhaseDone[phaseId] == false) {
          process->m_current_vector_index --;          
          setup_process(process);
          sim_thread_schedule(true);
        }
      }
    }
    return -1;  
  }    
  
  DEBUG("new block is %d core %d\n", new_block_id, core_id);
  // printf("new block is %d core %d\n", new_block_id, core_id);
  // m_simBase->sampling_bbv_profile << "new block " << new_block_id
  //                                 << " running " << m_simBase->running_block_num
  //                                 << " core " << core_id
  //                                 << endl;
  m_simBase->curBlockList[new_block_id] = true;
  if (*KNOB(KNOB_INTRA_SAMPLING_ENABLED) == true) {                
    // Check if the current phase is stable phase
    if (m_simBase->CheckStablePhase() == true) 
      m_simBase->isStablePhase = true;
  }    
  
  STAT_EVENT(SM_CTA_LAUNCHED);
  
  // set up block
  m_simBase->m_block_schedule_info[new_block_id]->m_start_to_fetch     = true;
  m_simBase->m_block_schedule_info[new_block_id]->m_dispatched_core_id = core_id; 
  m_simBase->m_block_schedule_info[new_block_id]->m_sched_cycle        = core->get_cycle_count(); 

  // set up core
  core->m_running_block_num = core->m_running_block_num + 1;
  m_simBase->running_block_num ++;
  // cout << "@" << core->get_cycle_count() << " running block increase!! " << m_simBase->running_block_num << endl;
  core->m_fetching_block_id = new_block_id; 

  DEBUG("new block is allocated to core:%d running_block:%d fetching_block_id:%d \n" ,
        core_id, new_block_id, core->m_fetching_block_id); 

  if (m_simBase->monitored_block == -1) {
    // printf("new block is %d core %d\n", new_block_id, core_id);
    m_simBase->monitored_block = new_block_id;
    // m_simBase->model_bbv_profile << "Monitored " << new_block_id << " core " << core_id << " count " << *KNOB(KNOB_MODEL_SIZE_RATIO) << " cycle " << core->get_cycle_count() << endl;
    // cout << "Monitored " << new_block_id << " core " << core_id << " count " << *KNOB(KNOB_MODEL_SIZE_RATIO) << " cycle " << core->get_cycle_count() << endl;
  }  
  return new_block_id; 
}

