/*
Copyright (c) <2012>, <Georgia Institute of Technology> All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted 
provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions 
and the following disclaimer.

Redistributions in binary form must reproduce the above copyright notice, this list of 
conditions and the following disclaimer in the documentation and/or other materials provided 
with the distribution.

Neither the name of the <Georgia Institue of Technology> nor the names of its contributors 
may be used to endorse or promote products derived from this software without specific prior 
written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR 
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY 
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
POSSIBILITY OF SUCH DAMAGE.
*/


/**********************************************************************************************
* File         : macsim.cc
* Author       : HPArch
* Date         : 1/7/2008
* SVN          : $Id: main.cc 911 2009-11-20 19:08:10Z kacear $:
* Description  : main function
*********************************************************************************************/


#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <sys/time.h>
#include <cmath>
#include <tuple>

#include "macsim.h"
#include "assert_macros.h"
#include "global_types.h"
#include "core.h"
#include "trace_read.h"
#include "frontend.h"
#include "knob.h"
#include "process_manager.h"
#include "debug_macros.h"
#include "statistics.h"
#include "memory.h"
#include "dram.h"
#include "utils.h"
#include "bug_detector.h"
#include "fetch_factory.h"
#include "pref_factory.h"
#include "network.h"
#include "network_ring.h"
#include "network_mesh.h"
#include "network_simple.h"
#include "factory_class.h"
#include "dram.h"

#include "all_knobs.h"
#include "all_stats.h"
#include "statistics.h"


#ifdef POWER_EI
#include "ei_power.h"
#endif


using namespace std;


///////////////////////////////////////////////////////////////////////////////////////////////


#define DEBUG(args...)   _DEBUG(*m_simBase->m_knobs->KNOB_DEBUG_SIM, ## args)


///////////////////////////////////////////////////////////////////////////////////////////////


// =======================================
// Macsim constructor
// =======================================
macsim_c::macsim_c()
{
  m_simBase = this;

  m_num_active_threads             = 0; 
  m_num_waiting_dispatched_threads = 0;
  m_total_num_application          = 0;
  m_process_count                  = 0; 
  m_process_count_without_repeat   = 0;
  m_all_threads                    = 0;
  m_no_threads_per_block           = 0;
  m_total_retired_block            = 0;
  m_end_simulation                 = false;
  m_repeat_done                    = false;
  m_gpu_paused                     = true;

  for (int ii = 0; ii < MAX_NUM_CORES; ++ii) {    
    m_core_cycle[ii]     = 0;
    m_core_end_trace[ii] = false;
    m_sim_end[ii]        = false;
    m_core_started[ii]   = false;
  }

  m_termination_check[0] = false;
  m_termination_check[1] = false;

  m_simulation_cycle = 0;
  m_core0_inst_count = 0;

  kernel_launch_counter = 0;
  start_tracking = false;
  max_block_id = -1;
  running_block_num = 0;
  running_thread_num = 0;
  m_tot_block_num = 0;
  m_tot_active_threads = 0;
  
  monitored_block = -1;
  monitored_count = 0;
  AMAT_base = 0;
  AMAT = 0;
  warp_mem_insts = 0;
  max_ipc = 0;
  min_ipc = 1000;
  predicted_ipc = 0;
  sample_count = 0;
  acc_insts = 0;
  acc_cycles = 0;
  acc_warp_insts = 0;
  skipped_insts = 0;
  acc_l1_accesses = 0;
  acc_l1_misses = 0;
  acc_l2_accesses = 0;
  acc_l2_misses = 0;
  active_warps_count = 0;
  active_cycles_count = 0;
  curBlockId = 0;
  curPhaseId = 0;

  m_prof_insts_retired = 0;
  m_prof_cycles_retired = 0;
  totalKernelPhaseWeight= 0;
  totalKernelPhaseL1Accesses = 0;
  totalKernelPhaseL1Misses = 0;
  totalKernelPhaseL2Accesses = 0;
  totalKernelPhaseL2Misses = 0;  
  maxBB = 1;
  monitored_thread = -1;
  PCBBMap[8000] = 0;
  curKernelIndex = -1;
}


// =======================================
// Macsim destructor
// =======================================
macsim_c::~macsim_c()
{
}


// =======================================
// initialize knobs
// =======================================
void macsim_c::init_knobs(int argc, char** argv)
{
  report("initialize knobs");

  // Create the knob managing class
  m_knobsContainer = new KnobsContainer();

  // Get a reference to the actual knobs for this component instance
  m_knobs = m_knobsContainer->getAllKnobs();

#ifdef USING_SST
  string  paramPath(argv[0]);
  string  tracePath(argv[1]);
  string outputPath(argv[2]);

  //Apply specified params.in file
  m_knobsContainer->applyParamFile(paramPath);

  //Specify trace_file_list and update the knob
  m_knobsContainer->updateKnob("trace_name_file", tracePath);

  //Specify output path for statistics
  m_knobsContainer->updateKnob("out", outputPath);

  //save the states of all knobs to a file
  m_knobsContainer->saveToFile(outputPath + "/params.out");

#else
  // apply values from parameters file
  m_knobsContainer->applyParamFile("params.in");

  // apply the supplied command line switches
  char* pInvalidArgument = NULL;
  if(!m_knobsContainer->applyComandLineArguments(argc, argv, &pInvalidArgument)) {
  }

  // save the states of all knobs to a file
  m_knobsContainer->saveToFile("params.out");
#endif
}


// =======================================
// register wrapper functions to allocate objects later
// =======================================
void macsim_c::register_functions(void)
{
  mem_factory_c::get()->register_class("l3_coupled_network", default_mem);
  mem_factory_c::get()->register_class("l3_decoupled_network", default_mem); 
  mem_factory_c::get()->register_class("l2_coupled_local", default_mem); 
  mem_factory_c::get()->register_class("no_cache", default_mem);
  mem_factory_c::get()->register_class("l2_decoupled_network", default_mem);
  mem_factory_c::get()->register_class("l2_decoupled_local", default_mem);

  dram_factory_c::get()->register_class("FRFCFS", frfcfs_controller);
  dram_factory_c::get()->register_class("FCFS", fcfs_controller);

  fetch_factory_c::get()->register_class("rr", fetch_factory);
  pref_factory_c::get()->register_class(pref_factory);
  bp_factory_c::get()->register_class("gshare", default_bp); 

  llc_factory_c::get()->register_class("default", default_llc);

  network_factory_c::get()->register_class("ring", default_network);
  network_factory_c::get()->register_class("mesh", default_network);
  network_factory_c::get()->register_class("simple_noc", default_network);
}


// =======================================
// memory allocation
// =======================================
void macsim_c::init_memory(void)
{
  // pool allocation
  m_thread_pool           = new pool_c<thread_s>(10, "thread_pool"); 
  m_section_pool          = new pool_c<section_info_s>(100, "section_pool"); 
  m_mem_map_entry_pool    = new pool_c<mem_map_entry_c>(200, "mem_map_pool");
  m_heartbeat_pool        = new pool_c<heartbeat_s>(10, "heartbeat_pool");
  m_bp_recovery_info_pool = new pool_c<bp_recovery_info_c>(10, "bp_recovery_info_pool");
  m_trace_node_pool       = new pool_c<thread_trace_info_node_s>(10, "thread_node_pool");
  m_uop_pool              = new pool_c<uop_c>(1000, "uop_pool");

  m_block_id_mapper = new multi_key_map_c;
  m_process_manager = new process_manager_c(m_simBase);
  m_trace_reader = new trace_reader_wrapper_c(m_simBase);

  // block schedule info
  block_schedule_info_s* block_schedule_info = new block_schedule_info_s;
  m_block_schedule_info[0] = block_schedule_info;

  // dummy invalid uop
  m_invalid_uop = new uop_c(m_simBase);
  m_invalid_uop->init();
  
  // interconnection network
  string network_type = KNOB(KNOB_NOC_TOPOLOGY)->getValue();
  m_network = network_factory_c::get()->allocate(network_type, m_simBase);

  // main memory
  string memory_type = m_simBase->m_knobs->KNOB_MEMORY_TYPE->getValue();
  m_memory = mem_factory_c::get()->allocate(memory_type, m_simBase);

  // dram controller
  m_num_mc = m_simBase->m_knobs->KNOB_DRAM_NUM_MC->getValue();
  m_dram_controller = new dram_c*[m_num_mc];
  // int num_noc_node = *m_simBase->m_knobs->KNOB_NUM_SIM_CORES
  int num_noc_node = *m_simBase->m_knobs->KNOB_NUM_SIM_SMALL_CORES
    + *m_simBase->m_knobs->KNOB_NUM_L3;
  for (int ii = 0; ii < m_num_mc; ++ii) {
    m_dram_controller[ii] = dram_factory_c::get()->allocate(
        m_simBase->m_knobs->KNOB_DRAM_SCHEDULING_POLICY->getValue(), m_simBase);
    m_dram_controller[ii]->init(ii);
  }

  // initialize memory
  m_memory->init();

  // bug detector
  if (*KNOB(KNOB_BUG_DETECTOR_ENABLE)) {
    printf("enabling bug detector\n");
    m_bug_detector = new bug_detector_c(m_simBase);
  }
}


// =======================================
// initialize output streams 
// =======================================
void macsim_c::init_output_streams()
{
    string stderr_file = *m_simBase->m_knobs->KNOB_STDERR_FILE;
    string stdout_file = *m_simBase->m_knobs->KNOB_STDOUT_FILE;
    string status_file = *m_simBase->m_knobs->KNOB_STDOUT_FILE;

    if (strcmp(stderr_file.c_str(), "NULL")) {
        g_mystderr = file_tag_fopen(stderr_file.c_str(), "w", m_simBase);

        if (!g_mystderr) {
            fprintf(stderr, "\n");
            fprintf(stderr, "%s:%d: ASSERT FAILED (I=%s  C=%s):  ", __FILE__, __LINE__,
                unsstr64(m_core0_inst_count), unsstr64(m_simulation_cycle));
            fprintf(stderr, "%s '%s'\n", "mystderr", stderr_file.c_str());
            breakpoint(__FILE__, __LINE__);
            exit(15);
        }
    }

    if (strcmp(stdout_file.c_str(), "NULL")){
        g_mystdout = file_tag_fopen(stdout_file.c_str(), "w", m_simBase);

        if (!g_mystdout) {
            fprintf(stderr, "\n");
            fprintf(stderr, "%s:%d: ASSERT FAILED (I=%s  C=%s):  ", __FILE__, __LINE__,
                unsstr64(m_core0_inst_count), unsstr64(m_simulation_cycle));

            fprintf(stderr, "%s '%s'\n", "mystdout", stdout_file.c_str()); 
            breakpoint(__FILE__, __LINE__);
            exit(15);
        }
    }

    if (!strcmp(status_file.c_str(), "NULL")) {
        g_mystatus = fopen(status_file.c_str(), "a"); 

        if (!g_mystatus) {
            fprintf(stderr, "\n");
            fprintf(stderr, "%s:%d: ASSERT FAILED (I=%s  C=%s):  ", __FILE__, __LINE__,
                unsstr64(m_core0_inst_count), unsstr64(m_simulation_cycle));
            fprintf(stderr, "%s '%s'\n", "mystatus", status_file.c_str());
            breakpoint(__FILE__, __LINE__);
            exit(15);
        }
    }
}


// =======================================
// initialize cores 
// =======================================
void macsim_c::init_cores(int num_max_core)
{
    int num_large_cores        = *KNOB(KNOB_NUM_SIM_LARGE_CORES);
    int num_large_medium_cores = *KNOB(KNOB_NUM_SIM_LARGE_CORES) + *KNOB(KNOB_NUM_SIM_MEDIUM_CORES);

    report("initialize cores (" << num_large_cores << "/" 
            << (num_large_medium_cores - num_large_cores) << "/" 
      << *KNOB(KNOB_NUM_SIM_SMALL_CORES) <<")");

    ASSERT(num_max_core == (*KNOB(KNOB_NUM_SIM_SMALL_CORES) + \
        *KNOB(KNOB_NUM_SIM_MEDIUM_CORES) + *KNOB(KNOB_NUM_SIM_LARGE_CORES)));


    // based on the core type, add cores into type-specific core pools
    
    // large coresno_mcs
    for (int ii = 0; ii < num_large_cores; ++ii) { 
        m_core_pointers[ii] = new core_c(ii, m_simBase, UNIT_LARGE);
        m_core_pointers[ii]->init();
        m_core_pointers[ii]->pref_init();

        // insert to the core type pool
        if (static_cast<string>(*m_simBase->m_knobs->KNOB_LARGE_CORE_TYPE) == "ptx")
            m_ptx_core_pool.push(ii);
        else
            m_x86_core_pool.push(ii);
    }

    // medium cores
    int total_core = num_large_cores;
    for (int ii = 0; ii < *KNOB(KNOB_NUM_SIM_MEDIUM_CORES); ++ii) { 
        m_core_pointers[ii + num_large_cores] = new core_c(ii + num_large_cores, m_simBase, UNIT_MEDIUM);
        m_core_pointers[ii + num_large_cores]->init();
        m_core_pointers[ii + num_large_cores]->pref_init();

        // insert to the core type pool
        if (static_cast<string>(*m_simBase->m_knobs->KNOB_MEDIUM_CORE_TYPE) == "ptx")
            m_ptx_core_pool.push(ii + total_core);
        else
            m_x86_core_pool.push(ii + total_core);
    }

    // small cores
    total_core += *m_simBase->m_knobs->KNOB_NUM_SIM_MEDIUM_CORES;
    for (int ii = 0; ii < *KNOB(KNOB_NUM_SIM_SMALL_CORES); ++ii) { 
        m_core_pointers[ii + num_large_medium_cores] = 
            new core_c(ii + num_large_medium_cores, m_simBase, UNIT_SMALL);
        m_core_pointers[ii + num_large_medium_cores]->init();
        m_core_pointers[ii + num_large_medium_cores]->pref_init();

        // insert to the core type pool
        if (static_cast<string>(*m_simBase->m_knobs->KNOB_CORE_TYPE) == "ptx")
            m_ptx_core_pool.push(ii + total_core);
        else
            m_x86_core_pool.push(ii + total_core);
    }
}


#ifdef IRIS
// =======================================
// initialize IRIS parameters (with config file, preferrably)
// =======================================
void macsim_c::init_iris_config(map<string, string> &params)  //passed g_iris_params here
{
  params["topology"] = KNOB(KNOB_IRIS_TOPOLOGY)->getValue();

  // todo
  // 1. mapping function
  // 2. # port for torus
  // 3. rc method
  // 4. other knobs
  if (params["topology"] == "ring") {
    params["no_ports"] = "3";
    stringstream sstr;
    for (int ii = 0; ii < 64; ++ii) {
      sstr << ii;
      if (ii != 63)
        sstr << ',';
    }
    sstr >> params["mapping"];
    params["rc_method"] = "RING_ROUTING";
  }
  else if (params["topology"] == "mesh" || params["topology"] == "torus") {
    params["grid_size"] = KNOB(KNOB_IRIS_GRIDSIZE)->getValue();
    params["no_ports"]  = "5"; // check for torus
    params["rc_method"] = "XY_ROUTING_HETERO";
    params["mapping"]  = "0,1,2,5,6,7,8,9,10,13,14,15,16,17,18,21,22,23,24,25,26,29,30,31,32,33,34,37,38,39,40,41,42,45,46,47,48,49,50,53,54,55,56,57,58,61,62,63,11,12,19,20,27,28,35,36,43,44,51,52,3,4,59,60,";                                                             
    //original spinal order "0,1,2,48,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,3,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66";

//"0,1,2,60,61,3,4,5,6,7,8,48,49,9,10,11,12,13,14,50,51,15,16,17,18,19,20,52,53,21,22,23,24,25,26,54,55,27,28,29,30,31,32,56,57,33,34,35,36,37,38,58,59,39,40,41,42,43,44,62,63,45,46,47";
    //"0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16";
  }//else if (params["topology"] == "spinalMesh") 
  
  //FIXME - always uses spinalMesh for testing
  if (0)
  {
    params["grid_size"] = KNOB(KNOB_IRIS_GRIDSIZE)->getValue();
    params["no_ports"]  = "5"; // 4 for north/south, 2 for west/east, 1 for interface
    params["mapping"]  = "0,1,2,60,61,3,4,5,6,7,8,48,49,9,10,11,12,13,14,50,51,15,16,17,18,19,20,52,53,21,22,23,24,25,26,54,55,27,28,29,30,31,32,56,57,33,34,35,36,37,38,58,59,39,40,41,42,43,44,62,63,45,46,47";
    //"0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66";
    cout << "reading mesh topology in init iris config" << endl;
    params["rc_method"] = "XY_ROUTING_HETERO"; //doesn't work in param file, might need to use quotes
  }
  params["no_vcs"]         = KNOB(KNOB_IRIS_NUM_VC)->getValue();
  params["credits"]        = KNOB(KNOB_IRIS_CREDIT)->getValue();
  params["int_buff_width"] = KNOB(KNOB_IRIS_INT_BUFF_WIDTH)->getValue();
  params["link_width"]     = KNOB(KNOB_IRIS_LINK_WIDTH)->getValue();

  string s;
  ToString(s, KNOB(KNOB_DRAM_NUM_MC)->getValue());
  params["no_mcs"] = s; 

  //number of nodes depends on MacSim configuration (# of L1+L3(acts as L2))+MC nodes)
  ToString(s, m_macsim_terminals.size());
  params["no_nodes"] = s;
          
  report("Routing method: " << params["rc_method"] << "\n");       
#if FIXME
  //if (! key.compare("-iris:self_assign_dest_id"))
   //           params.insert(pair<string,string>("self_assign_dest_id",value));
          if (! key.compare("-mc:resp_payload_len"))
              params.insert(pair<string,string>("resp_payload_len",value));
          if (! key.compare("-mc:memory_latency"))
              params.insert(pair<string,string>("memory_latency",value));
#endif
}
#endif


// =======================================
// IRIS
// initialize Iris/manifold network
// =======================================
void macsim_c::init_network(void)
{
#ifdef IRIS
  assert(*KNOB(KNOB_ENABLE_IRIS) == true && *KNOB(KNOB_ENABLE_NEW_NOC) == false);
  init_iris_config(m_iris_params);

  map<std::string, std::string>:: iterator it;

  it = m_iris_params.find("topology");
  if ((it->second).compare("ring") == 0) {
    m_iris_network = new Ring(m_simBase);
  } 
  else if ((it->second).compare("mesh") == 0) {
    m_iris_network = new Mesh(m_simBase);
  } 
  else if ((it->second).compare("torus") == 0) {
    m_iris_network = new Torus(m_simBase);
  } 

  //initialize iris network
  m_iris_network->parse_config(m_iris_params);
  m_simBase->no_nodes = m_macsim_terminals.size();
  report("number of macsim terminals: " << m_macsim_terminals.size() << "\n");

#define MAPI i //((Mesh*)m_iris_network)->mapping[i]

  for (unsigned int i=0; i<m_macsim_terminals.size(); i++) {
    //create component id
    manifold::kernel::CompId_t interface_id = manifold::kernel::Component::Create<NInterface>(0,m_simBase);
    manifold::kernel::CompId_t router_id = manifold::kernel::Component::Create<SimpleRouter>(0,m_simBase);

    //create component
    NInterface* interface = manifold::kernel::Component::GetComponent<NInterface>(interface_id);
    SimpleRouter* rr =manifold::kernel::Component::GetComponent<SimpleRouter>(router_id);

    //set node id
    interface->node_id = i;
    rr->node_id = i;

    //register clock
    manifold::kernel::Clock::Register<NInterface>(interface, &NInterface::tick, &NInterface::tock);
    manifold::kernel::Clock::Register<SimpleRouter>(rr, &SimpleRouter::tick, &SimpleRouter::tock);

    //push back
    m_iris_network->terminals.push_back(m_macsim_terminals[MAPI]);
    m_iris_network->terminal_ids.push_back(m_macsim_terminals[MAPI]->GetComponentId());
    m_iris_network->interfaces.push_back(interface);
    m_iris_network->interface_ids.push_back(interface_id);
    m_iris_network->routers.push_back(rr);
    m_iris_network->router_ids.push_back(router_id);

    //init
    m_macsim_terminals[MAPI]->parse_config(m_iris_params);
    m_macsim_terminals[MAPI]->init();
    interface->parse_config(m_iris_params);
    interface->init();
    rr->parse_config(m_iris_params);
    rr->init();
  }

  //initialize router outports
  for (unsigned int i = 0; i < m_macsim_terminals.size(); i++)
    m_iris_network->set_router_outports(i);

  m_iris_network->connect_interface_terminal();
  m_iris_network->connect_interface_routers();
  m_iris_network->connect_routers();

#ifdef POWER_EI
  //initialize power stats
  avg_power     = 0;
  total_energy  = 0;
  total_packets = 0;
#endif
#endif
}


// =======================================
// initialize simulation
// =======================================
void macsim_c::init_sim(void)
{
  report("initialize simulation");

  // start measuring time
  gettimeofday(&m_begin_sim, NULL);

  // make sure all the variable sizes are what we expect
  ASSERTU(sizeof(uns8)  == 1);
  ASSERTU(sizeof(uns16) == 2);
  ASSERTU(sizeof(uns32) == 4);
  ASSERTU(sizeof(uns64) == 8);
  ASSERTU(sizeof(int8)  == 1);
  ASSERTU(sizeof(int16) == 2);
  ASSERTU(sizeof(int32) == 4);
  ASSERTU(sizeof(int64) == 8);
}


#ifdef POWER_EI
// =======================================
// =======================================
void macsim_c::compute_power(void)
{
    m_ei_power = new ei_power_c(m_simBase);
    m_ei_power->ei_config_gen_top();    // to make config file for EI
    m_ei_power->ei_main();
    
    delete m_ei_power;
}
#endif


// =======================================
// open traces from trace_file_list file
// =======================================
void macsim_c::open_traces(string trace_list)
{
  fstream tracefile(trace_list.c_str(), ios::in);

  tracefile >> m_total_num_application;

  // create processes
  for (int ii = 0; ii < m_total_num_application; ++ii) {
    string line;
    tracefile >> line;
    m_process_manager->create_process(line);
    m_process_manager->sim_thread_schedule(true);
  }
}


// =======================================
// deallocate memory
// =======================================
void macsim_c::deallocate_memory(void)
{
  // memory deallocation
  delete m_thread_pool;
  delete m_section_pool; 
  delete m_mem_map_entry_pool;
  delete m_heartbeat_pool;
  delete m_bp_recovery_info_pool;
  delete m_trace_node_pool;
  delete m_uop_pool;
  delete m_invalid_uop;
  delete m_memory;
  delete m_network;

  if (*m_simBase->m_knobs->KNOB_BUG_DETECTOR_ENABLE)
    delete m_bug_detector;

  // deallocate cores
  int num_large_cores        = *KNOB(KNOB_NUM_SIM_LARGE_CORES);
  int num_large_medium_cores = *KNOB(KNOB_NUM_SIM_LARGE_CORES) + *KNOB(KNOB_NUM_SIM_MEDIUM_CORES);
  for (int ii = 0; ii < num_large_cores; ++ii) {
    delete m_core_pointers[ii];
    m_core_pointers[ii] = NULL;
  }

  for (int ii = 0; ii < *KNOB(KNOB_NUM_SIM_MEDIUM_CORES); ++ii) {
    delete m_core_pointers[ii + num_large_cores];
    m_core_pointers[ii + num_large_cores] = NULL;
  }

  for (int ii = 0; ii < *KNOB(KNOB_NUM_SIM_SMALL_CORES); ++ii) {
    delete m_core_pointers[ii + num_large_medium_cores];
    m_core_pointers[ii + num_large_medium_cores] = NULL;
  }
}


// =======================================
// finalize simulation 
// =======================================
void macsim_c::fini_sim(void)
{
  report("finalize simulation");

  // execution time calculation
  gettimeofday(&m_end_sim, NULL);
  REPORT("elapsed time:%.1f seconds\n", \
      ((m_end_sim.tv_sec - m_begin_sim.tv_sec)*1000000 + \
       m_end_sim.tv_usec - m_begin_sim.tv_usec)/1000000.0);

  int second = static_cast<int>(
      ((m_end_sim.tv_sec - m_begin_sim.tv_sec)*1000000 + 
       m_end_sim.tv_usec - m_begin_sim.tv_usec)/1000000.0);
  STAT_EVENT_N(EXE_TIME, second);

#ifdef POWER_EI
  // compute power if enable_energy_introspector is enabled
  if (*KNOB(KNOB_ENABLE_ENERGY_INTROSPECTOR)) {
    compute_power();
  }
#endif


#ifdef IRIS
  ofstream irisTraceFile;
  string logName = "iris_log_mesh_stall";

  irisTraceFile.open(logName.c_str());
  if (irisTraceFile.is_open())
  {
    cout << "printing to file " << logName << endl;

    //mapping info
    irisTraceFile << ":Topology mapping:" << endl;

    const char* message_class_type[] = {"INVALID", "PROC", "L1", "L2", "L3", "MC"};
    for(unsigned int i=0; i<m_macsim_terminals.size(); i++)
    {
      int node_id = ((Ring*)m_iris_network)->mapping[i];
      int type = m_simBase->m_macsim_terminals.at(node_id)->mclass;

      irisTraceFile << message_class_type[type] << "," 
        << m_macsim_terminals[node_id]->ptx << "," << node_id << ",";
    }
    irisTraceFile << endl;

    //summary info
    irisTraceFile << ":Per Node Summary: node id, total packets out, total flits in, total flits out, cycles: ib, sa, vca, st, average buffer size, average packet latency, average flit latency,  " << endl;
    for(unsigned int i=0; i<m_iris_network->routers.size(); i++)
    {
      irisTraceFile << m_iris_network->routers[i]->print_csv_stats();
    }

    //detailed, router/packet state vs time info
    irisTraceFile << ":Detail info: clock cycle, req ID, mem state, msg type, current node, dst node, component (R=Router), input buffer port0.vc0, port0,vc1, port1,vc0, etc..." << endl;
    irisTraceFile << m_simBase->network_trace.str();
  }
  irisTraceFile.close();

  for (unsigned int ii = 0; ii < m_iris_network->routers.size(); ++ii) {
    //        m_iris_network->routers[i]->print_stats();
    m_iris_network->routers[ii]->power_stats();
  }
#ifdef POWER_EI
  cout << "Average Network power: " << avg_power << "W\n"
    << "Total Network Energy: " << total_energy << "J\n"
    << "Total packets " << total_packets << "\n";
#endif
#endif
}



int get_gcd(int a, int b)
{
  if (b == 0)
    return a;

  return get_gcd(b, a % b);
}

int get_lcm(int a, int b)
{
  return (a * b) / get_gcd(a, b);
}


// =======================================
//Initialization before simulation run
// =======================================
void macsim_c::initialize(int argc, char** argv) 
{
  g_mystdout = stdout;
  g_mystderr = stderr;
  g_mystatus = NULL;

  // initialize knobs
  init_knobs(argc, argv);

  // initialize stats
  m_coreStatsTemplate = new CoreStatistics(m_simBase);
  m_ProcessorStats = new ProcessorStatistics(m_simBase);

  m_allStats = new all_stats_c(m_ProcessorStats);
  m_allStats->initialize(m_ProcessorStats, m_coreStatsTemplate);

  // init_per_core_stats(*m_simBase->m_knobs->KNOB_NUM_SIM_CORES, m_simBase);
  init_per_core_stats(*m_simBase->m_knobs->KNOB_NUM_SIM_SMALL_CORES, m_simBase);
  // cout << "number of cores : " << *m_simBase->m_knobs->KNOB_NUM_SIM_CORES << "\n";
  cout << "number of cores : " << *m_simBase->m_knobs->KNOB_NUM_SIM_SMALL_CORES << "\n";

  // register wrapper functions
  register_functions();

  // initialize simulation
  init_sim();

#ifdef IRIS
  master_clock = new manifold::kernel::Clock(1); //clock has to be global or static
#endif

  // initialize cores
  //  init_cores(*KNOB(KNOB_NUM_SIM_CORES));
  init_cores(*KNOB(KNOB_NUM_SIM_SMALL_CORES));

  // init memory
  init_memory();

  // initialize some of my output streams to the standards */
  init_output_streams();


#ifdef IRIS
  REPORT("Initializing sim IRIS\n");
  manifold::kernel::Manifold::Init(0, NULL);

  // initialize interconnect network
  init_network();
#else
  if (*KNOB(KNOB_ENABLE_NEW_NOC)) {
    report("Initializing new noc\n");
    init_network();
  }
#endif

  // initialize clocks
  init_clock_domain();


  // open traces
  string trace_name_list = static_cast<string>(*KNOB(KNOB_TRACE_NAME_FILE));
  open_traces(trace_name_list);

  // any number other than 0, to pass the first simulation loop iteration
  m_num_running_core = 10000; 
}


// =======================================
// To maintain different clock frequency for CPU, GPU, NOC, L3, MC
// =======================================
void macsim_c::init_clock_domain(void)
{
  m_clock_internal = 0;
  float domain_f[5];
  domain_f[0] = *KNOB(KNOB_CLOCK_CPU);
  domain_f[1] = *KNOB(KNOB_CLOCK_GPU);
  domain_f[2] = *KNOB(KNOB_CLOCK_L3);
  domain_f[3] = *KNOB(KNOB_CLOCK_NOC);
  domain_f[4] = *KNOB(KNOB_CLOCK_MC);


  // allow only .x format
  for (int ii = 0; ii < 1; ++ii) {
    bool found = false;
    for (int jj = 0; jj < 5; ++jj) {
      int int_cast = static_cast<int>(domain_f[jj]);
      float float_cast = static_cast<float>(int_cast);
      if (domain_f[jj] != float_cast) {
        found = true;
        break;
      }
    }

    if (found) {
      for (int jj = 0; jj < 5; ++jj) {
        domain_f[jj] *= 10;
      }
    }
    else {
      break;
    }
  }

//  int domain_i[5];
  for (int ii = 0; ii < 5; ++ii) {
    m_domain_freq[ii] = static_cast<int>(domain_f[ii]);
    m_domain_count[ii] = 0;
    m_domain_next[ii] = 0;
  }


  m_clock_lcm = 100; /* 10 ns loop */

  report("Clock LCM           : " << m_clock_lcm);
  report("CPU clock frequency : " << *KNOB(KNOB_CLOCK_CPU) << " GHz");
  report("GPU clock frequency : " << *KNOB(KNOB_CLOCK_GPU) << " GHz");
  report("L3  clock frequency : " << *KNOB(KNOB_CLOCK_L3)  << " GHz");
  report("NOC clock frequency : " << *KNOB(KNOB_CLOCK_NOC) << " GHz");
  report("MC  clock frequency : " << *KNOB(KNOB_CLOCK_MC)  << " GHz");
}

#define CLOCK_CPU 0
#define CLOCK_GPU 1
#define CLOCK_L3  2
#define CLOCK_NOC 3
#define CLOCK_MC  4


#define GET_NEXT_CYCLE(domain) \
  ++m_domain_count[domain]; \
  m_domain_next[domain] = static_cast<int>(1.0*m_clock_lcm*m_domain_count[domain]/m_domain_freq[domain]);

// =======================================
// Single cycle step of simulation state : returns running status
// =======================================
int macsim_c::run_a_cycle() 
{
  // termination condition check
  // 1. no active threads in the system
  // 2. repetition has been done (in case of multiple applications simulation)
  // 3. no core has been executed in the last cycle;
  if (m_termination_check[0] && m_termination_check[1] && 
      (m_num_active_threads == 0 || m_repeat_done || m_num_running_core == 0))  {
    return 0; //simulation finished
  }

  if (m_termination_check[0] && m_termination_check[1]) {
    m_termination_check[0] = false;
    m_termination_check[1] = false;
    m_num_running_core = 0;
  }

  Counter pivot = m_core_cycle[0] + 1;


  // interconnection
  if (m_clock_internal == m_domain_next[CLOCK_NOC]) {
#ifdef IRIS
    manifold::kernel::Manifold::Run((double) m_simulation_cycle);       //IRIS
    manifold::kernel::Manifold::Run((double) m_simulation_cycle);       //IRIS for half tick?
#else
    m_network->run_a_cycle();
#endif
    GET_NEXT_CYCLE(CLOCK_NOC);
  }

  // run memory system
  if (m_clock_internal == m_domain_next[CLOCK_L3]) {
    m_memory->run_a_cycle();
    GET_NEXT_CYCLE(CLOCK_L3);
  }
  
  // run dram controllers
  if (m_clock_internal == m_domain_next[CLOCK_MC]) {
    for (int ii = 0; ii < m_num_mc; ++ii) {
      m_dram_controller[ii]->run_a_cycle();
    }
    GET_NEXT_CYCLE(CLOCK_MC);
  }


  // core execution loop
  // for (int kk = 0; kk < *KNOB(KNOB_NUM_SIM_CORES); ++kk) {
  for (int kk = 0; kk < *KNOB(KNOB_NUM_SIM_SMALL_CORES); ++kk) {
    // use pivot to randomize core run_cycle pattern 
    // unsigned int ii = (kk+pivot) % *m_simBase->m_knobs->KNOB_NUM_SIM_CORES;
    unsigned int ii = (kk+pivot) % *m_simBase->m_knobs->KNOB_NUM_SIM_SMALL_CORES;

    core_c *core = m_core_pointers[ii];
    string core_type = core->get_core_type();
    if (core_type == "ptx" && m_clock_internal != m_domain_next[CLOCK_GPU]) {
      continue;
    }
    else if (core_type != "ptx" && m_clock_internal != m_domain_next[CLOCK_CPU]) {
      continue;
    }

    // increment core cycle
    core->inc_core_cycle_count();
    m_core_cycle[ii]++;

    // core ended or not started    
    if (m_sim_end[ii] || !m_core_started[ii]) {
      continue;
    }

    // check whether all ops in this core have been completed.
    if (core->m_running_thread_num == 0 && (core->m_unique_scheduled_thread_num >= 1)) { 
      if (m_num_waiting_dispatched_threads == 0)  {
        m_sim_end[ii] = true;
      }
    }

    // active core : running a cycle and update stats
    if (!m_sim_end[ii])  {
      // run a cycle
      m_memory->run_a_cycle_core(ii);
      core->run_a_cycle();

      m_num_running_core++;
      STAT_CORE_EVENT(ii, CYC_COUNT);
      STAT_CORE_EVENT(ii, NUM_SAMPLES);
      STAT_CORE_EVENT_N(ii, NUM_ACTIVE_BLOCKS, core->m_running_block_num);
      STAT_CORE_EVENT_N(ii, NUM_ACTIVE_THREADS, core->m_running_thread_num);
    }

    // checking for threads 
    if (m_sim_end[ii] != true) {
      // when KNOB_MAX_INSTS is set, execute each thread for KNOB_MAX_INSTS instructions
      if (*m_simBase->m_knobs->KNOB_MAX_INSTS && 
          core->m_num_thread_reach_end == core->m_unique_scheduled_thread_num) {
        m_sim_end[ii] = true;
      }
      // when KNOB_SIM_CYCLE_COUNT is set, execute only KNOB_SIM_CYCLE_COUNT cycles
      else if (*KNOB(KNOB_SIM_CYCLE_COUNT) && m_simulation_cycle >= *KNOB(KNOB_SIM_CYCLE_COUNT)) {
        m_sim_end[ii] = true;
      }
    }

    if (!m_sim_end[ii]) { 
      // advance queues to prepare for the next cycle
      core->advance_queues();

      // check heartbeat
      core->check_heartbeat(false);

      // forward progress check in every 10000 cycles
      if (!(m_core_cycle[ii] % 10000))  
        core->check_forward_progress();
    } 

    // when a core has been completed, do last print heartbeat 
    if (m_sim_end[ii] || m_core_end_trace[ii]) 
      core->check_heartbeat(true);
  }


  // increase simulation cycle
  m_simulation_cycle++;
  STAT_EVENT(CYC_COUNT_TOT);
    
  if (m_clock_internal == m_domain_next[CLOCK_CPU]) {
    STAT_EVENT(CYCLE_CPU);
    m_termination_check[0] = true;
    GET_NEXT_CYCLE(CLOCK_CPU);
  }

  if (m_clock_internal == m_domain_next[CLOCK_GPU]) {
    STAT_EVENT(CYCLE_GPU);
    m_termination_check[1] = true;
    GET_NEXT_CYCLE(CLOCK_GPU);

    // increase sampling unit retired cycles
    if (!m_simBase->doneStablePhase) {      
      m_unit_cycles_retired ++;
      acc_cycles ++;
      m_prof_cycles_retired ++;
    }else{      
      m_unit_cycles_retired = 0;
    }

    m_tot_block_num += running_block_num;
    m_tot_active_threads += running_thread_num;
  }

  if (++m_clock_internal == m_clock_lcm) {
    m_clock_internal = 0;
    for (int ii = 0; ii < 5; ++ii) {
      m_domain_count[ii] = 0;
      m_domain_next[ii]  = 0;
    }
  }

  return 1; //simulation not finished
}


// =======================================
// Simulation end cleanup
// =======================================
void macsim_c::finalize()
{
  // deallocate memory
  deallocate_memory();

  // finalize simulation
  fini_sim();

  // dump out stat files at the end of simulation
  m_ProcessorStats->saveStats();  

  // tommy24 - Reset Unit counting  
  double interval_CPI, kernel_CPI, l1_rate, l2_rate, kernel_l1_rate, kernel_l2_rate, diverge_ratio, kernel_diverge_ratio;
    
  m_kernel_cycles_retired += m_unit_cycles_retired;
  m_kernel_insts_retired += m_unit_insts_retired;
  m_kernel_warp_insts_retired += m_unit_warp_insts_retired;    
  m_kernel_l1_access += m_prof_l1_access;
  m_kernel_l2_access += m_prof_l2_access;
  m_kernel_l1_misses += m_prof_l1_misses;
  m_kernel_l2_misses += m_prof_l2_misses;
  m_kernel_mem_insts += m_prof_mem_insts;
  m_kernel_mem_uops += m_prof_mem_uops;
          

  interval_CPI = (double)m_simBase->m_unit_cycles_retired / (double)m_simBase->m_unit_insts_retired;
  kernel_CPI = (double)m_simBase->m_kernel_cycles_retired / (double)m_simBase->m_kernel_insts_retired; 

  l1_rate = (double)m_simBase->m_prof_l1_misses / (double)m_simBase->m_prof_l1_access;
  l2_rate = (double)m_simBase->m_prof_l2_misses / (double)m_simBase->m_prof_l2_access;
  diverge_ratio = (double)m_simBase->m_prof_mem_uops / (double)m_simBase->m_prof_mem_insts;

  kernel_l1_rate = (double)m_simBase->m_kernel_l1_misses / (double)m_simBase->m_kernel_l1_access;
  kernel_l2_rate = (double)m_simBase->m_kernel_l2_misses / (double)m_simBase->m_kernel_l2_access;
  kernel_diverge_ratio = (double)m_simBase->m_kernel_mem_uops / (double)m_simBase->m_kernel_mem_insts;

  if (*KNOB(KNOB_IDEAL_BBV_ENABLED) != 0) {
    m_simBase->GenIdealBBVs(true, &m_simBase->ideal_bbv_profile);
    double acc_thread_IPC = (float)m_simBase->acc_insts / (float)m_simBase->acc_cycles;
    double acc_IPC = (float)m_simBase->acc_warp_insts / (float)m_simBase->acc_cycles;
    double total_IPC = (float)m_simBase->m_prof_insts_retired / (float)m_simBase->m_prof_cycles_retired;

    m_simBase->ideal_bbv_profile << "Kernel_IPC_W " << acc_IPC << " " << acc_thread_IPC << " " << m_simBase->acc_warp_insts
                                 << " " << m_simBase->acc_cycles << endl;
    m_simBase->ideal_bbv_profile << "Total_IPC_W " << total_IPC << " " << m_prof_insts_retired << " " << m_simBase->m_prof_cycles_retired << endl;                                     

    m_simBase->ideal_bbv_profile << "Kernel_Diverge_Ratio: " << kernel_diverge_ratio 
                                 << " L1 rate: " << kernel_l1_rate << " accesses: " <<  m_simBase->m_kernel_l1_access 
                                 << " L2 rate: " << kernel_l2_rate << " accesses: " <<  m_simBase->m_kernel_l2_access 
                                 << endl;
    m_simBase->ideal_bbv_profile << "Kernel_Sample_Ratio "
                                 << 1 - (float)m_simBase->skipped_insts / (float)m_simBase->acc_warp_insts
                                 << " " << m_simBase->acc_warp_insts
                                 << " " << m_simBase->acc_warp_insts - m_simBase->skipped_insts << endl;          
  }

  if (*KNOB(KNOB_SIMPOINT_BBV_ENABLED) != 0) {
    m_simBase->GenIdealBBVs(true, &m_simBase->simpoint_bbv_profile);
    double acc_thread_IPC = (float)m_simBase->acc_insts / (float)m_simBase->acc_cycles;
    double acc_IPC = (float)m_simBase->acc_warp_insts / (float)m_simBase->acc_cycles;
    double total_IPC = (float)m_simBase->m_prof_insts_retired / (float)m_simBase->m_prof_cycles_retired;

    m_simBase->simpoint_bbv_profile << "Kernel_IPC_W " << acc_IPC << " " << acc_thread_IPC << " " << m_simBase->acc_warp_insts
                                 << " " << m_simBase->acc_cycles << endl;
    m_simBase->simpoint_bbv_profile << "Total_IPC_W " << total_IPC << " " << m_prof_insts_retired << " " << m_simBase->m_prof_cycles_retired << endl;                                     

    m_simBase->simpoint_bbv_profile << "Kernel_Diverge_Ratio: " << kernel_diverge_ratio 
                                 << " L1 rate: " << kernel_l1_rate << " accesses: " <<  m_simBase->m_kernel_l1_access 
                                 << " L2 rate: " << kernel_l2_rate << " accesses: " <<  m_simBase->m_kernel_l2_access 
                                 << endl;
    m_simBase->simpoint_bbv_profile << "Kernel_Sample_Ratio "
                                 << 1 - (float)m_simBase->skipped_insts / (float)m_simBase->acc_warp_insts
                                 << " " << m_simBase->acc_warp_insts
                                 << " " << m_simBase->acc_warp_insts - m_simBase->skipped_insts << endl;          
  }

  if (*KNOB(KNOB_MODEL_BBV_ENABLED) != 0) {
    m_simBase->GenIdealBBVs(true, &m_simBase->model_bbv_profile);
    double acc_thread_IPC = (float)m_simBase->acc_insts / (float)m_simBase->acc_cycles;
    double acc_IPC = (float)m_simBase->acc_warp_insts / (float)m_simBase->acc_cycles;
    double total_IPC = (float)m_simBase->m_prof_insts_retired / (float)m_simBase->m_prof_cycles_retired;

    m_simBase->model_bbv_profile << "Kernel_IPC_W " << acc_IPC << " " << acc_thread_IPC << " " << m_simBase->acc_warp_insts
                                 << " " << m_simBase->acc_cycles << endl;
    m_simBase->model_bbv_profile << "Total_IPC_W " << total_IPC << " " << m_prof_insts_retired << " " << m_simBase->m_prof_cycles_retired << endl;                                     

    m_simBase->model_bbv_profile << "Kernel_Diverge_Ratio: " << kernel_diverge_ratio 
                                 << " L1 rate: " << kernel_l1_rate << " accesses: " <<  m_simBase->m_kernel_l1_access 
                                 << " L2 rate: " << kernel_l2_rate << " accesses: " <<  m_simBase->m_kernel_l2_access 
                                 << endl;
    m_simBase->model_bbv_profile << "Kernel_Sample_Ratio "
                                 << 1 - (float)m_simBase->skipped_insts / (float)m_simBase->acc_warp_insts
                                 << " " << m_simBase->acc_warp_insts
                                 << " " << m_simBase->acc_warp_insts - m_simBase->skipped_insts << endl;          
  }

  if (*KNOB(KNOB_INTRA_SAMPLING_ENABLED) != 0) {
    m_simBase->GenIdealBBVs(true, &m_simBase->sampling_bbv_profile);
    double acc_thread_IPC = (float)m_simBase->acc_insts / (float)m_simBase->acc_cycles;
    double acc_CPI = (float)m_simBase->acc_cycles / (float)m_simBase->acc_warp_insts;
    double total_IPC = (float)m_simBase->m_prof_insts_retired / (float)m_simBase->m_prof_cycles_retired;

    int kernelPhase = m_simBase->kernelPhaseID[m_simBase->curKernelPhaseKernel];
    int phaseId = m_simBase->kernelPhaseID[m_simBase->curKernelPhaseKernel];
    double PhaseWeight = ((double)m_simBase->kernelPhaseWeight[phaseId] / (double)m_simBase->totalKernelPhaseWeight);
        
    m_simBase->curKernelPhaseWeight = PhaseWeight;
    m_simBase->curKernelPhaseID = phaseId;
    m_simBase->totalKernelPhaseCPI += PhaseWeight * acc_CPI;
    m_simBase->totalKernelPhaseL1Accesses += PhaseWeight * m_simBase->acc_l1_accesses;
    m_simBase->totalKernelPhaseL1Misses += PhaseWeight * m_simBase->acc_l1_misses;
    m_simBase->totalKernelPhaseL2Accesses += PhaseWeight * m_simBase->acc_l2_accesses;
    m_simBase->totalKernelPhaseL2Misses += PhaseWeight * m_simBase->acc_l2_misses;
    
    m_simBase->sampling_bbv_profile << "KernelPhaseIPC " << acc_CPI << " " << PhaseWeight << endl;
    
    m_simBase->sampling_bbv_profile << "Kernel_IPC_W " << (1.0 / acc_CPI) << " " << acc_thread_IPC << " " << m_simBase->acc_warp_insts
                                    << " " << m_simBase->acc_cycles << endl;
    m_simBase->sampling_bbv_profile << "Total_IPC_W " << total_IPC << " " << m_prof_insts_retired << " " << m_simBase->m_prof_cycles_retired << endl;

    m_simBase->sampling_bbv_profile << "Kernel_L1_Rate " << m_simBase->acc_l1_misses/m_simBase->acc_l1_accesses << " " << m_simBase->acc_l1_accesses << endl;
    m_simBase->sampling_bbv_profile << "Kernel_L2_Rate " << m_simBase->acc_l2_misses/m_simBase->acc_l2_accesses << " " << m_simBase->acc_l2_accesses << endl;        

    m_simBase->sampling_bbv_profile << "Kernel_Diverge_Ratio: " << kernel_diverge_ratio 
                                    << " L1 rate: " << kernel_l1_rate << " accesses: " <<  m_simBase->m_kernel_l1_access 
                                    << " L2 rate: " << kernel_l2_rate << " accesses: " <<  m_simBase->m_kernel_l2_access 
                                    << endl;
    m_simBase->sampling_bbv_profile << "Kernel_Sample_Ratio "
                                    << 1 - (float)m_simBase->skipped_insts / (float)m_simBase->acc_warp_insts
                                    << " " << m_simBase->acc_warp_insts
                                    << " " << m_simBase->acc_warp_insts - m_simBase->skipped_insts << endl;          

    m_simBase->sampling_bbv_profile << "Kernel_Weight " << m_simBase->curKernelPhaseWeight
                                    << " "
                                    << m_simBase->curKernelPhaseID
                                    << endl;
    
    m_simBase->sampling_bbv_profile << "WeightedKernelIPC "
                                    << (1.0 / m_simBase->totalKernelPhaseCPI) << endl;

    m_simBase->sampling_bbv_profile << "WeightedL1MissRate "
                                    << (float)m_simBase->totalKernelPhaseL1Misses/ (float)m_simBase->totalKernelPhaseL1Accesses << endl;

    m_simBase->sampling_bbv_profile << "WeightedL2MissRate "
                                    << (float)m_simBase->totalKernelPhaseL2Misses/ (float)m_simBase->totalKernelPhaseL2Accesses << endl;

    m_simBase->sampling_bbv_profile << "SimulatedInsts "
                                    << m_simBase->totalKernelInsts << endl;
  }

  if (*KNOB(KNOB_PC_BB_ENABLED) != 0) {
    string macperf_path = string(getenv("MACPERF_ROOT"));  
    string pc_bb_path;
    
    pc_bb_path = macperf_path + "/inputs/input_bbvs/PCBBMAP/" + m_simBase->bench_name.c_str() + ".tb";    
    m_simBase->pc_bb_profile.open(pc_bb_path, ofstream::out);
    
    for (map<unsigned int, unsigned int>::iterator it = m_simBase->AllPCBBMap.begin(); it != m_simBase->AllPCBBMap.end(); it ++) {
      m_simBase->pc_bb_profile << it->first << " " << it->second << endl;
    }

    m_simBase->pc_bb_profile.close();
    m_simBase->curBB.clear();
  }

  BB_counter.clear();
  MEM_counter.clear();
  MEMFOOT_counter.mem_foots.clear();      
    
  m_prof_l1_misses = 0;
  m_prof_l1_access = 0;
  m_prof_l2_misses = 0;
  m_prof_l2_access = 0;
  m_prof_mem_insts = 0;
  m_prof_mem_uops = 0;
  m_unit_cycles_retired = 0;
  m_unit_insts_retired = 0;
  m_unit_warp_insts_retired = 0;
  m_kernel_cycles_retired = 0;
  m_kernel_insts_retired = 0;
  m_kernel_warp_insts_retired = 0;
  m_kernel_l1_misses = 0;
  m_kernel_l1_access = 0;
  m_kernel_l2_misses = 0;
  m_kernel_l2_access = 0;
  m_kernel_mem_insts = 0;
  m_kernel_mem_uops = 0;
  m_tb_retired_counts = 0;
  skipped_insts = 0;
  //////////////////////////////////////////////

  if (*KNOB(KNOB_TB_BBV_ENABLED) != 0) {
    m_simBase->tb_bbv_profile.close();
  }
  
  if (*KNOB(KNOB_IDEAL_BBV_ENABLED) != 0) {
    m_simBase->ideal_bbv_profile.close();
  }

  if (*KNOB(KNOB_SAMPLING_ENABLED) != 0) {    
    WriteSamplingResults();
  }

  if (*KNOB(KNOB_MEM_PROF_ENABLED) != 0) {
    m_simBase->mem_profile.close();
  }
  cout << "Done\n";
}

// =======================================
// Get PC to basic block id mapping
// =======================================
void macsim_c::GetSampleUnitSize(int max_tbs)
{
  int tbsize = GetTBSize();

  if (tbsize == 0) {
    cout << "No TB info!" << endl;
    exit(0);
  }
  
  if (*KNOB(KNOB_SAMPLE_UNIT_SIZE) == 0)
    sample_unit_size = tbsize * max_tbs * (*KNOB(KNOB_NUM_SIM_SMALL_CORES)) * (*KNOB(KNOB_TB_SIZE_RATIO));
  else
    sample_unit_size = *KNOB(KNOB_SAMPLE_UNIT_SIZE);
  
  if (*KNOB(KNOB_MODEL_BBV_ENABLED) != 0 || *KNOB(KNOB_INTRA_SAMPLING_ENABLED) != 0)
    sample_unit_size = 0;
  
  // cout << "Sampling Unit Size " << sample_unit_size << " occupancy " << max_tbs * (*KNOB(KNOB_NUM_SIM_SMALL_CORES)) << " tbsize " << tbsize
  //      << " ratio " << *KNOB(KNOB_TB_SIZE_RATIO) << endl;
}

// =======================================
// Get TB Size
// =======================================
int macsim_c::GetTBSize()
{
  ifstream tbsize_file;
  
  string macperf_path = string(getenv("MACPERF_ROOT"));
  string tbsize_path = macperf_path + "/inputs/input_bbvs/TBSIZE";
  string bench_name;
  int tbsize;
  unsigned int totalTbSize;
  float maxRatio, minRatio;

  // open traces
  ifstream trace_file;
  char trace_path[1024];  
  string trace_name_list = static_cast<string>(*KNOB(KNOB_TRACE_NAME_FILE));
  trace_file.open(trace_name_list, ifstream::in);
  if (!trace_file.good())
    ASSERTM(0, "error reading from trace_file_list: %s\n", tbsize_path.c_str());
  trace_file.getline(trace_path, 1024);
  trace_file.getline(trace_path, 1024);  

  string bench_path = string(trace_path);
  
  tbsize_file.open(tbsize_path, ifstream::in);
  
  if (!tbsize_file.good())
    ASSERTM(0, "error reading from TBSIZE file: %s\n", tbsize_path.c_str());

  while(!tbsize_file.eof()) {
    tbsize_file >> bench_name >> tbsize;    
    if (bench_path.find(bench_name) != std::string::npos) {      
      cout << bench_name << " $ " << tbsize << endl;      
      tbsize_file.close();    
      return tbsize;
    }
  }    
}

// =======================================
// Get PC to basic block id mapping
// =======================================
void macsim_c::GetPCBBMapping(string kernelName)
{
#if 1
  ifstream bb_file;
  unsigned int pc, bb_id;  
  
  string macperf_path = string(getenv("MACPERF_ROOT"));
  string pcmap_path = macperf_path + "/inputs/input_bbvs/PCBBMAP/";

  DIR *dp = opendir(pcmap_path.c_str());
  struct dirent *dirp;
  struct stat filestat;
  string filepath;
  bool foundFile = false;

  // while ((dirp = readdir( dp ))) {
  //   string fileName = string(dirp->d_name);
  //   filepath = pcmap_path + "/" + fileName;

  //   if (std::string::npos == fileName.find("tb")) continue;

  //   if (std::string::npos == kernelName.find(kernelName)) continue;
        
  //   foundFile = true;
  //   break;
  // }

  // if (!foundFile)
  //   assert(0);

  filepath = pcmap_path + kernelName + ".tb";
  bb_file.open(filepath, ifstream::in);
  if (!bb_file.good())
    ASSERTM(0, "error reading from pcmap: %s\n", filepath.c_str());

  cout << filepath << endl;
  while(!bb_file.eof()) {
    bb_file >> pc >> bb_id;
    // cout << "PC " << pc << " " << bb_id << endl;
    m_simBase->PC_to_BB_map[kernelName][pc] = bb_id;
  }
  bb_file.close();
  
#else
  ifstream bb_file;
  unsigned int pc, bb_id;
  string kernel_name;  
  
  string macperf_path = string(getenv("MACPERF_ROOT"));
  string pcmap_path = macperf_path + "/misc/ocelot-pcmap";

  DIR *dp = opendir(pcmap_path.c_str());
  struct dirent *dirp;
  struct stat filestat;
  string filepath;
  bool foundFile = false;

  while ((dirp = readdir( dp ))) {
    string fileName = string(dirp->d_name);
    filepath = pcmap_path + "/" + fileName;

    if (std::string::npos == fileName.find("pcmap")) continue;

    int lastIndex = fileName.find_last_of(".");
    string exactKernelName = fileName.substr(0, lastIndex);

        
    if (std::string::npos == kernelName.find(exactKernelName)) continue;
    
    cout << "exact " << exactKernelName << endl;
    foundFile = true;
    break;
  }

  if (!foundFile)
    assert(0);
  
  bb_file.open(filepath, ifstream::in);
  if (!bb_file.good())
    ASSERTM(0, "error reading from pcmap: %s\n", filepath.c_str());
  
  while(!bb_file.eof()) {
    bb_file >> pc >> bb_id >> kernel_name;
    // cout << "PC " << pc << " " << bb_id << " " << kernel_name << endl;
    m_simBase->PC_to_BB_map[kernel_name][pc] = bb_id;
  }
  bb_file.close();
#endif
}

// =======================================
// tommy24 - Generate Ideal BBV files
// =======================================
void macsim_c::GenIdealBBVs(bool endPhase, ofstream *fs)
{  
  int total_insts = 0;
  int total_reqs = 0;
  unsigned int total_foots = MEMFOOT_counter.mem_foots.size();
  int total_tbs = m_tb_retired_counts;

  ofstream *cur_profile = fs;

  if (m_unit_cycles_retired == 0)
    return;

  for (map<unsigned int, bb_info>::iterator it = MEM_counter.begin(); it != MEM_counter.end(); it ++)     
    total_reqs += it->second.mem_reqs;  

  // thread instruction counts
  if (endPhase)
    *cur_profile << "end_T";
  else
    *cur_profile << "T";
  
  for (map<unsigned int, bb_info>::iterator it = BB_counter.begin(); it != BB_counter.end(); it ++) {
    *cur_profile << ":" << it->first + 1 << ":" << it->second.insts;
    *cur_profile << " ";
    total_insts += it->second.insts;
  }

  // Insts in TB
  *cur_profile << ":I:" << total_insts;
  *cur_profile << " ";
  // Reqs in TB
  *cur_profile << ":M:" << total_reqs;
  *cur_profile << " ";
  // TBs in TB
  // *cur_profile << ":B:" << total_tbs;
  // *cur_profile << " ";
  // Footprints in TB
  *cur_profile << ":P:" << total_foots;
  *cur_profile << " ";
  // Active threads
  *cur_profile << ":A:" << m_tot_active_threads / m_unit_cycles_retired;
  *cur_profile << " ";
  // Active blocks
  *cur_profile << ":B:" << m_tot_block_num / m_unit_cycles_retired;
  *cur_profile << " ";
  // Ratio in mem
  *cur_profile << ":F:" << (float)total_reqs / (float)total_insts;
  *cur_profile << " ";
  *cur_profile << endl;

  acc_insts += total_insts;
  // acc_cycles += m_unit_cycles_retired;
  // m_prof_cycles_retired += m_unit_cycles_retired;

  double IPC = (float)total_insts / (float)m_unit_cycles_retired;
  double diff = (max_ipc - min_ipc) / min_ipc;
  double acc_IPC = (float)acc_insts / (float)acc_cycles;    

  if (endPhase)
    *cur_profile << "end_IPC_T: " << IPC << " " << acc_IPC << " " << abs(IPC - acc_IPC) / acc_IPC << endl; 
  else
    *cur_profile << "IPC_T: "<< IPC << " " << acc_IPC << " " << abs(IPC - acc_IPC) / acc_IPC << endl; 

  total_insts = 0; 

  // warp instruction counts
  if (endPhase)
    *cur_profile << "end_W";
  else
    *cur_profile << "W";
  
  for (map<unsigned int, bb_info>::iterator it = BB_counter.begin(); it != BB_counter.end(); it ++) {
    *cur_profile << ":" << it->first + 1 << ":" << it->second.warp_insts;
    *cur_profile << " ";
    total_insts += it->second.warp_insts;
  }
  
  // Insts in TB
  *cur_profile << ":I:" << total_insts;
  *cur_profile << " ";
  // Reqs in TB
  *cur_profile << ":M:" << total_reqs;
  *cur_profile << " ";
  // TBs in TB
  // *cur_profile << ":B:" << total_tbs;
  // *cur_profile << " ";
  // Footprints in TB
  *cur_profile << ":P:" << total_foots;
  *cur_profile << " ";
  // Active threads
  *cur_profile << ":A:" << m_tot_active_threads / m_unit_cycles_retired;
  *cur_profile << " ";
  // Active blocks
  *cur_profile << ":B:" << m_tot_block_num / m_unit_cycles_retired;
  *cur_profile << " ";
  // Ratio in mem
  *cur_profile << ":F:" << (float)total_reqs / (float)total_insts;
  *cur_profile << " ";
  *cur_profile << endl;  
  
  // acc_warp_insts += total_insts;
  // m_prof_insts_retired += total_insts;

  // IPC calculation
  IPC = (float)total_insts / (float)m_unit_cycles_retired;
  diff = (max_ipc - min_ipc) / min_ipc;
  acc_IPC = (float)acc_warp_insts / (float)acc_cycles;

  if (endPhase)
    *cur_profile << "end_IPC_W: " << IPC << " " << acc_IPC << " " << acc_warp_insts << endl; 
  else
    *cur_profile << "IPC_W: " << IPC << " " << acc_IPC << " " << acc_warp_insts << endl; 

  // cout << "IPC_W: " << IPC << " " << acc_IPC << " " << abs(IPC - acc_IPC) / acc_IPC << endl; 
  *cur_profile << "mem_ratio: " << (float)total_reqs / (float)total_insts << " " << total_reqs << endl;

  *cur_profile << "active_warps: " << (float)active_warps_count / (float)active_cycles_count << endl;
  active_warps_count = 0;
  active_cycles_count = 0;

  double cur_AMAT = AMAT / AMAT_base;
  if (endPhase)
    *cur_profile << "end_AMAT: " << cur_AMAT << endl;
  else
    *cur_profile << "AMAT: " << cur_AMAT << endl;
  
  sample_count ++;
  if (IPC > max_ipc)
    max_ipc = IPC;

  if (IPC < min_ipc)
    min_ipc = IPC;

  if (*KNOB(KNOB_INTRA_SAMPLING_ENABLED) != 0 && m_simBase->isStablePhase) {
    m_simBase->epoch_count ++;
    m_simBase->epochIPC.push_back(IPC);

    double diff = abs(IPC - prevIPC) / prevIPC;
    //if (m_simBase->epoch_count == 1) {
    if (diff < 0.1) {              
      m_simBase->StablePhaseIPC = IPC;
      m_simBase->StableL1MissRate = (double)m_simBase->m_prof_l1_misses / (double)m_simBase->m_prof_l1_access;
      m_simBase->StableL2MissRate = (double)m_simBase->m_prof_l2_misses / (double)m_simBase->m_prof_l2_access;
      m_simBase->StableL1L2Rate = (double)m_simBase->m_prof_l2_access / (double)m_simBase->m_prof_l1_access;
      // cout << "l1l2 " << m_simBase->StableL1L2Rate << " " << m_simBase->StableL2MissRate << endl;
      m_simBase->doneStablePhase = true;
      // predicted_ipc += IPC * 
      // cout << "Done Stable Phase " << m_simBase->curPhaseId << endl;
      m_simBase->sampling_bbv_profile << "Done Stable Phase " << m_simBase->curPhaseId << endl;
      m_simBase->sampling_bbv_profile << "epochIPC ";
      for (int i = 0; i < m_simBase->epochIPC.size(); i ++)
        m_simBase->sampling_bbv_profile << m_simBase->epochIPC[i] << " ";
      m_simBase->sampling_bbv_profile << endl;
      m_simBase->epochIPC.clear();      
    } else if (prevIPC != 0) {
      m_simBase->sampling_bbv_profile << "High IPC diff " << diff << endl;
    }

    prevIPC = IPC;    
  }

  // printf("max_ipc %f min_ipc %f diff %f\n", max_ipc, min_ipc, (max_ipc - min_ipc) / min_ipc);
  
  // memory requests
  if (endPhase)
    *cur_profile << "end_D";
  else
    *cur_profile << "D";
  for (map<unsigned int, bb_info>::iterator it = MEM_counter.begin(); it != MEM_counter.end(); it ++) {
    *cur_profile << ":" << it->first + 1 << ":" << it->second.mem_reqs;
    *cur_profile << " ";    
  }
  *cur_profile << endl;
  
  cur_profile->flush();  
}

void macsim_c::GetProfiling()
{
  int unitSize;  
  string macperf_path = string(getenv("MACPERF_ROOT"));    
  string profiling_path;
  string line;  

  profiling_path = macperf_path + "/inputs/input_bbvs/mem_profiles/model/" + m_simBase->bench_name.c_str() + ".mem";

  ifstream profilingFile;
  profilingFile.open(profiling_path.c_str(), ifstream::in);
  if (!profilingFile.good())
    ASSERTM(0, "error reading from profiling: %s\n", profiling_path.c_str());

  while(std::getline(profilingFile, line)) {
    istringstream iss(line);        
    
    iss >> warp_mem_ratio;
    iss >> warp_mem_reqs;
    iss >> warp_insts;    
    break;
  } 
  
  warp_mem_reqs /= m_no_threads_per_block;
  warp_insts /= m_no_threads_per_block;
  profilingFile.close();
  
  cout << "warp_mem_ratio " << warp_mem_ratio << " " << warp_mem_reqs << " " << warp_insts << endl;
}

void macsim_c::GetDynamic()
{
  
}

void macsim_c::MarkovModel(double p, int M, int threads)
{
  string macperf_path = string(getenv("MACPERF_ROOT"));
  string markov_script = macperf_path + "/scripts/MarkovChain.py";

  char args[256];
  snprintf(args, 256, "-p %g -M %d -threads %d", p, M, threads);

  char cmd[1025];
  sprintf(cmd, "%s %s", markov_script.c_str(), args);
  // string cmd = " -p " + p + " -M " + M + " -threads " + threads;
  // markov_script += cmd;
  int i = system(cmd);  
}

void macsim_c::MathModel()
{
  warp_mem_reqs /= (32 * monitored_count);
  warp_mem_insts /= (32 * monitored_count);  
  warp_insts /= (32 * monitored_count);
  // warp_insts = 840 / 4;
  // warp_mem_insts = 191 / 4;
  // warp_mem_reqs = warp_mem_insts;
  double cur_AMAT = AMAT / AMAT_base;  
  int N = *m_simBase->m_knobs->KNOB_MAX_THREADS_PER_CORE;
  int miss_reqs = ((float)m_prof_l1_misses / (float)m_prof_l1_access) * warp_mem_reqs;
  int S = miss_reqs * cur_AMAT;
  int R = warp_insts;
  int K = S / R;
  
  double E_m = ((float)(N - 1) * (1 + R)) / (2 * (1+K));
  double sigma = (N - 1) * ((4 * K + 1) * pow(R, 2) + 6 * K * R + (2 * K - 1)) / (12 * pow((1 + K), 2));

  double IPC = N * R / (R + K * R + E_m);
  sigma = sqrt(sigma);

  double p = (float)miss_reqs / (float)warp_insts;
  // double p = (float)warp_mem_insts / (float)warp_insts;
  MarkovModel(p, cur_AMAT, N);
  
  // printf("S %d R %d K %d N %d AMAT %f base %d ratio %f\n", S, R, K, N, cur_AMAT, AMAT_base, warp_mem_ratio);
  printf("K %d N %d AMAT %f base %d\n", K, N, cur_AMAT, AMAT_base);
  printf("MEM_INSTS %d REQ %d INSTS %d ratio %f\n", warp_mem_insts, warp_mem_reqs, R, (float)miss_reqs / (float)R);
  printf("ipc %f sigma %f\n", IPC, sigma);
  
  warp_mem_insts = 0;
  warp_insts = 0;
}

void macsim_c::GenTBBBVs(int block_id)
{
  unsigned int total_cycles = 0;
  unsigned int total_insts = 0;
  unsigned int total_reqs = 0;
  unsigned int total_foots = tblocks[block_id].memfoot_info.mem_foots.size();

  for(std::map<int, bb_info>::iterator it = tblocks[block_id].diverge_info.begin(); it != tblocks[block_id].diverge_info.end(); it ++) {
    total_insts += it->second.insts;    
  }
    
  for(std::map<int, bb_info>::iterator it = tblocks[block_id].diverge_info.begin(); it != tblocks[block_id].diverge_info.end(); it ++) {
  
    total_reqs += it->second.mem_reqs;
  }

  tb_bbv_profile << "T";
  for(std::map<int, bb_info>::iterator it = tblocks[block_id].diverge_info.begin(); it != tblocks[block_id].diverge_info.end(); it ++) {
    tb_bbv_profile << ":" << it->first + 1 << ":" << it->second.insts;
    tb_bbv_profile << " ";
  }  
  // Insts in TB
  tb_bbv_profile << ":I:" << total_insts;
  tb_bbv_profile << " ";
  // Reqs in TB
  tb_bbv_profile << ":M:" << total_reqs;
  tb_bbv_profile << " ";
  // Foots in TB
  tb_bbv_profile << ":P:" << total_foots;
  tb_bbv_profile << " ";

  tb_bbv_profile << ":ID:" << block_id;
  tb_bbv_profile << " ";  
  tb_bbv_profile << endl;

  tb_bbv_profile << "W";
  for(std::map<int, bb_info>::iterator it = tblocks[block_id].diverge_info.begin(); it != tblocks[block_id].diverge_info.end(); it ++) {
    tb_bbv_profile << ":" << it->first + 1 << ":" << it->second.warp_insts;
    tb_bbv_profile << " ";    
  }
  // Insts in TB
  tb_bbv_profile << ":I:" << total_insts;
  tb_bbv_profile << " ";
  // Reqs in TB
  tb_bbv_profile << ":M:" << total_reqs;
  tb_bbv_profile << " ";
  // Foots in TB
  tb_bbv_profile << ":P:" << total_foots;
  tb_bbv_profile << " ";

  tb_bbv_profile << ":ID:" << block_id;
  tb_bbv_profile << " ";  
  tb_bbv_profile << endl;

  tb_bbv_profile << "D";
  for(std::map<int, bb_info>::iterator it = tblocks[block_id].diverge_info.begin(); it != tblocks[block_id].diverge_info.end(); it ++) {
    if (it->second.mem_insts > 0)
      tb_bbv_profile << ":" << it->first + 1 << ":" << (double)it->second.mem_reqs;
    else
      tb_bbv_profile << ":" << it->first + 1 << ":" << 0;
    tb_bbv_profile << " ";    
  }
  // Insts in TB
  tb_bbv_profile << ":I:" << total_insts;
  tb_bbv_profile << " ";
  // Reqs in TB
  tb_bbv_profile << ":M:" << total_reqs;
  tb_bbv_profile << " ";
  // Foots in TB
  tb_bbv_profile << ":P:" << total_foots;
  tb_bbv_profile << " ";

  tb_bbv_profile << ":ID:" << block_id;
  tb_bbv_profile << " ";  
  tb_bbv_profile << endl;
    
  tb_bbv_profile.flush();
}

void macsim_c::GetIntraWeight()
{
  string macperf_path = string(getenv("MACPERF_ROOT"));
  string expName = *m_simBase->m_knobs->KNOB_EXP;
  string metricName = "intra";
  string hwName = *m_simBase->m_knobs->KNOB_HWCONFIG;            
  string weight_path;
  string line;

  weight_path = macperf_path + "/inputs/input_bbvs/weights/" + m_simBase->bench_name.c_str() + "@" + metricName + ".weights";

  ifstream weightFile;
  weightFile.open(weight_path.c_str(), ifstream::in);
  while(std::getline(weightFile, line)) {
    if (line != "") {
      istringstream iss(line);
      int tbInsts, memReqs;
      iss >> tbInsts;
      iss >> memReqs;
      tbweight.push_back(tbInsts);
      memweight.push_back(memReqs);
    }
  }
  weightFile.close();

  cout << "IntraWeight for " << metricName << " is loaded!" << endl;
}
void macsim_c::GetInterPoint()
{
  string macperf_path = string(getenv("MACPERF_ROOT"));
  string expName = *m_simBase->m_knobs->KNOB_EXP;
  string metricName = "inter";
  string hwName = *m_simBase->m_knobs->KNOB_HWCONFIG;            
  string point_path;
  string line;

  point_path = macperf_path + "/inputs/input_bbvs/interpoints/" + m_simBase->bench_name.c_str() + "@" + metricName + ".tp";

  ifstream pointFile;
  pointFile.open(point_path.c_str(), ifstream::in);
  if (!pointFile.good())
    ASSERTM(0, "error reading from InterPoint: %s\n", point_path.c_str());

  int phaseType, unitID, kernelSize;
  int idx = 0;
  while(std::getline(pointFile, line)) {
    istringstream iss(line);            
      
    iss >> phaseType;
    iss >> unitID;
    iss >> kernelSize;

    if (*KNOB(KNOB_INTER_SAMPLING_ENABLED) == 0)
      phaseType = idx;

    kernelPhaseDone[phaseType] = false;
    kernelPhaseID[idx] = phaseType;
    cout << "Phase " << phaseType << " unitId " << unitID << endl;

    if (kernelPhaseWeight.find(phaseType) == kernelPhaseWeight.end())
      kernelPhaseWeight[phaseType] = 0;
    
    kernelPhaseWeight[phaseType] += kernelSize;
    totalKernelPhaseWeight += kernelSize;
    // kernelPhaseWeight[phaseType] ++;
    // totalKernelPhaseWeight ++;    
    idx ++;
  }

  pointFile.close();
  cout << "InterPoint for " << metricName << " is loaded!" << endl;
}

void macsim_c::GetIntraPoint()
{
  string macperf_path = string(getenv("MACPERF_ROOT"));
  string expName = *m_simBase->m_knobs->KNOB_EXP;
  string metricName = "intra";
  string hwName = *m_simBase->m_knobs->KNOB_HWCONFIG;            
  string point_path;
  string line;

  point_path = macperf_path + "/inputs/input_bbvs/intrapoints/" + expName + "/" + hwName + "/" + m_simBase->bench_name.c_str() + "@" + metricName + ".tp";

  ifstream pointFile;
  pointFile.open(point_path.c_str(), ifstream::in);
  if (!pointFile.good())
    ASSERTM(0, "error reading from IntraPoint: %s\n", point_path.c_str());

  int phaseType, lastPhaseType;
  bool firstLine = false;
  int minBlockId, maxBlockId;      
  while(std::getline(pointFile, line)) {
    if (line.find("Kernel") == string::npos) {
      istringstream iss(line);            
      
      iss >> phaseType;            
      iss >> minBlockId;            
      iss >> maxBlockId;

      if (firstLine) {
        while (minBlockId > tbtype.size()) {
          tbtype.push_back(lastPhaseType);
        }
        firstLine = false;
      }

      tuple<int, int> range(minBlockId, maxBlockId);
      tbrange.push_back(range);      
      
      for (int i = minBlockId; i <= maxBlockId; i ++) {
        assert(i == tbtype.size());        
        tbtype.push_back(phaseType);                
      }
      // cout << "phaseType "  << phaseType << " " << minBlockId << " " << maxBlockId << endl;
    } else {
      firstLine = true;
      lastPhaseType = phaseType;
    }
  }

  pointFile.close();

  cout << "IntraPoint for " << metricName << " is loaded!" << endl;
}

void macsim_c::GetTBPoint()
{
  int unitSize;  
  string macperf_path = string(getenv("MACPERF_ROOT"));
  string expName = *m_simBase->m_knobs->KNOB_EXP;
  string metricName = *m_simBase->m_knobs->KNOB_METRIC_NAME;
  string hwName = *m_simBase->m_knobs->KNOB_HWCONFIG;            
  string point_path;
  string line;

  point_path = macperf_path + "/inputs/input_bbvs/points/" + expName + "/" + hwName + "/" + m_simBase->bench_name.c_str() + "@" + metricName + ".tp";

  ifstream pointFile;
  pointFile.open(point_path.c_str(), ifstream::in);
  if (!pointFile.good())
    ASSERTM(0, "error reading from TBPoint: %s\n", point_path.c_str());

  while(std::getline(pointFile, line)) {
    istringstream iss(line);
    int tbId, phaseId;
    vector<int> tblist;
    string ID;
    double weight;
    
    iss >> phaseId;
    iss >> weight;
    iss >> ID;

    phaseWeight[phaseId] = weight;
    printf("%d tbid ", phaseId);
    while (iss >> tbId) {
      tblist.push_back(tbId);
      printf("%d ", tbId);
    }
    printf("\n");
    tbpoints.push_back(make_pair(phaseId, tblist));
  }
  
  pointFile.close();

  cout << "TBPoint for " << metricName << " is loaded!" << endl;
}

void macsim_c::WriteSamplingResults()
{
  string macperf_path = string(getenv("MACPERF_ROOT"));
  string expName = *m_simBase->m_knobs->KNOB_EXP;
  string hwName = *m_simBase->m_knobs->KNOB_HWCONFIG;
  string metricName = *m_simBase->m_knobs->KNOB_METRIC_NAME;
  ofstream samplingFile;
  double totalCPI = 0;
  double totalWeight = 0;
  double totalL1Rate = 0;
  double totalL2Rate = 0;

  string sampling_path = macperf_path + "/outputs/" + expName + "/" + hwName + "/" + m_simBase->bench_name.c_str() + "@" + metricName + ".tbsim";  
  samplingFile.open(sampling_path.c_str());
  
  for (map<int, double>::iterator it = phaseCPI.begin(); it != phaseCPI.end(); it ++) {
    int phaseId = it->first;

    totalCPI += phaseCPI[phaseId] * phaseWeight[phaseId];
    totalL1Rate += phaseL1Rate[phaseId] * phaseWeight[phaseId];
    totalL2Rate += phaseL2Rate[phaseId] * phaseWeight[phaseId];
    totalWeight += phaseWeight[phaseId];
    
    samplingFile << "sampling_phase_" << phaseId << "_cpi_" << metricName << " " << phaseCPI[phaseId] << endl;
    samplingFile << "sampling_phase_" << phaseId << "_insts_" << metricName << " " << phaseInsts[phaseId] << endl;
    samplingFile << "sampling_phase_" << phaseId << "_l1_rate_" << metricName << " " << phaseL1Rate[phaseId] << endl;
    samplingFile << "sampling_phase_" << phaseId << "_l2_rate_" << metricName << " " << phaseL2Rate[phaseId] << endl;
  }

  samplingFile << "sampling_total_cpi_" << metricName << " " << totalCPI << endl;
  samplingFile << "sampling_total_weight_" << metricName << " " << totalWeight << endl;
  samplingFile << "sampling_total_l1_rate_" << metricName << " " << totalL1Rate << endl;
  samplingFile << "sampling_total_l2_rate_" << metricName << " " << totalL2Rate << endl;
  samplingFile.close();
}

bool macsim_c::CheckStablePhase()
{
  bool isTBStable = true;
  int min_block_id = -1;
  int first_block_id = -1;
  for (map<unsigned int, bool>::iterator it = curBlockList.begin(); it != curBlockList.end(); it ++) {
    unsigned int tbId = it->first;

    if (first_block_id == -1)
      first_block_id = tbId;

    if (min_block_id == -1)
      min_block_id = tbId;
    else if (tbId < min_block_id)
      min_block_id = tbId;
    
    if (tbtype[tbId] == 0 || tbtype[first_block_id] != tbtype[tbId]) {
      isTBStable = false;
      break;
    }
  }  
  
  if (isTBStable) {
    if (!isStablePhase || (m_simBase->curPhaseId != tbtype[min_block_id])) {    
      // cout << "Stable Phase " << m_simBase->curPhaseId << endl;      
      m_simBase->curBlockId = min_block_id;
      m_simBase->curPhaseId = tbtype[min_block_id];
      m_simBase->sampling_bbv_profile << "New Stable Phase " << m_simBase->curPhaseId
                                      << " Block " << min_block_id << endl;
      // cout << "New Stable Phase " << m_simBase->curPhaseId << " " << tbtype[min_block_id]
      //      << " " << isTBStable << endl;

      m_simBase->epoch_count = 0;
      m_simBase->prevIPC = 0;
      m_simBase->doneStablePhase = false;     
    } 
  } else {    
    m_simBase->epoch_count = 0;
    m_simBase->prevIPC = 0;
    m_simBase->isStablePhase = false;
    m_simBase->doneStablePhase = false; 
  }
  return isTBStable;
}
