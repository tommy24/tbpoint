

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Getting Started}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


This chapter provides instructions for building, installing, and running \SIM.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Documentation and Other Support}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\SIM is hosted on Google Code at the following URL:
\urlc{http://code.google.com/p/macsim}. The project page for \SIM on
Google Code provides stable version of \SIM, detailed documentation,
issue/bug tracking, sample traces and so on. Users can use the project
page for filing issues and for contacting the maintainers of \SIM.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Building \SIM}
\label{sec:installation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Obtaining Source}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\SIM source code is maintained using subversion. Users can obtain a copy of the
source code using this command:

\begin{Verbatim}
svn co https://svn.research.cc.gatech.edu/macsim/trunk macsim-readonly --username readonly
\end{Verbatim}

\noindent
Note that currently password is not set for readonly account, so
`enter' when you prompt password. Due to the technical issue, we do
not allow anonymous checkout now. As soon as the issue has been
resolved, we will update this documentation.

Users will have read-only permission by default and users interested
in contributing to \SIM must contact
\href{mailto:macsim-dev@googlegroups.com}{macsim-dev@googlegroups.com}.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Requirements}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

To build \SIM the following are required:

\begingroup
\renewcommand\descriptionlabel[1]{\textit{\hspace\labelsep{#1}}}
%\renewcommand\descriptionlabel[1]{\hspace\labelsep\cs{#1}}
\begin{description}\firmlist
  \item[Operating System] At present only Linux distributions are supported. \SIM has been tested on:
   \Verb+Ubuntu+, \Verb+Redhat (TODO)+
  \item[Compiler] Any compiler that supports the C++0x (or C++11)
    standard. \SIM has been verified to work with: \Verb+gcc 4.4 or higher+, \Verb+icc (will work on)+
  \item[Autotools] - Autotools (automake, autoconf,
    ...) version 2.65 or higher is required. Autotools can be installed using the commands:
\begin{Verbatim}
Ubuntu: apt-get install autotools-dev automake autoconf
Redhat: todo
\end{Verbatim}
  \item[Libraries] - The zlib library is required and it can be installed using the command:
\begin{Verbatim}
Ubuntu: apt-get install zlib1g-dev
\end{Verbatim}
\end{description}
\endgroup




\ignore{
\subsection{Directory Structure}

This section explains the directory structure of \SIM simulator.

\smallskip
\begin{lstlisting}
macsim/
  tag/ branch/ trunk/
\end{lstlisting}
\smallskip

\textit{Tag} directory has tagged version of \SIM
simulators. \textit{Branch} directory is for diverged \SIM, which is
currently empty. \textit{Trunk} directory is current working directory
for \SIM.

Following is more detailed information about \textit{Trunk} directory.

\smallskip
\begin{lstlisting}
trunk/
  bin/ def/ doc/ params/ scripts/ src/ tools/
\end{lstlisting}
\smallskip

\textit{Bin} directory contains the \SIM binary after the building
process. \textit{def} directory has knob (Section~\ref{sec:knob}) and
statistics (Section~\ref{sec:stat}) definitions. \textit{doc} has the
documentation. \textit{scripts} includes several scripts files that
are using during the building process. \textit{src} contains all
source files. \textit{tools} has several useful tools.
\ignore{ (Section~\ref{sec:tool}) }
}

\ignore
{
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Source organization}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\TODO{check this, i'm not sure what is included in the copy that be available
  publically} 

The top-level source directory of \SIM contains several source, script and tool
directories, README and INSTALL files and files required for building \SIM
using autotools. Below is a brief description of the contents of the top-level
source directory of \SIM.


\begin{description}\firmlist

\item [bin] Build output directory.

\item [def] Contains definitions of parameters (see Sections~\ref{sec:run}
    and~\ref{sec:knob}) and events for statistics (see Section~\ref{sec:stat}).

\item [doc] Contains \SIM documentation.

\item [params] Contains sample parameter (see Sections~\ref{sec:run}
    and~\ref{sec:knob}) configuration files.

\item [scripts] Contains scripts used in build of \SIM.

\item [src] Contains \SIM source files (.cc and .h files).

\item [tools] Contains x86 trace generator and trace reader.

\item [README, INSTALL] README and INSTALL files containing proceduce to build
and run \SIM and patch \SIM to make it usable as a SST component (see
    Section~\ref{sec:sst}).

\item [autogen.sh] Script file to generate makefile to build \SIM.

\item [configure.in aclocal.m4 Makefile.am Makefile.in] Files required by
autotools to generate makefile to build \SIM.


\end{description}
}

\ignore
{
  The top-level source directory of \SIM contains several directories including
  \Verb+bin+, \Verb+def+, \Verb+doc+, \Verb+params+, \Verb+scripts+, \Verb+src+ and
  \Verb+tools+ and the \Verb+README+ and \Verb+INSTALL+ files and the files
  required for building \SIM using autotools.
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Build}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The GNU Autotools (aclocal, automake, autoconf, ...) are used to build \SIM.
After checking out a copy of the \SIM source code, the following commands have
to be executed (These instructions are also available in \Verb+INSTALL+ file
    included in the \SIM source).


\begin{Verbatim}
aclocal 
automake 
--add-missing 
autoconf 
./configure 
make
\end{Verbatim}

\noindent
Users can enter all the above commands in one go as shown below:

\begin{Verbatim}
aclocal && automake --add-missing && autoconf && ./configure && make
\end{Verbatim}

\noindent Alternatively, users can just run the autogen.sh script file included
in the \SIM source and then run the \Verb+make+ to build \SIM.

\begin{Verbatim}
./autogen.sh
make
\end{Verbatim}

\noindent
On a successful build, the binary \bin will be generated in the \Verb+trunk/bin/+
directory.




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Build Types}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Three types of build, each of which uses different compiler flags are
supported. The build types are:

\begin{itemize}
  \item opt : default, optimized version (-O3 flag)
  \item dbg : debug version (-g3 flag)
  \item gpf : gprof version (-pg flag)
\end{itemize}

\noindent
The build type can be specified as an argument to the \textit{make} command.

\begin{Verbatim}
To build the optimized version of the binary
make [opt] 

To build the debug version of the binary
make dbg

To build the gprof version of the binary
make gpf
\end{Verbatim}





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Running \SIM}
\label{sec:run}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

To run \bin binary, two additional files are required to be present 
the same directory as the binary.

\begin{itemize}
  \item params.in - defines architectural parameter values that will
  overwrite the default parameter values.

  \item trace\_file\_list - specifies the number of traces to run and
  the location of each trace.
\end{itemize}


Several pre-defined architectural parameter configuration files are provided in
\Verb+macsim-top/trunk/params+ directory. Table~\ref{table:param} lists the
pre-defined parameter files. To run \SIM with a particular architectural
configuration, users should copy the corresponding parameter file to \Verb+bin+
directory and rename it as \Verb+params.in+. For example, to use NVIDIA's
Fermi~\cite{fermi} GPU architecture, \Verb+params/params_gtx465+ should be
copied to \Verb+bin+ and renamed as \Verb+params.in+. 



\ignore{
We provide several pre-defined architecture parameters in
\Verb+macsim-top/trunk/params+ directory. Table~\ref{table:param}
shows the list of pre-defined parameter files. If you want to run \SIM
with a certain configuration, you need to copy the corresponding
parameter file to \Verb+bin+ directory and rename it as
\Verb+params.in+. For example, if you want to use NVIDIA's
Fermi~\cite{fermi} GPU architecture, the following command would be
sufficient:

\begin{Verbatim}
cp params/params_gtx465 bin/params.in
\end{Verbatim}
}

\begin{table}
\begin{footnotesize}
\begin{center}
\caption{Parameter Templates.}
\label{table:param}
\begin{tabular}{|l|l|l|}
\hline
File Name              & Description & Architecture                         \\ \hline \hline
params\_8800gt         &             & NVIDIA GeForce 8800 GT (G80)         \\
params\_gtx280         &             & NVIDIA GeForce GTX 280 (GT200)       \\
params\_gtx465         &             & NVIDIA GeForce GTX 465 (Fermi)       \\
params\_x86            &             & Intel's Sandy Bridge (CPU part only) \\
params\_hetero\_4c\_4g &             & Intel's Sandy Bridge (CPU + GPU)     \\ \hline
\end{tabular}
\end{center}
\end{footnotesize}
\end{table}


\Verb+trace_file_list+ specifies the list of traces to be simulated. Following is the
content of a sample \Verb+trace_file_list+.

\begin{Verbatim}
2 <-- number of traces
/trace/ptx/cuda2.2/FastWalshTransform/kernel_config.txt <-- trace 1 path
/trace/ptx/cuda2.2/BlackScholes/kernel_config.txt <-- trace 2 path
\end{Verbatim}

\noindent

Above, the first line specifies the number of traces (applications) that will
be simulated and each line thereafter specifies the path to the trace
configuration file in the trace directory of an application. The contents of a
sample trace configuration file are shown below.

\begin{Verbatim}
1 x86
0 0
\end{Verbatim}

\noindent In the first line, the first field indicates the number of threads in
the application (1 in this case) and the second field specifies the type of the
application (x86 in this case). Other lines contain a thread id and the starting 
point of the thread in terms of the instruction count of the main thread
(thread 0).


Several sample trace files are available at
\urlc{http://code.google.com/p/macsim}. Users can generate their own
traces by following instructions in Chapter~\ref{ch:trace}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Run}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The \SIM simulator can be run by executing the \bin binary located in the
\Verb+trunk/bin/+ directory.

\begin{Verbatim}
./macsim
\end{Verbatim}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Output}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\SIM generates a \Verb+params.out+ file and several files \Verb+.stat.out+
containing statistics at the end of a successful simulation. Users can specify
the output directory for these files by setting the
\Verb+STATISTICS\_OUT\_DIRECTORY+ parameter whose default value is the
directory containing the \bin binary. Simulation parameters are discussed in
detail in Section~\ref{sec:knob}.

\ignore{
several output files once the simulation is completed successfully.
 Here are the list of output files.

\begin{Verbatim}
params.out
statfile1.stat.out
statfile2.stat.out
...
statfilen.stat.out
\end{Verbatim}
}

\noindent \Verb+params.out+ enumerates all parameters with their values used
for the simulation. The \Verb+.stat.out+ contain statistics such as the number
of simulation cycles and so on. Chapter~\ref{sec:stat} details the kinds of
statistics supported, how to add new statistics and how to examine and
interpret the contents of \Verb+.stat.out+ files.


\ignore
{
 Other files are statistics outputs, which is mostly the number
of occurrence for architectural events during the simulation. Each
stat out file consists of following lines:

\begin{Verbatim}
STAT     Raw_counter_value     Value_per_type
\end{Verbatim}

\noindent
, where \Verb+STAT+ is the name of stat, \Verb+Raw_counter_value+ is
the raw value of the counter, and \Verb+Value_per_type+ is the value
based on the each stat type. There are three types of statistics:

\begin{itemize}

  \item COUNT - for counting the number of occurances of an event. Eg. number
  of cache hits. 

  \item RATIO - for calculating the ratio of number of occurances of one event
  over another. Eg. (number of cache hits / number of cache accesses) i.e.,
  cache hit ratio.

  \item DIST - for defining a group of related events and calculating the
  number of occurances of each event in the as a percent of the sum of the
  number of occurances of all events in the group.  Eg. If we want to know what
  percent of L1 data cache accesses (in a 2-level hierarchy) resulted in L1
  hits, L2 hits or memory accesses, we should define a distribution consisting
  on three events - L1 hits, L2 hits and L2 misses  - and update the
  counter for each event correctly. 
\end{itemize}


\noindent
We sometimes need to measure a certain event per core, so we further
categorize statistics into either global (common counter for all
cores) or per core (each core has its own counter). The suffix of all
per core stats is \Verb+_CORE_#+. Chapter~\ref{sec:stat} details
regarding statistics.


\begin{table}[htb]
\begin{footnotesize}
\begin{center}
\caption{Important Stats.} 
\label{table:stats}
\begin{tabular}{|l||l|c|l|}
\hline 
STAT & Description & Per-Core & Filename \\ \hline \hline
INST\_COUNT\_TOT            & \# of instructions                                    &      & general.stat.out \\ \hline 
INST\_COUNT\_CORE\_[0-11]   & \# of instructions in only the specificed core [0-11] & core & general.stat.out \\ \hline 
CYC\_COUNT\_TOT             & simulated cycles                                      &      & general.stat.out \\ \hline 
CYC\_COUNT\_CORE\_[0-11]    & simulated cycles in only the specificed core [0-11]   &      & general.stat.out \\ \hline 
CYC\_COUNT\_X86             & simulated cycles for x86 only                         &      & general.stat.out \\ \hline 
CYC\_COUNT\_PTX             & simulated cycles for ptx only                         &      & general.stat.out \\ \hline \hline 
                            & \# of fp instructions                                 &      &                  \\ \hline 
                            & \# of int instructions                                &      &                  \\ \hline 
                            & \# of load instructions                               &      &                  \\ \hline  
                            & \# of store instructions                              &      &                  \\ \hline  
BP\_ON\_PATH\_CORRECT       & \# of correctly predicted branches (DIST)             & core & bp.stat.def      \\ \hline  
BP\_ON\_PATH\_MISPREDICT    & \# of mis-predicted branches (DIST)                   & core & bp.stat.def      \\ \hline  
BP\_ON\_PATH\_MISFETCHT     & \# of mis-fetch branches (BTB MISS)(DIST)             & core & bp.stat.def      \\ \hline  
ICACHE\_HIT, ICACHE\_MISS   & \# of I-cache hitt,miss (DIST)                        &      & memory.stat.def  \\ \hline  
L[1-3]\_HIT\_CPU            & \# of l[1-3]cache hits from CPU                       &      & memory.stat.def  \\ \hline 
L[1-3]\_HIT\_GPU            & \# of l[1-3]cache hits from GPU                       &      & memory.stat.def  \\ \hline 
L[1-3]\_MISS\_CPU           & \# of l[1-3]cache misses from CPU                     &      & memory.stat.def  \\ \hline 
L[1-3]\_MISS\_GPU           & \# of l[1-3]cache misses from GPU                     &      & memory.stat.def  \\ \hline  \hline 
AVG\_MEMORY\_LATENCY        & average memory latency                                &      & memory.stat.def  \\ \hline \hline 
TOTAL\_DRAM                 & \# of DRAM accesses                                   &      & memory.stat.def  \\ \hline  
TOTAL\_DRAM\_READ           & \# of DRAM reads                                      &      & memory.stat.def  \\ \hline  
TOTAL\_DRAM\_WB             & \# of DRAM write backs                                &      & memory.stat.def  \\ \hline  
                            & \# of register reads                                  &      &                  \\ \hline  
                            & \# of register writes                                 &      &                  \\ \hline   \hline 
COAL\_INST, UNCOAL\_INST    & coalesced/uncoalesced mem requests (DIST)             &      & memory.stat.def  \\ \hline 
\end{tabular}
\end{center}
\end{footnotesize}
\end{table} 


You can 


\begin{Verbatim}
IPC = INST_COUNT_TOT / CYC_COUNT_TOT
CPI = CYC_COUNT_TOT / INST_COUNT_TOT
\end{Verbatim}

\begin{Verbatim}
IPC_1 = INST_COUNT_CORE_1 / CYC_COUNT_CORE_1
IPC_2 = INST_COUNT_CORE_2 / CYC_COUNT_CORE_2
...
IPC = harmonic_mean(IPC_1, IPC_2, ..., IPC_n)

\end{Verbatim}


Table~\ref{table:stats} shows important stats.
}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{SST-Macsim}
\label{sec:sst}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


Macsim is a part of the SST simulation framework~\cite{sst} and this section is
intended for SST users who want to use \SIM for GPU simulations.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Installing SST}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


Instructions for installing SST are provided in the Wiki
page at the SST Google Code repository~\cite{sst-google} at
\urlc{http://code.google.com/p/sst-simulator}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Building \SIM as a SST component}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


Check out a copy of \SIM from SVN in \textit{sst-top/sst/elements} and
apply the \Verb+macsim-sst.patch+. 

\begin{Verbatim}
cd sst-top/sst/elements
svn co https://svn.research.cc.gatech.edu/macsim/trunk macsim
cd macsim
patch -p0 -i macsim-sst.patch
\end{Verbatim}

Then, re-run the SST build procedure.

\begin{Verbatim}
cd sst-top
./autogen.sh
./configure --prefix=/usr/local --with-boost=/usr/local --with-zoltan=/usr/local --with-parmetis=/usr/local
\end{Verbatim}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Configuring the SST-\SIM component}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

An example SST sdl configuration file setup for simulating \SIM is shown below:

\begin{Verbatim}
<?xml version="1.0"?>
<sdl version="2.0"/>

<config>
  stopAtCycle=1000s
  partitioner=self
</config>

<sst>
  <component name=gpu0 type=macsimComponent.macsimComponent rank=0 >
    <params>
      <paramPath>./params.in</paramPath>
      <tracePath>./trace_file_list</tracePath>
      <outputPath>./results/</outputPath>
      <clock>1.4Ghz</clock>
    </params>
  </component>
</sst>
\end{Verbatim}

In this manner, an SST simulation configuration file can declare multiple
instances of \SIM as well as define the traces are run on each \SIM instance.

\subsection{Running a \SIM simulation in SST}

Ensure that SST and \SIM components are compiled and/or installed.  
Ensure that the paths and contents of both SST configuration sdl file and
\SIM \textit{params.in} configuration file are correct. Start the SST simulation 
either standalone or through MPI.

\begin{Verbatim}
sst.x [sdl-file]
\end{Verbatim}
or
\begin{Verbatim}
mpirun -np [x] sst.x [sdl-file]
\end{Verbatim}



\ignore{
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{How to Get Memory Traces Using \SIM}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\todo{TODO}
}









